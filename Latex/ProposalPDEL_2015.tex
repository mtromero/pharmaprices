%\input{tcilatex}
\documentclass{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amssymb}
\usepackage{dcolumn}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{footnote}
\usepackage{mathrsfs,amsfonts}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{tablefootnote}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{nameref}
\usepackage[all]{hypcap}
\usepackage [autostyle, english = american]{csquotes}
\usepackage{apacite}


\usepackage[cm]{fullpage}
\MakeOuterQuote{"}
\setcounter{MaxMatrixCols}{10}
%TCIDATA{OutputFilter=LATEX.DLL}
%TCIDATA{Version=5.50.0.2953}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{LastRevised=Wednesday, October 10, 2012 16:40:37}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{Language=American English}
%\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}{Acknowledgement}
\newtheorem{algorithm}{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}{Case}
\newtheorem{claim}{Claim}
\newtheorem{conclusion}{Conclusion}
\newtheorem{condition}{Condition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{criterion}{Criterion}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{exercise}{Exercise}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem{problem}{Problem}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remark}
\newtheorem{solution}{Solution}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\newtheorem{summary}{Summary}
%\usepackage{geometry}
\newenvironment{proof}[1][Proof]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\date{}     % Deleting this command produces today's date.
\newcommand{\ip}[2]{(#1, #2)}
 \newcommand{\Lagr}{\mathcal{L}}
      % Defines \ip{arg1}{arg2} to mean
                             % (arg1, arg2).
%\newcommand{\ip}[2]{\langle #1 | #2\rangle}
                             % This is an alternative definition of
                             % \ip that is commented out.
\title{Benefit plans, market power and pharmaceutical prices}
 
\author{Mauricio Romero}
\begin{document}             % End of preamble and beginning of text.
                   % Produces the title.

\maketitle


\section*{Introduction}



Several countries have moved towards the adoption of health benefit plans that explicitly define the services to be covered with public funds\cite{Tristao2014,Glassman2012}. Adopting benefits plans introduce at least two features to any health system. First, they reduce out-of-pocket expenditure, making consumers less price elastic; in an environment where suppliers have market power (as is often the case with pharmaceuticals) this could result in higher prices \cite{Duggan2006}. The second feature is the creation of larger buyer groups that might have enough bargaining power to negotiate lower prices.  I use quasi-experimental evidence to study the effect on prices and utilization of including a pharmaceutical in the benefit plan of a middle-income country. In addition, I study how the effect varies depending on the characteristics (e.g., number of firms and consumer restrictions) of the pharmaceutical's market. 


I study the case of Colombia, which has a competitive health insurance market in which all insurance companies offer the same plan (the national benefit plan) and charge the same premium. Insurance companies compete for enrollees on the basis of quality \cite{Giedion2009}. To maximize profits insurance companies negotiate low prices with providers. This setting is similar to how Medicare Advantage works in the U.S. \cite{Brown2014} and to the health systems of The Netherlands and Switzerland\cite{Leu2009}; however, as previously mentioned, insurance companies in Colombia face higher restrictions in terms of the benefits they can offer and at which prices.\footnote{Insurance companies can, and often do, provide a discount in the premium for complementary insurance for their enrollees in the national benefit plan.} Colombia is a particularly interesting setting to study the effect of expanding the generosity of the national benefit plan. First, it is a middle-income country that went from having under 25\% of its population covered by some form of health insurance in 1993 to over 95\% in 2013, achieving nearly universal health care (UHC), and therefore can provide lessons to other low- and middle-income countries on the path towards UHC. Second, much of the research on institutional arrangements and their impact on pharmaceutical prices, utilization and innovation has focused on the United States and other developed countries. However, most countries (as is the case with Colombia) represent a small fraction of the pharmaceutical market and therefore the institutions they put in place to provide pharmaceuticals will almost no repercussions in the global market as they are unlikely to affect the supply of drugs or innovation. 

Using a difference-in-difference strategy I study the effect of including a pharmaceutical in the benefit plan and and find that the effect is a 22\% reduction in the price, approximately. Additionally, I provide suggestive evidence that this reduction in prices only happens when there are several substitutes within the benefit plan, suggesting that insurance companies are only able to negotiate lower prices when they can credibly threaten to switch consumers to other drugs. I also find that utilization (measured by quantities sold to providers) increases by 71\%.  In other words, although consumers become less price elastic once a pharmaceutical is included in the benefit plan, the institutional arrangement provides enough bargaining power to insurance companies to achieve lower prices (and the right incentives to pursue this goal). 

This is an important result as pharmaceuticals account for almost one-fifth of health expenditures in most OECD countries \cite{Brekke2012} and although they are private goods, they have positive externalities since they can prevent the spread of diseases. The institutions put in place for the provision of pharmaceuticals therefore have an important impact on health expenditures and health outcomes. For example, \citeA{Duggan2010} studies a similar setting in the U.S. where the government subsidizes participation in private prescription drug plans that negotiate with pharmaceutical companies over prices (Medicare Part D), and finds a reduction of about 20\% in pharmaceutical prices.  However, \citeA{Duggan2006} find that in the U.S. the institutional arrangement in place to procure drugs for Medicaid increases pharmaceutical prices. This highlights the importance of studying the effect of various institutional arrangements on prices and utilization in different contexts. 


\section{Background, Institutions and Incentives}


Before 1993 only 24\% of the population in Colombia had some form of health insurance, with significant inequality: 47\% of the highest quantile of income had health insurance but less than 5\% of the lowest quantile did. In turn, access to medical services was highly unequal. Over 33\% of the poorest quantile did not receive medical attention when sick in 1992 but this number was only 7\% for the highest quantile of income \cite{Gaviria2013}. Law 100 of 1993 set out to change this by introducing a universal health insurance scheme. 

Since 1993 every Colombian has been entitled to a comprehensive health benefit package (known as \textit{Plan Obligatorio de Salud} or POS). Individuals belong to either one of two regimes: Those with higher income belong to the contributory regime, while individuals with lower income belong to the subsidized regime. The former is financed with a payroll tax, while the latter is financed through central government expenditures. Initially, the health packages for the contributory and the subsidized regime where different but their pharmaceutical coverage was the same from 2006 to 2014, the period I analyze in this article\footnote{Originally the contributory benefit plan was more generous, but in 2008 the Constitutional Court ruled that both packages must be the same (i.e., the subsidized regime benefit package had to be expanded to include the same coverage as the contributory regime package). The standardization of the health packages occurred in stages, giving priority to the most vulnerable groups. In October 2009, the subsidized plan was expanded for children under 12 years old; then in February 2010 the plan was expanded for children under 18 years old; in November 2011 the plan was expanded for adults over 60 years old; finally, in July 2012 the plan for the rest of the population-those between 18 and 59 years old-was expanded.}. 


The system is a competitive health insurance market where individuals in both regimes are able to choose an insurance company, known as entidades promotoras de salud (health promotion entities, or EPS). The insurance companies are required to provide all the services included in the POS. In exchange, the insurer receives an ex-ante yearly risk-adjusted capitation payment known as \textit{unidad de pago por capitaci\'on} or UPC\footnote{There is also an ex-post redistribution of resources based on the prevalence of renal chronic disease per insurer.}. As of 2013, there were 24 insurance companies providing contributory plans and 48 providing subsidized plans. Given that the price and coverage of each benefit package is set by the government, insurers compete for enrollees on the basis of quality \cite{Giedion2009}. 

EPS profits can be written as:

\begin{equation}
\pi=\sum_{i \in E} UPC_{i}-Expenditure_i=\sum_{i \in E} UPC_{i}-\sum_{j \in POS} P_j Q_{ji},
\end{equation}

where $E$ is the set of enrollees in the EPS, $UPC_{i}$ is the risk-adjusted capitation payment the EPS receives from the government for enrollee $i$, $Expenditure_i$ is the enrollee's $i$ health expenditure, $POS$ is the set of treatments included in the benefit plan, $P_j$ is the price of treatment $j$ and $Q_{ji}$ is the quantity of treatment $j$ demanded by individual $i$. If we assume the EPS's have no influence over the $UPC$ or the $POS$, then EPS's have at least three forms of leverage to maximize profits. First, they can change the pool of individuals they enroll. If risk-adjusted payments are a perfect predictor of health expenditure (i.e., the expected value of health expenditure is equal to the risk-adjusted payment), then EPS's have no incentives to cream skim\footnote{Need to expand on the importance of second order moments.}. However, although in theory insurers may not legally select who they insure, in practice there have been reports of slow affiliation processes and misplaced applications, which may be evidence of cream skimming \cite{Castano2006,GomezSuarez2007}. This type of behavior is not unique to Colombia as risk-adjusted payments are seldom perfect. For example, after risk adjustment was introduced to Medicare Advantage in the U.S., insurance companies enrolled individuals with higher risk-adjusted payments but lower costs conditional on their risk \cite{Brown2014}. Additionally, risk-adjustment payments do not incorporate patients' preferences over care, which may result in moral hazard and adverse selection when prices and benefits are regulated \cite{Shepard2015}, as in the case of Colombia. 

Second, EPS's can influence the treatment demanded by their enrollees. In general, the quantity of care demanded by any given patient depends not only on price, but also on physicians' recommendations of treatment as patients usually do not fully understand the difference between treatment options and must rely on physicians' knowledge \cite{Arrow1963}. Insurance companies can steer patients away from costly treatments by charging higher co-payments or by influencing physicians' behavior. The latter option is especially attractive if co-payments are fixed. Although illegal, there have been reports of EPS's influencing physicians' behavior in Colombia \cite{Semana2014}.


\section*{Preliminary Results and Data Limitations}

I have conducted a preliminary analysis using price information from the \textit{Sistema de Informacion de Precios de Medicamentos} (SISMED). The SISMED is a government effort to collect pharmaceutical prices from pharmaceutical laboratories, importer and EPSs. However, several studies, including some from government agencies, have pointed that the information is unreliable in several cases \cite{Vacca2011,Departamento2012,Zapata2012}. Furthermore, pharmaceutics often come in and out of the sample giving an unbalance panel. However, using this database\footnote{The database is published in several PDFs by the government. I converted the information into usable plain text files and published them in \url{http://econweb.ucsd.edu/~mtromero/HealthData.html} for anyone to use.}, I have found that competition reduces prices by about 14\%, while reference pricing reduces prices by about 25\% (a draft of this article is available upon request). 


Although my findings are consistent with estimates of the effect of similar policies in the literature \cite{Duggan2010,Brekke2011}, there is a much richer and reliable database collected by the \textit{Observatorio del Medicamento} from the \textit{Federacion Medica Colombiana}. This information contains the SISMED information, combined with survey data from major pharmaceutical laboratories. Furthermore, it has quarterly frequency (compared to the yearly frequency of the SISMED) and information on regulation (such as reference pricing and whether the pharmaceutical is included in the POS or not). This information however has a cost of \$ COP 17,596,000 $\approx$ \$ USD 9,200.  I would like to run my analysis using this database.

\section*{Steps Forward}

With the information from \textit{Observatorio del Medicamento}-\textit{Federacion Medica Colombiana} I would re-estimate the effect of the policies using more reliable data. But furthermore, I would try to measure the effect of these policies on health outcomes and on the distribution of health expenditure. 

Finally, I plan to use this database for future research as I want the central topic of my dissertation to be the study of health care systems in developing countries.  

\section*{Budget}

Data from \textit{Observatorio del Medicamento}-\textit{Federacion Medica Colombiana}:  \$ USD 9,200

\bibliographystyle{apacite}
\bibliography{PharPrice}

\end{document}
