%\input{tcilatex}
\documentclass{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amssymb}
\usepackage{dcolumn}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{footnote}
\usepackage[flushleft]{threeparttable}
\usepackage{fullpage}
\usepackage{mathrsfs,amsfonts}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{tabularx}
\usepackage{tablefootnote}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{nameref}
\usepackage[all]{hypcap}

\usepackage [autostyle, english = american]{csquotes}
\usepackage{apacite}

\newcommand{\price}{20\%}
\newcommand{\quantity}{103\%}
\newcommand{\monopoly}{11\%}
\newcommand{\HH}{13\%}

\MakeOuterQuote{"}
\setcounter{MaxMatrixCols}{10}
%TCIDATA{OutputFilter=LATEX.DLL}
%TCIDATA{Version=5.50.0.2953}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{LastRevised=Wednesday, October 10, 2012 16:40:37}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{Language=American English}
%\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}{Acknowledgement}
\newtheorem{algorithm}{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}{Case}
\newtheorem{claim}{Claim}
\newtheorem{theorem}{Theorem}
\newtheorem{conclusion}{Conclusion}
\newtheorem{condition}{Condition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{criterion}{Criterion}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{exercise}{Exercise}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem{problem}{Problem}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remark}
\newtheorem{solution}{Solution}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\newtheorem{summary}{Summary}
%\usepackage{geometry}
\newenvironment{proof}[1][Proof]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\date{}     % Deleting this command produces today's date.
\newcommand{\ip}[2]{(#1, #2)}
 \newcommand{\Lagr}{\mathcal{L}}
      % Defines \ip{arg1}{arg2} to mean
                             % (arg1, arg2).
%\newcommand{\ip}[2]{\langle #1 | #2\rangle}
                             % This is an alternative definition of
                             % \ip that is commented out.
\doublespacing
\usepackage{etoolbox}
\AtBeginEnvironment{table}{\singlespacing}
\title{Drug coverage, disease coverage, and morbidity and mortality rates: The story of two diseases}
 
\author{Mauricio Romero}
\begin{document}             % End of preamble and beginning of text.
                   % Produces the title.
\maketitle


\section{Variation in coverage}\label{sec:mortality}


In this section I explore the effects on morbitiy and mortality of ``including'' a disease in the benefit plan. The benefit plan does not explicitly cover a list of diseases, it covers pharmaceutics and medical procedures. Therefore, I study whether coverage of pharmaceuticals targeted to curing, treating or preventing a disease decreases the mortality rate for that disease. To do so, I use a mapping between ICD-10 codes and ATC codes to link drugs to diseases and exploit variation in whether there is any drug listed in the benefit plan that can be used to cure, treat, or prevent a disease (defined by its ICD-10 code). 

To map drugs to diseases I rely on three sources: 1) The Therapeutic Target Database (TTD) provided by the BioInformatic and Drug Design Group at the National Unviersity of Singapore \cite{TTD2016}, 2) The MEDI database provided by the School of Medicine at the University of Vanderbilt \cite{MEDI2013}, and 3) An open source website known as MIA (Mapping ICD classification codes with ATC codes). All three databases provide a cross-walk between ICD-10 (or ICD-9) codes and ATC codes. \footnote{See \url{http://bidd.nus.edu.sg/group/bidd.htm} for more details on the TTD database; \url{https://medschool.vanderbilt.edu/cpm/center-precision-medicine-blog/medi-ensemble-medication-indication-resource} for details on the MIA database, and \url{http://metanet.ditad.org/mia/faq/}{http://metanet.ditad.org/mia/faq/} for details on the MIA database.} I use the ICD-10 code at the 3 digit level. I combine the information from all three databases to create a comprehensive cross-walk that covers 2,878 ATC codes, 1,821  ICD-10 codes and has 114,320 ATC-ICD pairs. 


It is important to note that this is not a one-to-one mapping (or even a many-to-one mapping). Often times one drug can be used to treat several diseases, and diseases often have more than one treatment (drug) option. To deal with this I first estimate the number of drugs listed on the benefit plan for each ATC code across time. I then merge this database (which has a unique observation for each ATC code in each year) with the cross-walk database (which has diseases and drugs listed multiple times). I then calculate the total number of drugs included in the benefit plan that can be used to treat each disease (ICD-10 code). This results in a database that has a unique observation for each ICD-10 code in each year. I use this as my measure for whether a disease is covered by the benefit plan or not.

I then use vital statistics and hospital records to estimate disease-specific mortality and morbidity rates. To do so I estimate the number of deaths and hospital visits associated with each ICD-10 code in each municipality of Colombia between 2007 and 2014.\footnote{Note that there is a two-year lag in the release of vital statistics by the National Administrative Department of Statistics (\textit{Departamento Administrativo Nacional de Estadística- DANE}) and as a result, 2015 data were not available.} I then estimate disease-specific mortality rates in each municipality using population estimates. Finally, I combine this information with my measure for whether a disease is covered by the benefit plan or not. 



\section{The story of: Myelodysplastic syndromes (D46)}

From Wikipeadia: ``A group of disorders caused when something disrupts the production of blood cells. Some types have no known cause. Others occur in response to cancer treatments or chemical exposure. Symptoms may include shortness of breath, fatigue, easy bruising, and paleness. Myelodysplastic syndrome may progress to leukemia. Transfusions and medications help manage symptoms. Medications or bone marrow transplants may lessen transfusion needs and slow or prevent progression to leukemia.''

Tioguanine, 

\section{The story of: Alcoholic liver disease(K70)}

\section{The story of: Neoplasm of uncertain behavior of oral cavity and digestive organs (D37)}


\section{The story of: Congenital deformities of feet (Q66)}

\section{The story of: Symptoms and signs involving appearance and behavior (R46)}

\section{The story of: Turner's syndrome  (Q96)}






\end{document}


