%\input{tcilatex}
\documentclass{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amssymb}
\usepackage{dcolumn}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{footnote}
\usepackage{mathrsfs,amsfonts}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{tablefootnote}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{nameref}
\usepackage[all]{hypcap}
\usepackage [autostyle, english = american]{csquotes}
\usepackage{apacite}


\usepackage[cm]{fullpage}
\MakeOuterQuote{"}
\setcounter{MaxMatrixCols}{10}
%TCIDATA{OutputFilter=LATEX.DLL}
%TCIDATA{Version=5.50.0.2953}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{LastRevised=Wednesday, October 10, 2012 16:40:37}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{Language=American English}
%\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}{Acknowledgement}
\newtheorem{algorithm}{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}{Case}
\newtheorem{claim}{Claim}
\newtheorem{conclusion}{Conclusion}
\newtheorem{condition}{Condition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{criterion}{Criterion}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{exercise}{Exercise}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem{problem}{Problem}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remark}
\newtheorem{solution}{Solution}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\newtheorem{summary}{Summary}
%\usepackage{geometry}
\newenvironment{proof}[1][Proof]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\date{}     % Deleting this command produces today's date.
\newcommand{\ip}[2]{(#1, #2)}
 \newcommand{\Lagr}{\mathcal{L}}
      % Defines \ip{arg1}{arg2} to mean
                             % (arg1, arg2).
%\newcommand{\ip}[2]{\langle #1 | #2\rangle}
                             % This is an alternative definition of
                             % \ip that is commented out.
\title{The effect of competition and reference pricing on pharmaceutical prices: Evidence from Colombia}
 
\author{Mauricio Romero}
\begin{document}             % End of preamble and beginning of text.
                   % Produces the title.

\maketitle

\begin{quote}
{\small ``At a time when money is tight, my advice to countries is this: before looking for places to cut spending on health care, look first for opportunities to improve efficiency''  Dr. Chan, Director of the World Health Organization.}
\end{quote}

\section*{Overview}

Health expenditure (both in per capita terms and as a share of GDP) has had an upward trend in almost every country in the world in the last 20 years\footnote{From 1995 to 2012, among those countries with World Development Indicators data the average increase in per capita spending (in 2005 PPP adjusted USD) was over 180\%, while the average increase in  health expenditure as a share of GDP was over 20\%.}. Pharmaceuticals account for almost a fifth of health expenditure in most OECD countries and this share will likely increase in the future \cite{Brekke2012}. Due to the nature of medical care the general social consensus is that a market base solution for medicine is intolerable  \cite{Arrow1963}. However, in order to cope with the increasing cost of health expenditures countries often introduce competition to curb costs but whenever competition is introduce it is done with a high degree of regulation \cite{Cutler2002}. Understanding the effect of different policies is essential to determine an optimal regulatory framework for health care. This research project will evaluate the impact of competition among insurance companies and the use of reference pricing in Colombia to curb the cost of pharmaceutical products.

\section*{Background}
Since 1993 every Colombian is entitled to a comprehensive health benefit package, known as the \textit{Plan Obligatorio de Salud}(POS). The system is a competitive health insurance market were individuals in both systems choose an insurer, known as \textit{Entidades Promotoras de Salud} (EPS). This insurer is committed to provide all the services included in the POS. In exchange, the insurer gets paid (by the government) an ex ante a yearly risk adjusted capitation payment known as \textit{Unidad de Pago por Capitacion} (UPC). 


Since the insurance companies (EPSs) are the residual claimants of any difference between the UPC and their expenditure, it is in their best interest to keep the price of pharmaceuticals and medical services (included in the POS) as low as possible, but are indifferent to the price of pharmaceuticals outside of the POS. However, EPSs are often forced by law to provide services not includes in the POS\footnote{This happens through two mechanisms. First, courts can force insurance companies to provide services and pharmaceutics not included in the POS whenever they consider it necessary in order protect the fundamental rights of patients.  Second, health providers can argue that they need to perform a procedure or prescribe a pharmaceutical that is not included in the POS to effectively treat a patient. In these cases a committee, known as ``Comite Tecnico Cientifico'' (Technical and Scientific Committee) or CTC decides whether the procedure/pharmaceutical is necessary or not.}, but whenever this happens they get reimbursed by the government for whatever expenses they had.


The amount of reimbursements grew dramatically in a few years from 0.1 to 2.4 trillion pesos between 2005 and 2010. In 2011, in an effort to reduce pharmaceutical prices the government introduce reference pricing, which sets a maximum reimburse amount, for some pharmaceuticals and expanded the POS to include others. In this article we evaluate the effect of both policies (reference pricing and induced competition by including a pharmaceutical in the POS) on prices by doing a difference in difference analysis that compares pharmaceutics affected by each policy to those not affected before and after the policy is implemented. 

Similar policies have been studied in other contexts, but never in a developing country framework. For example, \citeA{Brekke2011} studies the effect of reference pricing in Norway and finds that it reduces the price of generic and brand drugs by over 20\% and \citeA{Duggan2010} studies the effect of Medicare Part D, in which the government subsidizes participation in private prescription drug plans, and finds a reduction of about 20\% in pharmaceutical prices.

\section*{Preliminary Results and Data Limitations}

I have conducted a preliminary analysis using price information from the \textit{Sistema de Informacion de Precios de Medicamentos} (SISMED). The SISMED is a government effort to collect pharmaceutical prices from pharmaceutical laboratories, importer and EPSs. However, several studies, including some from government agencies, have pointed that the information is unreliable in several cases \cite{Vacca2011,Departamento2012,Zapata2012}. Furthermore, pharmaceutics often come in and out of the sample giving an unbalance panel. However, using this database\footnote{The database is published in several PDFs by the government. I converted the information into usable plain text files and published them in \url{http://econweb.ucsd.edu/~mtromero/HealthData.html} for anyone to use.}, I have found that competition reduces prices by about 14\%, while reference pricing reduces prices by about 25\% (a draft of this article is available upon request). 


Although my findings are consistent with estimates of the effect of similar policies in the literature \cite{Duggan2010,Brekke2011}, there is a much richer and reliable database collected by the \textit{Observatorio del Medicamento} from the \textit{Federacion Medica Colombiana}. This information contains the SISMED information, combined with survey data from major pharmaceutical laboratories. Furthermore, it has quarterly frequency (compared to the yearly frequency of the SISMED) and information on regulation (such as reference pricing and whether the pharmaceutical is included in the POS or not). This information however has a cost of \$ COP 17,596,000 $\approx$ \$ USD 9,200.  I would like to run my analysis using this database.

\section*{Steps Forward}

With the information from \textit{Observatorio del Medicamento}-\textit{Federacion Medica Colombiana} I would re-estimate the effect of the policies using more reliable data. But furthermore, I would try to measure the effect of these policies on health outcomes and on the distribution of health expenditure. Since over 40\% of the reimbursement claims are done by individuals in the highest quintile of income \cite{Gaviria2013}, including a pharmaceutical in the POS makes it available to a wider population even if the reduction on prices is smaller. 

Finally, I plan to use this database for future research as I want the central topic of my dissertation to be the study of health care systems in developing countries.  

\section*{Budget}

Data from \textit{Observatorio del Medicamento}-\textit{Federacion Medica Colombiana}:  \$ USD 9,200

\bibliographystyle{apacite}
\bibliography{PharPrice}

\end{document}
