\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}  
\usepackage{apacite}                     
\mode<presentation>{}
\usetheme{Warsaw}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Pharmaceutical Prices]{The effect of reference pricing and competition on pharmaceutical prices}
\author[Romero]{Mauricio Romero (UCSD) }
\normalsize
\date{\today}
\begin{document}
\frame{\titlepage}

\AtBeginSection[]
{
   \begin{frame}
       \frametitle{Table of Contents}
       \tableofcontents[currentsection]
   \end{frame}
}

\section{Motivation}


\begin{frame}{Motivation}
\begin{quote}
``At a time when money is tight, my advice to countries is this: \textbf{before} looking for places to \textbf{cut spending on health care}, look  first for opportunities to \textbf{improve efficiency}''

Dr. Chan, the Director-General of the World Health Organization.
\end{quote}



\end{frame}

%\begin{frame}{Motivation}
%\begin{itemize}
%\item Social consensus is that a market base solution for medicine is intolerable  \cite{Arrow1963}. 
%\item Increasing cost of health expenditures $\Rightarrow$ introduce (regulated) competition to curb costs \cite{Cutler2002}. 
%\end{itemize}
%\end{frame}

\begin{frame}{Research questions}
\begin{itemize}
\item How can we make pharmaceuticals cheaper without dampening innovation or restricting supply?
\item What is the effect of introducing financial incentives in health care?
\item Study two policies: reference pricing and induced competition
\item Are EPS fulfilling their role as intermediaries?
\end{itemize}
\end{frame}


\section{Background}

\begin{frame}{Background}
\begin{itemize}
\item Before 1993 only 24\% of the population in Colombia had some form of health insurance.
\item 47\% of the highest quantile of income had health insurance but less than 5\% of the lowest quantile did. 
\item Law 100 of 1993 set to change this by introducing a universal health insurance scheme.

\end{itemize}
\end{frame}

\begin{frame}{Background}
\begin{itemize}
\item Colombian health care system is a competitive health insurance market
\item Standardized health package (POS) and premium
\item Competition through quality
\end{itemize}
\end{frame}

\begin{frame}{Incentives}
\begin{itemize}
\item Insurance companies residual claimants of difference between premium and services provided
\item Insurance companies often forced to provide services not in the POS (reimbursed by the government)
\end{itemize}
\end{frame}



\begin{frame}[plain]{Reimbursements}
\begin{center}
\includegraphics[width=0.6\textwidth]{Reimb.pdf}
\end{center}
Reimbursements$\approx$ 0.5\% of GDP in 2010\\
Public health expenditure $\approx$ 5\% of GDP in 2010
\end{frame}

\begin{frame}{Policies}
To curb the cost of pharmaceutical, in 2011 the government:
\begin{itemize}
\item Adjusted the standardized health plan (POS)
\item Introduced reference pricing
\end{itemize}
\end{frame}



\section{Data}
\begin{frame}[label=dat1]{Data}
\begin{itemize}
\item \textit{Sistema de Informacion de Precios de Medicamentos} (SISMED)
\item PDF to Excel (Now available in my webpage for anyone to use!)
\item 2006 is unreliable (less than 2\% of pharmaceuticals reported)
\item From 2007-2013 data covers $\approx$ 80\% of pharmaceuticals
\end{itemize}
\hyperlink{sumstat1<1>}{\beamergotobutton{Summary Statistics}}
\end{frame}



\begin{frame}{Data}
\resizebox{1\textwidth}{!}{
\begin{tabular}{lrrrrrrr}
  \hline
 & 2007 & 2008 & 2009 & 2010 & 2011 & 2012 & 2013 \\ 
  \hline
No of pharmaceuticals & 7,285 & 5,530 & 4,913 & 5,632 & 4,891 & 6,645 & 6,919 \\ 
  Included in the plan & 2,797 & 2,086 & 1,822 & 2,051 & 1,768 & 2,450 & 2,612 \\ 
  Proportion & 0.38 & 0.38 & 0.37 & 0.36 & 0.36 & 0.37 & 0.38 \\ 
  With RP & 0 & 0 & 0 & 0 & 428 & 535 & 551 \\ 
  Proportion RP & 0.00 & 0.00 & 0.00 & 0.00 & 0.09 & 0.08 & 0.08 \\ 
   \hline
\end{tabular}
}

\end{frame}

\begin{frame}{Data}
\begin{itemize}
\item Information on 10,465 different pharmaceuticals
\item Only 2,366 pharmaceuticals  have information for all years
\item 1,407 different Anatomical Therapeutic Chemical (ATC) codes
\item 269 pharmacological subgroups in the sample
\item Unit of observation is a pharmaceutical (i.e. company and chemical compound pair)
\item Advil\circledR, Motrin\circledR, and Ibuprofeno MK\circledR are all ibuprofen but are different pharmaceuticals.
\end{itemize}
\end{frame}


\begin{frame}{Data}
\begin{itemize}
 
\item The price used in the analysis is the weighted average, by units sold, of the price per unit of product across presentations. 
\end{itemize}
 \begin{align*}
SP = &\frac{Price}{Units\times(\text{Quantity per unit})}\\
AP= & \frac{\sum_{p \in P}(\text{SP})_p \times (\text{Quantity Sold})_p}{\sum_{p} (\text{Quantity Sold)}_p},
\end{align*} 
\begin{itemize}
\item $P$ is the set of presentations
\item $SP$ is the standardized price (price per unit of product)
\item $AP$ is the average price across presentations. 
\end{itemize}
\end{frame}




\section{Empirical Analysis}

\begin{frame}[plain]{Reference Pricing}
\begin{center}
\includegraphics[width=0.8\textwidth]{VMR2.pdf}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Fix Effects Model}

 $$\log(Price)_{it}= \alpha (RP_{i} \times Post2011_t)+ X_{it} \beta +  \gamma_i +   \gamma_{t} +  \varepsilon_{it} $$
\begin{scriptsize}
\begin{itemize}
\item $\log(Price)_{it}$: log price (in  2008 COP) for molecule $i$ in time $t$
\item $RP_{i}$ $=1$ if molecule $i$ is subject to reference pricing post 2011
\item $Post2011_t$ $=1$ if $t$ is 2011, 2012 or 2013
\item $X_{it}$ Measures of competition for molecule $i$ is in the POS in time $t$
\item $\gamma_i$ molecule fixed effects
\item $\gamma_{t}$ year fixed effects 
\item $ \varepsilon_{it}$ error term
\end{itemize}
\end{scriptsize}

\end{frame}


\begin{frame}[plain]{Effect of reference pricing}
\begin{center}
\resizebox{1\textwidth}{!}{
\input{RegLgCompra_VMR_Prom_STD_0}
}
\end{center}
\end{frame}

\begin{frame}[plain]{Event Study}
\begin{center}
\resizebox{!}{0.5\textheight}{
\input{RegLgCompra_VMR_Prom_EventStudy}
}
\end{center}
\end{frame}

%\begin{frame}[plain]{POS}
%\begin{center}
%\includegraphics[width=0.8\textwidth]{POS1.pdf}
%\end{center}
%\end{frame}


\begin{frame}
\frametitle{Fix Effects Model}

 $$\log(Price)_{it}= \alpha POS_{it}+ X_{it} \beta +  \gamma_i +   \gamma_{t} +  \varepsilon_{it} $$
\begin{scriptsize}
\begin{itemize}
\item $\log(Price)_{it}$: log price (in  2008 COP) for molecule $i$ in time $t$
\item $POS_{it}$ $=1$ if molecule $i$ is in the POS in time $t$
\item $X_{it}$ Measures of competition for molecule $i$ is in the POS in time $t$
\item $\gamma_i$ molecule fixed effects
\item $\gamma_{t}$ year fixed effects 
\item $ \varepsilon_{it}$ error term
\end{itemize}
\end{scriptsize}

\end{frame}


\begin{frame}[plain]{Effect of competition}
\begin{center}
\resizebox{1\textwidth}{!}{
\input{RegLgCompra_POS_Prom_STD_0}
}
\end{center}
\end{frame}


\begin{frame}[plain]{Pseudo Event Study}
\begin{center}
\resizebox{1\textwidth}{!}{
\input{RegLgCompra_POS_PseudoEvent}
}
\end{center}
\end{frame}
%
%\begin{frame}
%\frametitle{First Difference Fix Effects Model}
%
% $$\Delta log(Price)_{it}= \alpha \Delta POS_{it}+ \beta \Delta X_{it}+  \gamma_i +   \gamma_{t} +  \varepsilon_{it} $$
%\begin{scriptsize}
%\begin{itemize}
%\item $\log(Price)_{it}$: log price (in  2008 COP) for molecule $i$ in time $t$
%\item $POS_{it}$ $=1$ if molecule $i$ is in the POS in time $t$
%\item $X_{it}$ Measures of competition for molecule $i$ is in the POS in time $t$
%\item $\gamma_i$ molecule fixed effects (trend)
%\item $\gamma_{t}$ year fixed effects 
%\item $ \varepsilon_{mt}$ error term
%\end{itemize}
%\end{scriptsize}
%
%\end{frame}
%
%\begin{frame}[plain]{Effect of competition}
%\resizebox{1\textwidth}{!}{
%\input{RegLgCompra_POS_Prom_STD_1}
%}
%\end{frame}


%\begin{frame}
%\frametitle{First Difference Fix Effects Model}
%
% $$\Delta log(Price)_{it}= \alpha \Delta RP_{it}+ \beta \Delta X_{it}+  \gamma_i +   \gamma_{t} +  \varepsilon_{it} $$
%\begin{scriptsize}
%\begin{itemize}
%\item $\log(Price)_{it}$: log price (in  2008 COP) for molecule $i$ in time $t$
%\item $POS_{it}$ $=1$ if molecule $i$ has RP in time $t$
%\item $X_{it}$ Measures of competition for molecule $i$ is in the POS in time $t$
%\item $\gamma_i$ molecule fixed effects (trend)
%\item $\gamma_{t}$ year fixed effects 
%\item $ \varepsilon_{mt}$ error term
%\end{itemize}
%\end{scriptsize}
%
%\end{frame}



\section{Effect on price paid by pharmacies}
%
%\begin{frame}[plain]{Pharmacy prices}
%\begin{center}
%\resizebox{1\textwidth}{!}{
%\input{RegPharma_STD_Total}
%}
%\end{center}
%\end{frame}

\begin{frame}[plain]{Pharmacy prices}
\begin{center}
\resizebox{!}{0.5\textheight}{
\input{RegPharma_STD_TotalEvent}
}
\end{center}
\end{frame}

\section{Conclusions}


\begin{frame}{Conclusions}
\begin{itemize}
\item Competition reduces prices by about 14\% (\citeA{Duggan2010} find reduction of 20\%)
\item Reference pricing reduces prices by about 25\% (\citeA{Brekke2011} find reduction of 20\%).
\item What about pharmacy prices? Look at what could have affected both markets in 2010.
\end{itemize}
\end{frame}

\section{Next Steps}


\begin{frame}{Next Steps}
\begin{itemize}
\item What happened in 2010 (RP and pharmacy prices)
\item Get better data
\item Effect on demand
\item Effect on expenditure
\item Effect on health outcomes (use DHS data?)
\item Distributional effects (POS vs RP)
\item Effect across geographical areas with different market power
\end{itemize}
\end{frame}


%\begin{frame}[plain]{Effect of both policies}
%\resizebox{1\textwidth}{!}{
%\input{RegLgCompra_Comb_Prom_STD_0}
%}
%\end{frame}
%
%\begin{frame}[plain]{Effect of both policies}
%\resizebox{1\textwidth}{!}{
%\input{RegLgCompra_Comb_Prom_STD_1}
%}
%\end{frame}





\begin{frame}
\frametitle{Thank you}
\begin{itemize}
\item Gracias
\item Asante Sana
\item Merci
\item Obrigado
\item Grazie
\end{itemize}
\end{frame}


\renewcommand*{\refname}{} % This will define heading of bibliography to be empty, so you can...
\begin{frame}[allowframebreaks]{Bibliography}
	\bibliographystyle{apacite}
\bibliography{PharPrice}
\end{frame}


\begin{frame}[plain,label=sumstat1]{Summary Statistics}

\resizebox{1\textwidth}{!}{
\begin{tabular}{@{\extracolsep{5pt}}lccccc} 
\\[-1.8ex]\hline 
\hline \\[-1.8ex] 
Statistic & \multicolumn{1}{c}{N} & \multicolumn{1}{c}{Mean} & \multicolumn{1}{c}{St. Dev.} & \multicolumn{1}{c}{Min} & \multicolumn{1}{c}{Max} \\ 
\hline \\[-1.8ex] 
\textbf{Panel A: Presentations } & &  &  & &  \\ 
No. of pharmaceutical presentation & 20,095 & 3.013 & 3.071 & 1 & 54 \\ 
\hline \\[-1.8ex]
\textbf{Panel B: Pharmaceutics } & &  &  & &  \\ 
In the POS (=1 yes) & 41,815 & 0.373 & 0.484 & 0 & 1 \\ 
No. of pharmaceutics in the same pharmacological subgroup & 41,815 & 147.126 & 113.370 & 1 & 556 \\
No. of pharmaceutics in the same pharmacological subgroup in the POS & 41,815 & 55.768 & 59.511 & 0 & 308 \\ 
No. of pharmaceutics with the same ATC & 41,815 & 34.835 & 36.631 & 1 & 231 \\ 
No. of pharmaceutics with the same ATC in the POS & 41,815 & 14.765 & 24.081 & 0 & 156 \\ 
\hline \\[-1.8ex]
\textbf{Panel C: ATC Codes } & &  &  & &  \\ 
Number of pharmaceutics per code/year & 7,881 & 5.306 & 7.418 & 1 & 81 \\ 
Proportion of pharmaceutics in the POS per code/year & 7,881 & 0.236 & 0.380 & 0.000 & 1.000 \\
\hline \\[-1.8ex]
\textbf{Panel D: pharmacological subgroup } & &  &  & &  \\ 
Number of pharmaceutics per group/year & 1,724 & 24.255 & 32.065 & 1 & 248 \\ 
Proportion of pharmaceutics in the POS per group/year &  1,724 & 0.277 & 0.304 & 0.000 & 1.000 \\ 
\hline \\[-1.8ex] 
\normalsize 
\end{tabular} 
}
\hyperlink{dat1<1>}{\beamerreturnbutton{Go Back}}
\end{frame}

\end{document}

