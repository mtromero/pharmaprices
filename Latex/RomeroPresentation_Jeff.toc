\beamer@endinputifotherversion {3.10pt}
\beamer@sectionintoc {1}{Motivation}{2}{0}{1}
\beamer@sectionintoc {2}{Background}{8}{0}{2}
\beamer@sectionintoc {3}{Data}{12}{0}{3}
\beamer@sectionintoc {4}{Empirical Analysis}{17}{0}{4}
\beamer@sectionintoc {5}{Effect on price paid by pharmacies}{28}{0}{5}
\beamer@sectionintoc {6}{Conclusions}{31}{0}{6}
\beamer@sectionintoc {7}{Next Steps}{33}{0}{7}
\contentsline {section}{}
