\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}  
\usepackage{apacite}                     
\mode<presentation>{}
\usetheme{Warsaw}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Pharmaceutical Prices]{Benefit plans and pharmaceutical prices: Evidence from Colombia}
\author[Romero]{Mauricio Romero}
\normalsize
\date{\today}
\begin{document}

\frame{\titlepage
}
% 
%\AtBeginSection[]
%{
%   \begin{frame}
%       \frametitle{Table of Contents}
%       \tableofcontents[currentsection]
%   \end{frame}
%}

\section{Motivation and context}

\begin{frame}[plain]
\frametitle{Motivation}
\begin{itemize}
%[<+->]
\item The world have moved towards the adoption of health benefit plans that explicitly define the services to be covered with public funds\cite{Tristao2014,Glassman2012}.
\begin{itemize}
\item $\downarrow$ out-of-pocket expenditure $\Rightarrow$ $\downarrow$ price elastic $\Rightarrow$ $\uparrow P$
\item Large buyer groups $\Rightarrow$ $\uparrow$ bargaining power $\Rightarrow$ $\downarrow P$
\item Effect on prices is unclear
\end{itemize}
\item Under the right institutional arrangement (incentive structure) benefit plans can expand care and reduce health expenditure.
\item Pharmaceuticals
\begin{itemize}
\item Account for almost a fifth of health expenditure in most OECD countries\cite{Brekke2012}
\item Although private goods, they have positive externalities
\end{itemize}
\item \textbf{This paper:} Impact of including a pharmaceutical in the benefit plan on prices and utilization in middle-income country
\end{itemize}
\end{frame}


\begin{frame}[plain]
\frametitle{Colombian health care system}
Key institutional details
\begin{itemize}
\item Private insurance plans compete for enrollees
\item Plans have identical:
\begin{itemize} 
\item benefit coverage (benefit plan or POS)
\item copayment structure
\item price (taxes)
\end{itemize}
\item Insurance plans compete through quality (network coverage, waiting times, etc.)
\item Insurance companies are compensated through a risk-adjusted capitation payment (UPC).
\item Insurance companies want to maximize 
\begin{equation}
\pi=\sum_{i \in E} UPC_{i}-Expenditure_i=\sum_{i \in E} UPC_{i}-\sum_{j \in POS} P_j Q_{ji},
\end{equation}
\end{itemize}
\end{frame}


%\begin{frame}[plain]
%\frametitle{Why this setting?}
%\begin{itemize}[<+->]
%\item Evidence from a middle-income country with a health system that resembles the design of many high-income countries.
%\item Evidence from a small player in the pharmaceutical market $\rightarrow$ unlikely to have an impact on innovation, world prices, etc.
%\item Benefit plan and premiums are identical $\rightarrow$ degree to which enrollees are steered to certain drugs
%\item Data!
%\end{itemize}
%\end{frame}


\section{Data and empirical strategy}

\begin{frame}[plain]
\frametitle{Data}

\textit{Sistema de Informacion de Precios de Medicamentos} (SISMED)
\begin{itemize}
\item Average price at which pharmaceutical companies sell to insurance companies and providers and total quantities
\item Information for 2007-2014
%\item 26,116 unique pharmaceuticals registered in Colombia during this period
%\item 13,869 have information on prices at some point in time and only 4,146 have information for all years
\item Each pharmaceutical is associated with at least one Anatomical Therapeutic Chemical (ATC) code
%\item There are 1,554 different ATC codes and 571 pharmacological subgroup in the sample.
\end{itemize}

\hyperlink{sumstat<1>}{\beamergotobutton{Sample}}
\hyperlink{sumstat2<1>}{\beamergotobutton{Summary Statistics}}
\end{frame}



\begin{frame}[plain]
\frametitle{Empirical Strategy}
Difference-in-difference:

\begin{eqnarray*}\label{eq:original}
\log(Price_{it})= \alpha POS_{it}+  \gamma_i +   \gamma_{t} +  \varepsilon_{it} 
\end{eqnarray*}
\begin{scriptsize}
\begin{itemize}
\item $Price_{it}$ price of pharmaceutical $i$ in year $t$
\item $POS_{it}$ indicates whether pharmaceutical $i$ was included in the POS at time $t$
\item $\gamma_i$ pharmaceutical fixed effects
\item $\gamma_{t}$ year fixed effects 
\item $ \varepsilon_{it}$ error term
\end{itemize}
$\alpha$ impact of including a pharmaceutical in the POS on its price
\end{scriptsize}
\end{frame}

\section{Results}

\begin{frame}[plain]
\frametitle{Effect on prices}
\begin{table}[H]
\caption{Effect of including a pharmaceutical in the standardized health plan on its price \label{tab:POS}}
\begin{center}
\resizebox{1\textwidth}{!}{
\begin{tabular}{l*{4}{c}}
\toprule
 &\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}\\
\midrule
\input{Reg_Prom_STD_Institucional_2}
\bottomrule
\multicolumn{5}{c}{\footnotesize \specialcell{Clustered standard errors, by therapeutic group, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\end{table}
\end{frame}

\begin{frame}[plain]
\frametitle{Effect on quantities}
\begin{table}[H]
\caption{Effect of including a pharmaceutical in the standardized health plan on the quantities sold \label{tab:POS2}}
\begin{center}
\resizebox{1\textwidth}{!}{
\begin{tabular}{l*{4}{c}}
\toprule
 &\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}\\
\midrule
\input{Reg_Unidades_STD_Institucional_2}
\bottomrule
\multicolumn{5}{c}{\footnotesize \specialcell{Clustered standard errors, by therapeutic group, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\end{table}
\end{frame}

\begin{frame}[plain]
\frametitle{Event Study}
Main threat to identification: parallel trends
\begin{eqnarray*}
\log(Price_{it})= \sum_{j=-7}^{7} \alpha_j 1_{\tau_i+j=t} + \gamma_i +\lambda_{i} \times t  +  \varepsilon_{it}\\
\log(Q_{it})= \sum_{j=-7}^{7} \alpha_j 1_{\tau_i+j=t} + \gamma_i  +\lambda_{i} \times t +  \varepsilon_{it}, 
\end{eqnarray*}
\end{frame}


\begin{frame}[plain]
\frametitle{Event study: prices}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\textwidth]{EventStudyPOS_Institucional_Prom_STD_2_completo_trend.pdf} 
\caption{\tiny Event study for the effect of including a pharmaceutical in the POS on its price. \textit{Source:} SISMED and INVIMA. \textit{Calculations:} Author. \label{fig:eve_pos_price}}
\end{center}
\end{figure}
\end{frame}

\begin{frame}[plain]
\frametitle{Event study: quantities}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\textwidth]{EventStudyPOS_Institucional_Unidades_STD_2_completo_trend.pdf} 
\caption{\tiny Event study for the effect of including a pharmaceutical in the POS on the quantities. \textit{Source:} SISMED and INVIMA. \textit{Calculations:} Author. \label{fig:eve_pos_price}}
\end{center}
\end{figure}
\end{frame}



\begin{frame}[plain]
\frametitle{Bargaining power and competition}
\begin{itemize}
\item Large buyer groups bargaining power argument depends on the availability of ``substitutes'' for a drug
\item First, define 2 categories for substitutes
\begin{itemize}
\item Same ATC (essentially the same active ingredients)
\item Same therapeutic group (drugs used to treat the same illness)
\end{itemize}
\item If there are no ``substitutes'' $\Rightarrow$ $\uparrow P$ after including a drug in the POS
\item If there are ``substitutes'' $\Rightarrow$ $\downarrow P$ after  including a drug in the POS
\end{itemize}

\begin{equation*}\label{eq:mkpower}
\log(Y_{it})= \alpha_1 POS_{it}\times Monopoly_{it}+\alpha_2 POS_{it} \times Competition_{it}+  \gamma_i +   \gamma_{t} +  \varepsilon_{it},
\end{equation*}

\end{frame}


\begin{frame}[plain]
\begin{table}[H]
\caption{Barganing and market power \label{tab:Mkt1}}
\begin{center}
\resizebox{!}{0.5\textheight}{
\begin{tabular}{l*{5}{c}}
\toprule
\multicolumn{6}{l}{\textbf{Panel A: Prices}} \\
 &\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}\\
\input{RegCompetenciaProm_STD_Institucional_2_trends}
\midrule
\multicolumn{6}{l}{\textbf{Panel A: Quantities}} \\
 &\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}\\
\input{RegCompetenciaUnidades_STD_Institucional_2_trends}
\bottomrule
\multicolumn{6}{l}{\footnotesize \specialcell{Clustered standard errors, by therapeutic group, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\end{table}
\end{frame}

\section{Closing remarks}


\begin{frame}[plain]
\frametitle{Concluding remarks}
Ideas on where to move from here? They are more than welcome!
\end{frame}

\begin{frame}[plain]
\frametitle{Thank you}
\begin{itemize}
\item Gracias
\item Asante Sana
\item Merci
\item Obrigado
\item Grazie
\end{itemize}

\end{frame}

\renewcommand*{\refname}{} % This will define heading of bibliography to be empty, so you can...
\begin{frame}[allowframebreaks]{Bibliography}
	\bibliographystyle{apacite}
\bibliography{PharPrice}
\end{frame}


\appendix

\begin{frame}[plain,label=sumstat]
\frametitle{Summary statistics}

\begin{table}[H]
\caption{Difference between pharmaceuticals characteristics by how much price information is observed.\label{tab:diff_charac}}
\begin{center}
\resizebox{1\textwidth}{!}{
\input{ObserbationsDiferences}
}
\end{center}
\end{table}
\end{frame} 

\begin{frame}[plain,label=sumstat2]
\begin{table}[H] \centering 
  \caption{} 
  \label{tab:descr} 
\resizebox{1\textwidth}{!}{
\begin{tabular}{lccccc} 



\toprule
 & \multicolumn{1}{c}{N} & \multicolumn{1}{c}{Mean} & \multicolumn{1}{c}{St. Dev.} & \multicolumn{1}{c}{Min} & \multicolumn{1}{c}{Max} \\ 
\midrule
\input{DescrpB}
%\midrule 
%%\tablefootnote{The unit of observation is the ATC code in a given year}
%\textbf{Panel C: ATC Codes } & &  &  & &  \\ 
%\input{DescrpC}
%\midrule
%%\tablefootnote{The unit of observation is the therapeutic group in a given year}
%\textbf{Panel D: pharmacological subgroup } & &  &  & &  \\ 
%\input{DescrpD}
\bottomrule

\multicolumn{6}{p{0.95\textwidth}}{{\tiny \textit{Source:} SISMED and INVIMA. \textit{Calculations:} Author.}} 
%Panel C shows summary statistics for ATC codes across all years and Panel D shows summary statistics for pharmacological subgroups across all years

\end{tabular} 
}
\end{table} 
\end{frame}


\end{document}
