\documentclass[pdf]{beamer}
\usepackage{booktabs}
\usepackage{tabularx,colortbl}
\usepackage{multirow}
\usepackage[export]{adjustbox}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{comment}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{dsfont}
\usepackage{bbm}
\usepackage[flushleft]{threeparttable}      % Option: Table notes
\usepackage{floatpag}
\usepackage{wrapfig}
\usepackage{relsize}
\usepackage{setspace}  
\usepackage{apacite}                     
\mode<presentation>{}
\usetheme{Warsaw}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\expandafter\def\expandafter\insertshorttitle\expandafter{%
  \insertshorttitle\hfill%
  \insertframenumber\,/\,\inserttotalframenumber}
\title[Pharmaceutical Prices]{Benefit plans, competition, and pharmaceutical prices}
\author[Romero]{Mauricio Romero}
\normalsize
\date{\today}
\setbeamertemplate{caption}[numbered]
\begin{document}

\frame{\titlepage
}
% 
%\AtBeginSection[]
%{
%   \begin{frame}
%       \frametitle{Table of Contents}
%       \tableofcontents[currentsection]
%   \end{frame}
%}

\section{Motivation and context}

\begin{frame}[plain]
 
\frametitle{Motivation}
\begin{itemize}
\vfill \item Pharmaceuticals
\begin{itemize}
\vfill \item Account for 20\% health expenditure in most OECD countries
\vfill \item Suppliers have market power
\end{itemize}

\vfill \item Benefit plans
\begin{itemize}
\vfill \item  Reduce price elasticity of enrollees
\vfill \item Large buyer groups
\end{itemize}

\end{itemize}
\end{frame}

\begin{frame}[plain]
\frametitle{This paper}
\begin{itemize}
\vfill \item What is the impact of including a drug in the benefit plan on its price and utilization?
\vfill \item What happens to ``similar'' drugs that see an increase in competition?
\end{itemize}
\end{frame}


\begin{frame}[plain]
\frametitle{Colombian health care system}
Key institutional details
\begin{itemize}
\vfill \item Private insurance plans compete for enrollees
\vfill \item Plans have identical:
\begin{itemize} 
\item benefit plan (POS)
\item copayment structure
\item price (taxes)
\end{itemize}
\vfill \item Insurance plans compete through quality (network coverage, waiting times, etc.)
\vfill \item Insurance companies are compensated through a risk-adjusted capitation payment (UPC).
\end{itemize}
\end{frame}

\begin{frame}[plain]
\frametitle{Colombian health care system}
Insurance plans profits
\begin{equation*}
\pi=\sum_{i \in E} UPC_{i}-Expenditure_i=\sum_{i \in E} UPC_{i}-\sum_{j \in POS} P_j Q_{ji},
\end{equation*}
\end{frame}



\section{Data}

\begin{frame}[plain,label=goback0]
\frametitle{Data}

\textit{Sistema de Informacion de Precios de Medicamentos} (SISMED)
\begin{itemize}
\vfill \item Average price at which pharmaceutical companies sell to providers
\vfill \item Information for 2007-2014
\vfill \item Each pharmaceutical is associated with at least one Anatomical Therapeutic Chemical (ATC) code
\end{itemize}

\hyperlink{sumstat<1>}{\beamergotobutton{Sample}}
\hyperlink{sumstat2<1>}{\beamergotobutton{Summary Statistics}}
\end{frame}


\begin{frame}[plain]
\frametitle{Empirical Strategy}
Difference-in-difference:

\begin{eqnarray*}\label{eq:original}
\log(Price_{it})= \alpha POS_{it}+ X_{it} \beta + \gamma_i +   \gamma_{t} +  \varepsilon_{it} 
\end{eqnarray*}
\begin{scriptsize}
\begin{itemize}
\item $Price_{it}$ price of pharmaceutical $i$ in year $t$
\item $POS_{it}$ indicates whether pharmaceutical $i$ was included in the POS at time $t$
\item $X_{it}$ Pharmaceutical controls: whether there are generics with the same ATC code, and the (log) number of competitors with the same ATC code
\item $\gamma_i$ pharmaceutical fixed effects
\item $\gamma_{t}$ year fixed effects 
\item $ \varepsilon_{it}$ error term
\end{itemize}
$\alpha$ impact of including a pharmaceutical in the POS on its price
\end{scriptsize}
\end{frame}

\section{Results}




\begin{frame}[plain,label=goback1]
\frametitle{Event study: prices}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.85\textwidth]{EventStudyPOS_Institucional_Prom_STD_2_completo_trend.pdf} 
\caption{\tiny Event study for the effect of including a pharmaceutical in the POS on its price. \textit{Source:} SISMED and INVIMA. \textit{Calculations:} Author. \label{fig:eve_pos_price}}
\end{center}
\end{figure}
\hyperlink{reg_p<1>}{\beamergotobutton{Regression}}
\hyperlink{fig2011<1>}{\beamergotobutton{Entry 2011}}
\hyperlink{fig2012<1>}{\beamergotobutton{Entry 2012}}
\end{frame}

\begin{frame}[plain,label=goback2]
\frametitle{Event study: quantities}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.85\textwidth]{EventStudyPOS_Institucional_Unidades_STD_2_completo_trend.pdf} 
\caption{\tiny Event study for the effect of including a pharmaceutical in the POS on the quantities. \textit{Source:} SISMED and INVIMA. \textit{Calculations:} Author. \label{fig:eve_pos_price}}
\end{center}
\end{figure}
\hyperlink{reg_q<1>}{\beamergotobutton{Regression}}
\hyperlink{fig2011_q<1>}{\beamergotobutton{Entry 2011}}
\hyperlink{fig2012_q<1>}{\beamergotobutton{Entry 2012}}
\end{frame}





\begin{frame}[plain]
\frametitle{What happens to incumbents?}
\begin{itemize}
\vfill\item Restrict to drugs that are always in the benefit plan during the study period
\vfill\item What happens when they see an increase in competition?
\end{itemize}

\begin{eqnarray*}\label{eq:original}
\log(Price_{it})= \alpha POSCompetition_{it}+ X_{it} \beta + \gamma_i +   \gamma_{t} +  \varepsilon_{it} 
\end{eqnarray*}

\end{frame}

\begin{frame}[plain]
\begin{table}[H]
\caption{Barganing and market power - incumbent \label{tab:Mkt1}}
\begin{center}
\resizebox{1\textwidth}{!}{
\begin{tabular}{l*{6}{c}}
\toprule
 &\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}\\
\input{RegCompetenciaIncumbentProm_STD_Institucional_2_trends}
\bottomrule
\multicolumn{7}{l}{\footnotesize \specialcell{Clustered standard errors, by therapeutic group, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\end{table}
\end{frame}

\begin{frame}[plain]
\begin{table}[H]
\caption{Barganing and market power - incumbent \label{tab:Mkt1}}
\begin{center}
\resizebox{1\textwidth}{!}{
\begin{tabular}{l*{6}{c}}
\toprule
 &\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}\\
\input{RegCompetenciaIncumbentUnidades_STD_Institucional_2_trends}
\bottomrule
\multicolumn{7}{l}{\footnotesize \specialcell{Clustered standard errors, by therapeutic group, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\end{table}
\end{frame}
%
%
\begin{frame}[plain]
\frametitle{What happens to incumbents?}
\begin{itemize}
\vfill \item Consistent with a segmented market
\vfill \item Loyal vs price sensitive consumers (physicians)
\end{itemize}
\end{frame}

\section{Closing remarks}


\begin{frame}[plain]
\frametitle{Concluding remarks}
Ideas on where to move from here? They are more than welcome!
\end{frame}

\begin{frame}[plain]
\frametitle{Thank you}
\begin{itemize}
\item Gracias
\item Asante Sana
\item Merci
\item Obrigado
\item Grazie
\end{itemize}

\end{frame}

\renewcommand*{\refname}{} % This will define heading of bibliography to be empty, so you can...
\begin{frame}[allowframebreaks]{Bibliography}
	\bibliographystyle{apacite}
\bibliography{PharPrice}
\end{frame}


\appendix

\begin{frame}[plain,label=sumstat]
\frametitle{Summary statistics}

\begin{table}[H]
\caption{Difference between pharmaceuticals characteristics by how much price information is observed.\label{tab:diff_charac}}
\begin{center}
\resizebox{1\textwidth}{!}{
\input{ObserbationsDiferences}
}
\end{center}
\end{table}
\hyperlink{goback0<1>}{\beamergotobutton{Go back}}
\end{frame} 

\begin{frame}[plain,label=sumstat2]
\begin{table}[H] \centering 
  \caption{} 
  \label{tab:descr} 
\resizebox{1\textwidth}{!}{
\begin{tabular}{lccccc} 



\toprule
 & \multicolumn{1}{c}{N} & \multicolumn{1}{c}{Mean} & \multicolumn{1}{c}{St. Dev.} & \multicolumn{1}{c}{Min} & \multicolumn{1}{c}{Max} \\ 
\midrule
\input{DescrpB}
%\midrule 
%%\tablefootnote{The unit of observation is the ATC code in a given year}
%\textbf{Panel C: ATC Codes } & &  &  & &  \\ 
%\input{DescrpC}
%\midrule
%%\tablefootnote{The unit of observation is the therapeutic group in a given year}
%\textbf{Panel D: pharmacological subgroup } & &  &  & &  \\ 
%\input{DescrpD}
\bottomrule

\multicolumn{6}{p{0.95\textwidth}}{{\tiny \textit{Source:} SISMED and INVIMA. \textit{Calculations:} Author.}} 
%Panel C shows summary statistics for ATC codes across all years and Panel D shows summary statistics for pharmacological subgroups across all years

\end{tabular} 
}
\end{table} 
\hyperlink{goback0<1>}{\beamergotobutton{Go back}}
\end{frame}



\begin{frame}[plain,label=reg_p]
\frametitle{Effect on prices}
\begin{table}[H]
\caption{Effect of including a pharmaceutical in the standardized health plan on its price \label{tab:POS}}
\begin{center}
\resizebox{1\textwidth}{!}{
\begin{tabular}{l*{4}{c}}
\toprule
 &\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}\\
\midrule
\input{Reg_Prom_STD_Institucional_2}
\bottomrule
\multicolumn{5}{c}{\footnotesize \specialcell{Clustered standard errors, by therapeutic group, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\end{table}
\hyperlink{goback1<1>}{\beamergotobutton{Go back}}
\end{frame}

\begin{frame}[plain,label=reg_q]
\frametitle{Effect on quantities}
\begin{table}[H]
\caption{Effect of including a pharmaceutical in the standardized health plan on the quantities sold \label{tab:POS2}}
\begin{center}
\resizebox{1\textwidth}{!}{
\begin{tabular}{l*{4}{c}}
\toprule
 &\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}\\
\midrule
\input{Reg_Unidades_STD_Institucional_2}
\bottomrule
\multicolumn{5}{c}{\footnotesize \specialcell{Clustered standard errors, by therapeutic group, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\end{table}
\hyperlink{goback2<1>}{\beamergotobutton{Go back}}
\end{frame}

\begin{frame}[plain,label=fig2011]
\frametitle{Event study for those drugs that enter in 2011: prices}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.85\textwidth]{EventStudyPOS_precio_completo_trend_2011_entry.pdf} 
\caption{\tiny Event study for the effect of including a pharmaceutical in the POS on its price. \textit{Source:} SISMED and INVIMA. \textit{Calculations:} Author. \label{fig:eve_pos_price}}
\end{center}
\end{figure}
\hyperlink{goback1<1>}{\beamergotobutton{Go back}}
\end{frame}

\begin{frame}[plain,label=fig2012]
\frametitle{Event study for those drugs that enter in 2012: prices}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.85\textwidth]{EventStudyPOS_precio_completo_trend_2012_entry.pdf} 
\caption{\tiny Event study for the effect of including a pharmaceutical in the POS on its price. \textit{Source:} SISMED and INVIMA. \textit{Calculations:} Author. \label{fig:eve_pos_price}}
\end{center}
\end{figure}
\hyperlink{goback1<1>}{\beamergotobutton{Go back}}
\end{frame}


\begin{frame}[plain,label=fig2011_q]
\frametitle{Event study for those drugs that enter in 2011: quantities}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.85\textwidth]{EventStudyPOS_quantity_completo_trend_2011_entry.pdf} 
\caption{\tiny Event study for the effect of including a pharmaceutical in the POS on its price. \textit{Source:} SISMED and INVIMA. \textit{Calculations:} Author. \label{fig:eve_pos_price}}
\end{center}
\end{figure}
\hyperlink{goback2<1>}{\beamergotobutton{Go back}}
\end{frame}

\begin{frame}[plain,label=fig2012_q]
\frametitle{Event study for those drugs that enter in 2012: quantities}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.85\textwidth]{EventStudyPOS_quantity_completo_trend_2012_entry.pdf} 
\caption{\tiny Event study for the effect of including a pharmaceutical in the POS on its price. \textit{Source:} SISMED and INVIMA. \textit{Calculations:} Author. \label{fig:eve_pos_price}}
\end{center}
\end{figure}
\hyperlink{goback2<1>}{\beamergotobutton{Go back}}
\end{frame}


\begin{frame}[plain]
\frametitle{Introduction of drugs into the POS}
\begin{figure}[H]
\begin{center}
\includegraphics[width=0.9\textwidth]{FrequencyAddPOS.pdf} 
\caption{\tiny Number of drugs that get introduced in the POS by year. \textit{Source:} SISMED and INVIMA. \textit{Calculations:} Author. \label{fig:eve_pos_price}}
\end{center}
\end{figure}
\hyperlink{goback2<1>}{\beamergotobutton{Go back}}
\end{frame}

\begin{frame}[plain]
\frametitle{Effect on prices}
\begin{table}[H]
\caption{Effect of including a pharmaceutical in the standardized health plan on its price \label{tab:POS}}
\begin{center}
\resizebox{1\textwidth}{!}{
\begin{tabular}{l*{4}{c}}
\toprule
 &\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}&\multicolumn{1}{c}{log(Price)}\\
\midrule
\input{RegByStatus_Prom_STD_Institucional_2}
\bottomrule
\multicolumn{5}{c}{\footnotesize \specialcell{Clustered standard errors, by therapeutic group, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\end{table}
\end{frame}

\begin{frame}[plain]
\frametitle{Effect on quantities}
\begin{table}[H]
\caption{Effect of including a pharmaceutical in the standardized health plan on the quantities sold \label{tab:POS2}}
\begin{center}
\resizebox{1\textwidth}{!}{
\begin{tabular}{l*{4}{c}}
\toprule
 &\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}&\multicolumn{1}{c}{log(Q)}\\
\midrule
\input{RegByStatus_Unidades_STD_Institucional_2}
\bottomrule
\multicolumn{5}{c}{\footnotesize \specialcell{Clustered standard errors, by therapeutic group, in parenthesis\\  \sym{*} \(p<0.10\), \sym{**} \(p<0.05\), \sym{***} \(p<0.01\) }}\\
\end{tabular}
}
\end{center}
\end{table}
\end{frame}

\end{document}
