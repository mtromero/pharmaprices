\beamer@endinputifotherversion {3.33pt}
\beamer@sectionintoc {1}{Motivation}{2}{0}{1}
\beamer@sectionintoc {2}{Background}{5}{0}{2}
\beamer@sectionintoc {3}{Data}{11}{0}{3}
\beamer@sectionintoc {4}{Empirical Analysis}{16}{0}{4}
\beamer@sectionintoc {5}{Effect on price paid by pharmacies}{24}{0}{5}
\beamer@sectionintoc {6}{Conclusions}{26}{0}{6}
\beamer@sectionintoc {7}{Next Steps}{28}{0}{7}
\contentsline {section}{}
