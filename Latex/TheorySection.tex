%\input{tcilatex}
\documentclass{article}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amssymb}
\usepackage{dcolumn}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{footnote}
\usepackage{fullpage}
\usepackage{mathrsfs,amsfonts}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{tabularx}
\usepackage{tablefootnote}
\usepackage[plainpages=false,pdfpagelabels]{hyperref}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{nameref}
\usepackage[all]{hypcap}

\usepackage [autostyle, english = american]{csquotes}
\usepackage{apacite}
\MakeOuterQuote{"}
\setcounter{MaxMatrixCols}{10}
%TCIDATA{OutputFilter=LATEX.DLL}
%TCIDATA{Version=5.50.0.2953}
%TCIDATA{<META NAME="SaveForMode" CONTENT="1">}
%TCIDATA{BibliographyScheme=Manual}
%TCIDATA{LastRevised=Wednesday, October 10, 2012 16:40:37}
%TCIDATA{<META NAME="GraphicsSave" CONTENT="32">}
%TCIDATA{Language=American English}
%\newtheorem{theorem}{Theorem}
\newtheorem{acknowledgement}{Acknowledgement}
\newtheorem{algorithm}{Algorithm}
\newtheorem{axiom}{Axiom}
\newtheorem{case}{Case}
\newtheorem{claim}{Claim}
\newtheorem{conclusion}{Conclusion}
\newtheorem{condition}{Condition}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{criterion}{Criterion}
\newtheorem{definition}{Definition}
\newtheorem{example}{Example}
\newtheorem{exercise}{Exercise}
\newtheorem{lemma}{Lemma}
\newtheorem{notation}{Notation}
\newtheorem{problem}{Problem}
\newtheorem{proposition}{Proposition}
\newtheorem{remark}{Remark}
\newtheorem{solution}{Solution}
\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}
\newcommand{\specialcell}[2][c]{%
    \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}
}
\newtheorem{summary}{Summary}
%\usepackage{geometry}
\newenvironment{proof}[1][Proof]{\noindent\textbf{#1.} }{\ \rule{0.5em}{0.5em}}
\date{}     % Deleting this command produces today's date.
\newcommand{\ip}[2]{(#1, #2)}
 \newcommand{\Lagr}{\mathcal{L}}
      % Defines \ip{arg1}{arg2} to mean
                             % (arg1, arg2).
%\newcommand{\ip}[2]{\langle #1 | #2\rangle}
                             % This is an alternative definition of
                             % \ip that is commented out.
\onehalfspacing
\title{Financial incentives and pharmaceutical prices: Evidence from Colombia}
 
\author{Mauricio Romero\thanks{University of California - San Diego, 9500 Gilman Dr, San Diego, CA 92093, U.S. \textit{e-mail contact:} mtromero@ucsd.edu \textit{Phone:} 858-6107632 } }
\begin{document}             % End of preamble and beginning of text.
                   % Produces the title.
\maketitle

\section{Theory}

Suppose we have a continuum of agents of measure 1, each trying to solve the following problem

\begin{eqnarray*}
\max_{c_i,d_i} u(c_i,d_i) &=& (c_i^{\frac{\eta-1}{\eta}}+ d_i^{\frac{\eta-1}{\eta}})^{\frac{\eta}{\eta-1}} \\
\end{eqnarray*}
s.t.
\begin{eqnarray*}
\left(\sum_{j=1}^N  d_{i,j}^{\frac{\rho-1}{\rho}} \right)^{\frac{\rho}{\rho-1}} &=& d_{i} \\
c_i+\sum d_{i,j} p_j &=& y_i \\
\end{eqnarray*}

These consumers are price takers, i.e. they take $p_j$ as given for all $j$. Therefore the demand for each product is:

\begin{eqnarray*}
c_i &=& \left(\frac{1}{P} \right)^{1-\eta} y_i \\
d_i &=& \left(\frac{P_d}{P} \right)^{1-\eta} \frac{y_i}{P_d} \\
d_{i,j} &=& \left(\frac{P_j}{P_d} \right)^{1-\rho} \left(\frac{P_d}{P} \right)^{1-\eta} \frac{y_i}{P_j} \\
\end{eqnarray*}
where

\begin{eqnarray*}
P &=& (P_d^{1-\eta}+1)^{\frac{1}{1-\eta}}\\
P_d &=& \left(\sum_{j=1}^N P_j^{1-\rho} \right)^{\frac{1}{1-\rho}}\\
\end{eqnarray*}



Suppose additionally we have $N$ pharmaceutical companies under monopolistic competition. Each firms solves the following problem:


\begin{eqnarray*}
\max_{p_j} p_j Q^d(p_j)-c_j Q^d(p_j)  \\
\end{eqnarray*}

where $Q^d(p_j)= \int_{0}^1 d_{i,j}(p_j) d_i=\left(\frac{P_j}{P_d} \right)^{1-\rho} \left(\frac{P_d}{P} \right)^{1-\eta} \frac{Y}{P_j}$

where $Y=\int y_i d_i$


and therefore the profit maximizing condition is:


 
 \begin{eqnarray*}
Q^d(p_j)+p_j \frac{\partial Q^d(p_j)}{\partial p_j}-c_j \frac{\partial Q^d(p_j)}{\partial p_j} &=& 0 \\
1+\frac{p_j}{Q^d(p_j)} \frac{\partial Q^d(p_j)}{\partial p_j}-c_j \frac{1}{Q^d(p_j)}\frac{\partial Q^d(p_j)}{\partial p_j} &=& 0 \\
p_j+p_j\varepsilon-c_j \varepsilon &=& 0 \\
\end{eqnarray*}

and therefore the optimal price is such that
\begin{equation}
p_j = c_j \frac{\varepsilon_j}{1+\varepsilon_j}
\end{equation}

where $\varepsilon_j$ is the price elasticity for good $j$. Notice the price elasticity is $\varepsilon_j=-\eta s_j-\rho (1-s_j)$ where $s_j=\frac{p_j^{1-\rho}}{\sum_{j=1}^N p_j^{1-\rho}}$. Therefore in equilibrium each firm would be the set of prices that makes the following equation hold for all $j$:

\begin{eqnarray*}
p_j &=& c_j \frac{\varepsilon_j}{1+\varepsilon_j} \\
\end{eqnarray*}

Now if we assume that $c_j=c$ for all $j$, then we can find a symmetric equilibrium by assuming all firms will set the same price in which case:


\begin{eqnarray*}
p &=& c \frac{-\eta \frac{1}{N}-\rho \frac{N-1}{N}}{1-\eta \frac{1}{N}-\rho (\frac{N-1}{N})} \\
p &=& c \frac{\eta -\rho(N-1)}{N-\eta-\rho(N-1)} \\
\end{eqnarray*}

If there is only one pharmaceutical then we would have:
\begin{eqnarray*}
p &=& c \frac{\eta}{1-\eta} \\
\end{eqnarray*}


%\begin{eqnarray*}
%\frac{p_j}{Q^d(p_j)} \frac{\partial Q^d(p_j)}{\partial p_j} &=& \frac{\partial \ln Q^d(p_j)}{\partial \ln p_j} \\
%&=& \frac{\partial \ln \left(\frac{p_j}{P_d} \right)^{\frac{1}{\gamma-1}} \left(\frac{P_d}{P} \right)^{\frac{1}{\theta-1}} Y}{\partial \ln p_j} \\
%&=& \frac{1}{\gamma-1} \frac{\partial  \ln \left(\frac{p_j}{P_d} \right)}{\partial \ln p_j} +\frac{1}{\theta-1} \frac{\partial  \ln \frac{P_d}{P}}{\partial \ln p_j}+\frac{\partial  \ln Y}{\partial \ln p_j} \\
%&=& \frac{1}{\gamma-1} \frac{\partial  \ln p_j }{\partial \ln p_j}-\frac{1}{\gamma-1} \frac{\partial  \ln P_d}{\partial \ln p_j} +\frac{1}{\theta-1} \frac{\partial  \ln P_d}{\partial \ln p_j}-\frac{1}{\theta-1} \frac{\partial  \ln P}{\partial \ln p_j} \\
%&=& \frac{1}{\gamma-1} -\frac{1}{\gamma-1} \frac{\partial  \ln \left(\sum_{j=1}^N P_j^{\frac{\gamma}{\gamma-1}} \right)^{\frac{\gamma-1}{\gamma}}}{\partial \ln p_j} +\frac{1}{\theta-1} \frac{\partial  \ln \left(\sum_{j=1}^N P_j^{\frac{\gamma}{\gamma-1}} \right)^{\frac{\gamma-1}{\gamma}}}{\partial \ln p_j}-\frac{1}{\theta-1} \frac{\partial  \ln (P_d^{\frac{\theta}{\theta-1}}+1)^{\frac{\theta-1}{\theta}}}{\partial \ln p_j} \\
%&=& \frac{1}{\gamma-1} -\frac{1}{\gamma} \frac{\partial  \ln \left(\sum_{j=1}^N P_j^{\frac{\gamma}{\gamma-1}} \right)}{\partial \ln p_j} +\frac{\gamma-1}{\gamma}\frac{1}{\theta-1} \frac{\partial  \ln \left(\sum_{j=1}^N P_j^{\frac{\gamma}{\gamma-1}} \right)}{\partial \ln p_j}-\frac{1}{\theta} \frac{\partial  \ln (P_d^{\frac{\theta}{\theta-1}}+1)}{\partial \ln p_j} \\
%\end{eqnarray*}

Now lets suppose that consumers are now aggregated under an insurance company, who's objective is to minimize expenditure, given by $\sum_{j=1}^N Q^d(\gamma_1+\gamma_2 p_j)[\gamma_1+(1-\gamma_2)p_j]$, where $\gamma \in(0,1)$ is the coinsurance rate and they negotiate prices directly with the pharmaceutical companies.

\section{No competition on the pharmaceutical company side}
If there is no competition on the pharmaceutical companies side, there isn't really anything the insurance company can do (i.e. their outside option is just to suck it up!).

Now, what does change is that consumers become less price elastic. To see this notice that with insurance an $1\%$ increase in prices, translates into $\frac{\gamma_2 p_j}{\gamma_1+\gamma_2p_j}\%<1\%$ increase in the priced paid by consumers, which means that the price elasticity of demand becomes approximately $-\eta \frac{\gamma_1+\gamma_2p_j}{\gamma_2 p_j}$, which means the price set by the pharmaceutical firm increases as the consumers become less price elastic.

\section{Competition on the pharmaceutical company side}

In this case we can think of this as a three person bargaining game, where the insurance company deals with each pharmaceutical lab separately. If they reach an agreement then the price is set and consumers demand whatever they want. If they do not reach an agreement the pharmaceutical lab just sets the price equal to the monopoly price taking into account the mark up.

So I guess this looks like its going nowhere... but lets see... lets assume the company makes no deal with company 2 and is making a deal with company 1...

Then company 1 

%s.t. to providing a minimal level of aggregate level of pharmaceuticals, $i.e. \left(\sum_{j=1}^N  \left(Q^d(c p_j) \right) ^\gamma \right)^{\frac{1}{\gamma}} =\overline{D}$, where $c$ is the coinsurance rate. Formally, the insurance company's problem is:
%
%\begin{eqnarray*}
%\min_{Q_1,...,Q_N} \sum_{j=1}^N Q^d(c p_j) (1-c)p_j
%\end{eqnarray*}
%
%s.t.
%
%$$\left(\sum_{j=1}^N  \left(Q^d(c p_j) \right) ^\gamma \right)^{\frac{1}{\gamma}} =\overline{D}$$
%
%


 




\end{document}


