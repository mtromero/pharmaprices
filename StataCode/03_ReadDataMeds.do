/*
import delimited "$basein/POS/MedicamentosPOS_2016_ResolucionAResolucion.csv", clear 
replace medcodigo=substr(medcodigo,1,7)
collapse (max) medres008 medres029 medres5521 medres5926 medres5592, by(medcodigo)
rename medres008 pos2010
rename medres029 pos2012
rename medres5521 pos2014
rename medres5926 pos2015
rename medres5592 pos2016
rename medcodigo ATC
compress
save "$base_out/MedicamentosPOS_2016_ResolucionAResolucion.dta", replace

import delimited "$basein/POS/2011-AC 29.csv", clear varnames(1)
rename codigo_atc ATC
keep ATC
replace ATC=strltrim(strrtrim(ATC))
replace ATC=ustrtrim(ATC)
replace ATC= trim(ATC)
replace ATC= stritrim(ATC)
bys ATC:gen n=_n
drop if n>1
drop n
merge 1:1 ATC using "$base_out/MedicamentosPOS_2016_ResolucionAResolucion.dta"
replace pos2012=1 if pos2012==0 & _merge==3
drop _merge
save "$base_out/MedicamentosPOS_2016_ResolucionAResolucion.dta", replace

import delimited "$basein/POS/2013-RES 5521.csv", clear varnames(1) bindquote(strict)
rename codigo_atc ATC
keep ATC
replace ATC=substr(ATC,1,7)
bys ATC:gen n=_n
drop if n>1
drop n
merge 1:1 ATC using "$base_out/MedicamentosPOS_2016_ResolucionAResolucion.dta"
replace pos2014=1 if pos2014==0 & _merge==3
drop _merge
save "$base_out/MedicamentosPOS_2016_ResolucionAResolucion.dta", replace
merge m:1 ATC using "$base_out/ATC_Multi.dta"
drop if _merge==2
drop _merge 
save "$base_out/MedicamentosPOS_2016_ResolucionAResolucion.dta", replace

gen nchar=strlen(ATC)
preserve
keep if nchar==7
drop nchar
bys id_combinado: egen pos2010a=max(pos2010) if id_combinado!=.
bys id_combinado: egen pos2012a=max(pos2012) if id_combinado!=.
bys id_combinado: egen pos2014a=max(pos2014) if id_combinado!=.
bys id_combinado: egen pos2015a=max(pos2015) if id_combinado!=.
bys id_combinado: egen pos2016a=max(pos2016) if id_combinado!=.
replace pos2010=pos2010a if pos2010a!=.
replace pos2012=pos2012a if pos2012a!=.
replace pos2014=pos2014a if pos2014a!=.
replace pos2015=pos2015a if pos2015a!=.
replace pos2016=pos2016a if pos2016a!=.
drop id_combinado-pos2016a
save "$base_out/MedicamentosPOS_FULLATC.dta", replace
restore
preserve
keep if nchar==5
drop nchar
foreach var of varlist pos2010-pos2016{
	replace `var'=. if `var'==0
}
save "$base_out/MedicamentosPOS_ATC5.dta", replace
restore
preserve
keep if nchar==4
drop nchar
foreach var of varlist pos2010-pos2016{
	replace `var'=. if `var'==0
}
save "$base_out/MedicamentosPOS_ATC4.dta", replace
restore


import delimited "$base_out/DataGenerico_Final.csv", clear case(preserve)
drop if CUM1=="NA"
sort CUM1
drop in 26260
replace Generico="" if Generico=="NA"
replace Franja="" if Franja=="SIN ESPECIFICAR"
replace Inserto="" if Inserto=="NA"
replace IDRegistro="" if IDRegistro=="NA"
destring, replace
drop NombreRegistro
save "$base_out/DataGenerico_Final.dta" ,replace

*/

use "$base_out/Medicamentos_Total.dta" ,clear
replace VMR=min(VMR+PMV,1)
replace Institucional_Lab_Prom_STD=. if MediID=="19939440 1" /*only two data points, 2007 and 2009, and they are exactly the same... dont trust it*/
replace Institucional_Lab_Unidades_STD=. if MediID=="19939440 1" /*only two data points, 2007 and 2009, and they are exactly the same... dont trust it*/
*drop some pharma that are crappy crappy info
#delimit ;
foreach cum in
4098
32667
38884
43910
44405
48062
48618
50423
57641
218019
219277
19905554
19927114
19931051
19935853
19936001
19941379
19949649
19950100
19965075
19966780
19969030
19989121
20006590
20009441
{;
replace Institucional_Lab_Prom_STD=. if CUM1==`cum';
replace Institucional_Lab_Unidades_STD=. if CUM1==`cum';
};
#delimit cr



foreach canal in  Lab May  {
	foreach tipo in  Comercial Institucional  {
		capture drop diff
		capture drop tipo1
		capture drop tipo2
		capture drop tipo3
		capture drop tipo4
		gen diff=(`tipo'_`canal'_Max-`tipo'_`canal'_Min)/`tipo'_`canal'_Min
		qui sum diff,d
		gen tipo1=(diff>r(p99) & !missing(diff))
		capture drop diff
		gen diff=(`tipo'_`canal'_Max-`tipo'_`canal'_Min)
		qui sum diff,d
		gen tipo2=(diff>r(p99) & !missing(diff))
		gen tipo3=(((Institucional_Lab_Max_STD+0.1)<Institucional_Lab_Prom_STD | (Institucional_Lab_Min_STD-0.1)>Institucional_Lab_Prom_STD) & !missing(Institucional_Lab_Prom_STD))
		gen tipo4=(mod(Institucional_Lab_Unidades,1)!=0 & Institucional_Lab_Unidades!=.)
		replace `tipo'_`canal'_Prom_STD=. if tipo1==1 | tipo2==1 | tipo3==1 | tipo4==1
		replace `tipo'_`canal'_Unidades_STD=. if tipo1==1 | tipo2==1 | tipo3==1 | tipo4==1
	}
}



drop Comercial* Compra* Institucional_Lab_Min_STD Institucional_May_Min_STD Institucional_Lab_Max_STD Institucional_May_Max_STD Institucional_Lab_Min Institucional_Lab_Max Institucional_Lab_Unidades Institucional_Lab_Prom Institucional_May_Min Institucional_May_Max Institucional_May_Unidades Institucional_May_Prom

gen ImportarModalidad=0
replace ImportarModalidad=1 if MODALIDAD=="IMPORTAR Y VENDER" | MODALIDAD=="IMPORTAR, EMPACAR Y VENDER"  | MODALIDAD=="IMPORTAR, ENVASAR Y VENDER" | MODALIDAD=="IMPORTAR, FABRICAR Y VENDER" | MODALIDAD=="IMPORTAR, SEMIELABORAR Y VENDER"

gen FECHA_EXPEDICION2 = date(FECHA_EXPEDICION, "YMD")
format FECHA_EXPEDICION2 %td
gen FECHA_VENCIMIENTO2 = date(FECHA_VENCIMIENTO, "YMD")
format FECHA_VENCIMIENTO2 %td		

gen FECHA_ACTIVO2 = date(FECHA_ACTIVO, "YMD")
format FECHA_ACTIVO2 %td
gen FECHA_INACTIVO2 = date(FECHA_INACTIVO, "YMD")
format FECHA_INACTIVO2 %td	
 

gen AnosExpedicion=Periodo-year(FECHA_EXPEDICION2)
gen YearExpedicion=year(FECHA_EXPEDICION2)
gen YearVencimiento=year(FECHA_VENCIMIENTO2)
gen YearActivo=year(FECHA_ACTIVO2)
gen YearInactivo=year(FECHA_INACTIVO2)
*drop if Periodo<max(YearExpedicion,YearActivo) & Institucional_Lab_Prom_STD==.
*drop if Periodo>min(YearVencimiento,YearInactivo)
compress
xtset MediID2 Periodo, yearly

replace ATC0="" if ATC0=="NA"
replace ATC1="" if ATC1=="NA"
replace ATC2="" if ATC2=="NA"
replace ATC3="" if ATC3=="NA"
drop if ATC0==""

replace DESCRIPCION_ATC0="" if DESCRIPCION_ATC0=="NA"
replace DESCRIPCION_ATC1="" if DESCRIPCION_ATC1=="NA"
replace DESCRIPCION_ATC2="" if DESCRIPCION_ATC2=="NA"
replace DESCRIPCION_ATC3="" if DESCRIPCION_ATC3=="NA"

replace PRINCIPIO_ACTIVO0="" if PRINCIPIO_ACTIVO0=="NA"
replace PRINCIPIO_ACTIVO1="" if PRINCIPIO_ACTIVO1=="NA"
replace PRINCIPIO_ACTIVO2="" if PRINCIPIO_ACTIVO2=="NA"
replace PRINCIPIO_ACTIVO3="" if PRINCIPIO_ACTIVO3=="NA"


save "$base_out/Medicamentos_TotalFill.dta" ,replace

preserve
keep MediID2 CUM1 Periodo
sort MediID2 CUM1 Periodo
quietly by MediID2 CUM1 Periodo:  gen dup = cond(_N==1,0,_n)
drop if dup>1
collapse (count) Presentations=MediID2, by(CUM1 Periodo)
label var Presentations "No. of pharmaceutical presentation"
save "$base_out/PresentationsCUM1.dta" ,replace
restore



sort CUM1 Periodo

collapse (firstnm)  ATC1 DESCRIPCION_ATC1 PRINCIPIO_ACTIVO1 ATC2 DESCRIPCION_ATC2 PRINCIPIO_ACTIVO2 ATC3 DESCRIPCION_ATC3 PRINCIPIO_ACTIVO3  ATC0 DESCRIPCION_ATC0 PRINCIPIO_ACTIVO0 (max) AnosExpedicion ImportarModalidad POS VMR YearExpedicion YearVencimiento YearActivo YearInactivo, by(CUM1 Periodo)
foreach var of varlist ATC1 DESCRIPCION_ATC1 PRINCIPIO_ACTIVO1 ATC2 DESCRIPCION_ATC2 PRINCIPIO_ACTIVO2 ATC3 DESCRIPCION_ATC3 PRINCIPIO_ACTIVO3  ATC0 DESCRIPCION_ATC0 PRINCIPIO_ACTIVO0{
by CUM1: egen `var'_mode=  mode(`var'), maxmode
replace `var'= `var'_mode
drop `var'_mode
}


xtset CUM1 Periodo, yearly
tsfill
xfill ATC1 DESCRIPCION_ATC1 PRINCIPIO_ACTIVO1 ATC2 DESCRIPCION_ATC2 PRINCIPIO_ACTIVO2 ATC3 DESCRIPCION_ATC3 PRINCIPIO_ACTIVO3  ATC0 DESCRIPCION_ATC0 PRINCIPIO_ACTIVO0  ImportarModalidad  YearExpedicion YearVencimiento, i(CUM1)
replace  AnosExpedicion=Periodo-YearExpedicion if AnosExpedicion==.

sort CUM1 Periodo
by CUM1: gen cont=_n 
replace POS=0 if POS!=0 & POS!=1 & !missing(POS)


replace POS=1 if CUM1==19902206 & Periodo==2011 /*This is wrong in the data*/
*Get rid of sandwich situations
by CUM1: replace POS=1 if POS==0 & POS[_n-1]==1 & POS[_n+1]==1 & !missing(POS[_n+1]) & !missing(POS[_n-1])
by CUM1: replace POS=0 if POS==1 & POS[_n-1]==0 & POS[_n+1]==0 & !missing(POS[_n+1]) & !missing(POS[_n-1])

by CUM1: replace POS=1 if POS==0 & POS[_n-2]==1 & POS[_n+2]==1 & !missing(POS[_n+2]) & !missing(POS[_n-2])
by CUM1: replace POS=0 if POS==1 & POS[_n-2]==0 & POS[_n+2]==0 & !missing(POS[_n+2]) & !missing(POS[_n-2])

by CUM1: replace POS=1 if POS==0 & POS[_n-1]==1 & POS[_n+1]==1 & !missing(POS[_n+1]) & !missing(POS[_n-1])
by CUM1: replace POS=0 if POS==1 & POS[_n-1]==0 & POS[_n+1]==0 & !missing(POS[_n+1]) & !missing(POS[_n-1])

by CUM1: replace POS=1 if POS==0 & POS[_n-3]==1 & POS[_n+3]==1 & !missing(POS[_n+3]) & !missing(POS[_n-3])
by CUM1: replace POS=0 if POS==1 & POS[_n-3]==0 & POS[_n+3]==0 & !missing(POS[_n+3]) & !missing(POS[_n-3])

by CUM1: replace POS=1 if POS==0 & POS[_n-2]==1 & POS[_n+2]==1 & !missing(POS[_n+2]) & !missing(POS[_n-2])
by CUM1: replace POS=0 if POS==1 & POS[_n-2]==0 & POS[_n+2]==0 & !missing(POS[_n+2]) & !missing(POS[_n-2])

by CUM1: replace POS=1 if POS==0 & POS[_n-1]==1 & POS[_n+1]==1 & !missing(POS[_n+1]) & !missing(POS[_n-1])
by CUM1: replace POS=0 if POS==1 & POS[_n-1]==0 & POS[_n+1]==0 & !missing(POS[_n+1]) & !missing(POS[_n-1])


by CUM1: ipolate POS cont,  generate(POS2)
*At this point only missing at beggining at end... I think its safe to assume that POS status was the same before first obs and after last obs
sort CUM1 Periodo
by CUM1: replace POS2 = POS2[_n-1] if missing(POS2)
gsort +CUM1 -Periodo
by CUM1: replace POS2 = POS2[_n-1] if missing(POS2)
sort CUM1 Periodo

*Bro the weird cases
*bro CUM1 Periodo POS POS2 if POS2!=0 & POS2!=1 


gen atc=ATC0
merge m:1 atc using "$base_out/POS/OutIn2010.dta"
replace POS2=0 if _merge==3 & Periodo>=2010
drop if _merge==2
drop _merge

merge m:1 atc using "$base_out/POS/InAfter2008.dta"
replace POS2=0 if _merge==3 & Periodo<2008
drop if _merge==2
drop _merge

/*
merge m:1 atc using "$base_out/POS/InAfter2010.dta"
replace POS2=0 if _merge==3 & Periodo<2010
drop if _merge==2
drop _merge
*/

merge m:1 atc using "$base_out/POS/InAfter2012.dta"
replace POS2=0 if _merge==3 & Periodo<2012
drop if _merge==2
drop _merge

merge m:1 atc using "$base_out/POS/OutAfter2014.dta"
replace POS2=0 if _merge==3 & Periodo>=2014
drop if _merge==2
drop _merge

merge m:1 atc using "$base_out/POS/InAfter2014.dta"
replace POS2=0 if _merge==3 & Periodo<2014
drop if _merge==2
drop _merge

merge m:1 atc using "$base_out/POS/InAfter2014_AllATC.dta"
replace POS2=1 if _merge==3 & Periodo>=2014
drop if _merge==2
drop _merge

merge m:1 atc using "$base_out/POS/InAfter2015_AllATC.dta"
replace POS2=1 if _merge==3 & Periodo>=2015
drop if _merge==2
drop _merge

merge m:1 atc using "$base_out/POS/InAfter2016.dta"
replace POS2=0 if _merge==3 & Periodo<2016
drop if _merge==2
drop _merge

merge m:1 atc using "$base_out/POS/InAfter2016_AllATC.dta"
replace POS2=1 if _merge==3 & Periodo>=2016
drop if _merge==2
drop _merge

gen atc5=substr(atc,1,5)


merge m:1 atc5 using "$base_out/POS/InAfter2016_AllATC_5.dta"
replace POS2=1 if _merge==3 & Periodo>=2016
drop if _merge==2
drop _merge

gen atc4=substr(atc,1,4)

merge m:1 atc4 using "$base_out/POS/InAfter2016_AllATC_4.dta"
replace POS2=1 if _merge==3 & Periodo>=2016
drop if _merge==2
drop _merge

drop atc atc4 atc5
 


*Esta concentracion solo esta desde 2014 apra A02AF02*
replace POS2=0 if ATC0=="A02AF02"  /*A02AF02 never in POS*/
*Esta concentracion solo esta desde 2014 apra A02BA02*
replace POS2=0 if CUM1==19984331 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=0 if CUM1==19984331 & Periodo==2013 
*OMEPRAZOL*
replace POS2=0 if CUM1==46415 & Periodo==2009 & POS2!=0 & POS2!=1 
replace POS2=0 if CUM1==46415 & Periodo==2010 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==224202 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==224202 & Periodo==2013 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==224202 & Periodo==2014 & POS2!=0 & POS2!=1 
replace POS2=0 if CUM1==224202 & Periodo==2011 & POS2!=0 & POS2!=1 
replace POS2=0 if CUM1==224202 & Periodo==2010 & POS2!=0 & POS2!=1 
replace POS2=0 if CUM1==224202 & Periodo==2009 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==20017191 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==20021271 & Periodo==2012 & POS2!=0 & POS2!=1 
*A04AA01 in 2012*
replace POS2=1 if CUM1==19965360 & Periodo==2012 & POS2!=0 & POS2!=1   



replace POS2=0 if CUM1==45695 & Periodo==2010 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==56399 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19912566 & Periodo==2011 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19948710 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19960161 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19960161 & Periodo==2013 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19963605 & Periodo==2012 & POS2!=0 & POS2!=1 

replace POS2=1 if CUM1==19968458 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19973739 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19973739 & Periodo==2013 & POS2!=0 & POS2!=1 

replace POS2=1 if CUM1==19980483 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19983877  & POS2!=0 & POS2!=1 



replace POS2=1 if CUM1==19984832 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19987303 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==20004850 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==20004850 & Periodo==2013 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==20005175 & Periodo==2012 & POS2!=0 & POS2!=1 

replace POS2=1 if CUM1==20029565 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==20029565 & Periodo==2013 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==6326 & Periodo==2014 & POS2!=0 & POS2!=1 



replace POS2=0 if CUM1==1980809 & Periodo==2010 & POS2!=0 & POS2!=1 
replace POS2=0 if CUM1==1980809 & Periodo==2009 & POS2!=0 & POS2!=1 

replace POS2=1 if CUM1==19909838 & Periodo==2011 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19909838 & Periodo==2010 & POS2!=0 & POS2!=1 
replace POS2=0 if CUM1==19909838 & Periodo==2009 & POS2!=0 & POS2!=1 
replace POS2=0 if CUM1==19909838 & Periodo==2008 & POS2!=0 & POS2!=1 

replace POS2=1 if CUM1==19912414 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19912414 & Periodo==2013 & POS2!=0 & POS2!=1 

replace POS2=1 if CUM1==19927358 & Periodo==2012 & POS2!=0 & POS2!=1 

replace POS2=1 if CUM1==19930470 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19930470 & Periodo==2013 & POS2!=0 & POS2!=1 

replace POS2=1 if CUM1==19932974 & Periodo==2012 & POS2!=0 & POS2!=1 

replace POS2=0 if CUM1==19993030 & Periodo==2008 & POS2!=0 & POS2!=1 
replace POS2=0 if CUM1==19993030 & Periodo==2009 & POS2!=0 & POS2!=1 


replace POS2=1 if CUM1==19908725 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19908725 & Periodo==2013 & POS2!=0 & POS2!=1 


replace POS2=1 if CUM1==19924026  & POS2!=0 & POS2!=1 
replace POS2=0 if CUM1==6326 & POS2!=0 & POS2!=1 

replace POS2=1 if CUM1==19911014 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19911014 & Periodo==2013 & POS2!=0 & POS2!=1 

replace POS2=0 if CUM1==19945079 & Periodo==2013 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19945079 & Periodo==2014 & POS2!=0 & POS2!=1 

replace POS2=1 if CUM1==20027772 & Periodo>=2012 & POS2!=0 & POS2!=1 

replace POS2=0 if CUM1==19905591 & Periodo<=2011  
replace POS2=1 if CUM1==19905591 & Periodo>=2012 
replace POS2=0 if CUM1==201951 

replace POS2=1 if CUM1==228406 & Periodo==2012  /* TIROFIBAN was in 2012 */
replace POS2=1 if CUM1==19949247  /* FOSAMPRENAVIR tablets 700mg has been in the POS since at least 2006*/

replace POS2=0 if CUM1==30968 & Periodo<2010  /* H03BA02 was in 2010 only for 50 mg tablets */
replace POS2=1 if CUM1==30968 & Periodo>=2010  /* H03BA02 was in 2010 only for 50 mg tablets */

replace POS2=0 if CUM1==19952712 & Periodo<=2014  /* H03BA02 was in 2014 for all concentrations */
replace POS2=1 if CUM1==19952712 & Periodo>=2014  /* H03BA02 was in 2010 only for 50 mg tablets */


replace POS2=0 if ATC0=="G03GB02"  /*CLOMIFENO no esta en el POS*/
replace POS2=1 if ATC0=="V03AE02"  /*SEVELAMER ha estado en el POS desde 2006*/

replace POS2=1 if ATC0=="N03AX11" & Periodo>=2014  /*Topiramato ha estado en el POS desde 2014*/
replace POS2=1 if ATC0=="G03BA03"  /*TESTOSTERONA ÉSTER ha estado en el POS desde 2002*/
*replace POS2=0 if ATC0=="G03BA03" & Periodo<2012  /*TESTOSTERONA ÉSTER ha estado en el POS desde 2002*/

replace POS2=1 if ATC0=="L01XE01" & Periodo>=2014  /*L01XE01 -imatinib ha estado en el POS desde 2014*/
replace POS2=0 if ATC0=="L01XE01" & Periodo<2012  /*L01XE01 -imatinib no ha estado en el POS antes de 2012*/



replace POS2=0 if ATC0=="A02BC05" & Periodo<2012  /*A02BC05 only after 2012  in POS*/
replace POS2=0 if ATC0=="B01AB05"  /*B01AB05 never in POS*/
replace POS2=0 if ATC0=="B01AC04" & Periodo<2012  /*B01AC04 only after 2012  in POS*/
*replace POS2=0 if ATC0=="C01BD01" & Periodo<2010  /*C01BD01 only after 2003  in POS*/
replace POS2=0 if ATC0=="C07AG02" & Periodo<2012  /*C07AG02 only after 2012  in POS*/
replace POS2=0 if ATC0=="C10AA05" & Periodo<2012  /*C10AA05 only after 2012  in POS*/
replace POS2=0 if ATC0=="G03FA01" & Periodo<2012  /*G03FA01 only after 2012  in POS*/
replace POS2=0 if ATC0=="G03FA11"  /*G03FA11 never in POS*/
*replace POS2=0 if ATC0=="J01DB01" & Periodo<2012  /*J01DB01 only after 2003  in POS*/

replace POS2=0 if ATC0=="J01DD54"  /*G03FA11 never in POS*/
*replace POS2=0 if ATC0=="J01XA01" & Periodo<2012  /*J01XA01 only after 2003  in POS*/
replace POS2=0 if ATC0=="J02AX04" & Periodo<2012  /*J01XA01 only after 2012  in POS*/
replace POS2=0 if ATC0=="N01BB51" & Periodo<2012  /*N01BB51 only after 2012  in POS*/
*replace POS2=0 if ATC0=="N02AB03" & Periodo<2012  /*N01BB51 only after 2003  in POS*/

*replace POS2=0 if ATC0=="N03AE01" & Periodo<2012  /*N03AE01 only after 2003  in POS*/
*replace POS2=0 if ATC0=="N06AB03" & Periodo<2012  /*N03AE01 only after 2003  in POS*/
*replace POS2=0 if ATC0=="P01AB01" & Periodo<2012  /*N03AE01 only after 2003  in POS*/
*replace POS2=0 if ATC0=="S01BA07" & Periodo<2012  /*N03AE01 only after 2003  in POS*/

replace POS2=0 if ATC0=="M05BA04" & Periodo<2011  /*M05BA04 only after 2011  in POS*/
replace POS2=0 if ATC0=="B01AC04" & Periodo<2011  /*B01AC04 only after 2011  in POS*/


replace POS2=1 if CUM1==49257  /* J01XA01 was in since ever only for 500 mg */
replace POS2=1 if CUM1==19936619  /* J01XA01 was in since ever only for 500 mg */
replace POS2=1 if CUM1==19908147  /* N02AB03 was in since ever only for 500 mg */
replace POS2=1 if CUM1==38369  /* N06AB03 was in since ever only for 500 mg */
replace POS2=1 if CUM1==45999  /* P01AB01 was in since ever only for 500 mg */
replace POS2=1 if CUM1==19913115  /* S01BA07 was in since ever only for 500 mg */

replace POS2=1 if ATC0=="A02BC05" & Periodo>=2012  /*A02BC05 -in almost all presentations en el POS since 2012*/
replace POS2=0 if ATC0=="A02BC05" & Periodo<2012  /*A02BC05 -in almost all presentations en el POS since 2012*/
replace POS2=0 if CUM1==19995986  /*A02BC05 -in almost all presentations en el POS since 2012*/
replace POS2=0 if CUM1==20019161  /*A02BC05 -in almost all presentations en el POS since 2012*/
replace POS2=0 if CUM1==20035544  /*A02BC05 -in almost all presentations en el POS since 2012*/

*****OMEPRAZOL**
*20 MG ALWAYS
*40 MG SINCE 2012 (POLVO AND TABLET)
*ALL FOR TABLES SINCE 2014, AND 40 MG POLVO
#delimit ;
foreach num in 13165
16478
36637
37472
37640
38478
39065
39118
40432
41072
41649
42719
46415
47006
47768
47921
48011
53101
53143
55121
55501
56997
57376
59681
201035
201036
210735
211848
225507
226459
227534
19905098
19905581
19908310
19911013
19911229
19924512
19924807
19928498
19929991
19934404
19935328
19940971
19941718
19942064
19942717
19947045
19948144
19963176
19969813
19978829
19980102
19982841
19983110
19994903
20006984
20019785
20020711
20024410
20028020
20034537
20035335
20035947
20037876
20044134
20046748
20056433
20066115
20066115
20070604
19915411
19934313
19940078
19941786
19944920
19947032
19947323
19949604
19950313
19950315
19959965
19960407
19969630
19978880
19981886
19983474
19991568
20002764
20022925
20028539
20030113
20031311
20037098
20047462
20049858
20060286
20066111
20066117
20072422
{;
replace POS2=1 if CUM1==`num'  /* OMEPRAZOL de 20mg awalys*/;
};
#delimit cr

*polvo 40mg tambien
#delimit ;
foreach num in 38408
40026
59096
81153
207108
217312
226148
19915587
19922566
19925924
19931498
19947748
19960807
19961611
19963377
19968252
19982840
19989805
19990487
20002175
20007749
20012478
20017191
20021271
20050208
20062114
20066112
20070385
38408
50902
81153
19915587
19922566
19925924
19927187
19942823
19947748
19950089
19953406
19960807
19961611
19963377
19976385
19976877
19983523
19989805
19990487
19993804
20002175
20007749
20019878
20044344
20062114
20070385
20102222


{;
replace POS2=1 if CUM1==`num' & Periodo>=2012  /* OMEPRAZOL de 40mg since 2012 */;
replace POS2=0 if CUM1==`num' & Periodo<2012  /* OMEPRAZOL de 40mg since 2012 */;
};
#delimit cr

#delimit ;
foreach num in 200085
207085
227660
19948264
19955826
19980160
20006985
20014936
20028017
20044178
20055575
20070603{;
replace POS2=1 if CUM1==`num' & Periodo>=2014  /* OMEPRAZOL de 10mg since 2012 */;
replace POS2=0 if CUM1==`num' & Periodo<2014  /* OMEPRAZOL de 10mg since 2012 */;
};
#delimit cr
*post 2016 all forms
replace POS2=1 if ATC0=="A02BC01" & Periodo>=2016


*A04AA01
*For A04AA01 tablets of 8 mg since ever, and everything since 2014
#delimit ;
foreach num in 43722
46652
50525
229365
1980701
19968954
19972804
19989789
20054581{;
replace POS2=1 if CUM1==`num'  /* A04AA01  8mg since ever */;
};
#delimit cr

#delimit ;
foreach num in 19934567
19984840
20006120
46649
48078
200366
212743
229364
1983638
19972806
19992595
20054580
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014  /* A04AA01 everything else 2014 */;
replace POS2=0 if CUM1==`num' & Periodo<2014  /* A04AA01 everything else 2014 */;
};
#delimit cr
*las de inyeccion, solo 0.02% desde 2012, everything else since 2016
#delimit ;
foreach num in 
28203
28206
39415
50419
50524
50810
19923958
19924110
19931422
19938378
19946615
19956236
19957347
19960797
19963887
19965360
19965361
19968463
19968925
19968955
19970172
19989706
19989791
19992714
19992716
19994120
20001243
20010204
20010206
20012479
20012482
20014775
20019154
20046596
20062820
20065058
20068642
20077611
20081765
20095751
20097237
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007  /* A04AA01 everything else 2014 */;
replace POS2=0 if CUM1==`num' & Periodo<2007  /* A04AA01 everything else 2014 */;
};
#delimit cr

*tecnicamente otras formas de 0.02% solo in after 2014
#delimit ;
foreach num in 
200693
19965361
19992714
20010206
20012482
28203
50524
19960797
19968953
19989706
20019154
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014  /* A04AA01 everything else 2014 */;
replace POS2=0 if CUM1==`num' & Periodo<2014  /* A04AA01 everything else 2014 */;
};
#delimit cr

replace POS2=1 if ATC0=="A04AA01" & Periodo>=2016

replace POS2=0 if CUM1==19950478 & Periodo==2013

**B01AC04 75mg since 2012, everything since 2014

#delimit ;
foreach num in 227428
230389
19925585
19925979
19929048
19931746
19933487
19942021
19947545
19959943
19963649
19971140
19976641
19980931
19983450
19985119
19991671
19992951
19994402
19996538
20009605
20010508
20011353
20017856
20018072
20018281
20022454
20022950
20032042
20038667
20044924
20050486
20054456
20056052{;
replace POS2=1 if CUM1==`num' & Periodo>=2012  /* A04AA01 everything else 2014 */;
replace POS2=0 if CUM1==`num' & Periodo<2012  /* A04AA01 everything else 2014 */;
};
#delimit cr
replace POS2=1 if ATC0=="B01AC04" & Periodo>=2014  /*A02BC05 -in almost all presentations en el POS since 2012*/

replace POS2=1 if CUM1==33103 & Periodo>=2012  /* B01AD02 50mg since 2012  */
replace POS2=0 if CUM1==33103 & Periodo<2012  /* B01AD02 50mg since 2012  */
replace POS2=0 if CUM1==20007880  /* B01AD02 only 50mg since 2012  */

replace POS2=1 if ATC0=="C01CA24"  /*C01CA24 -in almost all presentations en el POS since ever*/

replace POS2=1 if CUM1==19950159 & Periodo>=2012  /* C01DA02 5mg since 2012  */


   

** C10AA05 or Atrovastina**
*10, 20 40mg since 2012
*everything since 2014
replace POS2=1 if ATC0=="C10AA05" & Periodo>=2014  /*C10AA05 - all presentations en el POS since 2014*/
replace POS2=0 if ATC0=="C10AA05" & Periodo<2012  /*C10AA05 - all presentations en el POS since 2014*/

#delimit ;
foreach num in 212446
19901981
19902069
19904879
19904881
19908238
19908677
19908834
19908835
19910917
19912864
19913752
19913753
19914375
19920431
19924521
19924606
19925209
19925222
19927013
19928457
19931419
19940844
19942638
19944539
19947007
19950622
19952033
19956987
19959638
19960068
19960072
19963163
19963282
19963809
19965791
19966943
19974880
19976842
19981162
19993409
19996344
19996345
19996546
19999370
20007636
20010199
20013223
20013867
20019969
20042523
20046665
20048131
20049371
212444
19902043
19902070
19904878
19904880
19908144
19912604
19913750
19913751
19914410
19922200
19924419
19924607
19926738
19927012
19927115
19928399
19931420
19940847
19943098
19947197
19950621
19952201
19956683
19956986
19959637
19962941
19963283
19963352
19963810
19965792
19966944
19967646
19974881
19991704
19995961
19996037
19996671
20010201
20020051
20022893
20027007
20028018
20042530
20046645
20049338
20052555
20061509
20062132
19919867
19931616
19935477
19936819
19959379
19959380
19962943
19964318
19977684
19981162
19996989
20007636
20008074
20020184
20025513
20029999
20042529
20043993
20049370
20052556
20061508
20067981
20061510{;
replace POS2=1 if CUM1==`num' & Periodo>=2012  /* C10AA05 some since 2012 */;
replace POS2=0 if CUM1==`num' & Periodo<2012  /* C10AA05 some since 2012 */;
};
#delimit cr

**J01DC02
* 500 mg since 2012 in tablet, everything else since 2014
*solucion 750mg o 5%
*I'll do tablets here


#delimit ;
foreach num in
44672
48034
19907610
19932353
20006926
20018332
20027868{;
replace POS2=1 if CUM1==`num' & Periodo>=2012  /* J01DC02 500mg  since 2012 */;
replace POS2=0 if CUM1==`num' & Periodo<2012  /* J01DC02 500mg  since 2012 */;
};
#delimit cr

#delimit ;
foreach num in
32970
43330
43331
46120
48033
19907609
19932352{;
replace POS2=1 if CUM1==`num' & Periodo>=2014  /* J01DC02 other  since 2012 */;
replace POS2=0 if CUM1==`num' & Periodo<2014  /* J01DC02 other since 2012 */;
};
#delimit cr


*AZITROMICINA or J01FA10
*im gonna do tablets here
*1g y 500g since 2012
*everything since 2014
   
#delimit ;
foreach num in
20030091
6411
11700
22784
51565
52784
55143
58021
59081
59992
205259
210571
228128
228739
228989
1980075
19907157
19908252
19908293
19911014
19912736
19922005
19928707
19940970
19943535
19947188
19952933
19961932
19969831
19970620
19970621
19982579
19983708
19992864
19992902
19997001
19999693
20004784
20005038
20007886
20013670
20020694
20024100
20026011
20027262
20028788
20050034
20052854
20057213
20057510
20060961
20062446{;
replace POS2=1 if CUM1==`num' & Periodo>=2012  /* J01FA10 other  since 2012 */;
replace POS2=0 if CUM1==`num' & Periodo<2012  /* J01FA10 other since 2012 */;
};
#delimit cr

#delimit ;
foreach num in
56781
19909455
19952931
19970784
19972033
19979319
19980518
19992757
20006975
20010216
20010841
20022874
20025973
20026177
20057210
20068317{;
replace POS2=1 if CUM1==`num' & Periodo>=2014  /* J01FA10 other  since 2014 */;
replace POS2=0 if CUM1==`num' & Periodo<2014  /* J01FA10 other since 2014 */;
};
#delimit cr

*AZITROMICINA or J01FA10
*200mg/5 mL (4%) since 2012
#delimit ;
foreach num in
19928711
19947945
19993964
55145
210570
19922012
20026062
20067544
20048561
226728{;
replace POS2=1 if CUM1==`num' & Periodo>=2012  /* J01FA10 200mg/5 mL (4%)  since 2012 */;
replace POS2=0 if CUM1==`num' & Periodo<2012  /* J01FA10 200mg/5 mL (4%) since 2012 */;
};
#delimit cr
*esta no esta
replace POS2=0 if CUM1==19933399 



replace POS2=1 if ATC0=="J02AX04" & Periodo>=2016  /*C10AA05 - all presentations en el POS since 2014*/
replace POS2=0 if ATC0=="J02AX04" & Periodo<2012  /*C10AA05 - all presentations en el POS since 2014*/

replace POS2=1 if ATC0=="J02AX04" & Periodo>=2016  /*J02AX04 - all presentations en el POS since 2016*/
replace POS2=0 if ATC0=="J02AX04" & Periodo<2012  /*J02AX04 - none presentations before 2012*/

replace POS2=1 if CUM1==19926495 & Periodo>=2012  /* J02AX04 other  since 2012 */
replace POS2=0 if CUM1==19926495 & Periodo<2012  /* J02AX04 other  since 2012 */


*ACIDO MICOFENÓLICO OR L04AA06
/*TABLETS OF 180mg 250mg 360mg 500mg SINCE 2012*
EVERYTHING ELSE SINCE 2014 */
*bUT ESSENTIALLY EVERYTHING SINCE 2012

replace POS2=1 if ATC0=="L04AA06" & Periodo>=2012  /*L04AA06 - MOST PRESENTATIONS SINCE 2012*/
replace POS2=0 if ATC0=="L04AA06" & Periodo<2012  /*L04AA06 - none presentations before 2012*/


*NEVER SINCE 2012, ALL SINCE 2016
replace POS2=1 if ATC0=="L04AB01" & Periodo>=2016  /*L04AB01 - all presentations en el POS since 2016*/
replace POS2=0 if ATC0=="L04AB01" & Periodo<2012  /*L04AB01 - none presentations before 2012*/
*iN PARTICE EVERYTHING
replace POS2=1 if ATC0=="L04AB01" & Periodo>=2012  /*L04AB01 - all presentations en el POS since 2016*/

*SINCE 2014 EVERYTHING LEVODOPA OR N04BA02
*BEFORE THAT ONLY SOME
replace POS2=1 if ATC0=="N04BA02" & Periodo>=2014  /*N04BA02 */
replace POS2=0 if CUM1==19951169 & Periodo==2013  /*N04BA02 */

*fOR N06AB06 ALL AFTER 2014, NONE BEFORE 2012, AND ONLY ONE NOT SINCE 2012
replace POS2=1 if ATC0=="N06AB06" & Periodo>=2014  /*L04AB01 - all presentations en el POS since 2014*/
replace POS2=0 if ATC0=="N06AB06" & Periodo<2012  /*L04AB01 - NONE presentations en el POS since 2012*/
replace POS2=0 if CUM1==19924198  /*L04AB01 - NONE presentations en el POS since 2012*/

*R07AA02, NOT BEFORE 2010, AND EVERYTHING POST 2016
replace POS2=1 if ATC0=="R07AA02" & Periodo>=2016  /*R07AA02 - all presentations en el POS since 2016*/
replace POS2=0 if ATC0=="R07AA02" & Periodo<2010  /*R07AA02 - NONE presentations en el POS since 2010*/
replace POS2=1 if CUM1==44762 & Periodo==2012  /*R07AA02 - NONE presentations en el POS since 2010*/


#delimit ;
foreach num in 19919762
19939438
19939440
19954216
19962720
20004618
20007820
20008046
20010423
20010424
20010697
20011003
20017669
20017670
20018723
20019119
20019763
20021508
20026661
20036816
20043595
20043596
20067040{;
replace POS2=1 if CUM1==`num' & Periodo>=2012  /* L01XE01 de 100mg y 400mg in since 2012*/;
};
#delimit cr


replace POS2=0 if CUM1==20071108  /* GLICINA IS NOT IN THE POS*/

replace POS2=0 if CUM1==208565 & Periodo<=2011  /* H04AA01 was in since 2012  */
replace POS2=1 if CUM1==208565 & Periodo>=2012  /* H04AA01 was in since 2012*/

replace POS2=1 if CUM1==16436 & Periodo>2003  //* V03AC01 de 500mg in since 2003*/


replace POS2=1 if ATC0=="M05BA04" & Periodo>=2014  /* M05BA04 de 10-70 g in since 2011, everything since 2014*/
replace POS2=0 if ATC0=="M05BA04" & Periodo<2011  /* M05BA04 de 10-70 g in since 2011, everything since 2014*/
replace POS2=0 if ATC0=="M05BA06" & Periodo<2007  /* M05BA06 6mg since 2012*/

*eplace POS2=1 if ATC0=="N03AX11" & Periodo>=2014  /* N03AX11 since 2014*/
replace POS2=0 if ATC0=="N03AX11" & Periodo<2014  /* N03AX11 since 2014*/

replace POS2=0 if ATC0=="N06AB10" & Periodo<2014  /* N06AB10 since 2014*/
replace POS2=1 if ATC0=="N06AB10" & Periodo>=2014  /* N06AB10 since 2014*/

replace POS2=0 if ATC0=="N03AF02" & Periodo<2014  /* N03AF02 since 2014*/
replace POS2=1 if ATC0=="N03AF02" & Periodo>=2014  /* N03AF02 since 2014*/

replace POS2=0 if ATC0=="S01EE01" & Periodo<2012  /* S01EE01 all available in Colombia since 2012*/
replace POS2=1 if ATC0=="S01EE01" & Periodo>=2012  /*S01EE01 all available in Colombia since 2012*/

replace POS2=0 if ATC0=="A02BC03" & Periodo<2014  /* A02BC03 since 2014*/
replace POS2=1 if ATC0=="A02BC03" & Periodo>=2014  /* A02BC03 since 2014*/

replace POS2=0 if ATC0=="N03AX14" & Periodo<2014  /* N03AX14 since 2014*/
replace POS2=1 if ATC0=="N03AX14" & Periodo>=2014  /* N03AX14 since 2014*/

replace POS2=0 if ATC0=="C10AA07" & Periodo<2014  /* C10AA07 since 2014*/
replace POS2=1 if ATC0=="C10AA07" & Periodo>=2014  /* C10AA07 since 2014*/


*A02BC02 all tablets since 2014
#delimit ;
foreach num in
29510 203129 229958 19963586 19981211 20016414 20017291 20028093 20028094 20035776 20038763 20040751 20043396 20043401 20045869 20045974 20047996 20048286 20048287 20056874 20060740 20067510 20070163 20072818 20079038 20082525 20086137 20088777 20088779 20099585 20100275
{;
replace POS2=0 if CUM1==`num' & Periodo<2014 ;
replace POS2=1 if CUM1==`num' & Periodo>=2014 ;
};
#delimit cr


*M05BA06 6mg since 2007, 1% since 2014, others since 2016*/
replace POS2=1 if ATC0=="M05BA06" & Periodo>=2016

*6ml
#delimit ;
foreach num in
19950632 20019370 20051749
{;
replace POS2=1 if CUM1==`num' ;
};
#delimit cr

#delimit ;
foreach num in
19900496 19973920 19980481 20017944 20051750 20063627
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014 ;
replace POS2=0 if CUM1==`num' & Periodo<2014 ;
};
#delimit cr



*tableta never in
#delimit ;
foreach num in
19949502 19949853 19962040 19964087 19964402 19973249 19975924 19976348 19980677 19980812 19982056 19987809 20002773 20009309 20012684 20013162 20014712 20016295 20023271 20028766 20032147 20045216 20050185 20053250 20054841 20056216 20056721 20077544
{;
replace POS2=0 if CUM1==`num' ;
};
#delimit cr


#delimit ;
foreach num in 25489
59741
59741
227060
230608
19902347
19907681
19915322
19920496
19922928
19924081
19925430
19926000
19926398
19926795
19927641
19927709
19929085
19929087
19930610
19930612
19932910
19932913
19938104
19940724
19943211
19943248
19943325
19946117
19946612
19951067
19951160
19951408
19952032
19954220
19955393
19957085
19962576
19963012
19963419
19964384
19964385
19964386
19966665
19972261
19972262
19975845
19985898
19988798
19996539
19996982
20004786
20037212{;
replace POS2=1 if CUM1==`num' & Periodo>=2011  /* M05BA04 in since 2011, everything since 2014*/;
};
#delimit cr


replace POS2=0 if ATC0=="C10AA05" & Periodo<2012  /* C10AA05 not before 2012*/
replace POS2=1 if ATC0=="C10AA05" & Periodo>=2014  /* C10AA05 all since 2014*/



#delimit ;
foreach num in 212446
19901981
19902069
19904879
19904881
19908238
19908677
19908834
19908835
19910917
19912864
19913752
19913753
19914375
19920431
19924521
19924606
19925209
19925222
19927013
19928457
19931419
19940844
19942638
19944539
19947007
19950622
19952033
19956987
19959638
19960068
19960072
19963163
19963282
19963809
19965791
19966943
19974880
19976842
19981162
19993409
19996344
19996345
19996546
19999370
20007636
20010199
20013223
20013867
20019969
20042523
20046665
20048131
20049371
20061510
212444
19902043
19902070
19904878
19904880
19908144
19912604
19913750
19913751
19914410
19922200
19924419
19924607
19926738
19927012
19927115
19928399
19931420
19940847
19943098
19947197
19950621
19952201
19956683
19956986
19959637
19962941
19963283
19963352
19963810
19965792
19966944
19967646
19974881
19991704
19995961
19996671
20010201
20020051
20022893
20027007
20028018
20042530
20046645
20049338
20052555
20061509
20062132
19919867
19931616
19935477
19959379
19959380
19962943
19962943
19964318
19977684
19981162
19996989
20007636
20008074
20020184
20025513
20029999
20042529
20043993
20049370
20052556
20061508
20067981{;
replace POS2=1 if CUM1==`num' & Periodo>=2012 ; /* C10AA05 de 10g, 20 and y 40g in since 2012*/
};
#delimit cr





replace POS2=1 if ATC0=="N05AA02" & Periodo>=2014  /* N05AA02 all since 2014*/

/*Solucion oral 4% e inyectable 25mg/ml since 2012, tablets of 100 and 25 since ever */
#delimit ;
foreach num in 34848
19921589
19953588
19959695
19976767
19976850
19977387
36749
19905096
19919778
19941611
19943590
19953210
19953848
19905097
19918131
19940779
19953169
19953957
19973772{;
replace POS2=1 if CUM1==`num' & Periodo>=2006 ; /*N05AA02: Solucion oral 4% e inyectable 25mg/ml since 2012, tablets of 100 and 25 since ever too*/
};
#delimit cr

replace POS2=1 if ATC0=="N05AX08" & Periodo>=2014  /* N05AX08 all since 2014*/
replace POS2=0 if ATC0=="N05AX08" & Periodo<2012  /* N05AX08 some only since 2012*/
/*since 2012
1mg 	TABLETA CON O SIN  RECUBRIMIENTO QUE NO MODIFIQUE LA LIBERACIÓN DEL FÁRMACO
2mg 	TABLETA CON O SIN  RECUBRIMIENTO QUE NO MODIFIQUE LA LIBERACIÓN DEL FÁRMACO
3mg 	TABLETA CON O SIN  RECUBRIMIENTO QUE NO MODIFIQUE LA LIBERACIÓN DEL FÁRMACO
4mg 	TABLETA CON O SIN  RECUBRIMIENTO QUE NO MODIFIQUE LA LIBERACIÓN DEL FÁRMACO

25mg 	POLVO ESTÉRIL PARA INYECCIÓN
37,5mg 	POLVO ESTÉRIL PARA INYECCIÓN
50mg 	POLVO ESTÉRIL PARA INYECCIÓN
*/

#delimit ;
foreach num in 19934446
19934447
19934448
19998043
20001795
40431
48325
49090
49091
49092
51804
227664
227665
227666
19926481
19926482
19926483
19927570
19927636
19931303
19931304
19936268
19936269
19937648
19939591
19945676
19945677
19946858
19946858
19947333
19960346
19960347
19960348
19963294
19968896
19968897
19968898
19975162
19979439
19979440
19979821
19984680
19985470
19985472
19987617
19989120
19989121
19989122
19989457
19994886
19997824
20007289
20007377
20007378
20019392
20019393
20019396
20019397
20021021
20021022
20021023
20029225
20029226
20029229
20029229
20037404
20037639
20037665
20037667
20054211
20054213
20055004
20055171
20067682
40431 48325 49090 49091 49092 51804 227664 227665 227666 19908251 19926481 19926482 19926483 19927570 19927635 19927636 19931303 19931304 19931305 19936268 19936269 19937648 19939591 19945676 19945677 19946856 19946858 19947333 19960346 19960347 19960348 19963294 19968896 19968897 19968898 19975161 19975162 19979439 19979440 19979821 19984680 19985470 19985472 19987617 19989120 19989121 19989122 19989457 19994886 19997824 20007289 20007377 20007378 20019392 20019393 20019396 20019397 20021021 20021022 20021023 20029225 20029226 20029229 20037404 20037639 20037665 20037667 20054211 20054213 20055004 20055171 20067682 20079475 20079478 20086800
19934446 19934447 19934448 19998043 2000179
{;
replace POS2=0 if CUM1==`num' & Periodo<2012 ; /*N05AX08: some since 2012*/
replace POS2=1 if CUM1==`num' & Periodo>=2012 ; /*N05AX08: some since 2012*/
};
#delimit cr

replace POS2=1 if ATC0=="N05AH03" & Periodo>=2014  /* N05AH03 all since 2014*/
replace POS2=0 if ATC0=="N05AH03" & Periodo<2012  /* N05AH03 some only since 2012*/

#delimit ;
foreach num in 210771
210773
19901795
19901797
19913730
19913731
19925689
19929004
19946412
19946457
19946458
19946498
19947426
19947427
19951528
19968710
19968711
19972544
19974414
19974415
19976269
19976270
19979471
19979472
19989359
19989369
19989372
19989933
19995764
19995765
20003402
20003404
20003418
20003419
20008548
20012570
20012572
20026380
20026381
20034254
20034256
20055890
20055892{;
replace POS2=1 if CUM1==`num' & Periodo>=2012 ; /*N05AH03: some since 2012*/
};
#delimit cr

replace POS2=1 if ATC0=="A10AB04" & Periodo>=2012  /* A10AB04 all since 2012*/
replace POS2=0 if ATC0=="A10AB04" & Periodo<2012  /* A10AB04*/


replace POS2=1 if ATC0=="A10AB01" & Periodo>=2007  /* A10AB01 some since 2007.. I THINK ITS ALL OF THEM*/
*replace POS2=0 if ATC0=="A10AB01" & Periodo<2012  /* A10AB01*/

replace POS2=1 if ATC0=="C01CA03" & Periodo>=2016  /* C01CA03 some since 2012, all since 2016*/
replace POS2=0 if ATC0=="C01CA03" & Periodo<2012  /* C01CA03 not before 2012*/

#delimit ;
foreach num in 19919775
19940783
19950257
19996378
20010500
20011879
20028435
20056358
20063321
19942365{;
replace POS2=1 if CUM1==`num' & Periodo>=2012 ; /*C01CA03: some since 2012*/
};
#delimit cr

replace POS2=1 if ATC0=="N03AF02" & Periodo>=2014  /* N03AF02 all since 2014*/
replace POS2=0 if ATC0=="N03AF02" & Periodo<2014  /* N03AF02 */

#delimit ;
foreach num in 33220
19953666
20009781
20016432
20065393
35430
19924677
19927982
19928418
19938963
19939082
19956916
19959404
19964024
19967706
19968773
19969030
19972421
19984022
19986486
19993871
20043673
20043279
20038079
19942581
{;
replace POS2=1 if CUM1==`num' & Periodo>=2006 ; /*N05CD08: some since ever*/
};
#delimit cr

#delimit ;
foreach num in 19920365
103795
19940108
19963261
19964025
19969728
19970168
19972420
19986485
19989535
19993870
20001225
20078491
20043772
20029924
19945171
19942582{;
replace POS2=1 if CUM1==`num' & Periodo>=2012 ; /*N05CD08: some since 2014*/
};
#delimit cr
*CLONAZEPAM or N03AE01
*for tablet only, everything after 2014
*2.5 and 0.5  since ever
replace POS2=1 if CUM1==19920065   /* N03AE01 in this concentration since ever*/





**C09DA01 or losaratan y diureticos no antes de 2012, todas las formas post 2014, y solo algunas en la mitad
replace POS2=0 if ATC0=="C09DA01" &  Periodo<2012 
replace POS2=1 if ATC0=="C09DA01" &  Periodo>=2012
#delimit ;
foreach num in 19964725
20034708
20033870
19977867
19964725
{;
replace POS2=0 if CUM1==`num' & (Periodo==2012 | Periodo==2013) ; /*C09DA01: some since 2012*/
};
#delimit cr 

**L01DB01 has been in all forms since 2016, but only 50 since 2012 and and 10 mg since 2007

replace POS2=1 if ATC0=="L01DB01" &  Periodo>=2016
#delimit ;
foreach num in 
38447
53004
202358
214067
227260
230449
19906420
19935608
19947745
20005641
20010928
20061062
20091053
{;
replace POS2=1 if CUM1==`num' & (Periodo>=2007) ; /*L01DB01 10 mg since 2007*/
};
#delimit ;
foreach num in 
38446
53005
58174
202357
227265
1980838
19906370
19935607
19947747
19989735
20011067
20012260
20061064
20062344
20089956
{;
replace POS2=0 if CUM1==`num' & (Periodo<2012) ; /*L01DB01 50 mg since 2012*/
replace POS2=1 if CUM1==`num' & (Periodo>=2012) ; /*L01DB01 50 mg since 2012*/
};
#delimit cr 

**ACICLOVIR or J05AB01 has been in 200 mg tablets, 250mg povlo esteril, and 3% cream since 2007. in 2014 all tablets are included, in 2016 all forms are included for polvo and tableta

#delimit ;
foreach num in 
212522
19947319
19956639
6329
16623
21309
22739
29744
33815
36639
36900
38138
38242
39027
40368
42870
47418
53077
56356
209835
217340
219094
1980643
19900585
19904990
19907357
19907891
19924189
19931129
19941385
19943984
19955443
19959793
19961342
19978207
19978742
19988011
19992687
20003257
20010821
20026642
20054549
20077761
20087305
28855
38239
46741
60033
1980647
19936621
19945134
19970364
20064603{;
replace POS2=1 if CUM1==`num' & (Periodo>=2007) ; /*J05AB01 200 mg tablets+250mg polvo since 2007*/
};
#delimit cr 

#delimit ;
foreach num in 
23825
209836
19908599
19953713
19986725
23814
51715
55545
57171
230399
19908596
19927588
19930982
19961161
19965863
20021168
20024274
20048631
20049737
20051680
20054418
20065997
20096643
20106922
{;
replace POS2=0 if CUM1==`num' & (Periodo<2014) ; /*J05AB01 all tablets since 2014*/
replace POS2=1 if CUM1==`num' & (Periodo>=2014) ; /*J05AB01 all tablets since 2014*/
};
#delimit cr 


#delimit ;
foreach num in 
189803
189806
189805
189807
69402
69403
34978
69609
69610
{;
replace POS2=0 if CUM1==`num' & (Periodo<2016) ; /*J05AB01 all forms of perenatal since 2016*/
replace POS2=1 if CUM1==`num' & (Periodo>=2016) ; /*J05AB01 all tablets perenatal 2016*/
};
#delimit cr 


#delimit ;
foreach num in
32869
38240
48824
59368
200977
226211
230353
19943997
19963993
20051506{;
replace POS2=1 if CUM1==`num' & (Periodo>=2007) ; /*J05AB01/S01AD03 solucion optima 3% since 2007*/
};
#delimit cr 


*estas formas de DE ACICLOVIR NO ESTAN EN EL POS

#delimit ;
foreach num in
51862
55503
56995
57173
214380
216491
19929760
23817
212311
19909054
19913337
19940740
23868
219096
19904937
19914148
20040100{;
replace POS2=0 if CUM1==`num'  ; /*J05AB01 solucion oral no esta en el POS*/
};
#delimit cr 


*MIDAZOLAM or N05CD08 in for 1% and 7.5mg tablets since 2007
*after 2012 5% also included, after 2014 all tablets, after 2016 all forms of injection too. no oral

#delimit ;
foreach num in
33220
19953666
19980134
20009781
20016432
20027033
20049190
20065393
20081223
20086482
20049190
{;
replace POS2=1 if CUM1==`num'  & (Periodo>=2007)  ; /*N05CD08 7.5mg tablets since 2007*/
};
#delimit cr 

#delimit ;
foreach num in
35430
19968773
20043673
19928418
19939082
19942581
19944864
19956916
19964024
19967706
19969030
19969728
19972421
19976046
19984022
19986486
19993871
20009779
20010843
20013573
20027920
20038079
20043279
20051376
20079253
20103344
19927982
{;
replace POS2=1 if CUM1==`num'  & (Periodo>=2007)  ; /*N05CD08 1% solucion since 2007*/
};
#delimit cr

#delimit ;
foreach num in
19920365
19924677
19938963
19959404
19963261
20021971
20021972
103795
19929818
19938434
19940108
19942582
19945171
19964025
19970168
19972420
19986485
19989535
19993870
20001225
20002662
20029924
20057389
20072024
20079255
20024845
20043772
20020413
20070851
20078491
20088547{;
replace POS2=0 if CUM1==`num'  & (Periodo<2012);
replace POS2=1 if CUM1==`num'  & (Periodo>=2012); /*N05CD08 5% solucion since 2012*/
};
#delimit cr
replace POS2= 0 if CUM1==20050229  /*N05CD08 forma oral never*/

*J02AC01 o Fluconazol has been in since 2007 as 200mg tablet, 1%-4% Suspensión oral and 200 mg/100 mL (0.2%) solución inyectable
*all tablets since 2014, and all injections since 2016
#delimit ;
foreach num in
17147
38382
39613
40438
42763
43311
43362
48560
49116
50729
51860
52790
55550
56612
201283
202236
209117
209295
218245
224220
19906130
19914735
19919304
19930050
19930849
19940902
19948178
19950311
19952768
19955126
19967998
19972157
19972307
19989498
19997914
20002160
20006069
20006411
20041224
20044617
20053245
20054557
20062103
20074907
57751
59338
229410
229410
20015451
20108172
{;
replace POS2=1 if CUM1==`num'  & (Periodo>=2007)  ; /*J02AC01 200mg and 1-4% solucion oral since 2007*/
};
#delimit cr

#delimit ;
foreach num in
19984012
20029578
20040482
19984012
20029578
20040482
19998303
19967918
20029581
23289
39281
40207
43363
56609
57005
1980525
48224
56585
39208
39406
40191
43365
48561
49590
52789
52789
56611
57256
207436
226256
227063
227519
1980782
19901231
19910098
19912984
19914737
19919305
19928895
19940576
19946579
19954767
19966243
19967512
19967513
19970835
19971954
19972539
19982987
19992899
19993967
20006723
20011035
20022471
20025213
20054419
20060561
20062320
20072660
19967918
20029581
{;
replace POS2=0 if CUM1==`num'  & (Periodo<2014);
replace POS2=1 if CUM1==`num'  & (Periodo>=2014);/*J02AC01 otras tabletas since 2014*/
};
#delimit cr
replace POS2=0 if CUM1==56587 /*J02AC01 de 5%*/

replace POS2=1 if CUM1==26605 & Periodo==2016   /* D08AG02  did not exit in 2016*/
replace POS2=1 if CUM1==19908128 & Periodo==2016   /* N07AA02  did not exit in 2016*/
replace POS2=1 if CUM1==19258 & Periodo==2016   /* A02BA02  did not exit in 2016*/
replace POS2=1 if CUM1==19963423 & Periodo>=2013   /* A03DB04  did not exit in 2013*/
replace POS2=1 if CUM1==19510 & Periodo>=2015   /* N02AX02  did not exit in 2015*/
replace POS2=1 if CUM1==19953252 & Periodo>=2016   /* C02DC01  did not exit in 2015*/


*LOPINAVIR Y RITONAVIR OR J05AR10 (PREVIOUS ATC J05AE11) HAS BEEN IN 133.3 mg+33.3 mg cápsulas+400 mg+ 100 mg/ 5 mLJarabe SINCE 2007
*ADD 200 mg + 50 mg  tabletas IN 2010, ALL TABLETS 2014, 
replace POS2=1 if CUM1==19911481 /*400 mg+ 100 mg/ 5 mLJarabe*/

#delimit ;
foreach num in
19994092
19967068
{;
replace POS2=0 if CUM1==`num'  & (Periodo<2010);
replace POS2=1 if CUM1==`num'  & (Periodo>=2010);/*J05AR10 o200 mg + 50 mg  since 2010*/
};
#delimit cr

*M01AE01 or ibuprofeno, solo esta en tabletas, nada mas.
*400 mg since 2007, 600 800mg since 2012, everything else since 2015


#delimit ;
foreach num in
10816
31007
39979
47709
51276
58073
213701
224238
226350
226585
227672
229579
1980492
1985022
19902406
19905841
19906322
19906578
19908183
19908679
19908827
19908828
19909491
19910948
19926594
19927727
19928212
19929084
19931872
19934074
19941668
19941880
19943439
19953066
19956889
19956890
19958484
19960823
19963944
19963955
19970402
19974456
19980057
19983609
19983609
19984124
19984848
19998201
20011525
20012273
20012299
20012720
20013528
20018830
20022469
20023505
20037261
20042944
20061861
20062919
20065165
20070049
20095641
20103713
51210
19936111
19940289
20067990
19931312
19925245
20082538
20013817
19920105
19963954
46664
39385
91687
216650
19995786
47072
19906324
19931892
19995784
39384
19929895
41286
{;
replace POS2=0 if CUM1==`num'; /*other forms*/
};
#delimit cr

#delimit ;
foreach num in
10045
12774
22798
31693
36085
38696
39022
39408
39681
40607
41004
43334
43909
44115
44251
45727
45989
47010
47927
48000
51278
51330
51419
51645
52415
52960
54908
54967
55377
55870
56393
56627
56643
58212
59229
60204
201869
203493
203907
205178
205180
208073
208265
208691
210364
210484
215307
217364
219425
219425
220346
1983159
19900510
19906007
19907794
19907896
19907971
19909623
19913082
19917669
19924494
19925246
19927717
19928948
19934149
19936284
19940577
19941207
19942018
19942196
19942285
19944048
19944399
19945287
19946538
19948710
19951618
19952765
19953326
19956780
19957923
19959522
19960345
19961409
19961850
19963269
19963379
19964553
19965163
19968025
19972158
19980114
19981311
19984705
19984776
19985887
19992180
19992222
19995785
19998622
20010931
20011014
20011034
20012282
20012721
20014702
20017258
20022467
20024832
20028831
20028842
20030412
20039860
20039995
20040731
20042035
20046209
20046983
20047668
20048843
20049194
20050760
20051037
20052646
20054312
20086147
20097689
20099551
20099680
56056
20012136
19947498
44865
48694
{;
replace POS2=1 if CUM1==`num'; /*400 mgs */
};
#delimit cr

#delimit ;
foreach num in
8572
23353
26400
36084
39679
40827
42312
43335
44117
46072
47898
48001
50928
52277
52961
53130
54903
55575
56394
56592
59623
200292
201868
210461
217362
227440
1980022
19900511
19901281
19907994
19909625
19924495
19926850
19942283
19943285
19949700
19952764
19953938
19954967
19957761
19961788
19969761
19982050
19994389
19996227
19999418
20003579
20004788
20006516
20020044
20022470
20028543
20048670
20065795
20067683
31330
40514
46324
48837
48887
50759
50929
55234
57474
59622
200294
201867
210371
213375
213375
217600
220344
221453
227055
1980020
1980039
19908489
19914680
19927883
19933760
19936155
19937597
19938879
19939076
19941182
19947857
19952769
19953297
19953298
19956778
19962799
19964314
19964554
19967649
19967851
19973140
19973410
19973739
19982400
19989828
19990439
19992455
19996999
19998203
20003532
20014671
20022466
20027170
20050181
20050870
20054787
20059983
20061571
20065796
20091752
20003579
20004788
20042081
20027170
{;
replace POS2=0 if CUM1==`num'  & (Periodo<2012);
replace POS2=1 if CUM1==`num'  & (Periodo>=2012);/*600 800mg since 2012 */
};
#delimit cr


#delimit ;
foreach num in
50927
20003579
20004788
19984677
20028543
19997253
20040731
20040819
20045825
20078981
20027170
44865
20042081
19984776
20028842
19972937
19983546
19984677
19984776
20028842
20054312
20028831
27570
229857
19960970
20018642
20031136
20099680
55951
200150
230439
19902141
20094605
20097878
19926982
9811
15996
26577
27570
35589
39555
45667
47551
53516
60195
200293
202743
205621
212569
213186
215135
216133
217372
219429
220348
221451
223275
223930
226494
229712
229857
1980018
19900939
19905081
19908266
19927356
19927365
19928963
19930543
19930821
19932308
19934144
19940410
19940414
19940495
19942169
19953939
19960970
19964512
19972937
19981310
19983546
19984677
19985886
19994559
19997253
20010838
20012181
20017257
20018642
20021824
20031136
20039151
20040819
20042036
20046207
20071370
20078981
56055
48694
26928
3749
15996
{;
replace POS2=0 if CUM1==`num'  & (Periodo<2014);/*other tablets since 2014 */
replace POS2=1 if CUM1==`num'  & (Periodo>=2014);/*other tablets since 2014 */
};
#delimit cr

replace POS2=0 if CUM1==229857 /*CALMIDOL COMPUESTO, NOT IN POS, NOT PURE IBUPROFENE*/


replace POS2=1 if CUM1==41460 & Periodo==2012   /* N03AE01 in this concentration since ever*/
replace POS2=1 if CUM1==19951170  /* N04BA02 in this concentration since ever*/


*N07CA91 in 50mg since 2007

#delimit ;
foreach num in
49452
19950623
20037584
20056532
20103649
{;
replace POS2=1 if CUM1==`num'  & (Periodo>=2007);/*other tablets since 2014 */
};
#delimit cr

*M05BA08 or acido zoledronico esta desde 2007 en condiciones raras, desde 2010 en 4mg polvo para injeccion
*desde 2012 4mg/5mg polvo para injeccion, desde 2014 4 mg; 4 mg/5mL(0.08%) y 4 mg/10mL(0.04%) 5 mg/100 mL (0.005%)
*desde 2016 todas las formas
replace POS2=1 if ATC0=="M05BA08"  & (Periodo>=2016)
#delimit ;
foreach num in
19914133
19956384
19973512
19976870
19997647
20011874
20012771
20013024
20022346
20038197
20049260
20064250
20081738
{;
replace POS2=1 if CUM1==`num'  & (Periodo>=2010);/*4mg polvo desde 2010 */
};
#delimit cr

#delimit ;
foreach num in
19959808
20013339
20053622
19952873
20013338
20018490
20023871
20032274
20039762
20047605
20053622
20055301
20059036
20060204
20060576
20072686
20072793
20074204
20093673
19959808
20013339
20053622
19938278
19993092
20032276
{;
replace POS2=1 if CUM1==`num'  & (Periodo>=2014);/*otras formas de 4mg y 5mg desde 2014 */
};
#delimit cr


sort CUM1 Periodo
#delimit ;
foreach num in
227428
230389
230389
19925585
19930138
19930483
19931746
19933487
19933932
19945241
19947351
19947545
19959943
19962379
19963649
19963841
19971140
19976641
19980931
19983450
19984486
19991671
19992951
19993887
19994402
19996538
20009605
20010508
20011353
20017856
20018072
20018281
20022454
20022950
20032042
20038667
20042061
20044924
20047250
20050486
20054456
20056052
20064602
{;
replace POS2=1 if CUM1==`num' & Periodo>=2011;  /*B01AC04 75mg only after 2008 in POS*/
replace POS2=0 if CUM1==`num' & Periodo<2011;  
};
#delimit cr

replace POS2=1 if ATC0=="B01AC04"  & Periodo>=2014 /*B01AC04 all forms post 2014*/


*G03AC03 Levonorgestrel 0.03mg since 2008
*since 2010
*75 mg,  mplante Subdermico
*0.75 mg tableta
*since 2012
*0.03 mg, 	TABLETAS CON O SIN RECUBRIMIENTO QUE NO MODIFIQUEN LA LIBERACIÓN DEL FÁRMACO
*75 mg, 	IMPLANTE SUBDÉRMICO
*0.75 mg 	TABLETAS CON O SIN RECUBRIMIENTO QUE NO MODIFIQUEN LA LIBERACIÓN DEL FÁRMACO
*52 mg. 	DISPOSITIVO INTRAUTERINO.  
*aLL TABLETS SINCE 2014

#delimit ;
foreach num in
19903056
19913335
19979564
19981711
20013439
{;
replace POS2=1 if CUM1==`num' & Periodo>=2008;  /*G03AC03 0.03mg only after 2011 in POS*/
replace POS2=0 if CUM1==`num' & Periodo<2008;  
};
#delimit cr


#delimit ;
foreach num in
19908046
19909989
19948263
19951553
19961092
19979560
19981398
19987914
19989785
19996115
20013627
20022334
20052210
20063439
20067960
20080147
20099554
20103221
{;
replace POS2=1 if CUM1==`num' & Periodo>=2010;  /*G03AC03 0.75mg only after 2010 in POS*/
replace POS2=0 if CUM1==`num' & Periodo<2010;  
};
#delimit cr

#delimit ;
foreach num in
19965149
19968924
19977582
19977706
19986358
19993039
19994000
19999383
20014969
20021340
20022331
20023279
20063441
20097975
20102114
20104469
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;  /*G03AC03 other tablets mg only after 2014 in POS*/
replace POS2=0 if CUM1==`num' & Periodo<2014;  
};
#delimit cr

#delimit ;
foreach num in
19934015
20071862
19900498
{;
replace POS2=1 if CUM1==`num' & Periodo>=2012;  /*G03AC03 idu y subdermico  only after 2012 in POS*/
replace POS2=0 if CUM1==`num' & Periodo<2012;  
};
#delimit cr


*A07EC02 or MESALAZINA in	500 mg 	tableta, MESALAZINA	 4 g 	ENEMA and 	500 mg 	SUPOSITORIO since 2007
*all tablets since 2014

*sup 500mg *4g enema *
#delimit ;
foreach num in
207347
19955593
20004998
20011405
20048039
207346
19955596
19998498
207357
19940343
19955592
19979281
19994114
19999923
20009138
20018758
20032763
20041981
20061489
20077662
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;  /*A07EC02 some since 2007 in POS*/
replace POS2=0 if CUM1==`num' & Periodo<2007;  
};
#delimit cr

#delimit ;
foreach num in
207348
230457
20063402
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;  /*A07EC02 other tablets since 2014 POS*/
replace POS2=0 if CUM1==`num' & Periodo<2014;  
};
#delimit cr

*Insulina A10AD01 since 2012
#delimit ;
foreach num in
19906294
19906297
19906298
19988930
20035580
20035582
19981638
{;
replace POS2=1 if CUM1==`num' & Periodo>=2012;  /*A10AD01*/
replace POS2=0 if CUM1==`num' & Periodo<2012;  
};
#delimit cr


replace POS2=0 if ATC0=="A10AD01" & Periodo<2012


*A10BA02 o metformina 805mg desde 2007, todas desde 2014
#delimit ;
foreach num in
59502
201945
206817
225273
19900128
19902470
19905554
19924363
19928226
19929761
19929778
19932854
19935232
19936956
19937139
19938535
19944665
19962518
19979286
19990473
19992192
19993386
19993678
19993869
19996818
20012284
20014096
20014308
20024869
20027772
20039128
20039445
20046981
20048284
20062735
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;  /*A10BA02 850mg tablets since 2014 POS*/
replace POS2=0 if CUM1==`num' & Periodo<2007;  
};
#delimit cr
*A10BA02 o metformina 805mg desde 2007, todas desde 2014
#delimit ;
foreach num in
59501
200378
19900126
19902471
19924099
19924365
19924741
19925483
19926882
19927530
19928478
19941292
19980565
19980567
19983328
19988017
19990471
19990471
19992192
19992466
19993385
19993676
20004233
20019179
20019181
20024834
20028095
20030392
20033850
20037707
20038971
20038972
20039128
20042822
20046981
20060344
20062736
20062739
20082251
20082257
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;  /*A10BA02 850mg tablets since 2014 POS*/
replace POS2=0 if CUM1==`num' & Periodo<2014;  
};
#delimit cr

*MR volver aqui: 9/april/2017
*B02BD02 or factor VIII more than 100 since 2007, and all since 2007, for all practical purposes all since 2007
replace POS2=0 if ATC0=="B02BD02" & Periodo<2007
replace POS2=1 if ATC0=="B02BD02" & Periodo>=2007

*all since 2016, some since 2007 (and the same until 2015)

replace POS2=1 if ATC0=="B03XA01" & Periodo>=2016
#delimit ;
foreach num in
34812
34813
55553
55554
1980826
19906032
19907615
19912905
19912907
19915099
19915100
19925894
19925896
19930202
19960158
19960160
19960161
19960162
19967735
19968466
19969732
19978667
19978668
19986609
20002042
20005999
20032712
20042835
20044729
20047839
20048531
20096478
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;  /*A10BA02 850mg tablets since 2014 POS*/
replace POS2=0 if CUM1==`num' & Periodo<2007;  
};
#delimit cr

*B05BB01 0.9% since 2012, others since 2016
#delimit ;
foreach num in
226317
1983088
19928759
19930292
19933210
19942559
19942561
19949649
19949698
19950323
19950324
19950325
19979159
20005003
20020123
20020124
20041458
20055559
20081180
20097309
{;
replace POS2=1 if CUM1==`num' & Periodo>=2012;  /*A10BA02 850mg tablets since 2014 POS*/
replace POS2=0 if CUM1==`num' & Periodo<2012;  
};
#delimit cr

*B05BB01 0.9% since 2012, others since 2016
#delimit ;
foreach num in
19949649
20097309
{;
replace POS2=1 if CUM1==`num' & Periodo>=2012;  /*B05BB01 0.9% since 2012*/
replace POS2=0 if CUM1==`num' & Periodo<2012;  
};
#delimit cr

*C07AB02  en 50 y 100 mg desde 2007, en 1 mg/mL  desde 2012, en todas tabletas desde 2014, en todas onyecciones desde 2016
#delimit ;
foreach num in
27041
33788
34189
34190
36096
39210
39225
50707
200541
227364
19908585
19908587
19908849
19924180
19926214
19926215
19928891
19928892
19928905
19929034
19929036
19929218
19930442
19930444
19930535
19942820
19943526
19943592
19947448
19947449
19947807
19947808
19951447
19951448
19953850
19953962
19961414
19961415
19962066
19962067
19968951
19976469
19976470
19976913
19977372
19978213
19992946
20001293
20001296
20003927
20005047
20009776
20012125
20012494
20013710
20013712
20028492
20030126
20030130
20032036
20046112
20056847
20056849
20063371
20071992
20078003
20090993
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;  /*C07AB02  50 100 mg*/
replace POS2=0 if CUM1==`num' & Periodo<2007;  
};
#delimit cr
#delimit ;
foreach num in
19968149
{;
replace POS2=1 if CUM1==`num' & Periodo>=2012;  /*C07AB02 1mg per ml since 2012*/
replace POS2=0 if CUM1==`num' & Periodo<2012;  
};
#delimit cr

#delimit ;
foreach num in
19901250
19927923
19996862
20001295
20005044
20056846
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;  /*C07AB02  other tablets mg*/
replace POS2=0 if CUM1==`num' & Periodo<2014;  
};
#delimit cr

#delimit ;
foreach num in
39227
213257
19944654
{;
replace POS2=1 if CUM1==`num' & Periodo>=2016;  /*C07AB02  other soluciones since 2016*/
replace POS2=0 if CUM1==`num' & Periodo<2016;  
};
#delimit cr
*C02AC01 150mg desde 2007, todas demas tabletas desde 2014
*for all practical purposes since ever
replace POS2=1 if ATC0=="C02AC01"

*C07AG02 only some since 2012, all since 2014. for all practical matters, all since 2012
replace POS2=1 if ATC0=="C07AG02"& Periodo>=2012
replace POS2=0 if ATC0=="C07AG02"& Periodo<2012

*D07AA02 1% crema and 0.5 locion since 2007
#delimit ;
foreach num in
21637
22133
40194
42927
60087
19953707
19965715
19976372
19995931
20006988
29686
19913611
19953706
19962528
22134
19930316
19941980
19965717
20001046
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;  /*D07AA02 1% crema since 2016*/
replace POS2=0 if CUM1==`num' & Periodo<2007;  
};
#delimit cr
* J01CR02 tablets *250 mg + 125 mg 500 mg + 125 mg 875 mg + 125 mg since 2012, others since 2014
#delimit ;
foreach num in
32566
48403
212051
223135
224401
224890
19907133
19909720
19935690
19992788
20004817
20004819
20009141
20013763
20016739
20017854
20017860
20028994
20032449
20046978
20047043
20057397
20057624
20070965
20094546
19952509
{;
replace POS2=0 if CUM1==`num' & Periodo<2012;  /*J01CR02 tablets*/
replace POS2=1 if CUM1==`num' & Periodo>=2012;  
};
#delimit cr

#delimit ;
foreach num in
32566
48403
209244
212051
214753
223135
224079
224399
224401
224890
226197
19900055
19925458
19925921
19933216
19935690
19949038
19949513
19963229
19992788
20004134
20004817
20004819
20009141
20013763
20016739
20017854
20017860
20028994
20032447
20032449
20044975
20045158
20046978
20047043
20053780
20057397
20057624
20070965
20077366
20077740
20077763
20094546
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;  /*J01CR02*/
};
#delimit cr
*(125 mg-400 mg + 28,5-62,5 mg)/5mL. 	POLVO PARA RECONSTITUIR A SUSPENSIÓN ORAL


*J01CR05 en 4.5 mg desde 2007
#delimit ;
foreach num in
203143
19953325
19961814
19976389
19979710
19981396
19988366
19989259
20089767
20096120
19988366
20005167
19941580
19961368
19974254
19988268
19989551
20006873
20010686
20010839
20022236
20025675
20038143
20057109
20057245
20059801
20060415
20067456
20078963
20082710
20085278
20110801
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;  /*J01CR054.5 crema since 2016*/
};
#delimit cr
*J01FA09 500 mg polvo since 2007, since 2012 250 mg/5mL (5%) 	SUSPENSIÓN ORAl+ 500 mg 	TABLETA CON
*since 2014 all tabletas
#delimit ;
foreach num in
54709
228384
19961536
20002059
20007237
20022768
20059630
20082197
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;  /*J01FA09*/
};
#delimit cr

#delimit ;
foreach num in
22808
23365
216980
19912612
19927246
19942789
19947497
19974859
20054553
14446
58682
212535
215472
215926
224939
225132
226115
226963
228036
229637
1980348
19901371
19906258
19907156
19907929
19911016
19911659
19915103
19921078
19921078
19925462
19928704
19933680
19934192
19935653
19936072
19943214
19948763
19950321
19951552
19952368
19961994
19973740
19973741
19975205
19976173
19977620
19980816
19983707
20004850
20005368
20005904
20014332
20022464
20035914
20038303
20042847
20048356
20050871
20054556
20072716
20087322
20090124
20097976
19969004
{;
replace POS2=0 if CUM1==`num' & Periodo<2012;  /*J01FA09*/
replace POS2=1 if CUM1==`num' & Periodo>=2012;  
};
#delimit cr

#delimit ;
foreach num in
7019
14438
22841
23302
36725
50284
205872
215928
222778
224940
225133
226118
228037
229987
19900797
19904026
19906259
19907930
19908104
19921076
19928708
19933217
19942476
19952371
19990193
20090125
{;
replace POS2=0 if CUM1==`num' & Periodo<2014;  /*J01FA09*/
replace POS2=1 if CUM1==`num' & Periodo>=2014;  
};
#delimit cr


*all J01MA02 since 2016, 100 mg/10 mL de base 	SOLUCIÓN INYECTABLE+ 250mg y 500mg tablet since 2007. all tablets since 2014
*Since 2007
#delimit ;
foreach num in
13422
38574
38645
39944
40429
40500
57662
59939
19913325
19924028
19938327
19942228
19942611
19944055
19954690
19955381
19984940
19988098
20026284
20104186
9870
30435
30517
34184
35644
35645
35704
36275
36276
37327
37330
37494
37535
37536
37990
37991
38628
38741
38828
38915
39317
39916
40214
40290
42603
42798
45238
45249
51066
51502
51653
55211
55549
55973
56435
56690
57463
58016
200979
206308
212827
212829
212943
217588
226732
1981855
1983977
1984171
19901466
19901467
19908362
19911658
19915834
19915835
19917655
19917662
19927558
19927594
19928944
19933867
19934372
19934376
19936283
19940107
19945267
19946052
19952920
19958762
19959953
19960665
19960666
19961841
19969884
19978030
19980727
19981982
19983584
19992720
19995068
19998297
20001556
20006198
20012064
20020095
20028145
20029332
20031521
20037405
20038179
20042810
20045171
20048529
20054318
20061160
20062482
20068314
20072017
20076358
20082918
20097857
{;
replace POS2=0 if CUM1==`num' & Periodo<2007;  /*J01MA02*/
replace POS2=1 if CUM1==`num' & Periodo>=2007;  
};
#delimit cr

#delimit ;
foreach num in
36277
37520
40273
40761
43315
48831
55111
55659
56379
56710
201047
201734
208690
208702
210275
217586
19905617
19914656
19914658
19925307
19940812
19960664
19961840
19963455
19963856
19980871
19981205
20003485
20061163
20076358
{;
replace POS2=0 if CUM1==`num' & Periodo<2014;  /*J01MA02*/
replace POS2=1 if CUM1==`num' & Periodo>=2014;  
};
#delimit cr



*Since 2016
#delimit ;
foreach num in
8022
40499
53356
55598
59401
59659
229609
19915519
19915521
19927394
19928322
19929589
19930137
19936138
19938405
19939271
19941791
19947597
19950541
19950968
19955382
19959704
19966903
19967005
19976441
19995246
19995247
19995248
20002113
20002118
20013311
20020742
20070438
20092482
{;
replace POS2=0 if CUM1==`num' & Periodo<2016;  /*J01MA02*/
replace POS2=1 if CUM1==`num' & Periodo>=2016;  
};
#delimit cr

*J05AB11 since 2012 in 1000 mg and 500 mg, all other tablets since 2014
*for practical purposes all since 2012
replace POS2=1 if ATC0=="J05AB11"& Periodo>=2012
replace POS2=0 if ATC0=="J05AB11"& Periodo<2012

*ATAZANAVIR or J05AE08 150/200 since 2007, 300 mh since 2012, all since 2014

#delimit ;
foreach num in
19946307
19946308
20012739
20012740
20016512
20016911
20021225
20023950
20025152
20041332
20057167
20064171
20076987
{;
replace POS2=0 if CUM1==`num' & Periodo<2007;  /*J05AE08*/
replace POS2=1 if CUM1==`num' & Periodo>=2007;  
};
#delimit cr

#delimit ;
foreach num in
19993218
20019310
20023907
20025153
20041334
20057160
20066897
20074086
20074086
20075967
20099322
{;
replace POS2=0 if CUM1==`num' & Periodo<2012;  /*J05AE08*/
replace POS2=1 if CUM1==`num' & Periodo>=2012;  
};
#delimit cr

#delimit ;
foreach num in
20066897
{;
replace POS2=0 if CUM1==`num' & Periodo<2014;  /*J05AE08*/
replace POS2=1 if CUM1==`num' & Periodo>=2014;  
};
#delimit cr

*J05AF02 since 2007 400 mg 	CÁPSULA DE LIBERACIÓN PROGRAMADA+ 100 mg 	CÁPSULA O TABLETA + 25 mg	CÁPSULA O TABLETA 
*2014 all regular tablets + 250 mg liberacion programada
*since 2016 all

*since 2007
#delimit ;
foreach num in
45232
45234
47095
47097
48157
50434
19918367
19927911
19963609
19968904
19971136
19979478
19983877
19985220
19988253
20011631
20028084
20029279
20038116
20080575
{;
replace POS2=0 if CUM1==`num' & Periodo<2007;  /*J05AF02*/
replace POS2=1 if CUM1==`num' & Periodo>=2007;  
};
#delimit cr

#delimit ;
foreach num in
19918366
20098438
45235
47094
47096
47100
49127
50429
50433
{;
replace POS2=0 if CUM1==`num' & Periodo<2014;  /*J05AF02*/
replace POS2=1 if CUM1==`num' & Periodo>=2014;  
};
#delimit cr

#delimit ;
foreach num in
19918365
{;
replace POS2=0 if CUM1==`num' & Periodo<2016;  /*J05AF02*/
replace POS2=1 if CUM1==`num' & Periodo>=2016;  
};
#delimit cr

replace POS2=1 if ATC0=="J07BH01"  & Periodo>=2012
replace POS2=0 if ATC0=="J07BH01"  & Periodo<2012

*L01BC02 since 2007 5%	UNGÜENTO Ó CREMA Ó GEL+500 mg/10 mL 	SOLUCIÓN INYECTABLE. after 2016 all forms of solucion inyctable

*since 2007
#delimit ;
foreach num in
19918365
58810
19930999
19964018
20028866
36810
200313
204174
214790
214794
227250
228657
19905209
19906657
19930046
19961610
19979777
19988561
20011827
20043050
20053621
20063240
20071503
{;
replace POS2=0 if CUM1==`num' & Periodo<2007;  /*L01BC02*/
replace POS2=1 if CUM1==`num' & Periodo>=2007;  
};
#delimit cr

*since 2016
#delimit ;
foreach num in
214788
214792
227728
228655
19906656
20011828
{;
replace POS2=0 if CUM1==`num' & Periodo<2016;  /*L01BC02*/
replace POS2=1 if CUM1==`num' & Periodo>=2016;  
};
#delimit cr
*not in
#delimit ;
foreach num in
216264
226023
{;
replace POS2=0 if CUM1==`num' ;  /*L01BC02*/ 
};
#delimit cr


*L01CA02 since 2007 1 mg/mL 	SOLUCIÓN INYECTABLE, afer 2016 all
*for all practical matters all since 2007
replace POS2=1 if ATC0=="L01CA02" 

*L01XX02 10.000 UI 	POLVO ESTÉRIL PARA INYECCIÓN SINCE 2007, ALL SINCE 2016
*FOR ALL PRACTICAL MATTERS ALL SINCE 2007
replace POS2=1 if ATC0=="L01XX02" 

*L04AA13 100mg +20mg TABLETS SINCE 2012, ALL TABLETS SINCE 2014
*FOR ALL PRACTICAL MATTERS ALL SINCE 2012
replace POS2=1 if ATC0=="L04AA13" & Periodo>=2012
replace POS2=0 if ATC0=="L04AA13" & Periodo<2012

*M03AC03 10 mg inyeccion since 2007, all since 2016

#delimit ;
foreach num in
44838
19936614
19970169
19971049
19982152
19998632
20028436
20037688
20071601
{;
replace POS2=1 if CUM1==`num' ;  /*M03AC03*/ 
};
#delimit cr
replace POS2=1 if ATC0=="M03AC03" & Periodo>=2016

*N04BA02 250 mg + 25 mg tableta since 2007, all since 2014
#delimit ;
foreach num in
47458
216838
225105
1980397
19935577
20052261
{;
replace POS2=1 if CUM1==`num' ;  /*N04BA02*/ 
};
#delimit cr
replace POS2=1 if ATC0=="N04BA02" & Periodo>=2014
replace POS2=0 if CUM1==19951171 & Periodo<2014



*N04BA03 only since 2012
/*
200 mg + 12,5 mg +  50 mg 	TABLETA CON O SIN RECUBRIMIENTO QUE NO MODIFIQUE LA LIBERACIÓN DEL FÁRMACO.
200 mg + 18,75 mg + 75 mg 	TABLETA CON O SIN RECUBRIMIENTO QUE NO MODIFIQUE LA LIBERACIÓN DEL FÁRMACO.
200 mg + 25 mg + 100 mg 	TABLETA CON O SIN RECUBRIMIENTO QUE NO MODIFIQUE LA LIBERACIÓN DEL FÁRMACO.
200 mg + 31,25 mg + 125 mg 	TABLETA CON O SIN RECUBRIMIENTO QUE NO MODIFIQUE LA LIBERACIÓN DEL FÁRMACO.
200 mg + 37,5 mg + 150 mg 	TABLETA CON O SIN RECUBRIMIENTO QUE NO MODIFIQUE LA LIBERACIÓN DEL FÁRMACO.
200 mg + 50 mg + 200 mg 	TABLETA CON O SIN RECUBRIMIENTO QUE NO MODIFIQUE LA LIBERACIÓN DEL FÁRMACO.
*/
*after 2014 all
*all practical matters, all since 2012
replace POS2=0 if ATC0=="N04BA03" & Periodo<2012
replace POS2=1 if ATC0=="N04BA03" & Periodo>=2012

*N04BC01 2,5 mg  tablets since 2007, all since 2014
replace POS2=0 if ATC0=="N04BC01" & Periodo<2007
replace POS2=1 if ATC0=="N04BC01" & Periodo>=2007

*the only two not 2.5mg
replace POS2=0 if CUM1==21060 & Periodo<2014
replace POS2=0 if CUM1==45333 & Periodo<2014

*N05AC04 25 mg/mL 	SOLUCIÓN INYECTABLE SINCE 2007, all since 2016
*practical matters all since 2007
replace POS2=0 if ATC0=="N05AC04" & Periodo<2007
replace POS2=1 if ATC0=="N05AC04" & Periodo>=2007

*N06DA03 since 2012
/*
1,5mg	CÁPSULA
3mg 	CÁPSULA
4,5mg	CÁPSULA
6mg 	CÁPSULA
18mg	PARCHE
27mg	PARCHE
9mg 	PARCHE
*/

*all tablets post 2014

#delimit ;
foreach num in
19985874
19985986
19985987
20089833
20089836
20103683
20103686
20103687
20104405
20104410
226606   226607   226608 20008022 20008023 20008025 20008026 20018590 20018593 20018595 20018596 20038208 20038210 20038211 20038213
{;
replace POS2=0 if CUM1==`num' & Periodo<2012;  /*N06DA03*/
replace POS2=1 if CUM1==`num' & Periodo>=2012;  
};
#delimit cr

#delimit ;
foreach num in
226605
{;
replace POS2=0 if CUM1==`num' & Periodo<2014;  /*N06DA03*/
replace POS2=1 if CUM1==`num' & Periodo>=2014;  
};
#delimit cr
*no esta
replace POS2=0 if CUM1==19985985

*R06AX13 since 2007
/*
10 mg 	TABLETA
5 mg/5 mL (0,1%) 	JARABE
*/
*all tablets post 2014
#delimit ;
foreach num in
22506 23848 34199 34548 36940 38301 38402 38451 41713 42466 46055 50342 51133 52608 53918 54559 54627 55152 55570 59714 59840 200285 201271 207435 208715 209832 210279 210437 212342 212537 212723 215289 219651 19900990 19907272 19908846 19910683 19911008 19924076 19925736 19936535 19942076 19943512 19948179 19949960 19952731 19961297 19965091 19965571 19966898 19974976 19976373 19981202 19982470 19991215 19998115 20024243 20036833 20042620 20053443 20068968 20081249 20103113
9109 9855 23379 28783 29396 32641 36452 37116 37663 38289 38418 38880 39032 39641 40412 41998 42808 47560 47925 50420 52359 53073 54386 54570 55151 55976 56687 59410 59836 200276 201278 207268 207360 208414 208738 208759 212069 212687 214251 214546 215762 218251 219649 226180 228011 19907271 19907969 19909015 19911018 19921040 19925248 19929385 19930544 19937237 19937445 19937596 19937865 19942062 19945531 19947988 19948176 19951679 19952732 19954766 19960816 19965758 19965759 19965760 19974974 19982268 19996082 19999956 20001238 20004887 20024239 20062950 20082913
20024239
{;
replace POS2=0 if CUM1==`num' & Periodo<2007;  /*R06AX13*/
replace POS2=1 if CUM1==`num' & Periodo>=2007;  
};
#delimit cr


replace POS2=1 if ATC0=="A12BA51"  /*A12BA51 has been always, but code seems to have changed to B05XA01*/
replace ATC0="B05XA01" if ATC0=="A12BA51"

*Glucosamina o M01AX05 nunca en el pos*
replace POS2=1 if ATC0=="M01AX05" 

*S01CA03, que es hidrocortisona, una solucion optica, solo entra en 2014 pero es otro codigo!*
replace POS2=0 if ATC0=="S01CA03"  

*B05CX01, que es glucosa, no esta en el pos*
replace POS2=0 if ATC0=="B05CX01"  

*D11AX98, que en el whoi no aparece pero en internet dice que es cromatiton, no esta en el pos*
replace POS2=0 if ATC0=="D11AX98"  

*Tobramicina o J01GB01,  no esta en el pos*
replace POS2=0 if ATC0=="J01GB01"  

*zolpidem o N05CF02,  no esta en el pos*
replace POS2=0 if ATC0=="N05CF02"  

*zolpidem o N05CF02,  no esta en el pos*
replace POS2=0 if ATC0=="N05CF02"  

*S01CB05 OR FLUOROMETOLONA HAS BEEN IN POS ALWAYS BUT UNDER CODE S01BA07
replace POS2=1 if CUM1==20017756   

*CONDROITIN sulfato o M01AX25 nunca ha estado en el POS
replace POS2=0 if ATC0=="M01AX25" 

*gluconato de potasio A12BA05 has been in the POS since 2007 at least (at 31%, the only avaible presentation in colombia)
replace POS2=1 if ATC0=="A12BA05" 

*METOXALEM or D05BA02 since 2007
replace POS2=1 if CUM1==48880 


*A10AB06 o  INSULINA GLULISINA 100 ui desde 2012
replace POS2=1 if CUM1==19950478 & Periodo>=2012 
replace POS2=1 if CUM1==19950479 & Periodo>=2012
replace POS2=0 if CUM1==19950478 & Periodo<2012 
replace POS2=0 if CUM1==19950479 & Periodo<2012

*C01CE02 o militrona en 1mg/ml desde 2012 1 mg/mL (0,1%)
*todas las formas desde 2016
replace POS2=0 if CUM1==39807 & Periodo<2016 /*no tableta*/
replace POS2=1 if ATC0=="C01CE02" & Periodo>=2016




#delimit ;
foreach num in
40198
{;
replace POS2=1 if CUM1==`num' & Periodo>=2016;  /*C01CE02 only  some after 2012 in POS*/
replace POS2=0 if CUM1==`num' & Periodo<2016;  
};
#delimit cr


#delimit ;
foreach num in
39807
19960132
20003652
20007619
20032545
20039606
20047729
20048046
20051350
20060004
20065257
20091135
{;
replace POS2=1 if CUM1==`num' & Periodo>=2012;  /*C01CE02 only  some after 2012 in POS*/
replace POS2=0 if CUM1==`num' & Periodo<2012;  
};
#delimit cr

*Ibuprofeno de liberiacion prolongada no esta
replace POS2=0 if CUM1==40514 
replace POS2=0 if CUM1==51210 


*M05BA04 *B01AC04 ya por que entran en 2011

*A12AA20 in all since 2014, some since 2007
replace POS2=0 if ATC0=="A12AA20"
#delimit ;
foreach num in
41661 44215 52621 211824 225159 225350 229046 230327 1982539 19900615 19918608 19924515 19927029 19928543 19929383 19934505 19935010 19937457 19942963 19944815 19962656 19965706 19996848 20005043 20012533 20013575 20013578
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;  /*A12AA20 all tablets after 2014 POS*/
};
#delimit cr

#delimit ;
foreach num in
30970 41661 44215 51105 52354 52621 55315 202297 202315 202406 210425 211824 213868 213927 217286 224264 224860 225159 225299 225350 228927 229046 229691 230327 1982539 1983216 19900615 19902583 19904540 19908078 19918608 19919176 19922485 19924515 19925388 19927029 19928131 19928543 19928841 19929383 19929554 19930338 19932712 19934505 19934649 19935010 19935310 19937457 19937599 19939306 19942963 19943327 19943337 19944815 19947223 19947321 19951009 19962614 19962615 19962656 19965706 19985719 19989158 19996848 20005043 20012533 20013575 20013578 20025161 20037895 20037896 20060958
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;  /*A12AA20 all tablets after 2014 POS*/
};
#delimit cr
replace POS2=0 if CUM1==20002038


*C01BD01 150 mg 200 mg tablets since 2007, post 2014 all tablets and 5% solucion, 2016 all inyecciones

#delimit ;
foreach num in
27635 45892 52270 200356 215275 1980389 19903280 19927021 19929035 19931611 19935330 20013691 20043928 20047527
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;  /*C01BD01*/
};
#delimit cr

#delimit ;
foreach num in
202350
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;  /*C01BD01*/
replace POS2=0 if CUM1==`num' & Periodo<2014;  /*C01BD01*/
};
#delimit cr

#delimit ;
foreach num in
52271 19900904 19907311 19944604 19992832 20025925 20047170
{;
replace POS2=1 if CUM1==`num' & Periodo>=2016;  /*C01BD01*/
replace POS2=0 if CUM1==`num' & Periodo<2016;  /*C01BD01*/
};
#delimit cr

*C08CA01 5mg tablet since 2007, all since 2014

#delimit ;
foreach num in
17134 44862 46783 48062 50345 50503 50505 52806 55742 55895 56009 200367 211170 224769 1983670 19909769 19909838 19915210 19928329 19928630 19929140 19930799 19938140 19939575 19942468 19948276 19953333 19959472 19966043 19970919 19977004 19999986 20002895 20005184 20007349 20009362 20011816 20012877 20025003 20031289 20037986 20038504 20038828 20039159 20051870 20053757 20056337 20059193 20067108 20068086 20068087 20076051 20096058
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;  /*C08CA01*/
};
#delimit cr
replace POS2=1 if ATC0=="C08CA01" & Periodo>=2014  /*C08CA01*/


*H02AB01 4 mg/mL de base solución inyectable and (3 mg de base + 3 mg)/mL suspensión inyectable since 2007
*after 2016 all
replace POS2=0 if ATC0=="H02AB01"  /*H02AB01*/
replace POS2=1 if ATC0=="H02AB01" & Periodo>=2016  /*H02AB01*/

#delimit ;
foreach num in
53392 215257 1983779 19904492 19931051 19943961 19945027 19953549 19965227 19974477 19974890 19976384 19980025 19980219 19994642 19997198 20003708 20068188 20077837 20094175 19971457
19987725
19997617
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;  /*H02AB01*/
};
#delimit cr

*not in since oral
#delimit ;
foreach num in
35015 46129 20087756
{;
replace POS2=0 if CUM1==`num';  /*H02AB01*/
};
#delimit cr

*AMOXICILINA or J01CA04
/*
since 2007 
250 mg de base tableta o cápsula
500 mg de base tableta o cápsula
125 mg/5 mL de base (2,5%) suspensión oral
250 mg/5 mL de base (5%) suspensión oral
 */
 *post 2014 all tablets 
 
 *since 2007
 #delimit ;
foreach num in
 16139 17583 22606 25582 25980 27747 29183 29696 29698 29898 33496 35816 36575 37460 37713 38809 41133 41435 42203 42308 42388 43475 44250 49660 50990 52239 55298 56532 56632 56698 205402 216981 224295 225461 226200 228269 228278 230540 19905676 19907354 19908215 19908288 19911231 19912130 19914576 19917661 19928324 19929635 19950194 19951098 19952186 19954567 19954568 19955522 19956138 19956278 19957837 19957924 19960652 19965141 19974084 19976478 19979477 19982893 19984155 19985590 19986267 19997692 19998714 20001095 20002904 20004301 20005676 20006710 20033145 20034334 20034458 20034876 20037246 20044171 20047999 20048559 20066350 20081161 20083462 20086565 20099719 20100042
 6536 32533 40632 44236 57682 203786 212314 214753 226702 228270 19990067 19994088 20005326 20028615
 17584 28594 29343 29897 30958 32915 33492 35374 35812 37353 42385 42821 44259 44389 44783 47198 53095 55299 57390 212725 216848 216979 226400 228263 19908833 19919625 19927018 19939106 19942677 19947733 19949568 19951175 19952063 19953234 19953295 19954082 19955982 19956507 19959432 19959433 19960035 19969145 19972542 19974312 19981079 19992863 19992903 19996725 19999922 20001096 20005325 20012583 20022463 20034332 20042366 20048777 20050605 20051970 20074012 20102583
 228266
 41432 209874 20005675
 6536 17583 22606 25582 25980 27747 28680 29183 29698 29898 33333 33496 35375 35816 36575 37713 38809 40632 41133 41435 42308 42388 44236 44250 45455 47539 49660 50990 52239 55138 55189 55298 56532 56632 56698 57682 203786 205402 211551 212314 214753 216981 225461 226702 228269 228270 228278 1980809 19905465 19905676 19908215 19911231 19912130 19914576 19917661 19928324 19929635 19944375 19944581 19950194 19952186 19954567 19954568 19955522 19956138 19957837 19957924 19960652 19965141 19974084 19976478 19982893 19985590 19986267 19990067 19994088 19997692 19998714 20001095 20002904 20004301 20005676 20028615 20034334 20034458 20034876 20037246 20044171 20047999 20048559 20086565 20100042
 16139 29696 43475 203208 226200 226701 1980605 19908288
 83541
 212310
 226198
 19953236
 20104000
 {;
replace POS2=1 if CUM1==`num';  /*J01CA04*/
};
#delimit cr
 
  #delimit ;
foreach num in
 4096 50758 51147 51434 52798 57339 203208 208195 213591 226701 228264 228265 230125 1980605 1984119 19905464 19905466 19905467 19907633 19908092 19909128 19914363 19943370 19949513 19990593 19990937 20025998 20063504 20086048
4096 50758 51147 213591 228264 228265 230125 1984119 19905464 19905466 19905467 19907633 19908092 19909128 19914363 19943370 19949513 19990593 19990937 20025998 20063504 20086048
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;  /*J01CA04*/
replace POS2=0 if CUM1==`num' & Periodo<2014;  /*J01CA04*/
};
#delimit cr
 
 
 *solucion inyectable no esta
   #delimit ;
foreach num in
55140
56531
208934
218008
6537
38807
56707
228267
1983142
20086045
11492 26827 37354 37748 208815 226212 19913146 19939447 19945788 19949567 20005678
 {;
replace POS2=0 if CUM1==`num';  /*J01CA04*/
};
#delimit cr

 *J01DB01
 /*
 since 2007
 250 mg tableta o cápsula
500 mg tableta o cápsula
125 mg/5 mL (2,5%) suspensión oral
250 mg/5 mL (5%) suspensión oral

afyer 2014 all tablets
*/
*since 2007
  #delimit ;
foreach num in
23086 25353 32808 34382 36574 39248 42397 43656 43894 44405 45430 56701 87108 205695 207834 215139 228274 1980010 1980539 1984133 1984368 1984797 19901019 19907674 19907675 19907676 19913071 19926484 19929856 19942500 19944374 19952375 19962841 19964889 19966639 19967330 19967543 19976961 19977215 19981236 19984590 19987295 19988080 19990133 19990490 19991241 19992907 20004125 20009373 20014222 20014813 20024267 20042787 20045406 20048430 20048556 20050526 20050872 20090864 20103297
6533 20036 22806 23228 25351 41309 42451 44406 56819 60069 205694 207831 210465 215187 216915 228319 229702 19952370 19963523 19968135 19972384 19974665 19980386 19990491 19992906 20010798 20019093 20042301 20045660 20050607 20051015 20090868 20102229 20103519
6539 48328 209656 226314 20051901
 {;
replace POS2=1 if CUM1==`num';  /*J01DB01*/
};
#delimit cr

*after 2014
  #delimit ;
foreach num in
23819 32975 47015 214147 228318 19901021 19934359 19964008 19974238 19988403 20014812
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;  /*J01DB01*/
replace POS2=0 if CUM1==`num' & Periodo<2014;  /*J01DB01*/
};
#delimit cr

*J05AF01 since 2007
/*300 mg tableta
10 mg/mL solución oral
100 mg tabletas o cápsula
10 mg/mL (1%) solución inyectable
*/
*all tablets post 2014
*all inyeccion since 2016


*100 y 300 mg since 2007
  #delimit ;
foreach num in
31194 39314 43269 203139 209800 230550 19903346 19908826 19925221 19925561 19926006 19926325 19928184 19928214 19943128 19947464 19947470 19954103 19962364 19965969 19967910 19970178 19982352 19992458 20025393 20048830 20064177
58185 1984997 19903302 19928186 19954104 19962365 19968903 19974944 19993109 20002097 20006700 20065187
19237
 {;
replace POS2=1 if CUM1==`num';  /*J05AF01*/
};
#delimit cr
*since 2014
  #delimit ;
foreach num in
58184 19954102 19970178
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;  /*J05AF01*/
replace POS2=0 if CUM1==`num' & Periodo<2014;  /*J05AF01*/
};
#delimit cr

*L01CD01 since 2007 SOLUCION INYECTABLE, 30 MG SOLUCION INYECTABLE, 100 - 150 MG 
  #delimit ;
foreach num in
55979 222085 227246 227875 227876 19907851 19907852 19912566 19913981 19915545 19915547 19937440 19937677 19946485 19952097 19952099 19957354 19962879 19962977 19967319 19967974 19968352 19968458 19968594 19973177 19973179 19973180 19976352 19976518 19979611 19980164 19991106 19994571 20007285 20012769 20012772 20014726 20014780 20018482 20018484 20020276 20022186 20022187 20029990 20029991 20047566 20047567 20055740 20055999 20056001 20057021 20061216 20063193 20064116 20065031 20071691 20071695 20083543
{;
replace POS2=1 if CUM1==`num';  /*L01CD01*/
};

*N03AF01 since 2007 200 mg tableta and 100 mg/5 mL suspensión oral
*since 2014 all tablets
  #delimit ;
foreach num in
30256 31405 32871 37489 43300 43595 43879 44056 44969 45526 45698 48701 50813 52437 52612 53858 55385 226680 227376 19905618 19919482 19919485 19932610 19944582 19955117 19970333
42698 44065 45695 56106 226679 19920221 19928456 20014490 2004658 37677
{;
replace POS2=1 if CUM1==`num';  /*N03AF01*/
};

  #delimit ;
foreach num in
23226 30257 40597 40614 43708 44057 45479 45909 48702 52226 52613 55383 227365 19919471
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;  /*N03AF01*/
replace POS2=0 if CUM1==`num' & Periodo<2014;  /*N03AF01*/
};
#delimit cr

replace POS2=0 if ATC0=="N07BC02" /*methadone not in POS*/

*R03BA01 since 2007
/*250 mcg/dosis 
50 mcg/dosis 
*/
  #delimit ;
foreach num in
19905579 19908101 19917370 19918635 19925470 19976639 19981614 19981616 19994878 19996407 20016967 20029005 19992492
{;
replace POS2=1 if CUM1==`num';  /*R03BA01*/
};
#delimit cr


*A03BB01 todas las tabletas desde 2014
replace POS2=0 if ATC0=="A03BB01" & Periodo<2012  /*A03BB01*/
  #delimit ;
foreach num in
17023 29374 54392 56113 19926739 19949535 19949799 19955212 19955410 19955834 19961361 19962505 19963375 19966187 19973908 19983149 19987296 19996477 19997697 20019759 20047056 20048801 20057197
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;  /*A03BB01*/
};
#delimit cr

  #delimit ;
foreach num in
17024 29373 212035 217210 225064 19903695 19920900 19924391 19927114 19948844 19951133 19959401 19960732 19976540 19977450 19985976 19992695 19997616 19997619
{;
replace POS2=0 if CUM1==`num' & Periodo<2012;  /*A03BB01*/
replace POS2=1 if CUM1==`num' & Periodo>=2012;  /*A03BB01*/
};
#delimit cr

*A06AB02 5 mg since ever
  #delimit ;
foreach num in
31462 19942092 19942093 19943175 19955787 20006150 20021450 20053049 20058441 20059772 19953922
{;
replace POS2=1 if CUM1==`num';   /*A06AB02*/
};
#delimit cr

*A07EC01 500 mg since ever
  #delimit ;
foreach num in
17129 30049 201145 19947421 19967884 19979656 20077756
{;
replace POS2=1 if CUM1==`num';   /*A07EC01*/
};
#delimit cr

  #delimit ;
foreach num in
20026114
{;
replace POS2=0 if CUM1==`num';   /*A07EC01*/
};
#delimit cr

*all forms of B03BA01 since 2016
replace POS2=1 if ATC0=="B03BA01" & Periodo>=2016

*B03BB01 all tablets since 2014,1 mg since 2007
  #delimit ;
foreach num in
32623 35662 213611 226204 19908845 19930285 19936345 19938134 19946265 19947508 19948032 19948034 19949191 19954855 19958683 19961706 19962752 19964526 19964527 19964631 19968296 19974643 19990265 19996240 20021574 20022703 20022704 20026598 20041344 20050206 20050550
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;   /*B03BB01*/
replace POS2=0 if CUM1==`num' & Periodo<2014;   /*B03BB01*/
};
#delimit cr
  #delimit ;
foreach num in
35662 226204 19908845 19936345 19938134 19947508 19958683 19961706 19962752 19964526 19990265 20021574 20022703
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;   /*B03BB01*/
};
#delimit cr

replace POS2=1 if substr(ATC0,1,5)=="C07AA" & Periodo>=2016
replace POS2=1 if substr(ATC0,1,5)=="C09AA" & Periodo>=2016

*C09CA01 50 and 100mg since 2007
  #delimit ;
foreach num in
32790 58811 59605 59606 206407 215193 219784 224004 224830 225054 19906462 19917705 19929989 19931835 19932984 19938374 19940171 19940653 19941597 19943330 19944682 19945836 19950925 19951605 19956447 19965332 19965499 19970131 19974781 19974783 19975017 19975772 19980115 19983990 19984061 19988266 19989256 19992900 19993040 19996998 19998202 19999986 20001322 20002216 20002258 20002262 20003175 20005114 20005885 20009079 20010197 20019619 20023283 20027707 20027708 20032072 20032566 20032631 20042784 20042807 20044138 20044139 20044885 20044887 20045848 20049336 20049373 20051543 20053069 20059016 20059017 20062603 20062604 20068256 20073966 20081759 20090127 20090128 20090441 20101140 20101909 20101913 20102676
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;   /*C09CA01*/
};
#delimit cr

replace POS2=1 if ATC0=="C09CA01" & Periodo>=2014

*C10AA02 sale en 2016

  #delimit ;
foreach num in
202385 215293 1983837 19900048 19914046 19915568 19925136 19931154 19934142 19938959 19955468 19965074 19987029 19990935 19994394 19996761 20004843 20019142 20063814 20066767
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;   /*G01AF01*/
};
#delimit cr


  #delimit ;
foreach num in
19985092{;
replace POS2=0 if CUM1==`num' & Periodo>=2007;   /*G01AF01*/
};
#delimit cr

*all tablets since 2014 G03AA07
  #delimit ;
foreach num in
19906274 19906276 19946997 19971488 19971833 19971971 19972438 19974735 19979565 19982941 19985113 19985872 19988571 19988755 19990317 19992684 19993160 19997297 19998799 20011474 20014311 20039679 20046074 20068867 20071421 20077143
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;   /*G03AA07*/
};
#delimit cr


*all inyeccion since 2014 J07AM01
  #delimit ;
foreach num in
29151 37414 46202 46209 214394 19902871 19940997 20001952 20013380 20039648 20046007 20053578
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;   /*J07AM01*/
};
#delimit cr


replace POS2=1 if ATC0=="J07BG01" & Periodo>=2012
  #delimit ;
foreach num in
6335 16821 19639 20472 22682 23371 23376 24024 25362 26826 27040 30094 32053 37903 38893 39427 40739 42984 43526 43557 43558 43619 43804 43897 44094 44214 45537 45707 47618 47881 50353 50388 50741 50930 51745 53070 54554 55033 55104 55156 55295 55496 55688 55689 55840 56107 56715 56835 68090 84892 100458 110574 200435 200976 203095 205061 205257 207750 208267 208735 208926 209237 209297 210455 211807 211963 214201 215301 218241 218604 218606 218871 219361 220572 224234 225467 226677 226678 227366 227368 227370 228907 229447 1980645 19902446 19907967 19908171 19913079 19915836 19916659 19916660 19917034 19917659 19919610 19924812 19927400 19931425 19934061 19934264 19934821
19934821 19938305 19940074 19940375 19940578 19942017 19949690 19953332 19953708 19956471 19959901 19960955 19962012 19962418 19962788 19969625 19969696 19972205 19972206 19975170 19977348 19980480 19986823 19987567 19987572 19992569 19999950 20004781 20006964 20007005 20014306 20015603 20018991 20019297 20022477 20022711 20023538 20039508 20046260 20049854 20053291 20054785 20060805 20061397 20074664 20082592 20083129 20108722
{;
replace POS2=0 if CUM1==`num' & Periodo<2014;   /*M01AB05*/
replace POS2=1 if CUM1==`num' & Periodo>=2014;   /*M01AB05*/
};
#delimit cr

  #delimit ;
foreach num in
20472 24024 26826 30094 39427 40739 43526 43558 43804 43897 44094 44214 45537 45707 47618 47881 50388 50930 53070 54554 55033 55104 55295 55688 56715 68090 84892 100458 200976 205061 207750 208267 208735 208926 209237 210455 211807 211963 215301 218241 218604 218871 225467 226677 226678 227368 1980645 19902446 19908171 19913079 19915836 19917659 19924812 19927400 19934061 19934821 19938305 19940074 19940375 19940578 19942017 19949690 19953332 19953708 19959901 19962012 19962418 19969625 19972205 19972206 19975170 19980480 19992569 19999950 20014306 20022477 20046260 20049854 20053291 20060805 20074664 20082592 20083129
{;
replace POS2=0 if CUM1==`num' & Periodo<2007;   /*M01AB05*/
replace POS2=1 if CUM1==`num' & Periodo>=2007;   /*M01AB05*/
};
#delimit cr

  #delimit ;
foreach num in
12080 22684 23275 24025 26560 31408 32375 37902 39148 39844 41277 42280 43525 43632 44093 44678 45520 45619 45992 47824 50080 52422 53102 54531 56720 201277 208152 210546 213950 218243 224236 225065 226224 227524 230149 1983090 19902863 19906630 19912419 19915397 19917658 19923439 19928083 19930673 19934565 19934676 19934768 19935743 19936725 19938558 19938662 19939395 19940261 19943735 19944710 19951658 19953296 19955738 19961628 19961984 19962606 19962627 19964972 19964973 19965096 19968522 19972113 19976172 19983265 19984534 19992190 19992191 20007815 20010698 20014964 20015069 20028343 20053292 20057226 20067651 20079148 20081158 20090901 20094482 20094922
{;
replace POS2=1 if CUM1==`num' & Periodo>=2016;   /*M01AB05*/
};
#delimit cr

*N02BB02 all since 2016
  #delimit ;
foreach num in
22144 33230 34363 36031 45421 49495 54981 211976 212037 213752 213805 219605 226260 1980535 19902781 19903818 19907058 19910760 19922562 19925842 19926125 19933145 19934459 19936280 19938608 19940147 19942696 19948403 19948404 19956382 19956383 19962731 19966738 19967690 19970691 19971851 19976588 19984508 19984509 19984510 19992155 19993029 19993030 19993031 19993036 19993038 20005020 20006845 20007276 20087766
{;
replace POS2=1 if CUM1==`num' & Periodo>=2016;   /*N02BB02*/
};
#delimit cr

*N02BE01 500mg since 2007
  #delimit ;
foreach num in
15100 24848 25040 25732 26401 29314 29561 29883 31911 32785 33288 34223 35539 37816 38737 38867 40193 42103 42572 43262 43560 43814 44014 44348 44471 44577 44977 45417 45597 46000 46893 51514 51671 52037 52478 53058 53560 54666 56395 56628 206744 207410 213786 217338 218873 219817 226002 226208 227348 229288 230676 19904043 19904924 19907353 19907895 19907946 19908969 19913329 19914355 19914655 19916212 19926645 19927360 19927639 19930545 19935303 19936063 19936309 19936408 19940411 19941414 19942077 19942289 19950659 19953509 19954763 19959771 19960696 19965340 19972249 19976357 19977458 19978351 19981307 19984803 19986458 20001191 20001400 20001852 20003224 20011033 20013797 20017286 20021699 20022351 20022460 20024323 20024803 20027859 20038520 20039728 20041822 20057285 20057605 20058810 20065478 20068891 20076091 20080388 20082110 20092739 20097132
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007 ;  /*N02BE01*/
};
#delimit cr

*other tablets
  #delimit ;
foreach num in
32187 35861 36461 36537 59133 19908980 19913211 19913212 19932198 19933379 19953195 19969695 19970510 19977884 19983410 19993377 20013950 20069013
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014 ;  /*N02BE01*/
replace POS2=0 if CUM1==`num' & Periodo<2014 ;  /*N02BE01*/
};
#delimit cr


*N06AB03 20mg since 2007
  #delimit ;
foreach num in
7051 10815 29593 38369 38613 38818 39989 40061 41513 45920 47547 50044 53920 56544 57994 58996 59769 200776 201017 201018 205733 206634 212854 226220 1984360 19905764 19907098 19908150 19909016 19913077 19919463 19919478 19923471 19926005 19927830 19932384 19941363 19942819 19950494 19951343 19952767 19953041 19956947 19957566 19959792 19967597 19969413 20007054 20007689 20015903 20042573 20047342 20058254
{;
replace POS2=1 if CUM1==`num' & Periodo>=2007;   /*N06AB03*/
};
#delimit cr

  #delimit ;
foreach num in
53010 53303 225255 19922407 20058253
{;
replace POS2=1 if CUM1==`num' & Periodo>=2014;   /*N06AB03*/
replace POS2=1 if CUM1==`num' & Periodo<2014 ;  /*N06AB03*/
};
#delimit cr



*S01AA30 never in POS
replace POS2=0 if ATC0=="S01AA30"
*A07CA99 always in POS
replace POS2=1 if ATC0=="A07CA99"
  
*since 2007
  #delimit ;
foreach num in
10121 11980 12526 13179 20678 23753 26675 26967 34239 35021 35152 38361 39648 43248 43258 43640 43649 43772 43936 44019 44232 44309 45306 45415 45602 46356 47016 47367 48006 48678 48679 50473 51063 51944 53052 54071 54646 55436 55506 56152 56162 56626 56667 56978 57374 200433 201464 201731 203221 208072 208266 208692 212295 215253 216303 220215 222182 222632 226278 1980398 1983128 1983899 1984148 19904212 19905675 19905733 19907951 19908403 19928467 19929403 19931036 19934892 19939383 19939634 19942964 19943616 19944947 19948549 19949999 19951045 19951050 19951051 19953088 19955409 19960694 19961088 19968292 19968293 19984788 19985112 19985931 19990272 19993317 20005347 20006408 20007872 20011555 20012911 20046655 20053383 20075237 20078492 20083911
48006
210272 
 {;
replace POS2=1 if CUM1==`num' & Periodo>=2007;   /*J01EE01*/
};
#delimit cr

*S01AA18 never in POS
replace POS2=0 if ATC0=="S01AA18"
 
*A03FA01 in all forms since 2014
replace POS2=0 if ATC1=="A03FA01" & Periodo>=2014
 
 *B05DB99 never in POS
replace POS2=0 if ATC0=="B05DB99"
 
 *C01CA07 perenatal since 2016
   #delimit ;
foreach num in
 33470 206613 224055 224994 229013 19929530 19940461 19941379 19942779 19951912 19954773 19957343 19964887 19973360 20014938 20017779 20021433 20024846 20051340 20062748 20068276 20078913 20096467
  {;
replace POS2=1 if CUM1==`num' & Periodo>=2016 ;  /*C01CA07*/
};
#delimit cr


 *J01FA01 500 mg since 2007
   #delimit ;
foreach num in
25798 19951104 19961934 18933 25517 33732 42638 43257 43654 215313 227056 19904211 19913083 19950489 19956045 19994630 20011733 20012030 20022849 20099136 34703 44058 227058 1980542 19914144
  {;
replace POS2=1 if CUM1==`num' & Periodo>=2007 ;  /*J01FA01*/
};
#delimit cr

   #delimit ;
foreach num in
 23668 31064 34658 35369 40433 40629 43922 44161 44527 45564 46813 51134 227061 19945541  19964640
  {;
replace POS2=1 if CUM1==`num' & Periodo>=2014 ;  /*J01FA01*/
replace POS2=0 if CUM1==`num' & Periodo<2014 ;  /*J01FA01*/
};
#delimit cr



*30/40mg J05AF04 since 2007+1 mg/mL solución oral
*all tablets since 2014

   #delimit ;
foreach num in
209773 209775 19904629 19904630 19925220 19926326 19926328 19945669 19947468 19962597 19962598 19979848 19979849 19984480 19987300 19987302 19988258 19988262 19996038 19996039 20023863
  {;
replace POS2=1 if CUM1==`num' & Periodo>=2007 ;  /*J05AF04*/
};
#delimit cr

   #delimit ;
foreach num in
209774 209776 19904482 19925685 19945669 19984480
  {;
  replace POS2=0 if CUM1==`num' & Periodo<2014 ;  /*J05AF04*/
replace POS2=1 if CUM1==`num' & Periodo>=2014 ;  /*J05AF04*/
};
#delimit cr

/*A11GA01 desde 2007 
100 mg/mL solución oral
500 mg tableta
500 mg/5 mL solución inyectable
*/
*2014 all tablets
*2016 all inyeccions

   #delimit ;
foreach num in
4098 30865 33812 44404 52051 53374 54932 55261 56345 58111 212835 224156 225389 227253 228934 19901592 19905179 19905274 19907350 19907351 19913542 19919787 19928431 19931723 19935011 19942254 19943161 19944583 19945297 19951357 19954857 19961871 19978032 19988249 19991526 20004086 20007547 20036454 20066125 20089447
 25317 56094 59452 201753 218592 19926091 19926153 19931401 19949644 19953959 19991526 20009449 20024251
 19933335 19943196 19970475 19999686 20062021 20098640 20046087
  29159 30490 31950 44434 229050 19904975 19966774 19969771 19996463 20012298 20020102 20046442 20056861 19973813
19982839
 {;
  replace POS2=0 if CUM1==`num' & Periodo<2007 ;  /*A11GA01*/
replace POS2=1 if CUM1==`num' & Periodo>=2007 ;  /*A11GA01*/
};
#delimit cr

   #delimit ;
foreach num in
3395 30864 32027 38635 47942 47943 50233 50349 52051 54849 55321 208066 208983 209804 215325 222169 223243 225389 228930 229859 19905034 19913541 19921537 19924962 19927412 19942648 19943420 19946787 19946791 19950727 19961869 19962026 19962142 19966321 19975743 20006895 20007548 20013734 20013837 20013875 20021445 20024687 20036067
 {;
  replace POS2=0 if CUM1==`num' & Periodo<2014 ;  /*A11GA01*/
replace POS2=1 if CUM1==`num' & Periodo>=2014 ;  /*A11GA01*/
};
#delimit cr

   #delimit ;
foreach num in
20011399 20013865 20032822 20058243 {;
  replace POS2=0 if CUM1==`num' & Periodo<2016 ;  /*A11GA01*/
replace POS2=1 if CUM1==`num' & Periodo>=2016 ;  /*A11GA01*/
};
#delimit cr

   #delimit ;
foreach num in
17279 21740 29159 30050 31950 44434 49114 225218 228441 1980705 19904975 19908620 19933110 19940341 19942619 19959363 19964790 19966774 19969771 20020102 20056861
31305 49539 58652 59661 19970342 20046820 20092139
{;
replace POS2=0 if CUM1==`num' ;  /*A11GA01*/
};
#delimit cr

/*NAPROXENO-M01AE02 205mg tableta since 2007, and 150 mg/ 5mL (3%)  solucion oral
all tablets since 2014, all soluciones since 2016*/
   #delimit ;
foreach num in
 19953408 17145 31191 33696 39114 43053 43424 43441 44418 51885 54083 55086 55373 55853 56677 56749 200896 200946 210462 216989 19927831 19929066 19930534 19933042 19936394 19938960 19940769 19942470 19944251 19947803 19953331 19953334 19953926 19959596 19969070 19973313 19980479 19984793 19991177 20006034 20018558 20023365 20037830 20047766 20052534 20060738 20071585 20071587 20071604 20086152 20087038 20090034
 19941950 19963174 19968117 19986585 20013112
 {;
  replace POS2=0 if CUM1==`num' & Periodo<2007 ;  /*M01AE02*/
replace POS2=1 if CUM1==`num' & Periodo>=2007 ;  /*M01AE02*/
};
#delimit cr

   #delimit ;
foreach num in
 11933 27422 30776 31190 40600 41772 43435 43584 43833 43993 45044 45407 51422 52537 53715 54084 54431 55494 56156 56158 56714 200824 207645 209434 211713 216299 223415 1980142 19904176 19904782 19911657 19915081 19928021 19928023 19929252 19930404 19940765 19946441 19952463 19952578 19953310 19957434 19959833 19960908 19962995 19968992 19990714 20002844 20005159 20007481 20009356 20009614 20017694 20019190 20024731 20028367 20030267 20036866 20037830 20041637 20042824 20052535 20057032 20059823 20061217 20071585 20071587 20071604 20073968 20090033 20090372 20095666 20097730 20097731 20099747 20099748
 {;
  replace POS2=0 if CUM1==`num' & Periodo<2014 ;  /*M01AE02*/
replace POS2=1 if CUM1==`num' & Periodo>=2014 ;  /*M01AE02*/
};
#delimit cr

   #delimit ;
foreach num in
 43730 52735 58037 229080 19927048 19939580 19948281 19956268 19971027 19975151 19983918 20010756 20070956 20077371
 {;
  replace POS2=0 if CUM1==`num' & Periodo<2016 ;  /*M01AE02*/
replace POS2=1 if CUM1==`num' & Periodo>=2016 ;  /*M01AE02*/
};
#delimit cr

   #delimit ;
foreach num in
 43728 53837 55851 56155 57194 19962593
 {;
  replace POS2=0 if CUM1==`num'  ;  /*M01AE02*/
};
#delimit cr

/*C09AA01 en 25 y 50 mg since 2007, other tablets since 2014*/
*for all prartical purposes all since 2007
replace POS2=1 if ATC0=="C09AA01"

*C09AA06 all tablets since 2014, which really means all since 2014
replace POS2=1 if ATC0=="C09AA06" & Periodo>=2014
replace POS2=0 if ATC0=="C09AA06" & Periodo<2014

replace POS2=1 if ATC0=="N03AX14" & Periodo>=2014
replace POS2=0 if ATC0=="N03AX14" & Periodo<2014

*CLINDAMICINA or D10AF01 in 15% since 2007, all inyeccion since 2016 

   #delimit ;
foreach num in
20028341
 {;
  replace POS2=1 if CUM1==`num'  ;  /*D10AF01*/
};
#delimit cr

   #delimit ;
foreach num in
16226
 {;
  replace POS2=1 if CUM1==`num' & Periodo>=2016  ;  /*D10AF01*/
};
#delimit cr




replace POS2=0 if ATC0=="N02BE51"
replace POS2=0 if ATC0=="A11AA03"
replace POS2=0 if ATC0=="V08AA05"
replace POS2=0 if ATC0=="M01AH02"
replace POS2=0 if ATC0=="A08AA10"


replace POS2=0 if ATC0=="R01BA52"
replace POS2=0 if ATC0=="M01AE51"
replace POS2=0 if ATC0=="R06AX11"
replace POS2=0 if ATC0=="N06AX16"
replace POS2=0 if ATC0=="R05CB06"
replace POS2=0 if ATC0=="R05CA03"


replace POS2=0 if ATC0=="D08AX08"
replace POS2=0 if ATC0=="M01AC06"
replace POS2=0 if ATC0=="C05AX04"
replace POS2=0 if ATC0=="A03FA02"
replace POS2=0 if ATC0=="M01AC01"
replace POS2=0 if ATC0=="A08AB01"
replace POS2=0 if ATC0=="M01AH01"
replace POS2=0 if ATC0=="D10AE01"
replace POS2=0 if ATC0=="G03AB06"


replace POS2=0 if ATC0=="A11AA02"
replace POS2=0 if ATC0=="P02CA05"
replace POS2=1 if ATC0=="B05BA01"
replace POS2=0 if ATC0=="N02BA51"
replace POS2=0 if ATC0=="G03CA53"
replace POS2=0 if ATC0=="V08CA03"

replace POS2=0 if ATC0=="A04AD10"
replace POS2=0 if ATC0=="B01AB10"
replace POS2=0 if ATC0=="P03AC54"
replace POS2=0 if ATC0=="G03CA09"
replace POS2=0 if ATC0=="B06AA02"

replace POS2=0 if ATC0=="B01AD04"
replace POS2=0 if ATC0=="C10AA06"
replace POS2=0 if ATC0=="A10BD09"
replace POS2=0 if ATC0=="C09DX02"
replace POS2=0 if ATC0=="B03AA03"
replace POS2=0 if ATC0=="C08CA12"
replace POS2=0 if ATC0=="M01AB06"
replace POS2=0 if ATC0=="B06AA02"
replace POS2=0 if ATC0=="C05AA05"
replace POS2=0 if ATC0=="R06AB52"
replace POS2=0 if ATC0=="G04CA04"
replace POS2=0 if ATC0=="R01BA02"
replace POS2=0 if ATC0=="G04CB02"
replace POS2=0 if ATC0=="R06AX27"

replace POS2=0 if ATC0=="A10BH01"
replace POS2=0 if ATC0=="J01FF02"



*HIOSCINA o A03BB01
/*
10 mg gragea
20 mg/ml  soluci6n inyectable
(0,020 + 2,5)g/5 ml soluci6n inyectable
all talets since 2014
all perenatal since 2016
*/

*since 2007
#delimit ;
foreach cum in
17023 29374 54392 56113 19926739 19949535 19949799 19955212 19955410 19955834 19961361 19962505 19963375 19966187 19973908 19983149 19987296 19996477 19997697 20019759 20047056 2004880
17024 29373 212035 217210 225064 19903695 19920900 19924391 19927114 19948844 19951133 19959401 19960732 19976540 19977450 19985976 19992695 19997616 19997619
{;
replace POS2=1 if CUM1==`cum';
};
#delimit cr

*since 2012

#delimit ;
foreach cum in
19997697 20057197
{;
replace POS2=1 if CUM1==`cum' & Periodo>=2014;
};
#delimit cr




gen ATC=ATC0
merge m:1 ATC using "$base_out/MedicamentosPOS_FULLATC.dta"
drop if _merge==2
gen _merge1=_merge
drop _merge
replace ATC=substr(ATC,1,5)
merge m:1 ATC using "$base_out/MedicamentosPOS_ATC5.dta", update replace
drop if _merge==2
gen _merge2=_merge
drop _merge
replace ATC=substr(ATC,1,4)
merge m:1 ATC using "$base_out/MedicamentosPOS_ATC4.dta", update  replace
drop if _merge==2
gen _merge3=_merge
drop _merge


bys CUM1: egen primeranopos=min(Periodo) if POS2==1
bys CUM1: replace primeranopos=primeranopos[_n-1] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n+1] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n-1] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n+2] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n-1] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n+3] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n-1] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n+4] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n-1] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n+5] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n-1] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n+6] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n-1] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n+7] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n-1] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n+8] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n-1] if primeranopos==.
bys CUM1: replace primeranopos=primeranopos[_n+9] if primeranopos==.

tab primeranopos if _merge1==1 & _merge2==1 & _merge3==1  

bys CUM1: egen algunavezpos=max(POS2)
bys CUM1: egen algunaveznopos=min(POS2)

tab ATC0 if primeranopos==2011 & algunavezpos!=algunaveznopos & _merge1==1 & _merge2==1 & _merge3==1 
tab ATC0 if primeranopos==2013 & algunavezpos!=algunaveznopos & _merge1==1 & _merge2==1 & _merge3==1 


*replace POS2=0 if _merge1==1 & _merge2==1 & _merge3==1  & algunavezpos!=algunaveznopos  /* NOT IN ANY OF THE "MODREN"*/
foreach atccode in D01BA01 P01AB07 L03AA02 A12CX01 R01AD09 C10AX06 C10AX09{
replace POS2=0 if ATC0=="`atccode'"
}

*replace POS2=0 if pos2010==0 & pos2012==1  & Periodo<2012 & POS2==1 & primeranopos>=2010 & ATC0!="M05BA04" & ATC0!="B01AC04" /*in 2012, but not 2010, and yet they claim to enter after 2010... must be wrong */
*replace POS2=0 if pos2010==0  & Periodo<2012 /* not in the 2010 resolucion, therefore not in POS until 2012 for sure*/
*replace POS2=0 if pos2010==0 & pos2012==0  & Periodo<2014 & algunavezpos!=algunaveznopos  /* not in the 2010 or 2012 resolucion, therefore not in POS until 2014 for sure*/
*replace POS2=0 if pos2010==0 & pos2012==0 & pos2014==0  & Periodo<2014 & algunavezpos!=algunaveznopos  /* not in the 2010 or 2012 or 2014 resolucion, therefore not in POS until 2015 for sure*/
*replace POS2=0 if pos2010==0 & pos2012==0 & pos2014==0 & pos2015==0  & Periodo<2016 & algunavezpos!=algunaveznopos  /* not in the 2010 or 2012 or 2014 or 2015 resolucion, therefore not in POS until 2016 for sure*/
*replace POS2=0 if pos2010==0 & pos2012==0 & pos2014==0 & pos2015==0 & pos2016==0  & Periodo<2017 & algunavezpos!=algunaveznopos  /* not in the 2010 or 2012 or 2014 or 2015 or 2016 resolucion, therefore not in POS until 2016 for sure*/
drop primeranopos algunavezpos algunaveznopos
drop pos2010-_merge3
drop ATC


/*
replace POS2=1 if CUM1==229243 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=0 if CUM1==19900781 & Periodo==2010 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19908725 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19908725 & Periodo==2013 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19981307 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19987914 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19927084 & Periodo==2012 & POS2!=0 & POS2!=1 
replace POS2=1 if CUM1==19968904  & POS2!=0 & POS2!=1 
*/




replace POS2=. if POS2!=0 & POS2!=1

tsset CUM1 Periodo
by CUM1: replace POS2=1 if POS2==0 & POS2[_n-1]==1 & POS2[_n+1]==1 & !missing(POS2[_n+1]) & !missing(POS2[_n-1])
by CUM1: replace POS2=0 if POS2==1 & POS2[_n-1]==0 & POS2[_n+1]==0 & !missing(POS2[_n+1]) & !missing(POS2[_n-1])

by CUM1: replace POS2=1 if POS2==0 & POS2[_n-2]==1 & POS2[_n+2]==1 & !missing(POS2[_n+2]) & !missing(POS2[_n-2])
by CUM1: replace POS2=0 if POS2==1 & POS2[_n-2]==0 & POS2[_n+2]==0 & !missing(POS2[_n+2]) & !missing(POS2[_n-2])

by CUM1: replace POS2=1 if POS2==0 & POS2[_n-1]==1 & POS2[_n+1]==1 & !missing(POS2[_n+1]) & !missing(POS2[_n-1])
by CUM1: replace POS2=0 if POS2==1 & POS2[_n-1]==0 & POS2[_n+1]==0 & !missing(POS2[_n+1]) & !missing(POS2[_n-1])

by CUM1: replace POS2=1 if POS2==0 & POS2[_n-3]==1 & POS2[_n+3]==1 & !missing(POS2[_n+3]) & !missing(POS2[_n-3])
by CUM1: replace POS2=0 if POS2==1 & POS2[_n-3]==0 & POS2[_n+3]==0 & !missing(POS2[_n+3]) & !missing(POS2[_n-3])

by CUM1: replace POS2=1 if POS2==0 & POS2[_n-2]==1 & POS2[_n+2]==1 & !missing(POS2[_n+2]) & !missing(POS2[_n-2])
by CUM1: replace POS2=0 if POS2==1 & POS2[_n-2]==0 & POS2[_n+2]==0 & !missing(POS2[_n+2]) & !missing(POS2[_n-2])

by CUM1: replace POS2=1 if POS2==0 & POS2[_n-1]==1 & POS2[_n+1]==1 & !missing(POS2[_n+1]) & !missing(POS2[_n-1])
by CUM1: replace POS2=0 if POS2==1 & POS2[_n-1]==0 & POS2[_n+1]==0 & !missing(POS2[_n+1]) & !missing(POS2[_n-1])


by CUM1: ipolate VMR cont,  generate(VMR2)
replace VMR2=. if VMR2!=0 & VMR2!=1 & VMR==.
compress

merge m:1 CUM1 Periodo using "$base_out/PresentationsCUM1.dta"
drop _merge
save "$base_out/Medicamentos_CUM.dta",replace


use "$base_out/Medicamentos_TotalFill.dta" ,clear

replace Institucional_Lab_Prom_STD=. if MediID=="19939440 1" /*only two data points, 2007 and 2009, and they are exactly the same... dont trust it*/
replace Institucional_Lab_Unidades_STD=. if MediID=="19939440 1" /*only two data points, 2007 and 2009, and they are exactly the same... dont trust it*/
#delimit ;
foreach cum in
4098
32667
38884
43910
44405
48062
48618
50423
57641
218019
219277
19905554
19927114
19931051
19935853
19936001
19941379
19949649
19950100
19965075
19966780
19969030
19989121
20006590
20009441
{;
replace Institucional_Lab_Prom_STD=. if CUM1==`cum';
replace Institucional_Lab_Unidades_STD=. if CUM1==`cum';
};
#delimit cr

foreach var in  Institucional_Lab Institucional_May {

gen double `var'_Valor=`var'_Unidades_STD*`var'_Prom_STD
replace `var'_Valor=. if `var'_Unidades_STD==. | `var'_Unidades_STD==0
}


collapse (firstnm) NombreRegistro  (sum) Institucional_Lab_Valor Institucional_May_Valor  Institucional_Lab_Unidades_STD Institucional_May_Unidades_STD, by(CUM1 Periodo)

merge m:1 CUM1 using "$base_out/DataGenerico_Final.dta"
drop if _merge==2
drop _merge

gen FranjaVerde=(Franja=="VERDE" | Franja=="VERDE - VIOLETA") & !missing(Franja)
gen FranjaVioleta=(Franja=="VIOLETA" | Franja=="VERDE - VIOLETA") & !missing(Franja)
gen OTC=(CondicionVenta=="SIN FORMULA FACULTATIVA") & !missing(CondicionVenta)


foreach var in  Institucional_Lab Institucional_May  {
replace `var'_Unidades_STD=. if `var'_Unidades_STD==0
replace `var'_Valor=. if `var'_Unidades_STD==. | `var'_Unidades_STD==0
}
compress
merge 1:1 CUM1 Periodo using "$base_out/Medicamentos_CUM.dta"
drop _merge
save "$base_out/Medicamentos_CUM.dta",replace

use "$base_out/Medicamentos_Total.dta" ,clear

replace VMR=min(VMR+PMV,1)
replace Institucional_Lab_Prom_STD=. if MediID=="19939440 1" /*only two data points, 2007 and 2009, and they are exactly the same... dont trust it*/
replace Institucional_Lab_Unidades_STD=. if MediID=="19939440 1" /*only two data points, 2007 and 2009, and they are exactly the same... dont trust it*/
#delimit ;
foreach cum in
4098
32667
38884
43910
44405
48062
48618
50423
57641
218019
219277
19905554
19927114
19931051
19935853
19936001
19941379
19949649
19950100
19965075
19966780
19969030
19989121
20006590
20009441
{;
replace Institucional_Lab_Prom_STD=. if CUM1==`cum';
replace Institucional_Lab_Unidades_STD=. if CUM1==`cum';
};
#delimit cr

*Clean weird prices (i.e., turn them into NA)
foreach canal in  Lab May  {
	foreach tipo in  Comercial Institucional  {
		capture drop diff
		capture drop tipo1
		capture drop tipo2
		capture drop tipo3
		capture drop tipo4
		gen diff=(`tipo'_`canal'_Max-`tipo'_`canal'_Min)/`tipo'_`canal'_Min
		qui sum diff,d
		gen tipo1=(diff>r(p99) & !missing(diff))
		capture drop diff
		gen diff=(`tipo'_`canal'_Max-`tipo'_`canal'_Min)
		qui sum diff,d
		gen tipo2=(diff>r(p99) & !missing(diff))
		gen tipo3=(((Institucional_Lab_Max_STD+0.1)<Institucional_Lab_Prom_STD | (Institucional_Lab_Min_STD-0.1)>Institucional_Lab_Prom_STD) & !missing(Institucional_Lab_Prom_STD))
		gen tipo4=(mod(Institucional_Lab_Unidades,1)!=0 & Institucional_Lab_Unidades!=.)
		replace `tipo'_`canal'_Prom_STD=. if tipo1==1 | tipo2==1 | tipo3==1 | tipo4==1
		replace `tipo'_`canal'_Unidades_STD=. if tipo1==1 | tipo2==1 | tipo3==1 | tipo4==1
	}
}

foreach var in  Institucional_Lab Institucional_May  {
	replace `var'_Prom_STD=. if `var'_Unidades_STD==0 | `var'_Unidades_STD==.
}

gen FECHA_EXPEDICION2 = date(FECHA_EXPEDICION, "YMD")
format FECHA_EXPEDICION2 %td
gen FECHA_VENCIMIENTO2 = date(FECHA_VENCIMIENTO, "YMD")
format FECHA_VENCIMIENTO2 %td		

gen FECHA_ACTIVO2 = date(FECHA_ACTIVO, "YMD")
format FECHA_ACTIVO2 %td
gen FECHA_INACTIVO2 = date(FECHA_INACTIVO, "YMD")
format FECHA_INACTIVO2 %td	
gen AnosExpedicion=Periodo-year(FECHA_EXPEDICION2)
gen YearExpedicion=year(FECHA_EXPEDICION2)
gen YearVencimiento=year(FECHA_VENCIMIENTO2)
gen YearActivo=year(FECHA_ACTIVO2)
gen YearInactivo=year(FECHA_INACTIVO2)
*drop if Periodo<max(YearExpedicion,YearActivo) & Institucional_Lab_Prom_STD==.
*drop if Periodo>min(YearVencimiento,YearInactivo)


collapse  (mean) Institucional_Lab_Prom_STD [aw=Institucional_Lab_Unidades_STD], by(CUM1 Periodo)
compress
merge 1:1 CUM1 Periodo using "$base_out/Medicamentos_CUM.dta"
drop _merge
save "$base_out/Medicamentos_CUM.dta",replace

use "$base_out/Medicamentos_Total.dta" ,clear


replace VMR=min(VMR+PMV,1)
replace Institucional_Lab_Prom_STD=. if MediID=="19939440 1" /*only two data points, 2007 and 2009, and they are exactly the same... dont trust it*/
replace Institucional_Lab_Unidades_STD=. if MediID=="19939440 1" /*only two data points, 2007 and 2009, and they are exactly the same... dont trust it*/
#delimit ;
foreach cum in
4098
32667
38884
43910
44405
48062
48618
50423
57641
218019
219277
19905554
19927114
19931051
19935853
19936001
19941379
19949649
19950100
19965075
19966780
19969030
19989121
20006590
20009441
{;
replace Institucional_Lab_Prom_STD=. if CUM1==`cum';
replace Institucional_Lab_Unidades_STD=. if CUM1==`cum';
};
#delimit cr






*Clean weird prices (i.e., turn them into NA)
foreach canal in  Lab May  {
	foreach tipo in  Comercial Institucional  {
		capture drop diff
		capture drop tipo1
		capture drop tipo2
		capture drop tipo3
		capture drop tipo4
		gen diff=(`tipo'_`canal'_Max-`tipo'_`canal'_Min)/`tipo'_`canal'_Min
		qui sum diff,d
		gen tipo1=(diff>r(p99) & !missing(diff))
		capture drop diff
		gen diff=(`tipo'_`canal'_Max-`tipo'_`canal'_Min)
		qui sum diff,d
		gen tipo2=(diff>r(p99) & !missing(diff))
		gen tipo3=(((Institucional_Lab_Max_STD+0.1)<Institucional_Lab_Prom_STD | (Institucional_Lab_Min_STD-0.1)>Institucional_Lab_Prom_STD) & !missing(Institucional_Lab_Prom_STD))
		gen tipo4=(mod(Institucional_Lab_Unidades,1)!=0 & Institucional_Lab_Unidades!=.)
		replace `tipo'_`canal'_Prom_STD=. if tipo1==1 | tipo2==1 | tipo3==1 | tipo4==1
		replace `tipo'_`canal'_Unidades_STD=. if tipo1==1 | tipo2==1 | tipo3==1 | tipo4==1
	}
}

foreach var in  Institucional_Lab Institucional_May  {
	replace `var'_Prom_STD=. if `var'_Unidades_STD==0 | `var'_Unidades_STD==.
}

gen FECHA_EXPEDICION2 = date(FECHA_EXPEDICION, "YMD")
format FECHA_EXPEDICION2 %td
gen FECHA_VENCIMIENTO2 = date(FECHA_VENCIMIENTO, "YMD")
format FECHA_VENCIMIENTO2 %td		

gen FECHA_ACTIVO2 = date(FECHA_ACTIVO, "YMD")
format FECHA_ACTIVO2 %td
gen FECHA_INACTIVO2 = date(FECHA_INACTIVO, "YMD")
format FECHA_INACTIVO2 %td	
gen AnosExpedicion=Periodo-year(FECHA_EXPEDICION2)
gen YearExpedicion=year(FECHA_EXPEDICION2)
gen YearVencimiento=year(FECHA_VENCIMIENTO2)
gen YearActivo=year(FECHA_ACTIVO2)
gen YearInactivo=year(FECHA_INACTIVO2)
*drop if Periodo<max(YearExpedicion,YearActivo) & Institucional_Lab_Prom_STD==.
*drop if Periodo>min(YearVencimiento,YearInactivo)

collapse  (mean) Institucional_May_Prom_STD [aw=Institucional_May_Unidades_STD], by(CUM1 Periodo)
compress
merge 1:1 CUM1 Periodo using "$base_out/Medicamentos_CUM.dta"
drop _merge
save "$base_out/Medicamentos_CUM.dta",replace
xtset CUM1 Periodo, yearly
compress
drop if ATC0==""
drop if Periodo==2006
save "$base_out/Medicamentos_CUM.dta",replace

replace ATC3="" if ATC3==ATC2 | ATC3==ATC1 | ATC3==ATC0
replace ATC2="" if  ATC2==ATC1 | ATC2==ATC0
replace ATC1="" if  ATC1==ATC0

gen ATC_Grupo0=substr(ATC0,1,5)
gen ATC_Grupo1=substr(ATC1,1,5)
gen ATC_Grupo2=substr(ATC2,1,5)
gen ATC_Grupo3=substr(ATC3,1,5)

replace ATC_Grupo3="" if ATC_Grupo3==ATC_Grupo2 | ATC_Grupo3==ATC_Grupo1 | ATC_Grupo3==ATC_Grupo0
replace ATC_Grupo2="" if  ATC_Grupo2==ATC_Grupo1 | ATC_Grupo2==ATC_Grupo0
replace ATC_Grupo1="" if  ATC_Grupo1==ATC_Grupo0

*First lets calculate HH-index
forvalues j=0/3{
preserve
drop if ATC_Grupo`j'==""
keep ATC_Grupo`j' Institucional_Lab_Valor Institucional_May_Valor  Periodo
rename ATC_Grupo`j' ATC_Grupo
save "$base_out/VentasGroup`j'.dta",replace
restore
preserve
drop if ATC_Grupo`j'=="" 
drop if POS==1
keep ATC_Grupo`j' Institucional_Lab_Valor Institucional_May_Valor  Periodo
rename ATC_Grupo`j' ATC_Grupo
save "$base_out/VentasGroup`j'_NOPOS.dta",replace
restore
preserve
drop if ATC_Grupo`j'=="" 
drop if POS==0
keep ATC_Grupo`j' Institucional_Lab_Valor Institucional_May_Valor  Periodo
rename ATC_Grupo`j' ATC_Grupo
save "$base_out/VentasGroup`j'_POS.dta",replace
restore
preserve
drop if ATC`j'==""
keep ATC`j' Institucional_Lab_Valor Institucional_May_Valor Periodo
rename ATC`j' ATC
save "$base_out/VentasATC`j'.dta",replace
restore
preserve
drop if ATC`j'=="" 
drop if POS==1
keep ATC`j' Institucional_Lab_Valor Institucional_May_Valor Periodo
rename ATC`j' ATC
save "$base_out/VentasATC`j'_NOPOS.dta",replace
restore
preserve
drop if ATC`j'=="" 
drop if POS==0
keep ATC`j' Institucional_Lab_Valor Institucional_May_Valor Periodo
rename ATC`j' ATC
save "$base_out/VentasATC`j'_POS.dta",replace
restore
}

preserve
use "$base_out/VentasATC0.dta", clear
append using "$base_out/VentasATC1.dta"
append using "$base_out/VentasATC2.dta"
append using "$base_out/VentasATC3.dta"
bysort ATC Periodo: egen  ValorTotal_Lab=total(Institucional_Lab_Valor), missing 
bysort ATC Periodo: egen  ValorTotal_May=total(Institucional_May_Valor), missing 
generate HHATC_Lab= (Institucional_Lab_Valor/ ValorTotal_Lab)^2 
generate HHATC_May= (Institucional_May_Valor/ ValorTotal_May)^2
collapse (sum)  HHATC_Lab HHATC_May, by( ATC Periodo)
replace HHATC_Lab=. if HHATC_Lab==0
replace HHATC_May=. if HHATC_May==0
rename ATC ATC0
rename HHATC_Lab HHATC0_Lab
rename HHATC_May HHATC0_May
save "$base_out/HHATC0.dta",replace
rename ATC0 ATC1
rename HHATC0_Lab HHATC1_Lab
rename HHATC0_May HHATC1_May
save "$base_out/HHATC1.dta",replace
rename ATC1 ATC2
rename HHATC1_Lab HHATC2_Lab
rename HHATC1_May HHATC2_May
save "$base_out/HHATC2.dta",replace
rename ATC2 ATC3
rename HHATC2_Lab HHATC3_Lab
rename HHATC2_May HHATC3_May
save "$base_out/HHATC3.dta",replace

use "$base_out/VentasGroup0.dta", clear
append using "$base_out/VentasGroup1.dta"
append using "$base_out/VentasGroup2.dta"
append using "$base_out/VentasGroup2.dta"



bysort ATC_Grupo Periodo: egen  ValorTotal_Lab=total(Institucional_Lab_Valor), missing 
bysort ATC_Grupo Periodo: egen  ValorTotal_May=total(Institucional_May_Valor), missing 
generate HHGrupo_Lab= (Institucional_Lab_Valor/ ValorTotal_Lab)^2 
generate HHGrupo_May= (Institucional_May_Valor/ ValorTotal_May)^2
collapse (sum)  HHGrupo_Lab HHGrupo_May, by(ATC_Grupo Periodo)
replace HHGrupo_Lab=. if HHGrupo_Lab==0
replace HHGrupo_May=. if HHGrupo_May==0
rename ATC_Grupo ATC_Grupo0
rename HHGrupo_Lab HHGrupo0_Lab
rename HHGrupo_May HHGrupo0_May
save "$base_out/HHGrupo0.dta",replace
rename ATC_Grupo0 ATC_Grupo1
rename HHGrupo0_Lab HHGrupo1_Lab
rename HHGrupo0_May HHGrupo1_May
save "$base_out/HHGrupo1.dta",replace
rename ATC_Grupo1 ATC_Grupo2
rename HHGrupo1_Lab HHGrupo2_Lab
rename HHGrupo1_May HHGrupo2_May
save "$base_out/HHGrupo2.dta",replace
rename ATC_Grupo2 ATC_Grupo3
rename HHGrupo2_Lab HHGrupo3_Lab
rename HHGrupo2_May HHGrupo3_May
save "$base_out/HHGrupo3.dta",replace

restore


preserve
use "$base_out/VentasATC0_NOPOS.dta", clear
append using "$base_out/VentasATC1_NOPOS.dta"
append using "$base_out/VentasATC2_NOPOS.dta"
append using "$base_out/VentasATC3_NOPOS.dta"
bysort ATC Periodo: egen  ValorTotal_Lab=total(Institucional_Lab_Valor), missing 
bysort ATC Periodo: egen  ValorTotal_May=total(Institucional_May_Valor), missing 
generate HHATC_Lab= (Institucional_Lab_Valor/ ValorTotal_Lab)^2 
generate HHATC_May= (Institucional_May_Valor/ ValorTotal_May)^2
collapse (sum)  HHATC_Lab HHATC_May, by( ATC Periodo)
replace HHATC_Lab=. if HHATC_Lab==0
replace HHATC_May=. if HHATC_May==0
rename ATC ATC0
rename HHATC_Lab HHATC0_Lab_NOPOS
rename HHATC_May HHATC0_May_NOPOS
save "$base_out/HHATC0_NOPOS.dta",replace
rename ATC0 ATC1
rename HHATC0_Lab HHATC1_Lab_NOPOS
rename HHATC0_May HHATC1_May_NOPOS
save "$base_out/HHATC1_NOPOS.dta",replace
rename ATC1 ATC2
rename HHATC1_Lab HHATC2_Lab_NOPOS
rename HHATC1_May HHATC2_May_NOPOS
save "$base_out/HHATC2_NOPOS.dta",replace
rename ATC2 ATC3
rename HHATC2_Lab HHATC3_Lab_NOPOS
rename HHATC2_May HHATC3_May_NOPOS
save "$base_out/HHATC3_NOPOS.dta",replace

use "$base_out/VentasGroup0_NOPOS.dta", clear
append using "$base_out/VentasGroup1_NOPOS.dta"
append using "$base_out/VentasGroup2_NOPOS.dta"
append using "$base_out/VentasGroup2_NOPOS.dta"



bysort ATC_Grupo Periodo: egen  ValorTotal_Lab=total(Institucional_Lab_Valor), missing 
bysort ATC_Grupo Periodo: egen  ValorTotal_May=total(Institucional_May_Valor), missing 
generate HHGrupo_Lab= (Institucional_Lab_Valor/ ValorTotal_Lab)^2 
generate HHGrupo_May= (Institucional_May_Valor/ ValorTotal_May)^2
collapse (sum)  HHGrupo_Lab HHGrupo_May, by(ATC_Grupo Periodo)
replace HHGrupo_Lab=. if HHGrupo_Lab==0
replace HHGrupo_May=. if HHGrupo_May==0
rename ATC_Grupo ATC_Grupo0
rename HHGrupo_Lab HHGrupo0_Lab_NOPOS
rename HHGrupo_May HHGrupo0_May_NOPOS
save "$base_out/HHGrupo0_NOPOS.dta",replace
rename ATC_Grupo0 ATC_Grupo1
rename HHGrupo0_Lab HHGrupo1_Lab_NOPOS
rename HHGrupo0_May HHGrupo1_May_NOPOS
save "$base_out/HHGrupo1_NOPOS.dta",replace
rename ATC_Grupo1 ATC_Grupo2
rename HHGrupo1_Lab HHGrupo2_Lab_NOPOS
rename HHGrupo1_May HHGrupo2_May_NOPOS
save "$base_out/HHGrupo2_NOPOS.dta",replace
rename ATC_Grupo2 ATC_Grupo3
rename HHGrupo2_Lab HHGrupo3_Lab_NOPOS
rename HHGrupo2_May HHGrupo3_May_NOPOS
save "$base_out/HHGrupo3_NOPOS.dta",replace

restore


preserve
use "$base_out/VentasATC0_POS.dta", clear
append using "$base_out/VentasATC1_POS.dta"
append using "$base_out/VentasATC2_POS.dta"
append using "$base_out/VentasATC3_POS.dta"
bysort ATC Periodo: egen  ValorTotal_Lab=total(Institucional_Lab_Valor), missing 
bysort ATC Periodo: egen  ValorTotal_May=total(Institucional_May_Valor), missing 
generate HHATC_Lab= (Institucional_Lab_Valor/ ValorTotal_Lab)^2 
generate HHATC_May= (Institucional_May_Valor/ ValorTotal_May)^2
collapse (sum)  HHATC_Lab HHATC_May, by( ATC Periodo)
replace HHATC_Lab=. if HHATC_Lab==0
replace HHATC_May=. if HHATC_May==0
rename ATC ATC0
rename HHATC_Lab HHATC0_Lab_POS
rename HHATC_May HHATC0_May_POS
save "$base_out/HHATC0_POS.dta",replace
rename ATC0 ATC1
rename HHATC0_Lab HHATC1_Lab_POS
rename HHATC0_May HHATC1_May_POS
save "$base_out/HHATC1_POS.dta",replace
rename ATC1 ATC2
rename HHATC1_Lab HHATC2_Lab_POS
rename HHATC1_May HHATC2_May_POS
save "$base_out/HHATC2_POS.dta",replace
rename ATC2 ATC3
rename HHATC2_Lab HHATC3_Lab_POS
rename HHATC2_May HHATC3_May_POS
save "$base_out/HHATC3_POS.dta",replace

use "$base_out/VentasGroup0_POS.dta", clear
append using "$base_out/VentasGroup1_POS.dta"
append using "$base_out/VentasGroup2_POS.dta"
append using "$base_out/VentasGroup2_POS.dta"



bysort ATC_Grupo Periodo: egen  ValorTotal_Lab=total(Institucional_Lab_Valor), missing 
bysort ATC_Grupo Periodo: egen  ValorTotal_May=total(Institucional_May_Valor), missing 
generate HHGrupo_Lab= (Institucional_Lab_Valor/ ValorTotal_Lab)^2 
generate HHGrupo_May= (Institucional_May_Valor/ ValorTotal_May)^2
collapse (sum)  HHGrupo_Lab HHGrupo_May, by(ATC_Grupo Periodo)
replace HHGrupo_Lab=. if HHGrupo_Lab==0
replace HHGrupo_May=. if HHGrupo_May==0
rename ATC_Grupo ATC_Grupo0
rename HHGrupo_Lab HHGrupo0_Lab_POS
rename HHGrupo_May HHGrupo0_May_POS
save "$base_out/HHGrupo0_POS.dta",replace
rename ATC_Grupo0 ATC_Grupo1
rename HHGrupo0_Lab HHGrupo1_Lab_POS
rename HHGrupo0_May HHGrupo1_May_POS
save "$base_out/HHGrupo1_POS.dta",replace
rename ATC_Grupo1 ATC_Grupo2
rename HHGrupo1_Lab HHGrupo2_Lab_POS
rename HHGrupo1_May HHGrupo2_May_POS
save "$base_out/HHGrupo2_POS.dta",replace
rename ATC_Grupo2 ATC_Grupo3
rename HHGrupo2_Lab HHGrupo3_Lab_POS
rename HHGrupo2_May HHGrupo3_May_POS
save "$base_out/HHGrupo3_POS.dta",replace

restore

*Now lets calculate sells, and commpetition
forvalues j=0/3{
preserve
drop if Periodo<max(YearExpedicion,YearActivo) & Institucional_Lab_Prom_STD==.
drop if Periodo>min(YearVencimiento,YearInactivo)
drop if ATC_Grupo`j'==""
gen VMR3=(VMR2>0) & !missing(VMR2)
foreach var in Generico POS2 VMR3 Inserto FranjaVerde FranjaVioleta OTC {
bysort ATC_Grupo`j'  Periodo (`var') : gen allmissing`var' = mi(`var'[1]) 
}
collapse  (min) allmissingGenerico allmissingPOS2 allmissingVMR3  allmissingInserto allmissingFranjaVerde allmissingFranjaVioleta allmissingOTC (sum) VentasTotalesGrupo_Lab=Institucional_Lab_Valor VentasTotalesGrupo_May=Institucional_May_Valor NumPOSGrupo=POS2 NumVMRGrupo=VMR3 GenericosGrupo=Generico InsertoGrupo=Inserto FranjaVerdeGrupo=FranjaVerde FranjaVioletaGrupo=FranjaVioleta OTCGrupo=OTC (count) CompetidoresGrupo=CUM1, by(ATC_Grupo`j'  Periodo)
foreach var in  Lab May  {
replace VentasTotalesGrupo_`var'=. if VentasTotalesGrupo_`var'==0
}
replace NumPOSGrupo = . if allmissingPOS2
replace GenericosGrupo = . if allmissingGenerico
replace NumVMRGrupo = . if allmissingVMR3
replace InsertoGrupo = . if allmissingInserto
replace FranjaVerdeGrupo = . if allmissingFranjaVerde
replace FranjaVioletaGrupo = . if allmissingFranjaVioleta
replace OTCGrupo = . if allmissingOTC


rename ATC_Grupo`j' ATC_Grupo
save "$base_out/MediByGroup`j'.dta",replace
restore
preserve
drop if Periodo<max(YearExpedicion,YearActivo) & Institucional_Lab_Prom_STD==.
drop if Periodo>min(YearVencimiento,YearInactivo)
drop if ATC`j'==""
gen VMR3=(VMR2>0) & !missing(VMR2)
foreach var in Generico POS2 VMR3 Inserto FranjaVerde FranjaVioleta OTC {
bysort ATC`j'  Periodo (`var') : gen allmissing`var' = mi(`var'[1]) 
}
collapse (min) allmissingGenerico allmissingPOS2 allmissingVMR3  allmissingInserto allmissingFranjaVerde allmissingFranjaVioleta allmissingOTC (sum) VentasTotalesATC_Lab=Institucional_Lab_Valor VentasTotalesATC_May=Institucional_May_Valor NumPOSATC=POS2 NumVMRATC=VMR3 GenericosATC=Generico InsertoATC=Inserto FranjaVerdeATC=FranjaVerde FranjaVioletaATC=FranjaVioleta OTCATC=OTC (count) CompetidoresATC=CUM1, by(ATC`j' Periodo)
foreach var in  Lab May  {
replace VentasTotalesATC_`var'=. if VentasTotalesATC_`var'==0
}
replace NumPOSATC = . if allmissingPOS2
replace GenericosATC = . if allmissingGenerico
replace NumVMRATC = . if allmissingVMR3
replace InsertoATC = . if allmissingInserto
replace FranjaVerdeATC = . if allmissingFranjaVerde
replace FranjaVioletaATC = . if allmissingFranjaVioleta
replace OTCATC = . if allmissingOTC
rename ATC`j' ATC
save "$base_out/MediByATC`j'.dta",replace
restore
}

preserve
use "$base_out/MediByGroup0.dta", clear
append using "$base_out/MediByGroup1.dta"
append using "$base_out/MediByGroup2.dta"
append using "$base_out/MediByGroup3.dta"
foreach var in NumPOSGrupo NumVMRGrupo CompetidoresGrupo GenericosGrupo InsertoGrupo FranjaVerdeGrupo FranjaVioletaGrupo OTCGrupo{
bysort ATC_Grupo  Periodo (`var') : gen AM`var' = mi(`var'[1]) 
}

collapse  (min) AMNumPOSGrupo AMNumVMRGrupo AMCompetidoresGrupo  AMGenericosGrupo AMInsertoGrupo AMFranjaVerdeGrupo AMFranjaVioletaGrupo AMOTCGrupo  (sum) VentasTotalesGrupo_Lab VentasTotalesGrupo_May  NumPOSGrupo=NumPOSGrupo NumVMRGrupo=NumVMRGrupo CompetidoresGrupo=CompetidoresGrupo GenericosGrupo=GenericosGrupo InsertoGrupo=InsertoGrupo FranjaVerdeGrupo=FranjaVerdeGrupo FranjaVioletaGrupo=FranjaVioletaGrupo OTCGrupo=OTCGrupo, by(ATC_Grupo  Periodo)
foreach var in  Lab May  {
replace VentasTotalesGrupo_`var'=. if VentasTotalesGrupo_`var'==0
}
replace NumPOSGrupo = . if AMNumPOSGrupo
replace NumVMRGrupo = . if AMNumVMRGrupo
replace CompetidoresGrupo = . if AMCompetidoresGrupo
replace GenericosGrupo = . if AMGenericosGrupo
replace InsertoGrupo = . if AMInsertoGrupo
replace FranjaVerdeGrupo = . if AMFranjaVerdeGrupo
replace FranjaVioletaGrupo = . if AMFranjaVioletaGrupo
replace OTCGrupo = . if AMOTCGrupo


rename ATC_Grupo ATC_Grupo0
rename NumPOSGrupo NumPOSGrupo0
rename NumVMRGrupo NumVMRGrupo0
rename CompetidoresGrupo CompetidoresGrupo0
rename VentasTotalesGrupo_Lab VentasTotalesGrupo0_Lab
rename VentasTotalesGrupo_May VentasTotalesGrupo0_May
rename GenericosGrupo GenericosGrupo0
rename InsertoGrupo InsertoGrupo0
rename FranjaVerdeGrupo FranjaVerdeGrupo0
rename FranjaVioletaGrupo FranjaVioletaGrupo0
rename OTCGrupo OTCGrupo0
save "$base_out/MediByGroup0.dta",replace

rename ATC_Grupo0 ATC_Grupo1
rename NumPOSGrupo0 NumPOSGrupo1
rename NumVMRGrupo0 NumVMRGrupo1
rename CompetidoresGrupo0 CompetidoresGrupo1
rename VentasTotalesGrupo0_Lab VentasTotalesGrupo1_Lab
rename VentasTotalesGrupo0_May VentasTotalesGrupo1_May
rename GenericosGrupo0 GenericosGrupo1
rename InsertoGrupo0 InsertoGrupo1
rename FranjaVerdeGrupo0 FranjaVerdeGrupo1
rename FranjaVioletaGrupo0 FranjaVioletaGrupo1
rename OTCGrupo0 OTCGrupo1
save "$base_out/MediByGroup1.dta",replace

rename ATC_Grupo1 ATC_Grupo2
rename NumPOSGrupo1 NumPOSGrupo2
rename NumVMRGrupo1 NumVMRGrupo2
rename CompetidoresGrupo1 CompetidoresGrupo2
rename VentasTotalesGrupo1_Lab VentasTotalesGrupo2_Lab
rename VentasTotalesGrupo1_May VentasTotalesGrupo2_May
rename GenericosGrupo1 GenericosGrupo2
rename InsertoGrupo1 InsertoGrupo2
rename FranjaVerdeGrupo1 FranjaVerdeGrupo2
rename FranjaVioletaGrupo1 FranjaVioletaGrupo2
rename OTCGrupo1 OTCGrupo2
save "$base_out/MediByGroup2.dta",replace

rename ATC_Grupo2 ATC_Grupo3
rename NumPOSGrupo2 NumPOSGrupo3
rename NumVMRGrupo2 NumVMRGrupo3
rename CompetidoresGrupo2 CompetidoresGrupo3
rename VentasTotalesGrupo2_Lab VentasTotalesGrupo3_Lab
rename VentasTotalesGrupo2_May VentasTotalesGrupo3_May
rename GenericosGrupo2 GenericosGrupo3
rename InsertoGrupo2 InsertoGrupo3
rename FranjaVerdeGrupo2 FranjaVerdeGrupo3
rename FranjaVioletaGrupo2 FranjaVioletaGrupo3
rename OTCGrupo2 OTCGrupo3
save "$base_out/MediByGroup3.dta",replace

use "$base_out/MediByATC0.dta", clear
append using "$base_out/MediByATC1.dta"
append using "$base_out/MediByATC2.dta"
append using "$base_out/MediByATC3.dta"

foreach var in NumPOSATC NumVMRATC CompetidoresATC GenericosATC InsertoATC FranjaVerdeATC FranjaVioletaATC OTCATC{
bysort ATC  Periodo (`var') : gen AM`var' = mi(`var'[1]) 
}

collapse  (min) AMNumPOSATC AMNumVMRATC AMCompetidoresATC  AMGenericosATC AMInsertoATC AMFranjaVerdeATC AMFranjaVioletaATC AMOTCATC  (sum) VentasTotalesATC_Lab VentasTotalesATC_May NumPOSATC=NumPOSATC NumVMRATC=NumVMRATC CompetidoresATC=CompetidoresATC GenericosATC=GenericosATC InsertoATC=InsertoATC FranjaVerdeATC=FranjaVerdeATC FranjaVioletaATC=FranjaVioletaATC OTCATC=OTCATC, by(ATC  Periodo)
foreach var in  Lab May  {
replace VentasTotalesATC_`var'=. if VentasTotalesATC_`var'==0
}
replace NumPOSATC = . if AMNumPOSATC
replace NumVMRATC = . if AMNumVMRATC
replace CompetidoresATC = . if AMCompetidoresATC
replace GenericosATC = . if AMGenericosATC
replace InsertoATC = . if AMInsertoATC
replace FranjaVerdeATC = . if AMFranjaVerdeATC
replace FranjaVioletaATC = . if AMFranjaVioletaATC
replace OTCATC = . if AMOTCATC

rename ATC ATC0
rename NumPOSATC NumPOSATC0
rename NumVMRATC NumVMRATC0
rename CompetidoresATC CompetidoresATC0
rename VentasTotalesATC_Lab VentasTotalesATC0_Lab
rename VentasTotalesATC_May VentasTotalesATC0_May
rename GenericosATC GenericosATC0
rename InsertoATC InsertoATC0
rename FranjaVerdeATC FranjaVerdeATC0
rename FranjaVioletaATC FranjaVioletaATC0
rename OTCATC OTCATC0
save "$base_out/MediByATC0.dta",replace

rename ATC0 ATC1
rename NumPOSATC0 NumPOSATC1
rename NumVMRATC0 NumVMRATC1
rename CompetidoresATC0 CompetidoresATC1
rename VentasTotalesATC0_Lab VentasTotalesATC1_Lab
rename VentasTotalesATC0_May VentasTotalesATC1_May
rename GenericosATC0 GenericosATC1
rename InsertoATC0 InsertoATC1
rename FranjaVerdeATC0 FranjaVerdeATC1
rename FranjaVioletaATC0 FranjaVioletaATC1
rename OTCATC0 OTCATC1
save "$base_out/MediByATC1.dta",replace
rename ATC1 ATC2
rename NumPOSATC1 NumPOSATC2
rename NumVMRATC1 NumVMRATC2
rename CompetidoresATC1 CompetidoresATC2
rename VentasTotalesATC1_Lab VentasTotalesATC2_Lab
rename VentasTotalesATC1_May VentasTotalesATC2_May
rename GenericosATC1 GenericosATC2
rename InsertoATC1 InsertoATC2
rename FranjaVerdeATC1 FranjaVerdeATC2
rename FranjaVioletaATC1 FranjaVioletaATC2
rename OTCATC1 OTCATC2
save "$base_out/MediByATC2.dta",replace
rename ATC2 ATC3
rename NumPOSATC2 NumPOSATC3
rename NumVMRATC2 NumVMRATC3
rename CompetidoresATC2 CompetidoresATC3
rename VentasTotalesATC2_Lab VentasTotalesATC3_Lab
rename VentasTotalesATC2_May VentasTotalesATC3_May
rename GenericosATC2 GenericosATC3
rename InsertoATC2 InsertoATC3
rename FranjaVerdeATC2 FranjaVerdeATC3
rename FranjaVioletaATC2 FranjaVioletaATC3
rename OTCATC2 OTCATC3
save "$base_out/MediByATC3.dta",replace

restore


gen VMR3=(VMR2>0) & !missing(VMR2)
preserve
drop if Periodo==2006
collapse (max) PosEver=POS2 (min) NoPosEver=POS2, by(CUM1)
gen AlwaysPOS=(PosEver==1 & NoPosEver==1)
gen NeverPOS=(PosEver==0 & NoPosEver==0)
gen POSChange=(PosEver==1 & NoPosEver==0)
save "$base_out/POSStatus.dta",replace
restore
*drop if POS2==.



preserve
sort CUM1 Periodo
by CUM1: egen max_price= max(Institucional_Lab_Prom_STD)
by CUM1: egen min_price= min(Institucional_Lab_Prom_STD)
gen ratio_price= max_price/min_price
collapse (mean) ratio_price, by(CUM1)
sum ratio_price,d
scalar def ratio_lab_95=r(p95)
scalar def ratio_lab_90=r(p90)
restore


preserve
sort CUM1 Periodo
scalar prop=3
by CUM1: egen max_price= max(Institucional_Lab_Prom_STD)
by CUM1: egen min_price= min(Institucional_Lab_Prom_STD)
gen ratio_price= max_price/min_price
gen VariacionIncreasePrice30_Lab=1 if ratio_price>`=scalar(ratio_lab_95)' & !missing(Institucional_Lab_Prom_STD)
gen VariacionIncreasePrice10_Lab=1 if ratio_price>`=scalar(ratio_lab_90)' & !missing(Institucional_Lab_Prom_STD)
recode VariacionIncreasePrice30_Lab . =0
recode VariacionIncreasePrice10_Lab . =0
tab VariacionIncreasePrice30_Lab
tab VariacionIncreasePrice10_Lab
*replace Institucional_Lab_Prom_STD =Q1_price-`=scalar(prop)'*IC_price if Institucional_Lab_Prom_STD < Q1_price-`=scalar(prop)'*IC_price & VariacionIncreasePrice_Lab==1 & !missing( Institucional_Lab_Prom_STD)
*replace Institucional_Lab_Prom_STD =Q3_price+`=scalar(prop)'*IC_price if Institucional_Lab_Prom_STD> Q3_price+`=scalar(prop)'*IC_price &  VariacionIncreasePrice_Lab==1 & !missing( Institucional_Lab_Prom_STD)

/*
by CUM1: egen Q1_quantity= pctile(Institucional_Lab_Unidades_STD), p(25)
by CUM1: egen Q3_quantity= pctile(Institucional_Lab_Unidades_STD), p(75)
by CUM1: egen  IC_quantity= iqr(Institucional_Lab_Unidades_STD)
gen VariacionIncreaseQuantity_Lab=1 if (Institucional_Lab_Prom_STD< Q1_quantity-`=scalar(prop)'*IC_quantity | Institucional_Lab_Prom_STD> Q3_quantity+`=scalar(prop)'*IC_quantity) & !missing(Institucional_Lab_Unidades_STD)
recode VariacionIncreaseQuantity_Lab . =0
tab VariacionIncreaseQuantity_Lab
replace Institucional_Lab_Unidades_STD =Q1_price-`=scalar(prop)'*IC_price if Institucional_Lab_Unidades_STD < Q1_price-`=scalar(prop)'*IC_price & VariacionIncreaseQuantity_Lab==1 & !missing( Institucional_Lab_Unidades_STD)
replace Institucional_Lab_Unidades_STD =Q3_price+`=scalar(prop)'*IC_price if Institucional_Lab_Unidades_STD> Q3_price+`=scalar(prop)'*IC_price &  VariacionIncreaseQuantity_Lab==1 & !missing( Institucional_Lab_Unidades_STD)
*/


/*
sort CUM1 Periodo
by CUM1: gen IncreasePrice_Lab=D.Institucional_Lab_Prom_STD/L.Institucional_Lab_Prom_STD
sum IncreasePrice_Lab,d
gen VariacionIncreasePrice_Lab=(IncreasePrice_Lab>r(p95) | IncreasePrice_Lab<r(p5)) if !missing(IncreasePrice_Lab)
replace IncreasePrice_Lab=r(p95) if IncreasePrice_Lab>r(p95) & !missing(IncreasePrice_Lab)
replace IncreasePrice_Lab=r(p5) if IncreasePrice_Lab<r(p5) & !missing(IncreasePrice_Lab)
by CUM1: replace Institucional_Lab_Prom_STD= (L.Institucional_Lab_Prom_STD*(1+IncreasePrice_Lab)) if VariacionIncreasePrice_Lab==1
by CUM1: replace IncreasePrice_Lab=D.Institucional_Lab_Prom_STD/L.Institucional_Lab_Prom_STD
sum IncreasePrice_Lab,d


by CUM1: gen IncreaseQuantity_Lab=D.Institucional_Lab_Unidades_STD/L.Institucional_Lab_Unidades_STD
sum IncreaseQuantity_Lab,d
gen VariacionIncreaseQuantity_Lab=(IncreaseQuantity_Lab>r(p95) | IncreaseQuantity_Lab<r(p5)) if !missing(IncreaseQuantity_Lab)
replace IncreaseQuantity_Lab=r(p95) if IncreaseQuantity_Lab>r(p95) & !missing(IncreaseQuantity_Lab)
replace IncreaseQuantity_Lab=r(p5) if IncreaseQuantity_Lab<r(p5) & !missing(IncreaseQuantity_Lab)
by CUM1: replace Institucional_Lab_Unidades_STD= (L.Institucional_Lab_Unidades_STD*(1+IncreaseQuantity_Lab)) if VariacionIncreaseQuantity_Lab==1
by CUM1: replace IncreaseQuantity_Lab=D.Institucional_Lab_Unidades_STD/L.Institucional_Lab_Unidades_STD
*/

*replace Periodo=. if Institucional_Lab_Prom_STD==.
collapse (first) Institucional_LabFirst=Institucional_Lab_Prom_STD ///
(last) Institucional_LabLast=Institucional_Lab_Prom_STD ///
(first) Institucional_LabFirstObs=Periodo ///
(last) Institucional_LabLastObs=Periodo ///
(mean) Institucional_LabMean=Institucional_Lab_Prom_STD ///
(sd) Institucional_LabSd=Institucional_Lab_Prom_STD ///
(max) VariacionIncreasePrice10_Lab=VariacionIncreasePrice10_Lab VariacionIncreasePrice30_Lab=VariacionIncreasePrice30_Lab ///
(count) Institucional_LabCount=Institucional_Lab_Prom_STD, by(CUM1)

foreach var in  Institucional_Lab  {
gen Algo`var'=(`var'Count>0) &!missing(`var'Count)
gen Completo`var'=(`var'Count==(`var'LastObs-`var'FirstObs+1)) if !missing(`var'Count)
gen CoefVar`var'=`var'Sd/`var'Mean
sum CoefVar`var', d
gen Variacion`var'P99=(CoefVar`var'>r(p99)) & !missing(CoefVar`var')
}
save "$base_out/PriceVariabilityLab.dta",replace
restore

preserve
sort CUM1 Periodo
by CUM1: egen max_price= max(Institucional_May_Prom_STD)
by CUM1: egen min_price= min(Institucional_May_Prom_STD)
gen ratio_price= max_price/min_price
collapse (mean) ratio_price, by(CUM1)
sum ratio_price,d
scalar def ratio_may_95=r(p95)
scalar def ratio_may_90=r(p90)
restore

preserve
sort CUM1 Periodo
scalar prop=3
by CUM1: egen max_price= max(Institucional_May_Prom_STD)
by CUM1: egen min_price= min(Institucional_May_Prom_STD)
gen ratio_price= max_price/min_price
gen VariacionIncreasePrice30_May=1 if ratio_price>`=scalar(ratio_may_95)' & !missing(Institucional_May_Prom_STD)
gen VariacionIncreasePrice10_May=1 if ratio_price>`=scalar(ratio_may_90)' & !missing(Institucional_May_Prom_STD)
recode VariacionIncreasePrice30_May . =0
recode VariacionIncreasePrice10_May . =0
tab VariacionIncreasePrice30_May
tab VariacionIncreasePrice10_May
/*
by CUM1: egen Q1_quantity= pctile(Institucional_May_Unidades_STD), p(25)
by CUM1: egen Q3_quantity= pctile(Institucional_May_Unidades_STD), p(75)
by CUM1: egen  IC_quantity= iqr(Institucional_May_Unidades_STD)
gen VariacionIncreaseQuantity_May=1 if (Institucional_May_Prom_STD< Q1_quantity-`=scalar(prop)'*IC_quantity | Institucional_May_Prom_STD> Q3_quantity+`=scalar(prop)'*IC_quantity) & !missing(Institucional_May_Unidades_STD)
recode VariacionIncreaseQuantity_May . =0
tab VariacionIncreaseQuantity_May
replace Institucional_May_Unidades_STD =Q1_price-`=scalar(prop)'*IC_price if Institucional_May_Unidades_STD < Q1_price-`=scalar(prop)'*IC_price & VariacionIncreaseQuantity_May==1 & !missing( Institucional_May_Unidades_STD)
replace Institucional_May_Unidades_STD =Q3_price+`=scalar(prop)'*IC_price if Institucional_May_Unidades_STD> Q3_price+`=scalar(prop)'*IC_price &  VariacionIncreaseQuantity_May==1 & !missing( Institucional_May_Unidades_STD)
*/

/*
sort CUM1 Periodo
by CUM1: gen IncreasePrice_May=D.Institucional_May_Prom_STD/L.Institucional_May_Prom_STD
sum IncreasePrice_May,d
gen VariacionIncreasePrice_May=(IncreasePrice_May>r(p95) | IncreasePrice_May<r(p5)) if !missing(IncreasePrice_May)
replace IncreasePrice_May=r(p95) if IncreasePrice_May>r(p95) & !missing(IncreasePrice_May)
replace IncreasePrice_May=r(p5) if IncreasePrice_May<r(p5) & !missing(IncreasePrice_May)
by CUM1: replace Institucional_May_Prom_STD= (L.Institucional_May_Prom_STD*(1+IncreasePrice_May)) if VariacionIncreasePrice_May==1
by CUM1: replace IncreasePrice_May=D.Institucional_May_Prom_STD/L.Institucional_May_Prom_STD
sum IncreasePrice_May,d


by CUM1: gen IncreaseQuantity_May=D.Institucional_May_Unidades_STD/L.Institucional_May_Unidades_STD
sum IncreaseQuantity_May,d
gen VariacionIncreaseQuantity_May=(IncreaseQuantity_May>r(p95) | IncreaseQuantity_May<r(p5)) if !missing(IncreaseQuantity_May)
replace IncreaseQuantity_May=r(p95) if IncreaseQuantity_May>r(p95) & !missing(IncreaseQuantity_May)
replace IncreaseQuantity_May=r(p5) if IncreaseQuantity_May<r(p5) & !missing(IncreaseQuantity_May)
by CUM1: replace Institucional_May_Unidades_STD= (L.Institucional_May_Unidades_STD*(1+IncreaseQuantity_May)) if VariacionIncreaseQuantity_May==1
by CUM1: replace IncreaseQuantity_May=D.Institucional_May_Unidades_STD/L.Institucional_May_Unidades_STD
*/


*drop if Institucional_May_Prom_STD==.
collapse (first) Institucional_MayFirst=Institucional_May_Prom_STD ///
(last) Institucional_MayLast=Institucional_May_Prom_STD ///
(first) Institucional_MayFirstObs=Periodo ///
(last) Institucional_MayLastObs=Periodo ///
(mean) Institucional_MayMean=Institucional_May_Prom_STD ///
(sd) Institucional_MaySd=Institucional_May_Prom_STD ///
(max) VariacionIncreasePrice10_May=VariacionIncreasePrice10_May VariacionIncreasePrice30_May=VariacionIncreasePrice30_May ///
(count) Institucional_MayCount=Institucional_May_Prom_STD, by(CUM1)

foreach var in  Institucional_May  {
gen Algo`var'=(`var'Count>0) &!missing(`var'Count)
gen Completo`var'=(`var'Count==(`var'LastObs-`var'FirstObs+1)) if !missing(`var'Count)
gen CoefVar`var'=`var'Sd/`var'Mean
sum CoefVar`var', d
gen Variacion`var'P99=(CoefVar`var'>r(p99)) & !missing(CoefVar`var')
}
save "$base_out/PriceVariabilityMay.dta",replace
restore


preserve
drop if Institucional_Lab_Prom_STD==.
sort CUM1 Periodo
collapse (first) PrimerANObsLab=Periodo (last) UltimoANOObsLab=Periodo, by(CUM1)
save "$base_out/PrimerANOObsLab.dta",replace
restore
preserve
drop if Institucional_May_Prom_STD==.
sort CUM1 Periodo
collapse (first) PrimerANObsMay=Periodo (last) UltimoANOObsMay=Periodo, by(CUM1)
save "$base_out/PrimerANOObsMay.dta",replace
restore
preserve
drop if POS2==.
drop if POS2==0
sort CUM1 Periodo
collapse (first) PrimerANOPOS=Periodo (last) UltimoANOPOS=Periodo, by(CUM1)
save "$base_out/PrimerANOPOS.dta",replace
restore
preserve
drop if VMR2==0
drop if VMR2==.
sort CUM1 Periodo
collapse (first) PrimerANOVMR=Periodo (last) UltimoANOVMR=Periodo, by(CUM1)
save "$base_out/PrimerANOVMR.dta",replace
restore


merge m:1 ATC0 Periodo using "$base_out/HHATC0.dta"
drop if _merge==2
drop _merge
merge m:1 ATC1 Periodo using "$base_out/HHATC1.dta"
drop if _merge==2
drop _merge
merge m:1 ATC2 Periodo using "$base_out/HHATC2.dta"
drop if _merge==2
drop _merge
merge m:1 ATC3 Periodo using "$base_out/HHATC3.dta"
drop if _merge==2
drop _merge

merge m:1 ATC0 Periodo using "$base_out/HHATC0_POS.dta"
drop if _merge==2
drop _merge
merge m:1 ATC1 Periodo using "$base_out/HHATC1_POS.dta"
drop if _merge==2
drop _merge
merge m:1 ATC2 Periodo using "$base_out/HHATC2_POS.dta"
drop if _merge==2
drop _merge
merge m:1 ATC3 Periodo using "$base_out/HHATC3_POS.dta"
drop if _merge==2
drop _merge

merge m:1 ATC0 Periodo using "$base_out/HHATC0_NOPOS.dta"
drop if _merge==2
drop _merge
merge m:1 ATC1 Periodo using "$base_out/HHATC1_NOPOS.dta"
drop if _merge==2
drop _merge
merge m:1 ATC2 Periodo using "$base_out/HHATC2_NOPOS.dta"
drop if _merge==2
drop _merge
merge m:1 ATC3 Periodo using "$base_out/HHATC3_NOPOS.dta"
drop if _merge==2
drop _merge


merge m:1 ATC_Grupo0 Periodo using "$base_out/HHGrupo0.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo1 Periodo using "$base_out/HHGrupo1.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo2 Periodo using "$base_out/HHGrupo2.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo3 Periodo using "$base_out/HHGrupo3.dta"
drop if _merge==2
drop _merge

merge m:1 ATC_Grupo0 Periodo using "$base_out/HHGrupo0_NOPOS.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo1 Periodo using "$base_out/HHGrupo1_NOPOS.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo2 Periodo using "$base_out/HHGrupo2_NOPOS.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo3 Periodo using "$base_out/HHGrupo3_NOPOS.dta"
drop if _merge==2
drop _merge

merge m:1 ATC_Grupo0 Periodo using "$base_out/HHGrupo0_POS.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo1 Periodo using "$base_out/HHGrupo1_POS.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo2 Periodo using "$base_out/HHGrupo2_POS.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo3 Periodo using "$base_out/HHGrupo3_POS.dta"
drop if _merge==2
drop _merge

merge m:1 ATC0 Periodo using "$base_out/MediByATC0.dta"
drop if _merge==2
drop _merge
merge m:1 ATC1 Periodo using "$base_out/MediByATC1.dta"
drop if _merge==2
drop _merge
merge m:1 ATC2 Periodo using "$base_out/MediByATC2.dta"
drop if _merge==2
drop _merge
merge m:1 ATC3 Periodo using "$base_out/MediByATC3.dta"
drop if _merge==2
drop _merge

merge m:1 ATC_Grupo0 Periodo using "$base_out/MediByGroup0.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo1 Periodo using "$base_out/MediByGroup1.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo2 Periodo using "$base_out/MediByGroup2.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo3 Periodo using "$base_out/MediByGroup3.dta"
drop if _merge==2
drop _merge

egen NumPOSATC=rowmax(NumPOSATC0 NumPOSATC1 NumPOSATC2 NumPOSATC3)
egen NumVMRATC=rowmax(NumVMRATC0 NumVMRATC1 NumVMRATC2 NumVMRATC3)
egen CompetidoresATC=rowmax(CompetidoresATC0 CompetidoresATC1 CompetidoresATC2 CompetidoresATC3)
egen GenericosATC=rowmax(GenericosATC0 GenericosATC1 GenericosATC2 GenericosATC3)
egen InsertoATC=rowmax(InsertoATC0 InsertoATC1 InsertoATC2 InsertoATC3)
egen FranjaVerdeATC=rowmax(FranjaVerdeATC0 FranjaVerdeATC1 FranjaVerdeATC2 FranjaVerdeATC3)
egen FranjaVioletaATC=rowmax(FranjaVioletaATC0 FranjaVioletaATC1 FranjaVioletaATC2 FranjaVioletaATC3)
egen OTCATC=rowmax(OTCATC0 OTCATC1 OTCATC2 OTCATC3)
egen VentasMaxATC_Lab=rowmax(VentasTotalesATC0_Lab VentasTotalesATC1_Lab VentasTotalesATC2_Lab VentasTotalesATC3_Lab)
egen VentasMaxATC_May=rowmax(VentasTotalesATC0_May VentasTotalesATC1_May VentasTotalesATC2_May VentasTotalesATC3_May)
egen HHATCMax_Lab=rowmax(HHATC0_Lab HHATC1_Lab HHATC2_Lab HHATC3_Lab)
egen HHATCMax_May=rowmax(HHATC0_May HHATC1_May HHATC2_May HHATC3_May)
egen HHATCMax_Lab_POS=rowmax(HHATC0_Lab_POS HHATC1_Lab_POS HHATC2_Lab_POS HHATC3_Lab_POS)
egen HHATCMax_May_POS=rowmax(HHATC0_May_POS HHATC1_May_POS HHATC2_May_POS HHATC3_May_POS)
egen HHATCMax_Lab_NOPOS=rowmax(HHATC0_Lab_NOPOS HHATC1_Lab_NOPOS HHATC2_Lab_NOPOS HHATC3_Lab_NOPOS)
egen HHATCMax_May_NOPOS=rowmax(HHATC0_May_NOPOS HHATC1_May_NOPOS HHATC2_May_NOPOS HHATC3_May_NOPOS)

egen NumPOSGrupo=rowmax(NumPOSGrupo0 NumPOSGrupo1 NumPOSGrupo2 NumPOSGrupo3)
egen NumVMRGrupo=rowmax(NumVMRGrupo0 NumVMRGrupo1 NumVMRGrupo2 NumVMRGrupo3)
egen CompetidoresGrupo=rowmax(CompetidoresGrupo0 CompetidoresGrupo1 CompetidoresGrupo2)
egen GenericosGrupo=rowmax(GenericosGrupo0 GenericosGrupo1 GenericosGrupo2 GenericosGrupo3)
egen InsertoGrupo=rowmax(InsertoGrupo0 InsertoGrupo1 InsertoGrupo2 InsertoGrupo3)
egen FranjaVerdeGrupo=rowmax(FranjaVerdeGrupo0 FranjaVerdeGrupo1 FranjaVerdeGrupo2 FranjaVerdeGrupo3)
egen FranjaVioletaGrupo=rowmax(FranjaVioletaGrupo0 FranjaVioletaGrupo1 FranjaVioletaGrupo2 FranjaVioletaGrupo3)
egen OTCGrupo=rowmax(OTCGrupo0 OTCGrupo1 OTCGrupo2 OTCGrupo3)
egen VentasMaxGrupo_Lab=rowmax(VentasTotalesGrupo0_Lab VentasTotalesGrupo1_Lab VentasTotalesGrupo2_Lab VentasTotalesGrupo3_Lab)
egen VentasMaxGrupo_May=rowmax(VentasTotalesGrupo0_May VentasTotalesGrupo1_May VentasTotalesGrupo2_May VentasTotalesGrupo3_May)
egen HHGrupoMax_Lab=rowmax(HHGrupo0_Lab HHGrupo1_Lab HHGrupo2_Lab HHGrupo3_Lab)
egen HHGrupoMax_May=rowmax(HHGrupo0_May HHGrupo1_May HHGrupo2_May HHGrupo3_May)


merge m:1 CUM1 using "$base_out/POSStatus.dta", keepus(AlwaysPOS NeverPOS POSChange)
drop _merge
merge m:1 CUM1 using "$base_out/PriceVariabilityLab.dta", keepus(*First *Last Completo*  Variacion* Algo* *Count)
drop _merge
merge m:1 CUM1 using "$base_out/PriceVariabilityMay.dta", keepus(*First *Last Completo*  Variacion* Algo* *Count)
drop _merge
merge m:1 CUM1 using "$base_out/PrimerANOPOS.dta"
drop _merge
merge m:1 CUM1 using "$base_out/PrimerANOVMR.dta"
drop _merge
merge m:1 CUM1 using "$base_out/PrimerANOObsLab.dta"
drop _merge
merge m:1 CUM1 using "$base_out/PrimerANOObsMay.dta"
drop _merge


compress
saveold "$base_out/MedicamentosRegs.dta",replace

reshape long VariacionIncreasePrice10_@ VariacionIncreasePrice30_@ PrimerANObs@ UltimoANOObs@ Institucional_@Count HHGrupo0_@ HHATC0_@ HHGrupo0_@_POS HHATC0_@_POS HHGrupo0_@_NOPOS HHATC0_@_NOPOS VentasTotalesGrupo0_@ VentasTotalesATC0_@ HHGrupo1_@ HHATC1_@ HHGrupo1_@_POS HHATC1_@_POS HHGrupo1_@_NOPOS HHATC1_@_NOPOS VentasTotalesGrupo1_@ VentasTotalesATC1_@ HHGrupo2_@ HHATC2_@ HHGrupo2_@_POS HHATC2_@_POS HHGrupo2_@_NOPOS HHATC2_@_NOPOS VentasTotalesGrupo2_@ VentasTotalesATC2_@ HHGrupo3_@ HHATC3_@ HHGrupo3_@_POS HHATC3_@_POS HHGrupo3_@_NOPOS HHATC3_@_NOPOS VentasTotalesGrupo3_@ VentasTotalesATC3_@  HHGrupoMax_@ HHATCMax_@ HHGrupoMax_@_POS HHATCMax_@_POS HHGrupoMax_@_NOPOS HHATCMax_@_NOPOS VentasMaxGrupo_@ VentasMaxATC_@ Institucional_@_Prom_STD Institucional_@_Valor Institucional_@_Unidades_STD Institucional_@First Institucional_@Last CompletoInstitucional_@  AlgoInstitucional_@ VariacionInstitucional_@P99, i(CUM1 Periodo) j(Canal) string

encode Canal, gen(Canal2)
rename Institucional__Prom_STD Institucional_Prom_STD
rename Institucional__Valor Institucional_Valor
rename Institucional__Unidades_STD Institucional_Unidades_STD
rename CompletoInstitucional_ CompletoInstitucional
rename AlgoInstitucional_ AlgoInstitucional


rename VentasMaxATC VentasMaxATC
rename VentasMaxGrupo_ VentasMaxGrupo
rename HHATCMax_ HHATCMax
rename HHGrupoMax_ HHGrupoMax
rename HHATCMax__POS HHATCMax_POS
rename HHGrupoMax__POS HHGrupoMax_POS
rename HHATCMax__NOPOS HHATCMax_NOPOS
rename HHGrupoMax__NOPOS HHGrupoMax_NOPOS

forvalues j=0/3{
rename VentasTotalesGrupo`j'_ VentasTotalesGrupo`j'
rename VentasTotalesATC`j'_ VentasTotalesATC`j'
rename HHATC`j'_ HHATC`j'
rename HHGrupo`j'_ HHGrupo`j'
rename HHATC`j'__POS HHATC`j'_POS
rename HHGrupo`j'__POS HHGrupo`j'_POS
rename HHATC`j'__NOPOS HHATC`j'_NOPOS
rename HHGrupo`j'__NOPOS HHGrupo`j'_NOPOS
}

compress

sort ATC0 Periodo
bys ATC0: egen PrimerANOPOSATC_temp=min(Periodo) if POS2==1
bys ATC0: egen PrimerANOPOSATC=mean(PrimerANOPOSATC_temp)
drop PrimerANOPOSATC_temp
gen LateEntrants=(PrimerANOPOS!=PrimerANOPOSATC & PrimerANOPOS!=.)

saveold "$base_out/MedicamentosRegsLong.dta",replace

/*
egen double Inst_Total_Unidades_STD2=rowtotal(Institucional_Lab_Unidades_STD Institucional_May_Prom_STD), missing
gen double Inst_Total_Prom_STD2=Institucional_Lab_Prom_STD if !missing(Institucional_Lab_Unidades_STD) & Institucional_Lab_Unidades_STD!=0 & (Institucional_May_Unidades_STD==0 | missing(Institucional_May_Unidades_STD))
replace Inst_Total_Prom_STD2=Institucional_May_Prom_STD if !missing(Institucional_May_Unidades_STD) & Institucional_May_Unidades_STD!=0 & (Institucional_Lab_Unidades_STD==0 | missing(Institucional_Lab_Unidades_STD))
replace Inst_Total_Prom_STD2=(Institucional_Lab_Prom_STD* Institucional_Lab_Unidades_STD+ Institucional_May_Prom_STD* Institucional_May_Unidades_STD)/( Institucional_Lab_Unidades_STD+ Institucional_May_Prom_STD) if Inst_Total_Unidades_STD!=0 & Institucional_Lab_Unidades_STD!=0 & Institucional_May_Prom_STD!=0 & !missing(Inst_Total_Unidades_STD) & !missing(Institucional_Lab_Unidades_STD) & !missing(Institucional_May_Prom_STD)
replace Inst_Total_Prom_STD2=. if Institucional_May_Unidades_STD==0 & Institucional_Lab_Unidades_STD==0 | (missing(Institucional_May_Unidades_STD) & missing(Institucional_Lab_Unidades_STD))
*/
*codebook CUM1
/*

bys Periodo: egen ValorTotalAno=sum(Institucional_Valor)
gsort +Periodo -Institucional_Valor
bys Periodo: gen Frac_ValorTotalAno=sum(Institucional_Valor)/ValorTotalAno
gen Keep=(Frac_ValorTotalAno<0.95) if !missing(Frac_ValorTotalAno)


label var Generico "Generic"
label var FranjaVerde "Essential Medicine"
label var FranjaVioleta "Controlled Substance"
label var Inserto "Insert"
label var OTC "OTC"
label var Presentations "No. of presentations"

label var CompetidoresGrupo "No. pharm. in therapeutical group"
label var CompetidoresATC "No. pharm. in ATC group"

drop if Canal2==2

encode ATC_Grupo0 , gen (ATC_Grupo)


label var NumPOSATC "No. drugs in ATC group in POS"
label var NumPOSGrupo "No. drugs in therapeutical in POS"
label var NumVMRATC "No. drugs in ATC group with RP"
label var NumVMRGrupo "No. drugs in pharmacological subgroup with RP"
gen DGenericosATC=(GenericosATC>0) if !missing(GenericosATC)
gen DGenericosGrupo=(GenericosGrupo>0) if !missing(GenericosGrupo)
gen LgCompetidoresATC=log(CompetidoresATC)
gen LgCompetidoresGrupo=log(CompetidoresGrupo)
gen LgNumPOSATC=log(NumPOSATC)
gen LgNumPOSGrupo=log(NumPOSGrupo)
replace LgNumPOSATC=0 if LgNumPOSATC==.
replace LgNumPOSGrupo=0 if LgNumPOSGrupo==.
label var LgNumPOSATC "log(No. drugs in ATC group in POS)"
label var LgNumPOSGrupo "log(No. drugs in pharmacological subgroup in POS)"

gen PropCompetidoresPOSATC=NumPOSATC/CompetidoresATC 
gen PropCompetidoresPOSGrupo=NumPOSGrupo/CompetidoresGrupo 
gen OnePOSATC=(NumPOSATC==1) & !missing(NumPOSATC)
gen OnePOSGrupo=(NumPOSGrupo==1) & !missing(NumPOSGrupo )


compress
xtset CUM1 Periodo, yearly
gen WeightShare2007=Institucional_Valor/ValorTotalAno if Periodo==2007
replace WeightShare2007=L.WeightShare2007 if WeightShare2007==.

gen missing_info=missing(Institucional_Prom_STD)
label var missing_info "Missing price/quantity information"



foreach var of varlist  Institucional_Prom_STD   Institucional_Unidades_STD {
	gen double Lg`var'=log(`var')
}

label var LgInstitucional_Prom_STD "Log average price for EPS and IPS"  
label var LgInstitucional_Unidades_STD "Log units for EPS and IPS"


label var POS "POS"
label var VMR "RP"
label var POS2 "POS"

label var VMR2 "RP"
label var VMR3 "RP"

gen NOPOS=1-POS

global control_basico VMR2 DGenericosATC HHATCMax
global control_dinamico VMR2 L.VMR2  L.L.VMR2  DGenericosATC L.DGenericosATC  L.L.DGenericosATC  HHATCMax L.HHATCMax L.L.HHATCMax
reghdfe LgInstitucional_Prom_STD POS2 $control_basico if VariacionIncreasePrice30_==0 & Institucional_Count==$panelcompelto, absorb(Periodo CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) resid(resid_price)


bys CUM1: egen minCompetidoresATC=min(CompetidoresATC)
bys CUM1: egen maxCompetidoresATC=max(CompetidoresATC)

 collapse resid_price PrimerANOPOS PrimerANOVMR maxCompetidoresATC  minCompetidoresATC (firstnm) ATC0 PRINCIPIO_ACTIVO0 DESCRIPCION_ATC0 , by(POS2 CUM1)
 drop if POS2==.
 reshape wide resid_price, i(CUM1) j(POS2)
 gen diff_precio=resid_price1-resid_price0
drop if diff_precio==. 
*/
