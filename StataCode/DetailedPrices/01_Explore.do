import delimited "$basein/DetailedPrices/EPS task1.txt", clear
rename v1 EPS
rename v2 ATC
rename v3 gasto
replace gasto=subinstr(gasto,"$","",.)
replace gasto=subinstr(gasto,".","",.)
drop if gasto=="MISSING DATA"
destring gasto, replace
bys ATC EPS: gen n=_n
drop if n>1
drop n
save "$base_out/Gasto_EPS.dta", replace

import delimited "$basein/DetailedPrices/DDD_perEPS.txt", clear
rename v1 ATC
rename v2 EPS
replace v6=v4 if v5==""
drop v3 v4
rename v5 DDD
rename v6 Enrollees

replace DDD=subinstr(DDD,"$","",.)
replace DDD=subinstr(DDD,".","",.)
replace DDD=subinstr(DDD,",",".",.)
drop if DDD=="MISSING DATA"
replace Enrollees=subinstr(Enrollees,"$","",.)
replace Enrollees=subinstr(Enrollees,".","",.)
replace Enrollees=subinstr(Enrollees,",",".",.)
drop if Enrollees=="MISSING DATA"
destring DDD Enrollees, replace
bys ATC EPS: gen n=_n
drop if n>1
drop n

preserve
collapse (mean) Enrollees, by(EPS)
sum Enrollees
gen Share=Enrollees/r(sum)
restore

save "$base_out/DDD_EPS.dta", replace


merge 1:1 EPS ATC using "$base_out/Gasto_EPS.dta"
drop _merge
gen DDD_Price=gasto/DDD
save "$base_out/EPS_Price.dta", replace

encode EPS, gen(EPS2)
encode ATC, gen(ATC2)

bys ATC2:egen sd=sd(DDD_Price)

reghdfe DDD_Price i.EPS2, a(ATC2)
gen lgDDD_Price=log(DDD_Price)
reghdfe lgDDD_Price i.EPS2, a(ATC2)

reghdfe DDD_Price Enrollees, a(ATC2)
reghdfe lgDDD_Price Enrollees, a(ATC2)


/*
collapse (mean) mean=DDD_Price (sd) sd=DDD_Price (max) max=DDD_Price (min) min=DDD_Price, by(ATC)
gen MMRatio=max/min
*/
