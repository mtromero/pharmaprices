set more off
clear all
set matsize 800
use "$base_out/MedicamentosRegsLong.dta",clear
bys Periodo: egen ValorTotalAno=sum(Institucional_Valor)
gsort +Periodo -Institucional_Valor
bys Periodo: gen Frac_ValorTotalAno=sum(Institucional_Valor)/ValorTotalAno
gen Keep=(Frac_ValorTotalAno<0.95) if !missing(Frac_ValorTotalAno)


label var Generico "Generic"
label var FranjaVerde "Essential Medicine"
label var FranjaVioleta "Controlled Substance"
label var Inserto "Insert"
label var OTC "OTC"
label var Presentations "No. of presentations"

label var CompetidoresGrupo "No. pharm. in therapeutical group"
label var CompetidoresATC "No. pharm. in ATC group"

drop if Canal2==2

encode ATC_Grupo0 , gen (ATC_Grupo)
encode ATC0 , gen (ATC)

label var NumPOSATC "No. drugs in ATC group in POS"
label var NumPOSGrupo "No. drugs in therapeutical in POS"
label var NumVMRATC "No. drugs in ATC group with RP"
label var NumVMRGrupo "No. drugs in pharmacological subgroup with RP"
gen DGenericosATC=(GenericosATC>0) if !missing(GenericosATC)
gen DGenericosGrupo=(GenericosGrupo>0) if !missing(GenericosGrupo)
gen LgCompetidoresATC=log(CompetidoresATC)
gen LgCompetidoresGrupo=log(CompetidoresGrupo)
gen LgNumPOSATC=log(NumPOSATC)
gen LgNumPOSGrupo=log(NumPOSGrupo)
replace LgNumPOSATC=0 if LgNumPOSATC==.
replace LgNumPOSGrupo=0 if LgNumPOSGrupo==.
label var LgNumPOSATC "log(No. drugs in ATC group in POS)"
label var LgNumPOSGrupo "log(No. drugs in pharmacological subgroup in POS)"

gen PropCompetidoresPOSATC=NumPOSATC/CompetidoresATC 
gen PropCompetidoresPOSGrupo=NumPOSGrupo/CompetidoresGrupo 
gen OnePOSATC=(NumPOSATC==1) & !missing(NumPOSATC)
gen OnePOSGrupo=(NumPOSGrupo==1) & !missing(NumPOSGrupo )


compress
xtset CUM1 Periodo, yearly
gen WeightShare2007=Institucional_Valor/ValorTotalAno if Periodo==2007
replace WeightShare2007=L.WeightShare2007 if WeightShare2007==.

gen missing_info=missing(Institucional_Prom_STD)
label var missing_info "Missing price/quantity information"



foreach var of varlist  Institucional_Prom_STD   Institucional_Unidades_STD {
	gen double Lg`var'=log(`var')
}

label var LgInstitucional_Prom_STD "Log average price for EPS and IPS"  
label var LgInstitucional_Unidades_STD "Log units for EPS and IPS"


label var POS "POS"
label var VMR "RP"
label var POS2 "POS"

label var VMR2 "RP"
label var VMR3 "RP"

gen NOPOS=1-POS

global control_basico VMR2 DGenericosATC HHATCMax
global control_dinamico VMR2 L.VMR2  L.L.VMR2  DGenericosATC L.DGenericosATC  L.L.DGenericosATC  HHATCMax L.HHATCMax L.L.HHATCMax

*reghdfe DGenericosATC POS2 if VariacionIncreasePrice30_==0, absorb(Periodo CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) 
*reghdfe DGenericosATC POS2 if Institucional_Count==$panelcompelto , absorb(Periodo CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) 
*reghdfe DGenericosATC POS2 if VariacionIncreasePrice30_==0 & Institucional_Count==$panelcompelto , absorb(Periodo CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) 

*reghdfe LgInstitucional_Prom_STD POS2 $control_basico if VariacionIncreasePrice30_==0 & Institucional_Count==$panelcompelto, absorb(Periodo CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
*reghdfe LgInstitucional_Prom_STD POS2 $control_basico if VariacionIncreasePrice30_==0 & Institucional_Count>=8, absorb(Periodo CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
*reghdfe LgInstitucional_Prom_STD c.POS2#CompetidoresATC_2007_recode  if Institucional_Count==$panelcompelto  &   VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)

reghdfe LgInstitucional_Prom_STD POS2 $control_basico if Institucional_Count==$panelcompelto  &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
reghdfe LgInstitucional_Prom_STD POS2 $control_basico if Institucional_Count==$panelcompelto  &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 Periodo#ATC)  vce(cluster ATC_Grupo)

/*
reghdfe DGenericosATC POS2 if VariacionIncreasePrice30_==0 & Institucional_Count==$panelcompelto, absorb(Periodo CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) resid(resid_generico)

 collapse resid_generico resid_price PrimerANOPOS PrimerANOVMR (firstnm) ATC0 PRINCIPIO_ACTIVO0 DESCRIPCION_ATC0 , by(POS2 CUM1)
 drop if POS2==.
 reshape wide resid_generico resid_price, i(CUM1) j(POS2)
 gen diff_precio=resid_price1-resid_price0
 gen diff_generico=resid_generico1- resid_generico0
drop if diff_precio==. & diff_generico==.
*/

foreach channel in   2{
	foreach tipo in DGenericosATC LgCompetidoresATC VMR2 DGenericosGrupo LgCompetidoresGrupo HHATCMax{
		eststo clear
		eststo: reghdfe `tipo' POS2  , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
		estadd ysumm
		estadd local BalancedPanel "No"
		estadd local Outliers "No"
		estadd local Molecule "Yes"
		estadd local Year "Yes"
		estadd local MoleculeTrends "No"
		estadd scalar NumMolecules=e(K1)

		eststo: reghdfe `tipo' POS2     if Institucional_Count==$panelcompelto  , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
		estadd ysumm
		estadd local BalancedPanel "Yes"
		estadd local Outliers "No"
		estadd local Molecule "Yes"
		estadd local Year "Yes"
		estadd local MoleculeTrends "No"
		estadd scalar NumMolecules=e(K1)

		eststo: reghdfe `tipo' POS2     if Institucional_Count==$panelcompelto , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
		estadd ysumm
		estadd local BalancedPanel "Yes"
		estadd local Outliers "No"
		estadd local Molecule "Yes"
		estadd local Year "No"
		estadd local MoleculeTrends "Yes"
		estadd scalar NumMolecules=e(K1)

		eststo: reghdfe `tipo' POS2     if Institucional_Count==$panelcompelto & VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
		estadd ysumm
		estadd local BalancedPanel "Yes"
		estadd local Outliers "Yes"
		estadd local Molecule "Yes"
		estadd local Year "No"
		estadd local MoleculeTrends "Yes"
		estadd scalar NumMolecules=e(K1)
		
		eststo: reghdfe `tipo' POS2     if  VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
		estadd ysumm
		estadd local BalancedPanel "Yes"
		estadd local Outliers "Yes"
		estadd local Molecule "Yes"
		estadd local Year "No"
		estadd local MoleculeTrends "Yes"
		estadd scalar NumMolecules=e(K1)



		 estout using "$latexcodes/Reg_`tipo'_`channel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
		 /*rename(D.POS2 POS2)*/ ///
		 replace keep(POS2)
		 *stats(N N_clust NumMolecules Molecule Year  BalancedPanel MoleculeTrends Outliers, fmt(a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "ATC x Year F.E." "Year F.E."   "Balanced panel?" "Molecule trends?" "Outliers excluded?"))
	}
}



foreach channel in   2{
	foreach nivel in ATC Grupo{
		eststo clear
		foreach tipo in DGenericos`nivel' HH`nivel'Max VMR2 {
			if("`nivel'"!="Grupo" | "`tipo'"!="VMR2"){
				eststo: reghdfe `tipo' POS2  if  Institucional_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				estadd ysumm
				estadd local BalancedPanel "Yes"
				estadd local Outliers "No"
				estadd local Molecule "Yes"
				estadd local Year "No"
				estadd local MoleculeTrends "Yes"
				estadd scalar NumMolecules=e(K1)
			}
		}
		 estout using "$latexcodes/Reg_Timing_`channel'_Completo_`nivel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
		 replace keep(POS2) ///
		 stats(N N_clust NumMolecules Molecule Year MoleculeTrends BalancedPanel Outliers, fmt(a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "ATC x Year F.E." "Year F.E." "Molecule trends?"  "Balanced panel?" "Outliers excluded?"))



		eststo clear
		foreach tipo in DGenericos`nivel' HH`nivel'Max  VMR2 {
			if("`nivel'"!="Grupo" | "`tipo'"!="VMR2"){
				eststo: reghdfe `tipo' POS2  if VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				estadd ysumm
				estadd local BalancedPanel "Yes"
				estadd local Outliers "Yes"
				estadd local Molecule "Yes"
				estadd local Year "No"
				estadd local MoleculeTrends "Yes"
				estadd scalar NumMolecules=e(K1)
			}
		}
		 estout using "$latexcodes/Reg_Timing_`channel'_trend_ratio30_`nivel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
		 replace keep(POS2) ///
		 stats(N N_clust NumMolecules Molecule Year MoleculeTrends BalancedPanel Outliers, fmt(a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "ATC x Year F.E." "Year F.E." "Molecule trends?"  "Balanced panel?" "Outliers excluded?"))

		eststo clear
		foreach tipo in DGenericos`nivel' HH`nivel'Max  VMR2 {
			if("`nivel'"!="Grupo" | "`tipo'"!="VMR2"){
				eststo: reghdfe `tipo' POS2  , absorb(CUM1 Periodo)  vce(cluster ATC_Grupo)
				estadd ysumm
				estadd local BalancedPanel "Yes"
				estadd local Outliers "Yes"
				estadd local Molecule "Yes"
				estadd local Year "No"
				estadd local MoleculeTrends "Yes"
				estadd scalar NumMolecules=e(K1)
			}
		}
		 estout using "$latexcodes/Reg_Timing_`channel'_`nivel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
		 replace keep(POS2) ///
		 stats(N N_clust NumMolecules Molecule Year MoleculeTrends BalancedPanel Outliers, fmt(a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "ATC x Year F.E." "Year F.E." "Molecule trends?"  "Balanced panel?" "Outliers excluded?"))

		
		eststo clear
		foreach tipo in DGenericos`nivel'  HH`nivel'Max VMR2 {
			if("`nivel'"!="Grupo" | "`tipo'"!="VMR2"){
				eststo: reghdfe `tipo' POS2  if Institucional_Count==$panelcompelto & VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				estadd ysumm
				estadd local BalancedPanel "Yes"
				estadd local Outliers "Yes"
				estadd local Molecule "Yes"
				estadd local Year "No"
				estadd local MoleculeTrends "Yes"
				estadd scalar NumMolecules=e(K1)
			}
		}
		 estout using "$latexcodes/Reg_Timing_`channel'_trendCompleto_ratio30_`nivel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
		 replace keep(POS2) ///
		 stats(N N_clust NumMolecules Molecule Year  BalancedPanel MoleculeTrends Outliers, fmt(a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "ATC x Year F.E." "Year F.E."  "Balanced panel?"  "Molecule trends?" "Outliers excluded?"))
	}
}





foreach channel in   2{
	foreach tipo in  Prom_STD Unidades_STD {
		foreach var in  Institucional{
			eststo clear
			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico  , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "No"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "Yes"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "No"
			estadd scalar NumMolecules=e(K1)

			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico    if  `var'_Count==$panelcompelto , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "Yes"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "No"
			estadd scalar NumMolecules=e(K1)

			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico   if `var'_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)


			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico   if `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)
			
			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico   if `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , absorb(CUM1 Periodo#ATC)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "Yes"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)


			eststo: reghdfe Lg`var'_`tipo' POS2 $control_dinamico if  `var'_Count==$panelcompelto & VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)
			
			
			 estout using "$latexcodes/Reg_`tipo'_`var'_`channel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
			 /*rename(D.POS2 POS2)*/ ///
			 replace keep(POS2) ///
			 stats(N N_clust NumMolecules Molecule ATC Year Controls LagControls  BalancedPanel MoleculeTrends Outliers, fmt(a2 a2 a2 %~#s %~#s %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "ATC x Year F.E." "Year F.E." "Controls" "Lagged Controls" "Balanced panel?" "Molecule trends?" "Outliers excluded?"))


		}
	}
}






 
foreach channel in   2{
	foreach tipo in  Prom_STD Unidades_STD {
		foreach var in  Institucional{
			eststo clear
			qui eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico  if Keep==1, absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "No"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "Yes"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "No"
			estadd scalar NumMolecules=e(K1)

			qui eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico   if  `var'_Count==$panelcompelto & Keep==1, absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "Yes"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "No"
			estadd scalar NumMolecules=e(K1)

			qui eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico  if `var'_Count==$panelcompelto & Keep==1, absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)


			qui eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico  if `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 & Keep==1, absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)
			
			qui eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico  if `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 & Keep==1, absorb(CUM1 Periodo#ATC)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "Yes"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)



			qui eststo: reghdfe Lg`var'_`tipo' POS2 $control_dinamico  if  `var'_Count==$panelcompelto & VariacionIncreasePrice30_==0 & Keep==1, absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)
			
			


			 estout using "$latexcodes/Reg_`tipo'_`var'_`channel'_bigsellers.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
			 /*rename(D.POS2 POS2)*/ ///
			 replace keep(POS2) ///
			 stats(N N_clust NumMolecules Molecule ATC Year Controls LagControls  BalancedPanel MoleculeTrends Outliers, fmt(a2 a2 a2 %~#s %~#s %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "ATC x Year F.E." "Year F.E." "Controls" "Lagged Controls" "Balanced panel?" "Molecule trends?" "Outliers excluded?"))
		}
	}
}



gen Ventas=Institucional_Prom_STD*Institucional_Unidades_STD
bys ATC_Grupo Periodo: egen VentasTotales=total(Ventas)
bys ATC_Grupo Periodo: egen VentasPOSTemp=total(Ventas) if POS==1
bys ATC_Grupo Periodo: egen VentasPOS=mean(VentasPOSTemp)
bys ATC_Grupo Periodo: egen VentasNOPOSTemp=total(Ventas) if POS==0
bys ATC_Grupo Periodo: egen VentasNOPOS=mean(VentasNOPOSTemp)
bys ATC_Grupo Periodo: egen VentasIndefPOSTemp=total(Ventas) if POS==.
bys ATC_Grupo Periodo: egen VentasIndefPOS=mean(VentasIndefPOSTemp)
gen SharePOS_Grupo=(VentasTotales-VentasIndefPOS-VentasNOPOS)/VentasTotales
replace SharePOS_Grupo=SharePOS_Grupo*100
gen ShareCompetitorsPOS_Grupo=NumPOSGrupo/CompetidoresGrupo
replace ShareCompetitorsPOS_Grupo=ShareCompetitorsPOS_Grupo*100
encode ATC0 , gen (ATC_0_2)


bys ATC_0_2 Periodo: egen VentasTotales_ATC=total(Ventas)
bys ATC_0_2 Periodo: egen VentasPOSTemp_ATC=total(Ventas) if POS==1
bys ATC_0_2 Periodo: egen VentasPOS_ATC=mean(VentasPOSTemp_ATC)
replace VentasPOS_ATC=0 if VentasPOS_ATC==.
bys ATC_0_2 Periodo: egen VentasNOPOSTemp_ATC=total(Ventas) if POS==0
bys ATC_0_2 Periodo: egen VentasNOPOS_ATC=mean(VentasNOPOSTemp_ATC)
replace VentasNOPOS_ATC=0 if VentasNOPOS_ATC==.
bys ATC_0_2 Periodo: egen VentasIndefPOSTemp_ATC=total(Ventas) if POS==.
bys ATC_0_2 Periodo: egen VentasIndefPOS_ATC=mean(VentasIndefPOSTemp_ATC)
replace VentasIndefPOS_ATC=0 if VentasIndefPOS_ATC==.
gen SharePOS_ATC=(VentasTotales_ATC-VentasIndefPOS_ATC-VentasNOPOS_ATC)/VentasTotales_ATC
replace SharePOS_ATC=SharePOS_ATC*100
gen ShareCompetitorsPOS_ATC=NumPOSATC/CompetidoresATC
replace ShareCompetitorsPOS_ATC=ShareCompetitorsPOS_ATC*100

label var SharePOS_ATC "POS Market share"
label var SharePOS_Grupo "POS Market share"

/*
gen logSharePOS_ATC=log(1+SharePOS_ATC)
gen SharePOS_ATC100=100*SharePOS_ATC
reghdfe LgInstitucional_Prom_STD SharePOS_ATC  $control_basico if NeverPOS==1 & Institucional_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
reghdfe LgInstitucional_Prom_STD logSharePOS_ATC  $control_basico if NeverPOS==1 & Institucional_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
reghdfe LgInstitucional_Prom_STD SharePOS_ATC100  $control_basico if NeverPOS==1 & Institucional_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
*/

*reghdfe SharePOS_ATC ShareCompetitorsPOS_`nivel'   if (NeverPOS==1 | AlwaysPOS==1) & Institucional_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)

*reghdfe LgInstitucional_Prom_STD   $control_basico AtLeastOneATC  if NeverPOS==1 & Institucional_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo) ffirst

gen AtLeastOneATC=(NumPOSATC>=1) & !missing(NumPOSATC)
gen AtLeastOneGrupo=(NumPOSGrupo>=1) & !missing(NumPOSGrupo)
					 
xtset CUM1 Periodo, yearly


					 
xtset CUM1 Periodo, yearly
foreach nivel in  Grupo ATC {
	foreach channel in   2{
		foreach tipo in  Prom_STD{
			foreach var in  Institucional{
				eststo clear
				qui eststo: reghdfe Lg`var'_`tipo' $control_basico AtLeastOne`nivel'  if NeverPOS==1  ,maxiter(100000) absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "No"
					estadd local Outliers "No"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "No"
					estadd scalar NumMolecules=e(K1)
				}

				qui eststo: reghdfe Lg`var'_`tipo' $control_basico AtLeastOne`nivel'    if NeverPOS==1 &  VariacionIncreasePrice30_==0, maxiter(100000) absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "No"
					estadd scalar NumMolecules=e(K1)
				}

				qui eststo: reghdfe Lg`var'_`tipo' $control_basico AtLeastOne`nivel'  if NeverPOS==1 & VariacionIncreasePrice30_==0, maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "Yes"
					estadd scalar NumMolecules=e(K1)
				}

				qui eststo: reghdfe Lg`var'_`tipo' $control_basico AtLeastOne`nivel'   if NeverPOS==1 & `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "Yes"
					estadd scalar NumMolecules=e(K1)
				}
				




				qui eststo: reghdfe Lg`var'_`tipo' $control_dinamico AtLeastOne`nivel'   if NeverPOS==1 & `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0  , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "Yes"
					estadd local MoleculeTrends "Yes"
					estadd scalar NumMolecules=e(K1)
				}
				
				



				if("`nivel'"=="Grupo"){
					 estout using "$latexcodes/Reg_`tipo'_`var'_`channel'_priceNOPOS_Number`nivel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
					 /*rename(D.POS2 POS2)*/ ///
					 replace keep(AtLeastOne`nivel') ///
					  stats(N N_clust NumMolecules Molecule Year Controls LagControls  BalancedPanel MoleculeTrends Outliers, fmt(a2 a2 a2  %~#s %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "Year F.E." "Controls" "Lagged Controls" "Balanced panel?" "Molecule trends?" "Outliers excluded?"))
				}
				else{
					 estout using "$latexcodes/Reg_`tipo'_`var'_`channel'_priceNOPOS_Number`nivel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
					 /*rename(D.POS2 POS2)*/ ///
					 replace keep(AtLeastOne`nivel') 
				}
			}
		}
	}

}


foreach nivel in  Grupo ATC{

	foreach channel in   2{
		foreach tipo in  Prom_STD{
			foreach var in  Institucional{
				eststo clear
				qui eststo: reghdfe Lg`var'_`tipo' $control_basico SharePOS_`nivel'  if AlwaysPOS==1  ,maxiter(100000) absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "No"
					estadd local Outliers "No"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "No"
					estadd scalar NumMolecules=e(K1)
				}

				qui eststo: reghdfe Lg`var'_`tipo' $control_basico  SharePOS_`nivel'   if AlwaysPOS==1 &  VariacionIncreasePrice30_==0, maxiter(100000) absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "Yes"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "No"
					estadd scalar NumMolecules=e(K1)
				}

				qui eststo: reghdfe Lg`var'_`tipo' $control_basico SharePOS_`nivel'  if AlwaysPOS==1 & VariacionIncreasePrice30_==0, maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "Yes"
					estadd scalar NumMolecules=e(K1)
				}

				qui eststo: reghdfe Lg`var'_`tipo' $control_basico SharePOS_`nivel'   if AlwaysPOS==1 & `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "Yes"
					estadd scalar NumMolecules=e(K1)
				}
				
	


				qui eststo: reghdfe Lg`var'_`tipo' $control_dinamico SharePOS_`nivel'  if AlwaysPOS==1 & `var'_Count==$panelcompelto &   VariacionIncreasePrice30_==0  , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "Yes"
					estadd local MoleculeTrends "Yes"
					estadd scalar NumMolecules=e(K1)
				}
					


				if("`nivel'"=="Grupo"){
					 estout using "$latexcodes/Reg_`tipo'_`var'_`channel'_pricePOS_`nivel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
					 /*rename(D.POS2 POS2)*/ ///
					 replace keep(SharePOS_`nivel') ///
					   stats(N N_clust NumMolecules Molecule Year Controls LagControls  BalancedPanel MoleculeTrends Outliers, fmt(a2 a2 a2  %~#s %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "Year F.E." "Controls" "Lagged Controls" "Balanced panel?" "Molecule trends?" "Outliers excluded?"))
		}
				else{
					 estout using "$latexcodes/Reg_`tipo'_`var'_`channel'_pricePOS_`nivel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
					 /*rename(D.POS2 POS2)*/ ///
					 replace keep(SharePOS_`nivel') 
				}
			}
		}
	}


	foreach channel in   2{
		foreach tipo in  Prom_STD{
			foreach var in  Institucional{
				eststo clear
				qui eststo: reghdfe Lg`var'_`tipo' $control_basico SharePOS_`nivel'  if NeverPOS==1  ,maxiter(100000) absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "No"
					estadd local Outliers "No"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "No"
					estadd scalar NumMolecules=e(K1)
				}

				qui eststo: reghdfe Lg`var'_`tipo' $control_basico SharePOS_`nivel'    if NeverPOS==1 &  VariacionIncreasePrice30_==0, maxiter(100000) absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "No"
					estadd scalar NumMolecules=e(K1)
				}

				qui eststo: reghdfe Lg`var'_`tipo' $control_basico SharePOS_`nivel' if NeverPOS==1 & VariacionIncreasePrice30_==0, maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "Yes"
					estadd scalar NumMolecules=e(K1)
				}

				qui eststo: reghdfe Lg`var'_`tipo' $control_basico SharePOS_`nivel'   if NeverPOS==1 & `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "Yes"
					estadd scalar NumMolecules=e(K1)
				}
				




				qui eststo: reghdfe Lg`var'_`tipo' $control_dinamico SharePOS_`nivel'   if NeverPOS==1 & `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0  , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "Yes"
					estadd local MoleculeTrends "Yes"
					estadd scalar NumMolecules=e(K1)
				}
				
				



				if("`nivel'"=="Grupo"){
					 estout using "$latexcodes/Reg_`tipo'_`var'_`channel'_priceNOPOS_`nivel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
					 /*rename(D.POS2 POS2)*/ ///
					 replace keep(SharePOS_`nivel') ///
					  stats(N N_clust NumMolecules Molecule Year Controls LagControls  BalancedPanel MoleculeTrends Outliers, fmt(a2 a2 a2  %~#s %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "Year F.E." "Controls" "Lagged Controls" "Balanced panel?" "Molecule trends?" "Outliers excluded?"))
				}
				else{
					 estout using "$latexcodes/Reg_`tipo'_`var'_`channel'_priceNOPOS_`nivel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
					 /*rename(D.POS2 POS2)*/ ///
					 replace keep(SharePOS_`nivel') 
				}
			}
		}
	}





/*
	foreach channel in   2{
		foreach tipo in  Prom_STD{
			foreach var in  Institucional{
				eststo clear
				qui eststo: reghdfe Lg`var'_`tipo' $control_basico (SharePOS_`nivel'=ShareCompetitorsPOS_`nivel')  ,maxiter(100000) absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "No"
					estadd local Outliers "No"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "No"
					estadd scalar NumMolecules=e(K1)
				}

				qui eststo: reghdfe Lg`var'_`tipo' $control_basico (SharePOS_`nivel'=ShareCompetitorsPOS_`nivel')    if  VariacionIncreasePrice30_==0, maxiter(100000) absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "No"
					estadd scalar NumMolecules=e(K1)
				}

				qui eststo: reghdfe Lg`var'_`tipo' $control_basico (SharePOS_`nivel'=ShareCompetitorsPOS_`nivel')  if  VariacionIncreasePrice30_==0, maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "Yes"
					estadd scalar NumMolecules=e(K1)
				}

				qui eststo: reghdfe Lg`var'_`tipo' $control_basico (SharePOS_`nivel'=ShareCompetitorsPOS_`nivel')   if  `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "No"
					estadd local MoleculeTrends "Yes"
					estadd scalar NumMolecules=e(K1)
				}
				



				qui eststo: reghdfe Lg`var'_`tipo'   $control_dinamico (SharePOS_`nivel'=ShareCompetitorsPOS_`nivel') if  `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				if("`nivel'"=="Grupo"){
					estadd ysumm
					estadd local BalancedPanel "Yes"
					estadd local Outliers "Yes"
					estadd local Molecule "Yes"
					estadd local ATC "No"
					estadd local Year "Yes"
					estadd local Controls "Yes"
					estadd local LagControls "Yes"
					estadd local MoleculeTrends "Yes"
					estadd scalar NumMolecules=e(K1)
				}
				
				


				if("`nivel'"=="Grupo"){
					 estout using "$latexcodes/Reg_`tipo'_`var'_`channel'_priceAll_`nivel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
					 /*rename(D.POS2 POS2)*/ ///
					 replace keep(SharePOS_`nivel') ///
					   stats(N N_clust NumMolecules Molecule Year Controls LagControls  BalancedPanel MoleculeTrends Outliers, fmt(a2 a2 a2  %~#s %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "Year F.E." "Controls" "Lagged Controls" "Balanced panel?" "Molecule trends?" "Outliers excluded?"))
	}
				else{
					 estout using "$latexcodes/Reg_`tipo'_`var'_`channel'_priceAll_`nivel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
					 /*rename(D.POS2 POS2)*/ ///
					 replace keep(SharePOS_`nivel') 
				}
			}
		}
	}
	*/
}



reghdfe LgInstitucional_Prom_STD POS2  $control_basico ShareCompetitorsPOS_ATC   if  Institucional_Count==$panelcompelto, absorb(CUM1 Periodo) vce(cluster ATC_Grupo)


foreach channel in   2{
	foreach tipo in  Prom_STD Unidades_STD {
		foreach var in  Institucional{
			eststo clear
			eststo: reghdfe Lg`var'_`tipo' POS2  $control_basico ShareCompetitorsPOS_ATC  , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "No"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "Yes"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "No"
			estadd scalar NumMolecules=e(K1)

			eststo: reghdfe Lg`var'_`tipo' POS2  $control_basico  ShareCompetitorsPOS_ATC   if  `var'_Count==$panelcompelto , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "Yes"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "No"
			estadd scalar NumMolecules=e(K1)

			eststo: reghdfe Lg`var'_`tipo' POS2  $control_basico ShareCompetitorsPOS_ATC   if `var'_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)


			eststo: reghdfe Lg`var'_`tipo' POS2  $control_basico ShareCompetitorsPOS_ATC  if `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)


			eststo: reghdfe Lg`var'_`tipo' POS2   $control_dinamico ShareCompetitorsPOS_ATC if  `var'_Count==$panelcompelto & VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)
			


			 estout using "$latexcodes/Reg_`tipo'_`var'_`channel'_SharePOS_ATC.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
			 /*rename(D.POS2 POS2)*/ ///
			 replace keep(POS2 ShareCompetitorsPOS_ATC) ///
			 stats(N N_clust NumMolecules Molecule Year Controls LagControls  BalancedPanel MoleculeTrends Outliers, fmt(a2 a2 a2  %~#s %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "Year F.E." "Controls" "Lagged Controls" "Balanced panel?" "Molecule trends?" "Outliers excluded?"))


		}
	}
}
/*

