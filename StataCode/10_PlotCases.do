set more off
clear all
set matsize 800
use "$base_out/MedicamentosRegsLong.dta",clear
bys Periodo: egen ValorTotalAno=sum(Institucional_Valor)
gsort +Periodo -Institucional_Valor
bys Periodo: gen Frac_ValorTotalAno=sum(Institucional_Valor)/ValorTotalAno
gen Keep=(Frac_ValorTotalAno<0.95) if !missing(Frac_ValorTotalAno)


label var Generico "Generic"
label var FranjaVerde "Essential Medicine"
label var FranjaVioleta "Controlled Substance"
label var Inserto "Insert"
label var OTC "OTC"
label var Presentations "No. of presentations"

label var CompetidoresGrupo "No. pharm. in therapeutical group"
label var CompetidoresATC "No. pharm. in ATC group"

drop if Canal2==2

encode ATC_Grupo0 , gen (ATC_Grupo)


label var NumPOSATC "No. drugs in ATC group in POS"
label var NumPOSGrupo "No. drugs in therapeutical in POS"
label var NumVMRATC "No. drugs in ATC group with RP"
label var NumVMRGrupo "No. drugs in pharmacological subgroup with RP"
gen DGenericosATC=(GenericosATC>0) if !missing(GenericosATC)
gen DGenericosGrupo=(GenericosGrupo>0) if !missing(GenericosGrupo)
gen LgCompetidoresATC=log(CompetidoresATC)
gen LgCompetidoresGrupo=log(CompetidoresGrupo)
gen LgNumPOSATC=log(NumPOSATC)
gen LgNumPOSGrupo=log(NumPOSGrupo)
replace LgNumPOSATC=0 if LgNumPOSATC==.
replace LgNumPOSGrupo=0 if LgNumPOSGrupo==.
label var LgNumPOSATC "log(No. drugs in ATC group in POS)"
label var LgNumPOSGrupo "log(No. drugs in pharmacological subgroup in POS)"

gen PropCompetidoresPOSATC=NumPOSATC/CompetidoresATC 
gen PropCompetidoresPOSGrupo=NumPOSGrupo/CompetidoresGrupo 
gen OnePOSATC=(NumPOSATC==1) & !missing(NumPOSATC)
gen OnePOSGrupo=(NumPOSGrupo==1) & !missing(NumPOSGrupo )


compress
xtset CUM1 Periodo, yearly
gen WeightShare2007=Institucional_Valor/ValorTotalAno if Periodo==2007
replace WeightShare2007=L.WeightShare2007 if WeightShare2007==.

gen missing_info=missing(Institucional_Prom_STD)
label var missing_info "Missing price/quantity information"



foreach var of varlist  Institucional_Prom_STD   Institucional_Unidades_STD {
	gen double Lg`var'=log(`var')
}

label var LgInstitucional_Prom_STD "Log average price for EPS and IPS"  
label var LgInstitucional_Unidades_STD "Log units for EPS and IPS"


label var POS "POS"
label var VMR "RP"
label var POS2 "POS"

label var VMR2 "RP"
label var VMR3 "RP"

replace POSChange=1 if PrimerANOPOS==2007
replace PrimerANOPOS=. if POSChange==0
replace UltimoANOPOS=. if POSChange==0
gen TiempoPOS=Periodo-PrimerANOPOS
replace TiempoPOS=UltimoANOPOS-Periodo if UltimoANOPOS!=$ultimoano

gen Price2007=Institucional_Prom_STD if Periodo==2007
bys CUM1: replace Price2007=Price2007[_n-1] if Price2007==.
gen RelativePrice=Institucional_Prom_STD/Price2007

replace PrimerANOPOS=99 if PrimerANOPOS==.

preserve
keep if ATC0=="N05CD08"  & Institucional_Count==$panelcompelto
bys Periodo PrimerANOPOS: egen AveragePrice=mean(RelativePrice)
twoway (line RelativePrice Periodo if CUM1==19942582) (line AveragePrice Periodo if PrimerANOPOS==2007) , xline(2012) legend(order (1 "DORMIPRON 15 MG" 2 "Midazolam listed since 2007")) title("Price evolution for N05CD08-Midazolam") graphregion(color(white))
graph export "$graphs/19942582.pdf", replace

twoway (line RelativePrice Periodo if CUM1==19945171) (line AveragePrice Periodo if PrimerANOPOS==2007), xline(2012) legend(order (1 "MIDAZOLAM 15MG - BRAUN" 2 "Midazolam listed since 2007"))  graphregion(color(white))
graph export "$graphs/19945171.pdf", replace
restore

preserve
keep if ATC_Grupo0=="C09DA"  & Institucional_Count==$panelcompelto
bys Periodo PrimerANOPOS: egen AveragePrice=mean(RelativePrice)
twoway (line RelativePrice Periodo if CUM1==19944003) (line AveragePrice Periodo if PrimerANOPOS==99) , xline(2012) legend(order (1 "SATOREN - FORTE " 2 "Angiotensin II not listed")) title("Price evolution for C09DA-Angiotensin II") graphregion(color(white))
graph export "$graphs/19944003.pdf", replace
restore

preserve
keep if ATC0=="J01CR02" & Institucional_Count==$panelcompelto
bys Periodo PrimerANOPOS: egen AveragePrice=mean(RelativePrice)
twoway (line RelativePrice Periodo if CUM1==223135) (line AveragePrice Periodo if PrimerANOPOS==99), xline(2012) legend(order (1 "CURAM 500 MG" 2 "Not listed")) title("Price evolution for J01CR02" "Amoxicillin and enzyme inhibitor ") graphregion(color(white))
graph export "$graphs/223135.pdf", replace
restore

preserve
keep if ATC_Grupo0=="N06AB" & Institucional_Count==$panelcompelto
bys Periodo PrimerANOPOS: egen AveragePrice=mean(RelativePrice)
twoway (line RelativePrice Periodo if CUM1==218588) (line AveragePrice Periodo if PrimerANOPOS==2007), xline(2014) legend(order (1 "SEROXAT 20 MG" 2 "Listed since 2007")) title("Price evolution for N06AB" "Selective serotonin reuptake inhibitors") graphregion(color(white))
graph export "$graphs/218588.pdf", replace
restore



preserve
keep if ATC_Grupo0=="N05AH" & Institucional_Count==$panelcompelto
bys Periodo PrimerANOPOS: egen AveragePrice=mean(RelativePrice)
twoway (line RelativePrice Periodo if CUM1==19946498) (line AveragePrice Periodo if PrimerANOPOS==2012), xline(2012) legend(order (1 "OLAZAP 5 MG" 2 "Listed since 2007")) title("Price evolution for N05AH" "Diazepines, oxazepines, thiazepines and oxepines") graphregion(color(white))
graph export "$graphs/19946498.pdf", replace
restore

/*
2012-N05CD08 - 19945171
-19942582
2012-C09DA01-19944003
2014-C08CA01-19928629
2012-J01CR02-223135
2014-N06AB05-218588
2012-N05AH03-19946498
2012-J01FA10-19947945
*/
