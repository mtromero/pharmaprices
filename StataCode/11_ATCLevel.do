set more off
clear all
set maxvar 10000
set matsize 10000
use "$base_out/MedicamentosRegsLong.dta",clear



label var CompetidoresGrupo "Nº pharm. in therapeutical group"
label var CompetidoresATC "Nº pharm. in ATC group"
drop if Canal2==2
tsset CUM1 Periodo

encode ATC_Grupo0 , gen (ATC_Grupo)
encode ATC0 , gen (ATC)



label var NumPOSATC "Nº pharm. in ATC group in POS"
label var NumPOSGrupo "Nº pharm. in therapeutical in POS"
label var NumVMRATC "Nº pharm. in ATC group with RP"
label var NumVMRGrupo "Nº pharm. in therapeutical with RP"

compress
*xtset CUM1 Periodo, yearly


global control_basico VMR2 DGenericosATC HHATCMax
global control_dinamico VMR2 L.VMR2  L.L.VMR2  DGenericosATC L.DGenericosATC  L.L.DGenericosATC  HHATCMax L.HHATCMax L.L.HHATCMax

collapse (sum) POS (mean) VMR HHATC0 GenericosATC0 CompetidoresATC0 ATC_Grupo, by(ATC Periodo) 

gen DVMR=(VMR>0) if !missing(VMR)	
gen DPOS=(POS>0) if !missing(POS)
gen DGenericosATC0=(GenericosATC0>0) if !missing(GenericosATC0)
gen lgCompetidoresATC0=log(CompetidoresATC0) if !missing(CompetidoresATC0)

eststo clear
eststo: reghdfe HHATC0 DPOS  , absorb(ATC Periodo) vce(cluster ATC_Grupo)
estadd ysumm
estadd scalar NumMolecules=e(N_clust)
eststo: reghdfe DGenericosATC0 DPOS  , absorb(ATC Periodo) vce(cluster ATC_Grupo)
estadd ysumm
estadd scalar NumMolecules=e(N_clust)
eststo: reghdfe DVMR DPOS   , absorb(ATC Periodo) vce(cluster ATC_Grupo)
estadd ysumm
estadd scalar NumMolecules=e(N_clust)
eststo: reghdfe lgCompetidoresATC0 DPOS   , absorb(ATC Periodo) vce(cluster ATC_Grupo)
estadd ysumm
estadd scalar NumMolecules=e(N_clust)
estout using "$latexcodes/Reg_ATC_Level.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
replace keep(DPOS) ///
stats(N N_clust, fmt(a2 a2) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" ))


bys ATC: egen PrimerANOPOS_temp=min(Periodo) if DPOS==1
bys ATC: egen PrimerANOPOS=mean(PrimerANOPOS_temp)
drop PrimerANOPOS_temp

bys ATC: egen UltimoANOPOS_temp=max(Periodo) if DPOS==1
bys ATC: egen UltimoANOPOS=mean(UltimoANOPOS_temp)
drop UltimoANOPOS_temp

replace PrimerANOPOS=. if PrimerANOPOS==2007
gen TiempoPOS=Periodo-PrimerANOPOS
replace TiempoPOS=UltimoANOPOS-Periodo if UltimoANOPOS!=$ultimoano


tabulate TiempoPOS, gen(TiempoPOSI)

foreach var of varlist TiempoPOSI* {
replace `var'=0 if `var'==.
}

sum TiempoPOS
gen TiempoPOS2=TiempoPOS-(r(min)-1)
replace TiempoPOS2=0 if TiempoPOS2==.


gen TiempoPOS3=.
replace TiempoPOS3=1 if TiempoPOS<=-4 & !missing(TiempoPOS)
replace TiempoPOS3=2 if TiempoPOS==-3 & !missing(TiempoPOS)
replace TiempoPOS3=3 if TiempoPOS==-2 & !missing(TiempoPOS)
replace TiempoPOS3=4 if TiempoPOS==-1 & !missing(TiempoPOS)
replace TiempoPOS3=5 if TiempoPOS==0 & !missing(TiempoPOS)
replace TiempoPOS3=6 if TiempoPOS==1 & !missing(TiempoPOS)
replace TiempoPOS3=7 if TiempoPOS==2 & !missing(TiempoPOS)
replace TiempoPOS3=8 if TiempoPOS==3 & !missing(TiempoPOS)
replace TiempoPOS3=9 if TiempoPOS>=4 & !missing(TiempoPOS)
replace TiempoPOS3=0 if TiempoPOS3==.

sum TiempoPOS
scalar base1=-r(min)
fvset base `=scalar(base1)' TiempoPOS2
char TiempoPOS2[omit] 0
fvset base 4 TiempoPOS3
char TiempoPOS3[omit] 0

reghdfe HHATC0 i.TiempoPOS3  , absorb(ATC Periodo) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on HH-Index") title("Evolution of HH-Index before and after" "a drug is included in the benefit plan") 
graph export "$graphs/EventStudyPOS_ATCLevel_HH.pdf", replace 

reghdfe DGenericosATC0 i.TiempoPOS3  , absorb(ATC Periodo) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on Generic") title("Evolution of generic subsitutes before and after" "a drug is included in the benefit plan") 
graph export "$graphs/EventStudyPOS_ATCLevel_Generics.pdf", replace 

reghdfe DVMR i.TiempoPOS3  , absorb(ATC Periodo) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on price regulations") title("Evolution of price regulations before and after" "a drug is included in the benefit plan") 
graph export "$graphs/EventStudyPOS_ATCLevel_PriceRegulations.pdf", replace 

reghdfe lgCompetidoresATC0 i.TiempoPOS3   , absorb(ATC Periodo) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on competitors") title("Evolution of number of competitors before and after" "a drug is included in the benefit plan") 
graph export "$graphs/EventStudyPOS_ATCLevel_Competitors.pdf", replace 
