/*
use "$base_out\MedicamentosRegs.dta",clear
sort CUM1 Periodo
by CUM1: gen diff=POS2-POS2[_n-1]
*/

set more off
clear all
set maxvar 10000
set matsize 10000
use "$base_out\MedicamentosRegsLong_Comercial.dta",clear
label var CompetidoresGrupo "N� pharm. in therapeutical group"
label var CompetidoresATC "N� pharm. in ATC group"

drop if Canal2==2
tsset CUM1 Periodo

encode ATC_Grupo0 , gen (ATC_Grupo)



label var NumPOSATC "N� pharm. in ATC group in POS"
label var NumPOSGrupo "N� pharm. in therapeutical in POS"
label var NumVMRATC "N� pharm. in ATC group with RP"
label var NumVMRGrupo "N� pharm. in therapeutical with RP"

compress
*xtset CUM1 Periodo, yearly



foreach var of varlist  Comercial_Prom_STD   Comercial_Unidades_STD {
gen double Lg`var'=log(`var')
}

label var LgComercial_Prom_STD "Log average price for EPS and IPS"  
label var LgComercial_Unidades_STD "Log units for EPS and IPS"


label var POS "POS"
label var VMR "RP"
label var POS2 "POS"

label var VMR2 "RP"
label var VMR3 "RP"


replace PrimerANOPOS=. if POSChange==0
replace UltimoANOPOS=. if POSChange==0
gen TiempoPOS=Periodo-PrimerANOPOS
replace TiempoPOS=UltimoANOPOS-Periodo if UltimoANOPOS!=2014


gen TiempoVMR=Periodo-PrimerANOVMR
tabulate TiempoPOS, gen(TiempoPOSI)
tabulate TiempoVMR, gen(TiempoVMRI)


foreach var of varlist TiempoPOSI1- TiempoVMRI10{
replace `var'=0 if `var'==.
}

gen TiempoPOS2=TiempoPOS+8
replace TiempoPOS2=0 if TiempoPOS2==.
gen TiempoVMR2=TiempoVMR+6
replace TiempoVMR2=0 if TiempoVMR2==.


fvset base 7 TiempoPOS2
char TiempoPOS2[omit] 0
fvset base 5 TiempoVMR2
char TiempoVMR2[omit] 0
 gen LgAnosExpedicion=log(AnosExpedicion)
by CUM1: gen CambioNumPOSATC=(D.NumPOSATC>0)
by CUM1: gen CambioNumPOSATC2=(D.NumPOSATC<0)
gen LgNumPOSATC=log(NumPOSATC)
gen LgNumPOSGrupo=log(NumPOSGrupo)
 gen DGenericos=(GenericosATC>0) if !missing(GenericosATC)
gen LgCompetidoresATC=log(CompetidoresATC)


foreach channel in   2{
foreach tipo in  Prom_STD Unidades_STD{
foreach var in  Comercial {

areg Lg`var'_`tipo' i.TiempoPOS2 VMR2 DGenericos LgCompetidoresATC i.Periodo , absorb(CUM1) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) ci rename(1.TiempoPOS2="-7" 2.TiempoPOS2 ="-6" 3.TiempoPOS2= "-5" 4.TiempoPOS2= "-4" 5.TiempoPOS2= "-3" 6.TiempoPOS2= "-2" 7.TiempoPOS2 ="-1" 8.TiempoPOS2= "0" ///
9.TiempoPOS2= "1" 10.TiempoPOS2= "2" 11.TiempoPOS2= "3" 12.TiempoPOS2 ="4" 13.TiempoPOS2 ="5" 14.TiempoPOS2 ="6" 15.TiempoPOS2 ="7")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on Price") title("Event study for POS")
 graph export "$graphs\EventStudyPOS_`var'_`tipo'_`channel'_normal.pdf", replace
 
areg Lg`var'_`tipo' i.TiempoPOS2 VMR2 DGenericos LgCompetidoresATC i.Periodo if  `var'_Count==8, absorb(CUM1) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) ci rename(1.TiempoPOS2="-7" 2.TiempoPOS2 ="-6" 3.TiempoPOS2= "-5" 4.TiempoPOS2= "-4" 5.TiempoPOS2= "-3" 6.TiempoPOS2= "-2" 7.TiempoPOS2 ="-1" 8.TiempoPOS2= "0" ///
9.TiempoPOS2= "1" 10.TiempoPOS2= "2" 11.TiempoPOS2= "3" 12.TiempoPOS2 ="4" 13.TiempoPOS2 ="5" 14.TiempoPOS2 ="6" 15.TiempoPOS2 ="7")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on Price") title("Event study for POS")
 graph export "$graphs\EventStudyPOS_`var'_`tipo'_`channel'_completo.pdf", replace
 
areg Lg`var'_`tipo' i.TiempoPOS2 VMR2 DGenericos LgCompetidoresATC c.Periodo#ATC_Grupo  if  `var'_Count==8, absorb(CUM1) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) ci rename(1.TiempoPOS2="-7" 2.TiempoPOS2 ="-6" 3.TiempoPOS2= "-5" 4.TiempoPOS2= "-4" 5.TiempoPOS2= "-3" 6.TiempoPOS2= "-2" 7.TiempoPOS2 ="-1" 8.TiempoPOS2= "0" ///
9.TiempoPOS2= "1" 10.TiempoPOS2= "2" 11.TiempoPOS2= "3" 12.TiempoPOS2 ="4" 13.TiempoPOS2 ="5" 14.TiempoPOS2 ="6" 15.TiempoPOS2 ="7")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on Price") title("Event study for POS")
 graph export "$graphs\EventStudyPOS_`var'_`tipo'_`channel'_completo_trend.pdf", replace
 
 
 areg Lg`var'_`tipo' i.TiempoPOS2 VMR2 DGenericos LgCompetidoresATC c.Periodo#ATC_Grupo  if  POSChange==1  & `var'_Count==8, absorb(CUM1) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) ci rename(1.TiempoPOS2="-7" 2.TiempoPOS2 ="-6" 3.TiempoPOS2= "-5" 4.TiempoPOS2= "-4" 5.TiempoPOS2= "-3" 6.TiempoPOS2= "-2" 7.TiempoPOS2 ="-1" 8.TiempoPOS2= "0" ///
9.TiempoPOS2= "1" 10.TiempoPOS2= "2" 11.TiempoPOS2= "3" 12.TiempoPOS2 ="4" 13.TiempoPOS2 ="5" 14.TiempoPOS2 ="6" 15.TiempoPOS2 ="7")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on Price") title("Event study for POS")
 graph export "$graphs\EventStudyPOS_`var'_`tipo'_`channel'_completo_trend_poschange.pdf", replace
}
}
}


foreach ano in  2010 2011 2012 2013 2014 {
areg LgComercial_Prom_STD i.TiempoPOS2 VMR2 DGenericos LgCompetidoresATC c.Periodo#ATC_Grupo  if ((PrimerANOPOS==`ano' & TiempoPOS!=.) | PrimerANOPOS==.) &  Comercial_Count==8, absorb(CUM1) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) ci rename(1.TiempoPOS2="-7" 2.TiempoPOS2 ="-6" 3.TiempoPOS2= "-5" 4.TiempoPOS2= "-4" 5.TiempoPOS2= "-3" 6.TiempoPOS2= "-2" 7.TiempoPOS2 ="-1" 8.TiempoPOS2= "0" ///
9.TiempoPOS2= "1" 10.TiempoPOS2= "2" 11.TiempoPOS2= "3" 12.TiempoPOS2 ="4" 13.TiempoPOS2 ="5" 14.TiempoPOS2 ="6" 15.TiempoPOS2 ="7")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on Price") title("Event study for POS")
 graph export "$graphs\EventStudyPOS_precio_completo_trend_`ano'_entry.pdf", replace
 }
 
 foreach ano in    2010 2011 2012  {
areg LgComercial_Prom_STD i.TiempoPOS2 VMR2 DGenericos LgCompetidoresATC c.Periodo#ATC_Grupo  if ((UltimoANOPOS==`ano' & UltimoANOPOS!=2014 & TiempoPOS!=.) | UltimoANOPOS==.) &  Comercial_Count==8, absorb(CUM1) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) ci rename(1.TiempoPOS2="-7" 2.TiempoPOS2 ="-6" 3.TiempoPOS2= "-5" 4.TiempoPOS2= "-4" 5.TiempoPOS2= "-3" 6.TiempoPOS2= "-2" 7.TiempoPOS2 ="-1" 8.TiempoPOS2= "0" ///
9.TiempoPOS2= "1" 10.TiempoPOS2= "2" 11.TiempoPOS2= "3" 12.TiempoPOS2 ="4" 13.TiempoPOS2 ="5" 14.TiempoPOS2 ="6" 15.TiempoPOS2 ="7")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on Price") title("Event study for POS")
 graph export "$graphs\EventStudyPOS_precio_completo_trend_`ano'_exit.pdf", replace
 }
 


foreach channel in   2{
foreach tipo in  Prom_STD Unidades_STD{
foreach var in  Comercial {

areg Lg`var'_`tipo' i.TiempoVMR2 POS2 DGenericos LgCompetidoresATC  i.Periodo , absorb(CUM1) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoVMR2) ci rename(1.TiempoVMR2="-5" 2.TiempoVMR2 ="-4" 3.TiempoVMR2= "-3" 4.TiempoVMR2= "-2" 5.TiempoVMR2= "-1" 6.TiempoVMR2= "0" 7.TiempoVMR2 ="1" 8.TiempoVMR2= "2" ///
9.TiempoVMR2= "3" 10.TiempoVMR2= "4")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on Price") title("Event study for RP")
 graph export "$graphs\EventStudyRP_`var'_`tipo'_`channel'_normal.pdf", replace
 
areg Lg`var'_`tipo' i.TiempoVMR2 POS2 DGenericos LgCompetidoresATC  i.Periodo if  `var'_Count==8, absorb(CUM1) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoVMR2) ci rename(1.TiempoVMR2="-5" 2.TiempoVMR2 ="-4" 3.TiempoVMR2= "-3" 4.TiempoVMR2= "-2" 5.TiempoVMR2= "-1" 6.TiempoVMR2= "0" 7.TiempoVMR2 ="1" 8.TiempoVMR2= "2" ///
9.TiempoVMR2= "3" 10.TiempoVMR2= "4")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on Price") title("Event study for RP")
 graph export "$graphs\EventStudyRP_`var'_`tipo'_`channel'_completo.pdf", replace
 
areg Lg`var'_`tipo' i.TiempoVMR2 POS2 DGenericos LgCompetidoresATC c.Periodo#ATC_Grupo if  `var'_Count==8, absorb(CUM1) vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoVMR2) ci rename(1.TiempoVMR2="-5" 2.TiempoVMR2 ="-4" 3.TiempoVMR2= "-3" 4.TiempoVMR2= "-2" 5.TiempoVMR2= "-1" 6.TiempoVMR2= "0" 7.TiempoVMR2 ="1" 8.TiempoVMR2= "2" ///
9.TiempoVMR2= "3" 10.TiempoVMR2= "4")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on Price") title("Event study for RP")
 graph export "$graphs\EventStudyRP_`var'_`tipo'_`channel'_completo_trend.pdf", replace
}
}
}

