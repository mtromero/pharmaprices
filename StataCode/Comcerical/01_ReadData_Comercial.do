

import delimited "$base_out\DataGenerico_Final.csv", clear case(preserve)
drop if CUM1=="NA"
sort CUM1
drop in 26260
replace Generico="" if Generico=="NA"
replace Franja="" if Franja=="SIN ESPECIFICAR"
replace Inserto="" if Inserto=="NA"
replace IDRegistro="" if IDRegistro=="NA"
destring, replace
drop NombreRegistro
save "$base_out\DataGenerico_Final.dta" ,replace


use "$base_out\Medicamentos_Total.dta" ,clear
drop Institucional* Compra* Comercial_Lab_Min_STD Comercial_May_Min_STD Comercial_Lab_Max_STD Comercial_May_Max_STD Comercial_Lab_Min Comercial_Lab_Max Comercial_Lab_Unidades Comercial_Lab_Prom Comercial_May_Min Comercial_May_Max Comercial_May_Unidades Comercial_May_Prom

gen ImportarModalidad=0
replace ImportarModalidad=1 if MODALIDAD=="IMPORTAR Y VENDER" | MODALIDAD=="IMPORTAR, EMPACAR Y VENDER"  | MODALIDAD=="IMPORTAR, ENVASAR Y VENDER" | MODALIDAD=="IMPORTAR, FABRICAR Y VENDER" | MODALIDAD=="IMPORTAR, SEMIELABORAR Y VENDER"

gen FECHA_EXPEDICION2 = date(FECHA_EXPEDICION, "YMD")
format FECHA_EXPEDICION2 %td
gen FECHA_VENCIMIENTO2 = date(FECHA_VENCIMIENTO, "YMD")
format FECHA_VENCIMIENTO2 %td		

gen AnosExpedicion=Periodo-year(FECHA_EXPEDICION2)

compress
xtset MediID2 Periodo, yearly

replace ATC0="" if ATC0=="NA"
replace ATC1="" if ATC1=="NA"
replace ATC2="" if ATC2=="NA"
replace ATC3="" if ATC3=="NA"
drop if ATC0==""

replace DESCRIPCION_ATC0="" if DESCRIPCION_ATC0=="NA"
replace DESCRIPCION_ATC1="" if DESCRIPCION_ATC1=="NA"
replace DESCRIPCION_ATC2="" if DESCRIPCION_ATC2=="NA"
replace DESCRIPCION_ATC3="" if DESCRIPCION_ATC3=="NA"

replace PRINCIPIO_ACTIVO0="" if PRINCIPIO_ACTIVO0=="NA"
replace PRINCIPIO_ACTIVO1="" if PRINCIPIO_ACTIVO1=="NA"
replace PRINCIPIO_ACTIVO2="" if PRINCIPIO_ACTIVO2=="NA"
replace PRINCIPIO_ACTIVO3="" if PRINCIPIO_ACTIVO3=="NA"


save "$base_out\Medicamentos_TotalFill_Comercial.dta" ,replace

preserve
keep MediID2 CUM1 Periodo
sort MediID2 CUM1 Periodo
quietly by MediID2 CUM1 Periodo:  gen dup = cond(_N==1,0,_n)
drop if dup>1
collapse (count) Presentations=MediID2, by(CUM1 Periodo)
label var Presentations "No. of pharmaceutical presentation"
save "$base_out\PresentationsCUM1_Comercial.dta" ,replace
restore



sort CUM1 Periodo
collapse (first)  ATC1 DESCRIPCION_ATC1 PRINCIPIO_ACTIVO1 ATC2 DESCRIPCION_ATC2 PRINCIPIO_ACTIVO2 ATC3 DESCRIPCION_ATC3 PRINCIPIO_ACTIVO3  ATC0 DESCRIPCION_ATC0 PRINCIPIO_ACTIVO0 (mean) AnosExpedicion ImportarModalidad POS VMR  , by(CUM1 Periodo)
foreach var of varlist ATC1 DESCRIPCION_ATC1 PRINCIPIO_ACTIVO1 ATC2 DESCRIPCION_ATC2 PRINCIPIO_ACTIVO2 ATC3 DESCRIPCION_ATC3 PRINCIPIO_ACTIVO3  ATC0 DESCRIPCION_ATC0 PRINCIPIO_ACTIVO0{
by CUM1: egen `var'_mode=  mode(`var'), maxmode
replace `var'= `var'_mode
drop `var'_mode
}
xtset CUM1 Periodo, yearly
sort CUM1 Periodo
by CUM1: gen cont=_n 
replace POS=0 if POS!=0 & POS!=1 & !missing(POS)
*Get rid of sandwich situations
by CUM1: replace POS=1 if POS==0 & POS[_n-1]==1 & POS[_n+1]==1 & !missing(POS[_n+1]) & !missing(POS[_n-1])
by CUM1: replace POS=0 if POS==1 & POS[_n-1]==0 & POS[_n+1]==0 & !missing(POS[_n+1]) & !missing(POS[_n-1])

by CUM1: replace POS=1 if POS==0 & POS[_n-2]==1 & POS[_n+2]==1 & !missing(POS[_n+2]) & !missing(POS[_n-2])
by CUM1: replace POS=0 if POS==1 & POS[_n-2]==0 & POS[_n+2]==0 & !missing(POS[_n+2]) & !missing(POS[_n-2])

by CUM1: replace POS=1 if POS==0 & POS[_n-1]==1 & POS[_n+1]==1 & !missing(POS[_n+1]) & !missing(POS[_n-1])
by CUM1: replace POS=0 if POS==1 & POS[_n-1]==0 & POS[_n+1]==0 & !missing(POS[_n+1]) & !missing(POS[_n-1])

by CUM1: replace POS=1 if POS==0 & POS[_n-3]==1 & POS[_n+3]==1 & !missing(POS[_n+3]) & !missing(POS[_n-3])
by CUM1: replace POS=0 if POS==1 & POS[_n-3]==0 & POS[_n+3]==0 & !missing(POS[_n+3]) & !missing(POS[_n-3])

by CUM1: replace POS=1 if POS==0 & POS[_n-2]==1 & POS[_n+2]==1 & !missing(POS[_n+2]) & !missing(POS[_n-2])
by CUM1: replace POS=0 if POS==1 & POS[_n-2]==0 & POS[_n+2]==0 & !missing(POS[_n+2]) & !missing(POS[_n-2])

by CUM1: replace POS=1 if POS==0 & POS[_n-1]==1 & POS[_n+1]==1 & !missing(POS[_n+1]) & !missing(POS[_n-1])
by CUM1: replace POS=0 if POS==1 & POS[_n-1]==0 & POS[_n+1]==0 & !missing(POS[_n+1]) & !missing(POS[_n-1])


by CUM1: ipolate POS cont,  generate(POS2)
*At this point only missing at beggining at end... I think its safe to assume that POS status was the same before first obs and after last obs
sort CUM1 Periodo
by CUM1: replace POS2 = POS2[_n+1] if missing(POS2)
by CUM1: replace POS2 = POS2[_n-1] if missing(POS2)
*Bro the weird cases
*bro CUM1 Periodo POS POS2 if POS2!=0 & POS2!=1 & !missing(POS2)
replace POS2=0 if CUM1==45695 & Periodo==2010
replace POS2=0 if CUM1==46415 & Periodo==2009
replace POS2=0 if CUM1==46415 & Periodo==2010

replace POS2=1 if CUM1==56399 & Periodo==2012
replace POS2=1 if CUM1==229243 & Periodo==2012

replace POS2=0 if CUM1==19900781 & Periodo==2010


replace POS2=1 if CUM1==19908725 & Periodo==2012
replace POS2=1 if CUM1==19908725 & Periodo==2013

replace POS2=1 if CUM1==19912566 & Periodo==2011

replace POS2=1 if CUM1==19924026

replace POS2=1 if CUM1==19948710

replace POS2=1 if CUM1==19960161 & Periodo==2012
replace POS2=1 if CUM1==19960161 & Periodo==2013

replace POS2=1 if CUM1==19963605 & Periodo==2012
replace POS2=1 if CUM1==19965360 & Periodo==2012
replace POS2=1 if CUM1==19968458 & Periodo==2012


replace POS2=1 if CUM1==19973739 & Periodo==2012
replace POS2=1 if CUM1==19973739 & Periodo==2013

replace POS2=1 if CUM1==19977532 & Periodo==2012
replace POS2=1 if CUM1==19980483 & Periodo==2012
replace POS2=1 if CUM1==19981307 & Periodo==2012

replace POS2=0 if CUM1==19984331 & Periodo==2012
replace POS2=0 if CUM1==19984331 & Periodo==2013

replace POS2=1 if CUM1==19984832 & Periodo==2012
replace POS2=1 if CUM1==19987303 & Periodo==2012
replace POS2=1 if CUM1==19987914 & Periodo==2012

replace POS2=1 if CUM1==20004850 & Periodo==2012
replace POS2=1 if CUM1==20004850 & Periodo==2013

replace POS2=1 if CUM1==20005175 & Periodo==2012
replace POS2=1 if CUM1==20017191 & Periodo==2012
replace POS2=1 if CUM1==20021271 & Periodo==2012

replace POS2=1 if CUM1==19927084 & Periodo==2012


replace POS2=1 if CUM1==20029565 & Periodo==2012
replace POS2=1 if CUM1==20029565 & Periodo==2013

replace POS2=1 if CUM1==19968904 
replace POS2=1 if CUM1==19983877 




replace POS2=. if POS2!=0 & POS2!=1


by CUM1: replace POS2=1 if POS2==0 & POS2[_n-1]==1 & POS2[_n+1]==1 & !missing(POS2[_n+1]) & !missing(POS2[_n-1])
by CUM1: replace POS2=0 if POS2==1 & POS2[_n-1]==0 & POS2[_n+1]==0 & !missing(POS2[_n+1]) & !missing(POS2[_n-1])

by CUM1: replace POS2=1 if POS2==0 & POS2[_n-2]==1 & POS2[_n+2]==1 & !missing(POS2[_n+2]) & !missing(POS2[_n-2])
by CUM1: replace POS2=0 if POS2==1 & POS2[_n-2]==0 & POS2[_n+2]==0 & !missing(POS2[_n+2]) & !missing(POS2[_n-2])

by CUM1: replace POS2=1 if POS2==0 & POS2[_n-1]==1 & POS2[_n+1]==1 & !missing(POS2[_n+1]) & !missing(POS2[_n-1])
by CUM1: replace POS2=0 if POS2==1 & POS2[_n-1]==0 & POS2[_n+1]==0 & !missing(POS2[_n+1]) & !missing(POS2[_n-1])

by CUM1: replace POS2=1 if POS2==0 & POS2[_n-3]==1 & POS2[_n+3]==1 & !missing(POS2[_n+3]) & !missing(POS2[_n-3])
by CUM1: replace POS2=0 if POS2==1 & POS2[_n-3]==0 & POS2[_n+3]==0 & !missing(POS2[_n+3]) & !missing(POS2[_n-3])

by CUM1: replace POS2=1 if POS2==0 & POS2[_n-2]==1 & POS2[_n+2]==1 & !missing(POS2[_n+2]) & !missing(POS2[_n-2])
by CUM1: replace POS2=0 if POS2==1 & POS2[_n-2]==0 & POS2[_n+2]==0 & !missing(POS2[_n+2]) & !missing(POS2[_n-2])

by CUM1: replace POS2=1 if POS2==0 & POS2[_n-1]==1 & POS2[_n+1]==1 & !missing(POS2[_n+1]) & !missing(POS2[_n-1])
by CUM1: replace POS2=0 if POS2==1 & POS2[_n-1]==0 & POS2[_n+1]==0 & !missing(POS2[_n+1]) & !missing(POS2[_n-1])


by CUM1: ipolate VMR cont,  generate(VMR2)
replace VMR2=. if VMR2!=0 & VMR2!=1 & VMR==.
compress

merge m:1 CUM1 Periodo using "$base_out\PresentationsCUM1_Comercial.dta"
drop _merge
save "$base_out\Medicamentos_CUM_Comercial.dta",replace


use "$base_out\Medicamentos_Total.dta" ,clear


foreach var in  Comercial_Lab Comercial_May {
gen double `var'_Valor=`var'_Unidades_STD*`var'_Prom_STD
}


collapse (first) NombreRegistro  (sum) Comercial_Lab_Valor Comercial_May_Valor  Comercial_Lab_Unidades_STD Comercial_May_Unidades_STD, by(CUM1 Periodo)

merge m:1 CUM1 using "$base_out\DataGenerico_Final.dta"
drop if _merge==2
drop _merge

gen FranjaVerde=(Franja=="VERDE" | Franja=="VERDE - VIOLETA") & !missing(Franja)
gen FranjaVioleta=(Franja=="VIOLETA" | Franja=="VERDE - VIOLETA") & !missing(Franja)
gen OTC=(CondicionVenta=="SIN FORMULA FACULTATIVA") & !missing(CondicionVenta)


foreach var in  Comercial_Lab Comercial_May  {
replace `var'_Unidades_STD=. if `var'_Unidades_STD==0
}
compress
merge 1:1 CUM1 Periodo using "$base_out\Medicamentos_CUM_Comercial.dta"
drop _merge
save "$base_out\Medicamentos_CUM_Comercial.dta",replace

use "$base_out\Medicamentos_Total.dta" ,clear
collapse  (mean) Comercial_Lab_Prom_STD [aw=Comercial_Lab_Unidades_STD], by(CUM1 Periodo)
compress
merge 1:1 CUM1 Periodo using "$base_out\Medicamentos_CUM_Comercial.dta"
drop _merge
save "$base_out\Medicamentos_CUM_Comercial.dta",replace
use "$base_out\Medicamentos_Total.dta" ,clear
collapse  (mean) Comercial_May_Prom_STD [aw=Comercial_May_Unidades_STD], by(CUM1 Periodo)
compress
merge 1:1 CUM1 Periodo using "$base_out\Medicamentos_CUM_Comercial.dta"
drop _merge
save "$base_out\Medicamentos_CUM_Comercial.dta",replace
xtset CUM1 Periodo, yearly
compress
drop if ATC0==""
drop if Periodo==2006
save "$base_out\Medicamentos_CUM_Comercial.dta",replace

replace ATC3="" if ATC3==ATC2 | ATC3==ATC1 | ATC3==ATC0
replace ATC2="" if  ATC2==ATC1 | ATC2==ATC0
replace ATC1="" if  ATC1==ATC0

gen ATC_Grupo0=substr(ATC0,1,5)
gen ATC_Grupo1=substr(ATC1,1,5)
gen ATC_Grupo2=substr(ATC2,1,5)
gen ATC_Grupo3=substr(ATC3,1,5)

replace ATC_Grupo3="" if ATC_Grupo3==ATC_Grupo2 | ATC_Grupo3==ATC_Grupo1 | ATC_Grupo3==ATC_Grupo0
replace ATC_Grupo2="" if  ATC_Grupo2==ATC_Grupo1 | ATC_Grupo2==ATC_Grupo0
replace ATC_Grupo1="" if  ATC_Grupo1==ATC_Grupo0

*First lets calculate HH-index
forvalues j=0/3{
preserve
drop if ATC_Grupo`j'==""
keep ATC_Grupo`j' Comercial_Lab_Valor Comercial_May_Valor  Periodo
rename ATC_Grupo`j' ATC_Grupo
save "$base_out\VentasGroup`j'_Comercial.dta",replace
restore
preserve
drop if ATC`j'==""
keep ATC`j' Comercial_Lab_Valor Comercial_May_Valor Periodo
rename ATC`j' ATC
save "$base_out\VentasATC`j'_Comercial.dta",replace
restore
}

preserve
use "$base_out\VentasATC0_Comercial.dta", clear
append using "$base_out\VentasATC1_Comercial.dta"
append using "$base_out\VentasATC2_Comercial.dta"
append using "$base_out\VentasATC3_Comercial.dta"

bysort ATC Periodo: generate HHATC_Lab= (Comercial_Lab_Valor/ sum(Comercial_Lab_Valor))^2 
bysort ATC Periodo: generate HHATC_May= (Comercial_May_Valor/ sum(Comercial_May_Valor))^2
collapse (mean)  HHATC_Lab HHATC_May, by( ATC Periodo)
rename ATC ATC0
rename HHATC_Lab HHATC0_Lab
rename HHATC_May HHATC0_May
save "$base_out\HHATC0_Comercial.dta",replace
rename ATC0 ATC1
rename HHATC0_Lab HHATC1_Lab
rename HHATC0_May HHATC1_May
save "$base_out\HHATC1_Comercial.dta",replace
rename ATC1 ATC2
rename HHATC1_Lab HHATC2_Lab
rename HHATC1_May HHATC2_May
save "$base_out\HHATC2_Comercial.dta",replace
rename ATC2 ATC3
rename HHATC2_Lab HHATC3_Lab
rename HHATC2_May HHATC3_May
save "$base_out\HHATC3_Comercial.dta",replace

use "$base_out\VentasGroup0_Comercial.dta", clear
append using "$base_out\VentasGroup1_Comercial.dta"
append using "$base_out\VentasGroup2_Comercial.dta"
append using "$base_out\VentasGroup2_Comercial.dta"

bysort ATC_Grupo Periodo: generate HHGrupo_Lab= (Comercial_Lab_Valor/ sum(Comercial_Lab_Valor))^2 
bysort ATC_Grupo Periodo: generate HHGrupo_May= (Comercial_May_Valor/ sum(Comercial_May_Valor))^2
collapse (mean)  HHGrupo_Lab HHGrupo_May, by( ATC_Grupo Periodo)
rename ATC_Grupo ATC_Grupo0
rename HHGrupo_Lab HHGrupo0_Lab
rename HHGrupo_May HHGrupo0_May
save "$base_out\HHGrupo0_Comercial.dta",replace
rename ATC_Grupo0 ATC_Grupo1
rename HHGrupo0_Lab HHGrupo1_Lab
rename HHGrupo0_May HHGrupo1_May
save "$base_out\HHGrupo1_Comercial.dta",replace
rename ATC_Grupo1 ATC_Grupo2
rename HHGrupo1_Lab HHGrupo2_Lab
rename HHGrupo1_May HHGrupo2_May
save "$base_out\HHGrupo2_Comercial.dta",replace
rename ATC_Grupo2 ATC_Grupo3
rename HHGrupo2_Lab HHGrupo3_Lab
rename HHGrupo2_May HHGrupo3_May
save "$base_out\HHGrupo3_Comercial.dta",replace

restore


*Now lets calculate sells, and commpetition
forvalues j=0/3{
preserve
drop if ATC_Grupo`j'==""
gen VMR3=(VMR2>0) & !missing(VMR2)
collapse  (sum) VentasTotalesGrupo_Lab=Comercial_Lab_Valor VentasTotalesGrupo_May=Comercial_May_Valor NumPOSGrupo=POS2 NumVMRGrupo=VMR3 GenericosGrupo=Generico InsertoGrupo=Inserto FranjaVerdeGrupo=FranjaVerde FranjaVioletaGrupo=FranjaVioleta OTCGrupo=OTC (count) CompetidoresGrupo=CUM1, by(ATC_Grupo`j'  Periodo)
rename ATC_Grupo`j' ATC_Grupo
save "$base_out\MediByGroup`j'_Comercial.dta",replace
restore
preserve
drop if ATC`j'==""
gen VMR3=(VMR2>0) & !missing(VMR2)
collapse (sum) VentasTotalesATC_Lab=Comercial_Lab_Valor VentasTotalesATC_May=Comercial_May_Valor NumPOSATC=POS2 NumVMRATC=VMR3 GenericosATC=Generico InsertoATC=Inserto FranjaVerdeATC=FranjaVerde FranjaVioletaATC=FranjaVioleta OTCATC=OTC (count) CompetidoresATC=CUM1, by(ATC`j' Periodo)
rename ATC`j' ATC
save "$base_out\MediByATC`j'_Comercial.dta",replace
restore
}

preserve
use "$base_out\MediByGroup0_Comercial.dta", clear
append using "$base_out\MediByGroup1_Comercial.dta"
append using "$base_out\MediByGroup2_Comercial.dta"
append using "$base_out\MediByGroup3_Comercial.dta"

collapse  (sum) VentasTotalesGrupo_Lab VentasTotalesGrupo_May  NumPOSGrupo=NumPOSGrupo NumVMRGrupo=NumVMRGrupo CompetidoresGrupo=CompetidoresGrupo GenericosGrupo=GenericosGrupo InsertoGrupo=InsertoGrupo FranjaVerdeGrupo=FranjaVerdeGrupo FranjaVioletaGrupo=FranjaVioletaGrupo OTCGrupo=OTCGrupo, by(ATC_Grupo  Periodo)
rename ATC_Grupo ATC_Grupo0
rename NumPOSGrupo NumPOSGrupo0
rename NumVMRGrupo NumVMRGrupo0
rename CompetidoresGrupo CompetidoresGrupo0
rename VentasTotalesGrupo_Lab VentasTotalesGrupo0_Lab
rename VentasTotalesGrupo_May VentasTotalesGrupo0_May
rename GenericosGrupo GenericosGrupo0
rename InsertoGrupo InsertoGrupo0
rename FranjaVerdeGrupo FranjaVerdeGrupo0
rename FranjaVioletaGrupo FranjaVioletaGrupo0
rename OTCGrupo OTCGrupo0
save "$base_out\MediByGroup0_Comercial.dta",replace

rename ATC_Grupo0 ATC_Grupo1
rename NumPOSGrupo0 NumPOSGrupo1
rename NumVMRGrupo0 NumVMRGrupo1
rename CompetidoresGrupo0 CompetidoresGrupo1
rename VentasTotalesGrupo0_Lab VentasTotalesGrupo1_Lab
rename VentasTotalesGrupo0_May VentasTotalesGrupo1_May
rename GenericosGrupo0 GenericosGrupo1
rename InsertoGrupo0 InsertoGrupo1
rename FranjaVerdeGrupo0 FranjaVerdeGrupo1
rename FranjaVioletaGrupo0 FranjaVioletaGrupo1
rename OTCGrupo0 OTCGrupo1
save "$base_out\MediByGroup1_Comercial.dta",replace

rename ATC_Grupo1 ATC_Grupo2
rename NumPOSGrupo1 NumPOSGrupo2
rename NumVMRGrupo1 NumVMRGrupo2
rename CompetidoresGrupo1 CompetidoresGrupo2
rename VentasTotalesGrupo1_Lab VentasTotalesGrupo2_Lab
rename VentasTotalesGrupo1_May VentasTotalesGrupo2_May
rename GenericosGrupo1 GenericosGrupo2
rename InsertoGrupo1 InsertoGrupo2
rename FranjaVerdeGrupo1 FranjaVerdeGrupo2
rename FranjaVioletaGrupo1 FranjaVioletaGrupo2
rename OTCGrupo1 OTCGrupo2
save "$base_out\MediByGroup2_Comercial.dta",replace

rename ATC_Grupo2 ATC_Grupo3
rename NumPOSGrupo2 NumPOSGrupo3
rename NumVMRGrupo2 NumVMRGrupo3
rename CompetidoresGrupo2 CompetidoresGrupo3
rename VentasTotalesGrupo2_Lab VentasTotalesGrupo3_Lab
rename VentasTotalesGrupo2_May VentasTotalesGrupo3_May
rename GenericosGrupo2 GenericosGrupo3
rename InsertoGrupo2 InsertoGrupo3
rename FranjaVerdeGrupo2 FranjaVerdeGrupo3
rename FranjaVioletaGrupo2 FranjaVioletaGrupo3
rename OTCGrupo2 OTCGrupo3
save "$base_out\MediByGroup3_Comercial.dta",replace

use "$base_out\MediByATC0_Comercial.dta", clear
append using "$base_out\MediByATC1_Comercial.dta"
append using "$base_out\MediByATC2_Comercial.dta"
append using "$base_out\MediByATC3_Comercial.dta"
collapse  (sum) VentasTotalesATC_Lab VentasTotalesATC_May NumPOSATC=NumPOSATC NumVMRATC=NumVMRATC CompetidoresATC=CompetidoresATC GenericosATC=GenericosATC InsertoATC=InsertoATC FranjaVerdeATC=FranjaVerdeATC FranjaVioletaATC=FranjaVioletaATC OTCATC=OTCATC, by(ATC  Periodo)

rename ATC ATC0
rename NumPOSATC NumPOSATC0
rename NumVMRATC NumVMRATC0
rename CompetidoresATC CompetidoresATC0
rename VentasTotalesATC_Lab VentasTotalesATC0_Lab
rename VentasTotalesATC_May VentasTotalesATC0_May
rename GenericosATC GenericosATC0
rename InsertoATC InsertoATC0
rename FranjaVerdeATC FranjaVerdeATC0
rename FranjaVioletaATC FranjaVioletaATC0
rename OTCATC OTCATC0
save "$base_out\MediByATC0_Comercial.dta",replace

rename ATC0 ATC1
rename NumPOSATC0 NumPOSATC1
rename NumVMRATC0 NumVMRATC1
rename CompetidoresATC0 CompetidoresATC1
rename VentasTotalesATC0_Lab VentasTotalesATC1_Lab
rename VentasTotalesATC0_May VentasTotalesATC1_May
rename GenericosATC0 GenericosATC1
rename InsertoATC0 InsertoATC1
rename FranjaVerdeATC0 FranjaVerdeATC1
rename FranjaVioletaATC0 FranjaVioletaATC1
rename OTCATC0 OTCATC1
save "$base_out\MediByATC1_Comercial.dta",replace
rename ATC1 ATC2
rename NumPOSATC1 NumPOSATC2
rename NumVMRATC1 NumVMRATC2
rename CompetidoresATC1 CompetidoresATC2
rename VentasTotalesATC1_Lab VentasTotalesATC2_Lab
rename VentasTotalesATC1_May VentasTotalesATC2_May
rename GenericosATC1 GenericosATC2
rename InsertoATC1 InsertoATC2
rename FranjaVerdeATC1 FranjaVerdeATC2
rename FranjaVioletaATC1 FranjaVioletaATC2
rename OTCATC1 OTCATC2
save "$base_out\MediByATC2_Comercial.dta",replace
rename ATC2 ATC3
rename NumPOSATC2 NumPOSATC3
rename NumVMRATC2 NumVMRATC3
rename CompetidoresATC2 CompetidoresATC3
rename VentasTotalesATC2_Lab VentasTotalesATC3_Lab
rename VentasTotalesATC2_May VentasTotalesATC3_May
rename GenericosATC2 GenericosATC3
rename InsertoATC2 InsertoATC3
rename FranjaVerdeATC2 FranjaVerdeATC3
rename FranjaVioletaATC2 FranjaVioletaATC3
rename OTCATC2 OTCATC3
save "$base_out\MediByATC3_Comercial.dta",replace

restore


gen VMR3=(VMR2>0) & !missing(VMR2)
preserve
collapse (max) PosEver=POS2 (min) NoPosEver=POS2, by(CUM1)
gen AlwaysPOS=(PosEver==1 & NoPosEver==1)
gen NeverPOS=(PosEver==0 & NoPosEver==0)
gen POSChange=(PosEver==1 & NoPosEver==0)
save "$base_out\POSStatus_Comercial.dta",replace
restore
*drop if POS2==.


preserve
sort CUM1 Periodo
by CUM1: gen IncreasePrice_Lab=D.Comercial_Lab_Prom_STD/L.Comercial_Lab_Prom_STD
sum IncreasePrice_Lab,d
gen VariacionIncreasePrice_Lab=(IncreasePrice_Lab>r(p95) | IncreasePrice_Lab<r(p5)) if !missing(IncreasePrice_Lab)
replace IncreasePrice_Lab=r(p95) if IncreasePrice_Lab>r(p95) & !missing(IncreasePrice_Lab)
replace IncreasePrice_Lab=r(p5) if IncreasePrice_Lab<r(p5) & !missing(IncreasePrice_Lab)
by CUM1: replace Comercial_Lab_Prom_STD= (L.Comercial_Lab_Prom_STD*(1+IncreasePrice_Lab)) if VariacionIncreasePrice_Lab==1
by CUM1: replace IncreasePrice_Lab=D.Comercial_Lab_Prom_STD/L.Comercial_Lab_Prom_STD


by CUM1: gen IncreaseQuantity_Lab=D.Comercial_Lab_Unidades_STD/L.Comercial_Lab_Unidades_STD
sum IncreaseQuantity_Lab,d
gen VariacionIncreaseQuantity_Lab=(IncreaseQuantity_Lab>r(p95) | IncreaseQuantity_Lab<r(p5)) if !missing(IncreaseQuantity_Lab)
replace IncreaseQuantity_Lab=r(p95) if IncreaseQuantity_Lab>r(p95) & !missing(IncreaseQuantity_Lab)
replace IncreaseQuantity_Lab=r(p5) if IncreaseQuantity_Lab<r(p5) & !missing(IncreaseQuantity_Lab)
by CUM1: replace Comercial_Lab_Unidades_STD= (L.Comercial_Lab_Unidades_STD*(1+IncreaseQuantity_Lab)) if VariacionIncreaseQuantity_Lab==1
by CUM1: replace IncreaseQuantity_Lab=D.Comercial_Lab_Unidades_STD/L.Comercial_Lab_Unidades_STD

*replace Periodo=. if Comercial_Lab_Prom_STD==.
collapse (first) Comercial_LabFirst=Comercial_Lab_Prom_STD ///
(last) Comercial_LabLast=Comercial_Lab_Prom_STD ///
(first) Comercial_LabFirstObs=Periodo ///
(last) Comercial_LabLastObs=Periodo ///
(mean) Comercial_LabMean=Comercial_Lab_Prom_STD ///
(sd) Comercial_LabSd=Comercial_Lab_Prom_STD ///
(max) VariacionIncreaseQuantity_Lab=VariacionIncreaseQuantity_Lab VariacionIncreasePrice_Lab=VariacionIncreasePrice_Lab ///
(count) Comercial_LabCount=Comercial_Lab_Prom_STD, by(CUM1)

foreach var in  Comercial_Lab  {
gen Algo`var'=(`var'Count>0) &!missing(`var'Count)
gen Completo`var'=(`var'Count==(`var'LastObs-`var'FirstObs+1)) if !missing(`var'Count)
gen CoefVar`var'=`var'Sd/`var'Mean
sum CoefVar`var', d
gen Variacion`var'P99=(CoefVar`var'>r(p99)) & !missing(CoefVar`var')
}
save "$base_out\PriceVariabilityLab_Comercial.dta",replace
restore

preserve
sort CUM1 Periodo
by CUM1: gen IncreasePrice_May=D.Comercial_May_Prom_STD/L.Comercial_May_Prom_STD
sum IncreasePrice_May,d
gen VariacionIncreasePrice_May=(IncreasePrice_May>r(p95) | IncreasePrice_May<r(p5)) if !missing(IncreasePrice_May)
replace IncreasePrice_May=r(p95) if IncreasePrice_May>r(p95) & !missing(IncreasePrice_May)
replace IncreasePrice_May=r(p5) if IncreasePrice_May<r(p5) & !missing(IncreasePrice_May)
by CUM1: replace Comercial_May_Prom_STD= (L.Comercial_May_Prom_STD*(1+IncreasePrice_May)) if VariacionIncreasePrice_May==1
by CUM1: replace IncreasePrice_May=D.Comercial_May_Prom_STD/L.Comercial_May_Prom_STD


by CUM1: gen IncreaseQuantity_May=D.Comercial_May_Unidades_STD/L.Comercial_May_Unidades_STD
sum IncreaseQuantity_May,d
gen VariacionIncreaseQuantity_May=(IncreaseQuantity_May>r(p95) | IncreaseQuantity_May<r(p5)) if !missing(IncreaseQuantity_May)
replace IncreaseQuantity_May=r(p95) if IncreaseQuantity_May>r(p95) & !missing(IncreaseQuantity_May)
replace IncreaseQuantity_May=r(p5) if IncreaseQuantity_May<r(p5) & !missing(IncreaseQuantity_May)
by CUM1: replace Comercial_May_Unidades_STD= (L.Comercial_May_Unidades_STD*(1+IncreaseQuantity_May)) if VariacionIncreaseQuantity_May==1
by CUM1: replace IncreaseQuantity_May=D.Comercial_May_Unidades_STD/L.Comercial_May_Unidades_STD

*drop if Comercial_May_Prom_STD==.
collapse (first) Comercial_MayFirst=Comercial_May_Prom_STD ///
(last) Comercial_MayLast=Comercial_May_Prom_STD ///
(first) Comercial_MayFirstObs=Periodo ///
(last) Comercial_MayLastObs=Periodo ///
(mean) Comercial_MayMean=Comercial_May_Prom_STD ///
(sd) Comercial_MaySd=Comercial_May_Prom_STD ///
(max) VariacionIncreaseQuantity_May=VariacionIncreaseQuantity_May VariacionIncreasePrice_May=VariacionIncreasePrice_May ///
(count) Comercial_MayCount=Comercial_May_Prom_STD, by(CUM1)

foreach var in  Comercial_May  {
gen Algo`var'=(`var'Count>0) &!missing(`var'Count)
gen Completo`var'=(`var'Count==(`var'LastObs-`var'FirstObs+1)) if !missing(`var'Count)
gen CoefVar`var'=`var'Sd/`var'Mean
sum CoefVar`var', d
gen Variacion`var'P99=(CoefVar`var'>r(p99)) & !missing(CoefVar`var')
}
save "$base_out\PriceVariabilityMay_Comercial.dta",replace
restore


preserve
drop if Comercial_Lab_Prom_STD==.
sort CUM1 Periodo
collapse (first) PrimerANObsLab=Periodo (last) UltimoANOObsLab=Periodo, by(CUM1)
save "$base_out\PrimerANOObsLab_Comercial.dta",replace
restore
preserve
drop if Comercial_May_Prom_STD==.
sort CUM1 Periodo
collapse (first) PrimerANObsMay=Periodo (last) UltimoANOObsMay=Periodo, by(CUM1)
save "$base_out\PrimerANOObsMay_Comercial.dta",replace
restore
preserve
drop if POS2==.
drop if POS2==0
sort CUM1 Periodo
collapse (first) PrimerANOPOS=Periodo (last) UltimoANOPOS=Periodo, by(CUM1)
save "$base_out\PrimerANOPOS_Comercial.dta",replace
restore
preserve
drop if VMR2==0
drop if VMR2==.
sort CUM1 Periodo
collapse (first) PrimerANOVMR=Periodo (last) UltimoANOVMR=Periodo, by(CUM1)
save "$base_out\PrimerANOVMR_Comercial.dta",replace
restore


merge m:1 ATC0 Periodo using "$base_out\HHATC0_Comercial.dta"
drop if _merge==2
drop _merge
merge m:1 ATC1 Periodo using "$base_out\HHATC1_Comercial.dta"
drop if _merge==2
drop _merge
merge m:1 ATC2 Periodo using "$base_out\HHATC2_Comercial.dta"
drop if _merge==2
drop _merge
merge m:1 ATC3 Periodo using "$base_out\HHATC3_Comercial.dta"
drop if _merge==2
drop _merge

merge m:1 ATC_Grupo0 Periodo using "$base_out\HHGrupo0_Comercial.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo1 Periodo using "$base_out\HHGrupo1_Comercial.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo2 Periodo using "$base_out\HHGrupo2_Comercial.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo3 Periodo using "$base_out\HHGrupo3_Comercial.dta"
drop if _merge==2
drop _merge

merge m:1 ATC0 Periodo using "$base_out\MediByATC0_Comercial.dta"
drop if _merge==2
drop _merge
merge m:1 ATC1 Periodo using "$base_out\MediByATC1_Comercial.dta"
drop if _merge==2
drop _merge
merge m:1 ATC2 Periodo using "$base_out\MediByATC2_Comercial.dta"
drop if _merge==2
drop _merge
merge m:1 ATC3 Periodo using "$base_out\MediByATC3_Comercial.dta"
drop if _merge==2
drop _merge

merge m:1 ATC_Grupo0 Periodo using "$base_out\MediByGroup0_Comercial.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo1 Periodo using "$base_out\MediByGroup1_Comercial.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo2 Periodo using "$base_out\MediByGroup2_Comercial.dta"
drop if _merge==2
drop _merge
merge m:1 ATC_Grupo3 Periodo using "$base_out\MediByGroup3_Comercial.dta"
drop if _merge==2
drop _merge

egen NumPOSATC=rowmax(NumPOSATC0 NumPOSATC1 NumPOSATC2 NumPOSATC3)
egen NumVMRATC=rowmax(NumVMRATC0 NumVMRATC1 NumVMRATC2 NumVMRATC3)
egen CompetidoresATC=rowmax(CompetidoresATC0 CompetidoresATC1 CompetidoresATC2 CompetidoresATC3)
egen GenericosATC=rowmax(GenericosATC0 GenericosATC1 GenericosATC2 GenericosATC3)
egen InsertoATC=rowmax(InsertoATC0 InsertoATC1 InsertoATC2 InsertoATC3)
egen FranjaVerdeATC=rowmax(FranjaVerdeATC0 FranjaVerdeATC1 FranjaVerdeATC2 FranjaVerdeATC3)
egen FranjaVioletaATC=rowmax(FranjaVioletaATC0 FranjaVioletaATC1 FranjaVioletaATC2 FranjaVioletaATC3)
egen OTCATC=rowmax(OTCATC0 OTCATC1 OTCATC2 OTCATC3)
egen VentasMaxATC_Lab=rowmax(VentasTotalesATC0_Lab VentasTotalesATC1_Lab VentasTotalesATC2_Lab VentasTotalesATC3_Lab)
egen VentasMaxATC_May=rowmax(VentasTotalesATC0_May VentasTotalesATC1_May VentasTotalesATC2_May VentasTotalesATC3_May)
egen HHATCMax_Lab=rowmax(HHATC0_Lab HHATC1_Lab HHATC2_Lab HHATC3_Lab)
egen HHATCMax_May=rowmax(HHATC0_May HHATC1_May HHATC2_May HHATC3_May)


egen NumPOSGrupo=rowmax(NumPOSGrupo0 NumPOSGrupo1 NumPOSGrupo2 NumPOSGrupo3)
egen NumVMRGrupo=rowmax(NumVMRGrupo0 NumVMRGrupo1 NumVMRGrupo2 NumVMRGrupo3)
egen CompetidoresGrupo=rowmax(CompetidoresGrupo0 CompetidoresGrupo1 CompetidoresGrupo2)
egen GenericosGrupo=rowmax(GenericosGrupo0 GenericosGrupo1 GenericosGrupo2 GenericosGrupo3)
egen InsertoGrupo=rowmax(InsertoGrupo0 InsertoGrupo1 InsertoGrupo2 InsertoGrupo3)
egen FranjaVerdeGrupo=rowmax(FranjaVerdeGrupo0 FranjaVerdeGrupo1 FranjaVerdeGrupo2 FranjaVerdeGrupo3)
egen FranjaVioletaGrupo=rowmax(FranjaVioletaGrupo0 FranjaVioletaGrupo1 FranjaVioletaGrupo2 FranjaVioletaGrupo3)
egen OTCGrupo=rowmax(OTCGrupo0 OTCGrupo1 OTCGrupo2 OTCGrupo3)
egen VentasMaxGrupo_Lab=rowmax(VentasTotalesGrupo0_Lab VentasTotalesGrupo1_Lab VentasTotalesGrupo2_Lab VentasTotalesGrupo3_Lab)
egen VentasMaxGrupo_May=rowmax(VentasTotalesGrupo0_May VentasTotalesGrupo1_May VentasTotalesGrupo2_May VentasTotalesGrupo3_May)
egen HHGrupoMax_Lab=rowmax(HHGrupo0_Lab HHGrupo1_Lab HHGrupo2_Lab HHGrupo3_Lab)
egen HHGrupoMax_May=rowmax(HHGrupo0_May HHGrupo1_May HHGrupo2_May HHGrupo3_May)


merge m:1 CUM1 using "$base_out\POSStatus_Comercial.dta", keepus(AlwaysPOS NeverPOS POSChange)
drop _merge
merge m:1 CUM1 using "$base_out\PriceVariabilityLab_Comercial.dta", keepus(*First *Last Completo*  Variacion* Algo* *Count)
drop _merge
merge m:1 CUM1 using "$base_out\PriceVariabilityMay_Comercial.dta", keepus(*First *Last Completo*  Variacion* Algo* *Count)
drop _merge
merge m:1 CUM1 using "$base_out\PrimerANOPOS_Comercial.dta"
drop _merge
merge m:1 CUM1 using "$base_out\PrimerANOVMR_Comercial.dta"
drop _merge
merge m:1 CUM1 using "$base_out\PrimerANOObsLab_Comercial.dta"
drop _merge
merge m:1 CUM1 using "$base_out\PrimerANOObsMay_Comercial.dta"
drop _merge


compress
saveold "$base_out\MedicamentosRegs_Comercial.dta",replace

reshape long VariacionIncreasePrice_@ VariacionIncreaseQuantity_@ PrimerANObs@ UltimoANOObs@ Comercial_@Count HHGrupo0_@ HHATC0_@ VentasTotalesGrupo0_@ VentasTotalesATC0_@ HHGrupo1_@ HHATC1_@ VentasTotalesGrupo1_@ VentasTotalesATC1_@ HHGrupo2_@ HHATC2_@ VentasTotalesGrupo2_@ VentasTotalesATC2_@ HHGrupo3_@ HHATC3_@ VentasTotalesGrupo3_@ VentasTotalesATC3_@  HHGrupoMax_@ HHATCMax_@ VentasMaxGrupo_@ VentasMaxATC_@ Comercial_@_Prom_STD Comercial_@_Valor Comercial_@_Unidades_STD Comercial_@First Comercial_@Last CompletoComercial_@  AlgoComercial_@ VariacionComercial_@P99, i(CUM1 Periodo) j(Canal) string

encode Canal, gen(Canal2)
rename Comercial__Prom_STD Comercial_Prom_STD
rename Comercial__Valor Comercial_Valor
rename Comercial__Unidades_STD Comercial_Unidades_STD
rename CompletoComercial_ CompletoComercial
rename AlgoComercial_ AlgoComercial


rename VentasMaxATC VentasMaxATC
rename VentasMaxGrupo_ VentasMaxGrupo
rename HHATCMax_ HHATCMax
rename HHGrupoMax_ HHGrupoMax

forvalues j=0/3{
rename VentasTotalesGrupo`j'_ VentasTotalesGrupo`j'
rename VentasTotalesATC`j'_ VentasTotalesATC`j'
rename HHATC`j'_ HHATC`j'
rename HHGrupo`j'_ HHGrupo`j'
}

compress
saveold "$base_out\MedicamentosRegsLong_Comercial.dta",replace

/*
egen double Inst_Total_Unidades_STD2=rowtotal(Comercial_Lab_Unidades_STD Comercial_May_Prom_STD), missing
gen double Inst_Total_Prom_STD2=Comercial_Lab_Prom_STD if !missing(Comercial_Lab_Unidades_STD) & Comercial_Lab_Unidades_STD!=0 & (Comercial_May_Unidades_STD==0 | missing(Comercial_May_Unidades_STD))
replace Inst_Total_Prom_STD2=Comercial_May_Prom_STD if !missing(Comercial_May_Unidades_STD) & Comercial_May_Unidades_STD!=0 & (Comercial_Lab_Unidades_STD==0 | missing(Comercial_Lab_Unidades_STD))
replace Inst_Total_Prom_STD2=(Comercial_Lab_Prom_STD* Comercial_Lab_Unidades_STD+ Comercial_May_Prom_STD* Comercial_May_Unidades_STD)/( Comercial_Lab_Unidades_STD+ Comercial_May_Prom_STD) if Inst_Total_Unidades_STD!=0 & Comercial_Lab_Unidades_STD!=0 & Comercial_May_Prom_STD!=0 & !missing(Inst_Total_Unidades_STD) & !missing(Comercial_Lab_Unidades_STD) & !missing(Comercial_May_Prom_STD)
replace Inst_Total_Prom_STD2=. if Comercial_May_Unidades_STD==0 & Comercial_Lab_Unidades_STD==0 | (missing(Comercial_May_Unidades_STD) & missing(Comercial_Lab_Unidades_STD))
*/
*codebook CUM1
