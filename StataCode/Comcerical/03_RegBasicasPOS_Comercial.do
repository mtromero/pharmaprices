set more off
clear all
set maxvar 10000
set matsize 10000
use "$base_out\MedicamentosRegsLong_Comercial.dta",clear
*keep if !missing(Generico)
label var Generico "Generic"
label var FranjaVerde "POS April 2015"
label var FranjaVioleta "Controlled Substance"
label var Inserto "Insert"
label var OTC "OTC"
label var Presentations "No. of presentations"

label var CompetidoresGrupo "N� pharm. in therapeutical group"
label var CompetidoresATC "N� pharm. in ATC group"

drop if Canal2==2

encode ATC_Grupo0 , gen (ATC_Grupo)



label var NumPOSATC "N� pharm. in ATC group in POS"
label var NumPOSGrupo "N� pharm. in therapeutical in POS"
label var NumVMRATC "N� pharm. in ATC group with RP"
label var NumVMRGrupo "N� pharm. in therapeutical with RP"
 gen DGenericos=(GenericosATC>0) if !missing(GenericosATC)
gen LgCompetidoresATC=log(CompetidoresATC)
compress
xtset CUM1 Periodo, yearly



foreach var of varlist  Comercial_Prom_STD   Comercial_Unidades_STD {
gen double Lg`var'=log(`var')
}

label var LgComercial_Prom_STD "Log average price for EPS and IPS"  
label var LgComercial_Unidades_STD "Log units for EPS and IPS"


label var POS "POS"
label var VMR "RP"
label var POS2 "POS"

label var VMR2 "RP"
label var VMR3 "RP"

/*Test variance
areg LgComercial_Lab_Prom_STD i.Periodo, absorb(CUM1) vce(cluster ATC_Grupo)
predict residPrecio, resid
areg POS i.Periodo, absorb(CUM1) vce(cluster ATC_Grupo)
predict residPOS, resid
areg VMR i.Periodo, absorb(CUM1) vce(cluster ATC_Grupo)
predict residVMR, resid
reg residPrecio residPOS residVMR
*R^2=0.0068
reg residPrecio POS VMR
*R^2=0.0025
*Pretty small numbers on the R^2... ufff
*/

*areg LgComercial_Unidades_STD POS2 VMR2  c.Periodo#ATC_Grupo if Comercial_Count==8, absorb(CUM1)  vce(cluster ATC_Grupo)
*areg LgComercial_Unidades_STD i.TiempoPOS2 VMR2  c.Periodo#ATC_Grupo if Comercial_Count==8, absorb(CUM1)  vce(cluster ATC_Grupo)


local controles  
*NumPOSATC CompetidoresATC DGenericos 
foreach channel in   2{
foreach tipo in  Prom_STD Unidades_STD{
foreach var in  Comercial{
eststo clear
eststo: areg Lg`var'_`tipo' POS2 VMR2 DGenericos LgCompetidoresATC i.Periodo, absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "No"
estadd local Outliers "No"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local Controls "Yes"
estadd local MoleculeTrends "No"
estadd scalar NumMolecules=e(df_a)+1
eststo: areg Lg`var'_`tipo' POS2 VMR2 DGenericos LgCompetidoresATC i.Periodo  if  `var'_Count==8, absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Outliers "No"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local Controls "Yes"
estadd local MoleculeTrends "No"
estadd scalar NumMolecules=e(df_a)+1

eststo: areg Lg`var'_`tipo' POS2 VMR2 DGenericos LgCompetidoresATC  c.Periodo#ATC_Grupo if `var'_Count==8, absorb(CUM1)  vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Outliers "No"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local Controls "Yes"
estadd local MoleculeTrends "Yes"
estadd scalar NumMolecules=e(df_a)+1

eststo: areg Lg`var'_`tipo' POS2 VMR2 DGenericos LgCompetidoresATC  c.Periodo#ATC_Grupo if `var'_Count==8 &  VariacionIncreasePrice_==0 &  VariacionIncreaseQuantity_==0, absorb(CUM1)  vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Outliers "Yes"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local Controls "Yes"
estadd local MoleculeTrends "Yes"
estadd scalar NumMolecules=e(df_a)+1


 estout using "$latexcodes\Reg_`tipo'_`var'_`channel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
 replace keep(POS2) ///
 stats(N N_clust NumMolecules Molecule Year Controls MoleculeTrends BalancePanel Outliers, fmt(a2 a2 a2 %~#s %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (therapeutic groups)" "N. of molecules" "Molecule F.E." "Year F.E." "Controls" "Molecule trends"  "Balance panel?" "Outliers excluded?"))


}
}
}

local controles 
*NumPOSATC CompetidoresATC DGenericos
foreach channel in   2{
foreach tipo in  Prom_STD Unidades_STD{
foreach var in  Comercial{
eststo clear
eststo: areg Lg`var'_`tipo' POS2 VMR2 DGenericos LgCompetidoresATC c.Periodo#ATC_Grupo if  `var'_Count==8, absorb(CUM1)  vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Obser "Change POS status"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local Controls "Yes"
estadd local MoleculeTrends "Yes"
estadd scalar NumMolecules=e(df_a)+1


eststo: areg Lg`var'_`tipo' POS2 VMR2 DGenericos  LgCompetidoresATC c.Periodo#ATC_Grupo if POSChange==1 &  `var'_Count==8, absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Obser "Change POS status"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local Controls "Yes"
estadd local MoleculeTrends "Yes"
estadd scalar NumMolecules=e(df_a)+1


eststo: areg Lg`var'_`tipo' POS2 VMR2 DGenericos LgCompetidoresATC c.Periodo#ATC_Grupo if (POSChange==1 |AlwaysPOS==1)  &  `var'_Count==8, absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Obser "Change POS status + Always POS"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local Controls "Yes"
estadd local MoleculeTrends "Yes"
estadd scalar NumMolecules=e(df_a)+1

eststo: areg Lg`var'_`tipo' POS2 VMR2 DGenericos  LgCompetidoresATC c.Periodo#ATC_Grupo if (POSChange==1 | NeverPOS==1)  & `var'_Count==8, absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Obser "Change POS status + Never POS"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local Controls "Yes"
estadd local MoleculeTrends "Yes"
estadd scalar NumMolecules=e(df_a)+1


 estout using "$latexcodes\RegByStatus_`tipo'_`var'_`channel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
 replace keep(POS2) ///
 stats(N N_clust NumMolecules Molecule Year Controls MoleculeTrends BalancePanel Obser , fmt(a2 a2 a2 %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (therapeutic groups)" "N. of molecules" "Molecule F.E." "Year F.E." "Controls" "Molecule trends" "Balance panel?" "Sample"))

}
}
}


