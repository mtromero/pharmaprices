set more off
clear all
set maxvar 10000
set matsize 10000
use "$base_out\MedicamentosRegsLong_Comercial.dta",clear
*drop if  VariacionIncrease_==1
drop if Canal2==2

label var CompetidoresGrupo "N� pharm. in therapeutical group"
label var CompetidoresATC "N� pharm. in ATC group"



encode ATC_Grupo0 , gen (ATC_Grupo)



label var NumPOSATC "N� pharm. in ATC group in POS"
label var NumPOSGrupo "N� pharm. in therapeutical in POS"
label var NumVMRATC "N� pharm. in ATC group with RP"
label var NumVMRGrupo "N� pharm. in therapeutical with RP"

compress
xtset CUM1 Periodo, yearly



foreach var of varlist  Comercial_Prom_STD   Comercial_Unidades_STD {
gen double Lg`var'=log(`var')
}

label var LgComercial_Prom_STD "Log average price for EPS and IPS"  
label var LgComercial_Unidades_STD "Log units for EPS and IPS"


label var POS "POS"
label var VMR "RP"
label var POS2 "POS"

label var VMR2 "RP"
label var VMR3 "RP"

gen AtLeastOnePOSATC=(NumPOSATC>=1) & !missing(NumPOSATC)
gen AtLeastOnePOSGrupo=(NumPOSGrupo >=1) & !missing(NumPOSGrupo )
gen MoreThanOnePOSATC=(NumPOSATC>1) & !missing(NumPOSATC)
gen MoreThanOnePOSGrupo=(NumPOSGrupo>1) & !missing(NumPOSGrupo )
gen OnePOSATC=(NumPOSATC==1) & !missing(NumPOSATC)
gen OnePOSGrupo=(NumPOSGrupo==1) & !missing(NumPOSGrupo )
gen LgNumPOSATC=log(NumPOSATC)
gen LgNumPOSGrupo=log(NumPOSGrupo)
gen DGenericos=(GenericosATC>0) if !missing(GenericosATC)
gen LgCompetidoresATC=log(CompetidoresATC)

gen PropCompetidoresPOSATC=NumPOSATC/CompetidoresATC 
gen PropCompetidoresPOSGrupo=NumPOSGrupo/CompetidoresGrupo 


*First lets see what happens to incumbents

foreach channel in 2{
foreach tipo in  Prom_STD Unidades_STD{
foreach var in  Comercial{
eststo clear


eststo: areg Lg`var'_`tipo' MoreThanOnePOSATC DGenericos LgCompetidoresATC   c.Periodo#ATC_Grupo if `var'_Count==8  & AlwaysPOS==1 , absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local MoleculeTrends "Yes"
estadd local MonopolyMeasure "ATC code"
estadd scalar NumMolecules=e(df_a)+1

eststo: areg Lg`var'_`tipo' LgNumPOSATC DGenericos LgCompetidoresATC   c.Periodo#ATC_Grupo if `var'_Count==8  & AlwaysPOS==1 , absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local MoleculeTrends "Yes"
estadd local MonopolyMeasure "ATC code"
estadd scalar NumMolecules=e(df_a)+1

eststo: areg Lg`var'_`tipo' c.MoreThanOnePOSATC#c.PropCompetidoresPOSATC DGenericos LgCompetidoresATC   c.Periodo#ATC_Grupo if `var'_Count==8  & AlwaysPOS==1 , absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local MoleculeTrends "Yes"
estadd local MonopolyMeasure "ATC code"
estadd scalar NumMolecules=e(df_a)+1



eststo: areg Lg`var'_`tipo' MoreThanOnePOSGrupo DGenericos LgCompetidoresATC c.Periodo#ATC_Grupo if `var'_Count==8  & AlwaysPOS==1 , absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local MoleculeTrends "Yes"
estadd local MonopolyMeasure "Therapeutic group"
estadd scalar NumMolecules=e(df_a)+1

eststo: areg Lg`var'_`tipo' LgNumPOSGrupo DGenericos LgCompetidoresATC  c.Periodo#ATC_Grupo if `var'_Count==8  & AlwaysPOS==1 , absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local MoleculeTrends "Yes"
estadd local MonopolyMeasure "Therapeutic group"
estadd scalar NumMolecules=e(df_a)+1

eststo: areg Lg`var'_`tipo' c.MoreThanOnePOSGrupo#c.PropCompetidoresPOSGrupo DGenericos LgCompetidoresATC  c.Periodo#ATC_Grupo if `var'_Count==8  & AlwaysPOS==1 , absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local BalancePanel "Yes"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local MoleculeTrends "Yes"
estadd local MonopolyMeasure "Therapeutic group"
estadd scalar NumMolecules=e(df_a)+1

estout using "$latexcodes\RegCompetenciaIncumbent`tipo'_`var'_`channel'_trends.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
replace keep(Comp2 Comp Comp3) ///
rename(LgNumPOSATC Comp LgNumPOSGrupo Comp MoreThanOnePOSGrupo Comp2 MoreThanOnePOSATC Comp2 c.MoreThanOnePOSGrupo#c.PropCompetidoresPOSGrupo Comp3 c.MoreThanOnePOSATC#c.PropCompetidoresPOSATC Comp3) ///
varl(Comp2 "\$>1\$ similar drugs in POS" Comp "log(\# similar drugs in POS)" Comp3 " (\$>1\$ similar drugs in POS) \$\times \$ (# similar drugs in POS)/(# similar drugs)") ///
stats(N N_clust NumMolecules Molecule Year MoleculeTrends BalancePanel MonopolyMeasure, fmt(a2 a2 a2 %~#s %~#s %~#s %~#s %~#s) labels ("No. of obs." "No. of clusters (therapeutic groups)" "No. of molecules" "Molecule F.E." "Year F.E." "Molecule trends" "Balance panel?" "Similarity by"))
}
}
}

replace LgNumPOSATC=-99 if LgNumPOSATC==.
replace LgNumPOSGrupo=-99 if LgNumPOSGrupo==.

 

 *Now we do full competition within the POS
foreach channel in   2{
foreach tipo in  Prom_STD Unidades_STD{
foreach var in  Comercial{
eststo clear
eststo: areg Lg`var'_`tipo' c.POS2 VMR2 DGenericos LgCompetidoresATC   c.Periodo#ATC_Grupo if `var'_Count==8 , absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local Obser "Info every year"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local MoleculeTrends "Yes"
estadd local MonopolyMeasure "None"
estadd scalar NumMolecules=e(df_a)+1


eststo: areg Lg`var'_`tipo' c.POS2#c.OnePOSATC c.POS2#c.MoreThanOnePOSATC DGenericos LgCompetidoresATC  VMR2   c.Periodo#ATC_Grupo if `var'_Count==8 , absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local Obser "Info every year"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local MoleculeTrends "Yes"
estadd local MonopolyMeasure "ATC Code"
estadd scalar NumMolecules=e(df_a)+1

eststo: areg Lg`var'_`tipo' c.POS2#c.OnePOSATC c.POS2#c.MoreThanOnePOSATC#c.LgNumPOSATC DGenericos LgCompetidoresATC VMR2  c.Periodo#ATC_Grupo if `var'_Count==8  , absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local Obser "Info every year"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local MoleculeTrends "Yes"
estadd local MonopolyMeasure "ATC Code"
estadd scalar NumMolecules=e(df_a)+1

eststo: areg Lg`var'_`tipo' c.POS2#c.OnePOSGrupo c.POS2#c.MoreThanOnePOSGrupo DGenericos LgCompetidoresATC VMR2  c.Periodo#ATC_Grupo if `var'_Count==8 , absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local Obser "Info every year"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local MoleculeTrends "Yes"
estadd local MonopolyMeasure "Therapeutic Group"
estadd scalar NumMolecules=e(df_a)+1

eststo: areg Lg`var'_`tipo' c.POS2#c.OnePOSGrupo c.POS2#c.MoreThanOnePOSGrup#c.LgNumPOSGrupo DGenericos LgCompetidoresATC VMR2  c.Periodo#ATC_Grupo if `var'_Count==8 , absorb(CUM1) vce(cluster ATC_Grupo)
estadd ysumm
estadd local Obser "Info every year"
estadd local Molecule "Yes"
estadd local Year "Yes"
estadd local MoleculeTrends "Yes"
estadd local MonopolyMeasure "Therapeutic Group"
estadd scalar NumMolecules=e(df_a)+1

estout using "$latexcodes\RegCompetencia`tipo'_`var'_`channel'_trends.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
replace keep(POS Comp Comp2 Comp3) ///
rename(POS2 POS c.POS2#c.MoreThanOnePOSGrupo Comp c.POS2#c.MoreThanOnePOSATC Comp c.POS2#c.OnePOSGrupo Comp2 c.POS2#c.OnePOSATC Comp2 ///
c.POS2#c.MoreThanOnePOSGrupo#c.LgNumPOSGrupo Comp3 c.POS2#c.MoreThanOnePOSATC#c.LgNumPOSATC Comp3) ///
varl(Comp "POS \$\times >1\$ similar drug in POS" Comp2 "POS \$\times 1\$ similar drug in POS" Comp3 "POS \$\times >1\$ similar drug in POS \$\times\$ log(\# similar drugs in POS)") ///
stats(N N_clust NumMolecules Molecule Year MoleculeTrends Obser MonopolyMeasure, fmt(a2 a2 a2 %~#s %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (therapeutic groups)" "N. of molecules" "Molecule F.E." "Year F.E." "Molecule Trend" "Pharmaceuticals" "Competition level"))
}
}
}



