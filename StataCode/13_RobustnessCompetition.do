set more off
clear all
set matsize 800
use "$base_out/MedicamentosRegsLong.dta",clear
bys Periodo: egen ValorTotalAno=sum(Institucional_Valor)
gsort +Periodo -Institucional_Valor
bys Periodo: gen Frac_ValorTotalAno=sum(Institucional_Valor)/ValorTotalAno
gen Keep=(Frac_ValorTotalAno<0.95) if !missing(Frac_ValorTotalAno)


label var Generico "Generic"
label var FranjaVerde "Essential Medicine"
label var FranjaVioleta "Controlled Substance"
label var Inserto "Insert"
label var OTC "OTC"
label var Presentations "No. of presentations"

label var CompetidoresGrupo "No. pharm. in therapeutical group"
label var CompetidoresATC "No. pharm. in ATC group"

drop if Canal2==2

encode ATC_Grupo0 , gen (ATC_Grupo)
encode ATC0 , gen (ATC)

label var NumPOSATC "No. drugs in ATC group in POS"
label var NumPOSGrupo "No. drugs in therapeutical in POS"
label var NumVMRATC "No. drugs in ATC group with RP"
label var NumVMRGrupo "No. drugs in pharmacological subgroup with RP"
gen DGenericosATC=(GenericosATC>0) if !missing(GenericosATC)
gen DGenericosGrupo=(GenericosGrupo>0) if !missing(GenericosGrupo)
gen LgCompetidoresATC=log(CompetidoresATC)
gen LgCompetidoresGrupo=log(CompetidoresGrupo)
gen LgNumPOSATC=log(NumPOSATC)
gen LgNumPOSGrupo=log(NumPOSGrupo)
replace LgNumPOSATC=0 if LgNumPOSATC==.
replace LgNumPOSGrupo=0 if LgNumPOSGrupo==.
label var LgNumPOSATC "log(No. drugs in ATC group in POS)"
label var LgNumPOSGrupo "log(No. drugs in pharmacological subgroup in POS)"

gen PropCompetidoresPOSATC=NumPOSATC/CompetidoresATC 
gen PropCompetidoresPOSGrupo=NumPOSGrupo/CompetidoresGrupo 
gen OnePOSATC=(NumPOSATC==1) & !missing(NumPOSATC)
gen OnePOSGrupo=(NumPOSGrupo==1) & !missing(NumPOSGrupo )


compress
xtset CUM1 Periodo, yearly
gen WeightShare2007=Institucional_Valor/ValorTotalAno if Periodo==2007
replace WeightShare2007=L.WeightShare2007 if WeightShare2007==.

gen missing_info=missing(Institucional_Prom_STD)
label var missing_info "Missing price/quantity information"



foreach var of varlist  Institucional_Prom_STD   Institucional_Unidades_STD {
	gen double Lg`var'=log(`var')
}

label var LgInstitucional_Prom_STD "Log average price for EPS and IPS"  
label var LgInstitucional_Unidades_STD "Log units for EPS and IPS"


label var POS "POS"
label var VMR "RP"
label var POS2 "POS"

label var VMR2 "RP"
label var VMR3 "RP"

gen NOPOS=1-POS

global control_basico VMR2 DGenericosATC HHATCMax
global control_dinamico VMR2 L.VMR2  L.L.VMR2  DGenericosATC L.DGenericosATC  L.L.DGenericosATC  HHATCMax L.HHATCMax L.L.HHATCMax

 

levelsof CUM1 if POSChange==1, local(levels) 
foreach l of local levels {
	di "`l'"
	qui: sum ATC_Grupo if CUM1==`l'
	qui: reghdfe LgInstitucional_Prom_STD POS2 $control_basico if CUM1==`l' | ATC_Grupo!=`r(mean)' , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
	matrix A=e(b)
	mat Resultados1 = nullmat(Resultados1),A[1,1]
	
	qui: sum ATC_Grupo if CUM1==`l'
	qui: reghdfe LgInstitucional_Prom_STD POS2 $control_basico if (CUM1==`l' | ATC_Grupo!=`r(mean)') & Institucional_Count==$panelcompelto, absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
	matrix A=e(b)
	mat Resultados2 = nullmat(Resultados2),A[1,1]
	
	qui: sum ATC_Grupo if CUM1==`l'
	qui: reghdfe LgInstitucional_Prom_STD POS2 $control_basico if (CUM1==`l' | ATC_Grupo!=`r(mean)') & Institucional_Count==$panelcompelto , absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
	matrix A=e(b)
	mat Resultados3 = nullmat(Resultados3),A[1,1]
	
	qui: sum ATC_Grupo if CUM1==`l'
	qui: reghdfe LgInstitucional_Prom_STD POS2 $control_basico if (CUM1==`l' | ATC_Grupo!=`r(mean)') & Institucional_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
	matrix A=e(b)
	mat Resultados4 = nullmat(Resultados4),A[1,1]
}


