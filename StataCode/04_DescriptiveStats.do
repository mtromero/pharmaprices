use "$base_out/MedicamentosRegs.dta", clear
*drop if  VariacionIncreasePrice_Lab==1 | VariacionIncreaseQuantity_Lab==1
preserve
drop if POSChange==0
keep if UltimoANOPOS==$ultimoano
collapse (mean) PrimerANOPOS, by(CUM1)
graph bar (count), over(PrimerANOPOS) graphregion(color(white)) title("") ytitle("Drugs") yla(, ang(h) nogrid) 
 graph export "$graphs/FrequencyAddPOS.pdf", replace
 restore
 
 preserve
drop if POSChange==0
keep if Institucional_LabCount==$panelcompelto
keep if UltimoANOPOS==$ultimoano
collapse (mean) PrimerANOPOS, by(CUM1)
graph bar (count), over(PrimerANOPOS) graphregion(color(white)) title("Drugs added to the benefit plan by year")
 graph export "$graphs/FrequencyAddPOS_panelCompleto.pdf", replace
 restore
 
 
 preserve
drop if POSChange==0
keep if Periodo==$ultimoano
keep if UltimoANOPOS!=$ultimoano
collapse (mean) UltimoANOPOS, by(CUM1)
graph bar (count), over(UltimoANOPOS) graphregion(color(white)) title("Drugs that exit the benefit plan by year")
 graph export "$graphs/FrequencyExitPOS.pdf", replace
 restore
 
  preserve
drop if POSChange==0
keep if Periodo==$ultimoano
keep if Institucional_LabCount==$panelcompelto
keep if UltimoANOPOS!=$ultimoano
collapse (mean) UltimoANOPOS, by(CUM1)
graph bar (count), over(UltimoANOPOS) graphregion(color(white)) title("Drugs that exit the benefit plan by year")
 graph export "$graphs/FrequencyExitPOS_panelCompleto.pdf", replace
 restore
 
drop if POS2==.
collapse (first)   ATC1 DESCRIPCION_ATC1  PRINCIPIO_ACTIVO1 ATC2 DESCRIPCION_ATC2 PRINCIPIO_ACTIVO2  ATC3 DESCRIPCION_ATC3 ///
PRINCIPIO_ACTIVO3  ATC0 DESCRIPCION_ATC0 PRINCIPIO_ACTIVO0  TipoIDRegistro IDRegistro NombreRegistro ///
 ATC_Grupo0 ATC_Grupo1 ATC_Grupo2 ATC_Grupo3 (mean) Presentations AlwaysPOS NeverPOS POSChange AlgoInstitucional_Lab AlgoInstitucional_May CompletoInstitucional_Lab ///
CompletoInstitucional_May  AnosExpedicion ImportarModalidad  Generico FranjaVerde FranjaVioleta Inserto OTC MultipleRegistro ///
VariacionIncreasePrice30_Lab VariacionIncreasePrice10_Lab ///
  , by(CUM1)



gen Grupo=0 if AlgoInstitucional_Lab==0 & CompletoInstitucional_Lab==0
replace Grupo=1 if AlgoInstitucional_Lab==1 & CompletoInstitucional_Lab==0
replace Grupo=2 if AlgoInstitucional_Lab==1 & CompletoInstitucional_Lab==1

label define vargrup 0 "No price observations" 1 "Observe prices some years" 2 "Observe prices every year"
label values Grupo vargrup
label var Generico "Generic"
label var FranjaVerde "Essential medication (must be sold under generic name)"
label var FranjaVioleta "Controlled substance"
label var Inserto "Insert"
label var OTC "OTC"
label var Presentations "No. of presentations"
label var ImportarModalidad "Imported"
label var AnosExpedicion "Yrs. in the market"
sort Grupo
unique ATC0, by(Grupo) gen(uniqueATC)
sort Grupo
unique ATC_Grupo0, by(Grupo) gen(uniqueGrupo)
by Grupo: replace uniqueATC=uniqueATC[_n-1] if uniqueATC==.
by Grupo: replace uniqueGrupo=uniqueGrupo[_n-1] if uniqueGrupo==.


eststo clear
sort Grupo
by Grupo: eststo: quietly  estpost  summarize Generico FranjaVerde FranjaVioleta OTC Presentations ImportarModalidad AnosExpedicion, listwise
esttab using "$latexcodes/ObserbationsDiferences.tex", replace booktabs cells("mean(fmt(a2) label(Mean))" "sd(fmt(a2) par label(SD))") label nodepvar mtitles("No price observations" "Some price observations" "Annual price observations")

gen NoObs=(Grupo==0)
gen SomeObs=(Grupo==1)
gen CompleteObs=(Grupo==2)
gen GrupoTera=substr(ATC0,1,5)
collapse (mean) PropObs=NoObs (sum) NumberMissing=NoObs (count) TotalMedi=NoObs , by( GrupoTera)

use "$base_out/MedicamentosRegs.dta", clear
drop if Generico==.
*drop if  VariacionIncrease_Lab==1
drop if POS2==.
unique CUM1
unique CUM1 if AlgoInstitucional_Lab==1
unique CUM1 if CompletoInstitucional_Lab==1
unique ATC0
unique ATC0 if AlgoInstitucional_Lab==1
unique ATC_Grupo0 if AlgoInstitucional_Lab==1

keep if AlgoInstitucional_Lab==1
keep if !missing(Generico)
label var Generico "Generic"
label var FranjaVerde "Essential medication (must be sold under generic name)"
label var FranjaVioleta "Controlled substance"
label var Inserto "Insert"
label var OTC "OTC"
label var Presentations "No. of presentations"
label var ImportarModalidad "Imported"


eststo clear
label var POS2 "POS"
label var CompetidoresATC "No. of drugs with the same ATC" 
label var CompetidoresGrupo "No. of drugs in the same pharmacological subgroup"
label var NumPOSGrupo "No. of drugs in the same pharmacological subgroup in the POS "
label var NumPOSATC "No. of drugs with the same ATC in the POS"
label var AlgoInstitucional_Lab "Some price observations"
label var CompletoInstitucional_Lab "Price info every year"

estpost tabstat POS2 CompetidoresATC NumPOSATC CompetidoresGrupo NumPOSGrupo Generico FranjaVerde FranjaVioleta OTC Presentations ImportarModalidad CompletoInstitucional_Lab, statistics(count p50 mean sd min p10 p90 max) columns(statistics)
estout using "$latexcodes/DescrpB.tex" , cells("mean(fmt(a2)) sd(fmt(a2)) min(fmt(a2)) p10(fmt(a2)) p90(fmt(a2)) max(fmt(a2))") label style(tex) mlabels(none) collabels(none) replace

drop if VariacionIncreasePrice30_Lab==1
eststo clear
estpost tabstat POS2 CompetidoresATC NumPOSATC CompetidoresGrupo NumPOSGrupo Generico FranjaVerde FranjaVioleta OTC Presentations ImportarModalidad CompletoInstitucional_Lab, statistics(count p50 mean sd min p10 p90 max) columns(statistics)
estout using "$latexcodes/DescrpC.tex" , cells("mean(fmt(a2)) sd(fmt(a2)) min(fmt(a2)) p10(fmt(a2)) p90(fmt(a2)) max(fmt(a2))") label style(tex) mlabels(none) collabels(none) replace

drop if VariacionIncreasePrice10_Lab==1
eststo clear
estpost tabstat POS2 CompetidoresATC NumPOSATC CompetidoresGrupo NumPOSGrupo Generico FranjaVerde FranjaVioleta OTC Presentations ImportarModalidad CompletoInstitucional_Lab, statistics(count p50 mean sd min p10 p90 max) columns(statistics)
estout using "$latexcodes/DescrpD.tex" , cells("mean(fmt(a2)) sd(fmt(a2)) min(fmt(a2)) p10(fmt(a2)) p90(fmt(a2)) max(fmt(a2))") label style(tex) mlabels(none) collabels(none) replace

use "$base_out/MedicamentosRegs.dta", clear
drop if Generico==.
drop if POS2==.
gen Grupo=0 if AlgoInstitucional_Lab==0 & CompletoInstitucional_Lab==0
replace Grupo=1 if AlgoInstitucional_Lab==1 & CompletoInstitucional_Lab==0
replace Grupo=2 if AlgoInstitucional_Lab==1 & CompletoInstitucional_Lab==1
tab Grupo, gen(Info_)

preserve
collapse (mean) Info_*, by(ATC0)
tab Info_1
tab Info_3
restore
collapse (mean) Info_*, by(ATC_Grupo0)
tab Info_1
tab Info_3
*graph bar Info_1 Info_2 Info_3, over(ATC_Grupo0) stack
*reshape long Info_, i(ATC0) j(Grupo)
