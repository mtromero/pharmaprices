set more off
clear all
set maxvar 10000
set matsize 10000
use "$base_out/MedicamentosRegsLong.dta",clear
label var CompetidoresGrupo "Nº pharm. in therapeutical group"
label var CompetidoresATC "Nº pharm. in ATC group"
drop if Canal2==2
tsset CUM1 Periodo

encode ATC_Grupo0 , gen (ATC_Grupo)
encode ATC0 , gen (ATC)



label var NumPOSATC "Nº pharm. in ATC group in POS"
label var NumPOSGrupo "Nº pharm. in therapeutical in POS"
label var NumVMRATC "Nº pharm. in ATC group with RP"
label var NumVMRGrupo "Nº pharm. in therapeutical with RP"

compress
*xtset CUM1 Periodo, yearly



foreach var of varlist  Institucional_Prom_STD   Institucional_Unidades_STD {
	gen double Lg`var'=log(`var')
}

label var LgInstitucional_Prom_STD "Log average price for EPS and IPS"  
label var LgInstitucional_Unidades_STD "Log units for EPS and IPS"


label var POS "POS"
label var VMR "RP"
label var POS2 "POS"

label var VMR2 "RP"
label var VMR3 "RP"

*why did i had this here... no idea, but it makes no sense
*replace POSChange=1 if PrimerANOPOS==2007
replace PrimerANOPOS=. if POSChange==0
replace UltimoANOPOS=. if POSChange==0
gen TiempoPOS=Periodo-PrimerANOPOS
replace TiempoPOS=UltimoANOPOS-Periodo if UltimoANOPOS!=$ultimoano


gen TiempoVMR=Periodo-PrimerANOVMR
tabulate TiempoPOS, gen(TiempoPOSI)
tabulate TiempoVMR, gen(TiempoVMRI)


foreach var of varlist TiempoPOSI* TiempoVMRI*{
replace `var'=0 if `var'==.
}

sum TiempoPOS
gen TiempoPOS2=TiempoPOS-(r(min)-1)
replace TiempoPOS2=0 if TiempoPOS2==.
sum TiempoVMR
gen TiempoVMR2=TiempoVMR-(r(min)-1)
replace TiempoVMR2=0 if TiempoVMR2==.

gen TiempoPOS3=.
replace TiempoPOS3=1 if TiempoPOS<=-4 & !missing(TiempoPOS)
replace TiempoPOS3=2 if TiempoPOS==-3 & !missing(TiempoPOS)
replace TiempoPOS3=3 if TiempoPOS==-2 & !missing(TiempoPOS)
replace TiempoPOS3=4 if TiempoPOS==-1 & !missing(TiempoPOS)
replace TiempoPOS3=5 if TiempoPOS==0 & !missing(TiempoPOS)
replace TiempoPOS3=6 if TiempoPOS==1 & !missing(TiempoPOS)
replace TiempoPOS3=7 if TiempoPOS==2 & !missing(TiempoPOS)
replace TiempoPOS3=8 if TiempoPOS==3 & !missing(TiempoPOS)
replace TiempoPOS3=9 if TiempoPOS>=4 & !missing(TiempoPOS)
replace TiempoPOS3=0 if TiempoPOS3==.

sum TiempoPOS
scalar base1=-r(min)
fvset base `=scalar(base1)' TiempoPOS2
char TiempoPOS2[omit] 0
fvset base 4 TiempoPOS3
char TiempoPOS3[omit] 0
sum TiempoVMR
scalar base2=-r(min)
fvset base `=scalar(base2)' TiempoVMR2
char TiempoVMR2[omit] 0
 gen LgAnosExpedicion=log(AnosExpedicion)
by CUM1: gen CambioNumPOSATC=(D.NumPOSATC>0)
by CUM1: gen CambioNumPOSATC2=(D.NumPOSATC<0)
gen LgNumPOSATC=log(NumPOSATC)
gen LgNumPOSGrupo=log(NumPOSGrupo)
 gen DGenericosATC=(GenericosATC>0) if !missing(GenericosATC)
gen LgCompetidoresATC=log(CompetidoresATC)
gen LgCompetidoresATCPOS=NumPOSATC
replace LgCompetidoresATCPOS=NumPOSATC-1 if POS==1
gen LgCompetidoresATCNOPOS=(CompetidoresATC-1)-NumPOSATC if POS==0
replace LgCompetidoresATCNOPOS=(CompetidoresATC-1)-NumPOSATC if POS==1

gen a=NumPOSATC+LgCompetidoresATCNOPOS

gen missing_info=missing(Institucional_Prom_STD)
label var missing_info "Missing price/quantity information"
drop if POS2==.

global control_basico VMR2 DGenericosATC HHATCMax
global control_dinamico VMR2 L.VMR2  L.L.VMR2  DGenericosATC L.DGenericosATC  L.L.DGenericosATC  HHATCMax L.HHATCMax L.L.HHATCMax

/*
reghdfe DGenericosATC i.TiempoPOS3    if  Institucional_Count==$panelcompelto & VariacionIncreasePrice30_==0 , absorb(CUM1 Periodo#ATC) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of `name2' before and after" "a drug is included in the benefit plan") 

reghdfe DGenericosATC POS2    if  Institucional_Count==$panelcompelto & VariacionIncreasePrice30_==0 , absorb(CUM1 Periodo#ATC) vce(cluster ATC_Grupo) resid(resid)

			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of `name2' before and after" "a drug is included in the benefit plan") 


reghdfe LgInstitucional_Prom_STD i.TiempoPOS3  $control_basico   if Institucional_Count==$panelcompelto & VariacionIncreasePrice30_==0, absorb(CUM1 Periodo#ATC) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of a drug's `name2' before and after" "it is included in the benefit plan") 

reghdfe LgInstitucional_Prom_STD POS2  $control_basico   if Institucional_Count==$panelcompelto & VariacionIncreasePrice30_==0, absorb(CUM1 Periodo#ATC) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of a drug's `name2' before and after" "it is included in the benefit plan") 

*/
 						
 * HHATCMax_NOPOS HHATCMax_POS  HHATCMax LgCompetidoresATCPOS LgCompetidoresATCNOPOS
foreach channel in   2{
	foreach tipo in  DGenericosATC LgCompetidoresATC VMR2  {
		foreach var in  Institucional {

			if "`tipo'"=="DGenericosATC" local name "Generic"
			if "`tipo'"=="LgCompetidoresATC" local name "Competitors"
			if "`tipo'"=="HHATCMax" local name "HH-Index"
			if "`tipo'"=="HHATCMax_NOPOS" local name "HH-Index (not listed)"
			if "`tipo'"=="HHATCMax_POS" local name "HH-Index (listed)"
			if "`tipo'"=="VMR2" local name "Price Regulations"
			if "`tipo'"=="LgCompetidoresATCPOS" local name "Competitors listed"
			if "`tipo'"=="LgCompetidoresATCNOPOS" local name "Competitors not listed"

			if "`tipo'"=="DGenericosATC" local name2 "Generic subsitutes"
			if "`tipo'"=="LgCompetidoresATC" local name2 "Number of competitors"
			if "`tipo'"=="HHATCMax" local name2 "HH-Index"
			if "`tipo'"=="HHATCMax_NOPOS" local name2 "HH-Index (not listed)"
			if "`tipo'"=="HHATCMax_POS" local name2 "HH-Index (listed)"
			if "`tipo'"=="VMR2" local name2 "Price regulations"
			if "`tipo'"=="LgCompetidoresATCPOS" local name2 "competitors listed"
			if "`tipo'"=="LgCompetidoresATCNOPOS" local name2 "competitors not listed"

			/*
			reghdfe `tipo' i.TiempoPOS2   , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) drop(0.TiempoPOS2) ci ///
			rename(1.TiempoPOS2="-9" 2.TiempoPOS2 ="-8" 3.TiempoPOS2= "-7" 4.TiempoPOS2= "-6" 5.TiempoPOS2= "-5" 6.TiempoPOS2= "-4" 7.TiempoPOS2 ="-3" 8.TiempoPOS2= "-2" ///
			9.TiempoPOS2= "-1" 10.TiempoPOS2= "0" 11.TiempoPOS2= "1" 12.TiempoPOS2 ="2" 13.TiempoPOS2 ="3" 14.TiempoPOS2 ="4" 15.TiempoPOS2 ="5" 16.TiempoPOS2 ="6" 17.TiempoPOS2 ="7" 18.TiempoPOS2 ="8" 19.TiempoPOS2 ="9")  ///
			 vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of `name2' before and after" "a drug is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_normal.pdf", replace
			 
			reghdfe `tipo' i.TiempoPOS2   if  Institucional_Count==$panelcompelto, absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) drop(0.TiempoPOS2) ci ///
			rename(1.TiempoPOS2="-9" 2.TiempoPOS2 ="-8" 3.TiempoPOS2= "-7" 4.TiempoPOS2= "-6" 5.TiempoPOS2= "-5" 6.TiempoPOS2= "-4" 7.TiempoPOS2 ="-3" 8.TiempoPOS2= "-2" ///
			9.TiempoPOS2= "-1" 10.TiempoPOS2= "0" 11.TiempoPOS2= "1" 12.TiempoPOS2 ="2" 13.TiempoPOS2 ="3" 14.TiempoPOS2 ="4" 15.TiempoPOS2 ="5" 16.TiempoPOS2 ="6" 17.TiempoPOS2 ="7" 18.TiempoPOS2 ="8" 19.TiempoPOS2 ="9")  ///
			 vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of `name2' before and after" "a drug is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_completo.pdf", replace
			 
			reghdfe `tipo' i.TiempoPOS2    if  Institucional_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) drop(0.TiempoPOS2) ci ///
			rename(1.TiempoPOS2="-9" 2.TiempoPOS2 ="-8" 3.TiempoPOS2= "-7" 4.TiempoPOS2= "-6" 5.TiempoPOS2= "-5" 6.TiempoPOS2= "-4" 7.TiempoPOS2 ="-3" 8.TiempoPOS2= "-2" ///
			9.TiempoPOS2= "-1" 10.TiempoPOS2= "0" 11.TiempoPOS2= "1" 12.TiempoPOS2 ="2" 13.TiempoPOS2 ="3" 14.TiempoPOS2 ="4" 15.TiempoPOS2 ="5" 16.TiempoPOS2 ="6" 17.TiempoPOS2 ="7" 18.TiempoPOS2 ="8" 19.TiempoPOS2 ="9")  ///
			 vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of `name2' before and after" "a drug is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_completo_trend.pdf", replace
			 
			reghdfe `tipo' i.TiempoPOS3    if  Institucional_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of `name2' before and after" "a drug is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_completo_trend_longrun.pdf", replace
			 
			reghdfe `tipo' i.TiempoPOS3    if  Institucional_Count==$panelcompelto & VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of `name2' before and after" "a drug is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_completo_trend_longrun_Ratio30.pdf", replace
			 
			reghdfe `tipo' i.TiempoPOS3    if   VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of `name2' before and after" "a drug is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_trend_longrun_Ratio30.pdf", replace 
			 */
			 
			 reghdfe `tipo' i.TiempoPOS3    , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3") yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))    vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) /*title("Evolution of `name2' before and after" "a drug is included in the benefit plan") */
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_longrun.pdf", replace 
			
			 reghdfe `tipo' i.TiempoPOS3   if Institucional_Count==$panelcompelto, absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")    yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) /*title("`name2' before and after" "a drug is included in the benefit plan") */
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_completo_longrun.pdf", replace 
			  
			  
			 reghdfe `tipo' i.TiempoPOS3   if Institucional_Count==$panelcompelto, absorb(CUM1 ATC#Periodo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) /*title("`name2' before and after" "a drug is included in the benefit plan") */
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_completo_ATCYEARFE_longrun.pdf", replace 
			 
		 
		}
	}
}


foreach channel in   2{
	foreach tipo in  Prom_STD Unidades_STD{
		foreach var in  Institucional {

			if "`tipo'"=="Prom_STD" local name "Price"
			if "`tipo'"=="Unidades_STD" local name "Quantity"

			if "`tipo'"=="Prom_STD" local name2 "Price"
			if "`tipo'"=="Unidades_STD" local name2 "Quantity sold"
			/*
			reghdfe Lg`var'_`tipo' i.TiempoPOS2   , absorb(Periodo CUM1) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) drop(0.TiempoPOS2) ci ///
			rename(1.TiempoPOS2="-9" 2.TiempoPOS2 ="-8" 3.TiempoPOS2= "-7" 4.TiempoPOS2= "-6" 5.TiempoPOS2= "-5" 6.TiempoPOS2= "-4" 7.TiempoPOS2 ="-3" 8.TiempoPOS2= "-2" ///
			9.TiempoPOS2= "-1" 10.TiempoPOS2= "0" 11.TiempoPOS2= "1" 12.TiempoPOS2 ="2" 13.TiempoPOS2 ="3" 14.TiempoPOS2 ="4" 15.TiempoPOS2 ="5" 16.TiempoPOS2 ="6" 17.TiempoPOS2 ="7" 18.TiempoPOS2 ="8" 19.TiempoPOS2 ="9")  ///
			  vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of a drug's `name2' before and after" "it is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`var'_`tipo'_`channel'_normal.pdf", replace
			 
			reghdfe Lg`var'_`tipo' i.TiempoPOS2   if  `var'_Count==$panelcompelto, absorb(Periodo CUM1) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) drop(0.TiempoPOS2) ci ///
			rename(1.TiempoPOS2="-9" 2.TiempoPOS2 ="-8" 3.TiempoPOS2= "-7" 4.TiempoPOS2= "-6" 5.TiempoPOS2= "-5" 6.TiempoPOS2= "-4" 7.TiempoPOS2 ="-3" 8.TiempoPOS2= "-2" ///
			9.TiempoPOS2= "-1" 10.TiempoPOS2= "0" 11.TiempoPOS2= "1" 12.TiempoPOS2 ="2" 13.TiempoPOS2 ="3" 14.TiempoPOS2 ="4" 15.TiempoPOS2 ="5" 16.TiempoPOS2 ="6" 17.TiempoPOS2 ="7" 18.TiempoPOS2 ="8" 19.TiempoPOS2 ="9")  ///
			vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of a drug's `name2' before and after" "it is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`var'_`tipo'_`channel'_completo.pdf", replace
			 
			reghdfe Lg`var'_`tipo' i.TiempoPOS2    if  `var'_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) drop(0.TiempoPOS2) ci ///
			rename(1.TiempoPOS2="-9" 2.TiempoPOS2 ="-8" 3.TiempoPOS2= "-7" 4.TiempoPOS2= "-6" 5.TiempoPOS2= "-5" 6.TiempoPOS2= "-4" 7.TiempoPOS2 ="-3" 8.TiempoPOS2= "-2" ///
			9.TiempoPOS2= "-1" 10.TiempoPOS2= "0" 11.TiempoPOS2= "1" 12.TiempoPOS2 ="2" 13.TiempoPOS2 ="3" 14.TiempoPOS2 ="4" 15.TiempoPOS2 ="5" 16.TiempoPOS2 ="6" 17.TiempoPOS2 ="7" 18.TiempoPOS2 ="8" 19.TiempoPOS2 ="9")  ///
			  vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of a drug's `name2' before and after" "it is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`var'_`tipo'_`channel'_completo_trend.pdf", replace
			 */
			 
			 /*
			 reghdfe Lg`var'_`tipo' i.TiempoPOS3   , absorb(Periodo CUM1) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of a drug's `name2' before and after" "it is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`var'_`tipo'_`channel'_normal_longrun.pdf", replace
			 
			reghdfe Lg`var'_`tipo' i.TiempoPOS3   if  `var'_Count==$panelcompelto, absorb(Periodo CUM1) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of a drug's `name2' before and after" "it is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`var'_`tipo'_`channel'_completo_longrun.pdf", replace
			 

			 
			 
			 reghdfe Lg`var'_`tipo' i.TiempoPOS3  $control_basico   if  `var'_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of a drug's `name2' before and after" "it is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_completo_trend_longrun.pdf", replace
			 
			 
			  reghdfe Lg`var'_`tipo' i.TiempoPOS3  $control_basico   if  `var'_Count==$panelcompelto & VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) title("Evolution of a drug's `name2' before and after" "it is included in the benefit plan") 
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_completo_trend_longrun_Ratio30.pdf", replace
			  */
			 reghdfe Lg`var'_`tipo' i.TiempoPOS3  $control_basico   if  VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3") yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))    vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) /*title("Evolution of a drug's `name2' before and after" "it is included in the benefit plan") */
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_trend_longrun_Ratio30.pdf", replace
			 
			  
			   reghdfe Lg`var'_`tipo' i.TiempoPOS3  $control_dinamico  if  `var'_Count==$panelcompelto & VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")  yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))    vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) /*title(" `name2' before and after" "it is included in the benefit plan") */
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_completo_trend_longrun_Ratio30_lagcontrols.pdf", replace
			
			 
			reghdfe Lg`var'_`tipo' i.TiempoPOS3  $control_basico  if  `var'_Count==$panelcompelto & VariacionIncreasePrice30_==0, absorb(CUM1 Periodo#ATC) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")  yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))    vertical yline(0) xtitle("Time to POS", size(large)) ytitle("Coefficient on `name'", size(large)) /*title("`name2' before and after" "it is included in the benefit plan") */
			 graph export "$graphs/EventStudyPOS_`tipo'_`channel'_completo_ATCYEARFE_longrun_Ratio30.pdf", replace
			 
			 
			
			 
		}
	}
}

exit




 
foreach ano in  2011 2012  2014 2015 2016 {
	reghdfe LgInstitucional_Prom_STD i.TiempoPOS2 $control_basico   if ((PrimerANOPOS==`ano' & TiempoPOS!=.) | PrimerANOPOS==.) &  Institucional_Count==$panelcompelto & VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
	coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) drop(0.TiempoPOS2) ci ///
	rename(1.TiempoPOS2="-9" 2.TiempoPOS2 ="-8" 3.TiempoPOS2= "-7" 4.TiempoPOS2= "-6" 5.TiempoPOS2= "-5" 6.TiempoPOS2= "-4" 7.TiempoPOS2 ="-3" 8.TiempoPOS2= "-2" ///
	9.TiempoPOS2= "-1" 10.TiempoPOS2= "0" 11.TiempoPOS2= "1" 12.TiempoPOS2 ="2" 13.TiempoPOS2 ="3" 14.TiempoPOS2 ="4" 15.TiempoPOS2 ="5" 16.TiempoPOS2 ="6" 17.TiempoPOS2 ="7" 18.TiempoPOS2 ="8" 19.TiempoPOS2 ="9")  ///
	 vertical yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))   yline(0) xtitle("Time to POS") ytitle("Coefficient on Price") /*title("Price before and after" "it is included in the benefit plan") */
	 graph export "$graphs/EventStudyPOS_precio_completo_trend_`ano'_entry_ratio30.pdf", replace
	 
	 reghdfe LgInstitucional_Prom_STD i.TiempoPOS2 $control_basico  if ((PrimerANOPOS==`ano' & TiempoPOS!=.) | PrimerANOPOS==.) & VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
	 coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) drop(0.TiempoPOS2) ci ///
	rename(1.TiempoPOS2="-9" 2.TiempoPOS2 ="-8" 3.TiempoPOS2= "-7" 4.TiempoPOS2= "-6" 5.TiempoPOS2= "-5" 6.TiempoPOS2= "-4" 7.TiempoPOS2 ="-3" 8.TiempoPOS2= "-2" ///
	9.TiempoPOS2= "-1" 10.TiempoPOS2= "0" 11.TiempoPOS2= "1" 12.TiempoPOS2 ="2" 13.TiempoPOS2 ="3" 14.TiempoPOS2 ="4" 15.TiempoPOS2 ="5" 16.TiempoPOS2 ="6" 17.TiempoPOS2 ="7" 18.TiempoPOS2 ="8" 19.TiempoPOS2 ="9")  ///
	 vertical yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))   yline(0) xtitle("Time to POS") ytitle("Coefficient on Price") /*title("Price before and after" "it is included in the benefit plan") */
	 graph export "$graphs/EventStudyPOS_precio_trend_`ano'_entry_ratio30.pdf", replace
	 
	 
	 reghdfe LgInstitucional_Prom_STD i.TiempoPOS2  $control_dinamico   if ((PrimerANOPOS==`ano' & TiempoPOS!=.) | PrimerANOPOS==.) &  Institucional_Count==$panelcompelto & VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
	coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) drop(0.TiempoPOS2) ci ///
	rename(1.TiempoPOS2="-9" 2.TiempoPOS2 ="-8" 3.TiempoPOS2= "-7" 4.TiempoPOS2= "-6" 5.TiempoPOS2= "-5" 6.TiempoPOS2= "-4" 7.TiempoPOS2 ="-3" 8.TiempoPOS2= "-2" ///
	9.TiempoPOS2= "-1" 10.TiempoPOS2= "0" 11.TiempoPOS2= "1" 12.TiempoPOS2 ="2" 13.TiempoPOS2 ="3" 14.TiempoPOS2 ="4" 15.TiempoPOS2 ="5" 16.TiempoPOS2 ="6" 17.TiempoPOS2 ="7" 18.TiempoPOS2 ="8" 19.TiempoPOS2 ="9")  ///
	 vertical yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))   yline(0) xtitle("Time to POS") ytitle("Coefficient on Price") /*title("Price before and after" "it is included in the benefit plan") */
	 graph export "$graphs/EventStudyPOS_precio_completo_trend_`ano'_entry_ratio30_lagcontrols.pdf", replace
	 

	 
	 reghdfe LgInstitucional_Unidades_STD i.TiempoPOS2 $control_basico  if ((PrimerANOPOS==`ano' & TiempoPOS!=.) | PrimerANOPOS==.) &  Institucional_Count==$panelcompelto & VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
	coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) drop(0.TiempoPOS2) ci ///
	rename(1.TiempoPOS2="-9" 2.TiempoPOS2 ="-8" 3.TiempoPOS2= "-7" 4.TiempoPOS2= "-6" 5.TiempoPOS2= "-5" 6.TiempoPOS2= "-4" 7.TiempoPOS2 ="-3" 8.TiempoPOS2= "-2" ///
	9.TiempoPOS2= "-1" 10.TiempoPOS2= "0" 11.TiempoPOS2= "1" 12.TiempoPOS2 ="2" 13.TiempoPOS2 ="3" 14.TiempoPOS2 ="4" 15.TiempoPOS2 ="5" 16.TiempoPOS2 ="6" 17.TiempoPOS2 ="7" 18.TiempoPOS2 ="8" 19.TiempoPOS2 ="9")  ///
	 vertical yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))   yline(0) xtitle("Time to POS") ytitle("Coefficient on Quantity") /*title("Evolution of a drug's quantity sold before and after" "it is included in the benefit plan") */
	 graph export "$graphs/EventStudyPOS_quantity_completo_trend_`ano'_entry_ratio30.pdf", replace
	 
	 reghdfe LgInstitucional_Unidades_STD i.TiempoPOS2 $control_basico  if ((PrimerANOPOS==`ano' & TiempoPOS!=.) | PrimerANOPOS==.) &  VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
	coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) drop(0.TiempoPOS2) ci ///
	rename(1.TiempoPOS2="-9" 2.TiempoPOS2 ="-8" 3.TiempoPOS2= "-7" 4.TiempoPOS2= "-6" 5.TiempoPOS2= "-5" 6.TiempoPOS2= "-4" 7.TiempoPOS2 ="-3" 8.TiempoPOS2= "-2" ///
	9.TiempoPOS2= "-1" 10.TiempoPOS2= "0" 11.TiempoPOS2= "1" 12.TiempoPOS2 ="2" 13.TiempoPOS2 ="3" 14.TiempoPOS2 ="4" 15.TiempoPOS2 ="5" 16.TiempoPOS2 ="6" 17.TiempoPOS2 ="7" 18.TiempoPOS2 ="8" 19.TiempoPOS2 ="9")  ///
	 vertical yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))   yline(0) xtitle("Time to POS") ytitle("Coefficient on Quantity") /*title("Evolution of a drug's quantity sold before and after" "it is included in the benefit plan") */
	 graph export "$graphs/EventStudyPOS_quantity_trend_`ano'_entry_ratio30.pdf", replace
	 
	  
	  reghdfe LgInstitucional_Unidades_STD i.TiempoPOS2 $control_dinamico  if ((PrimerANOPOS==`ano' & TiempoPOS!=.) | PrimerANOPOS==.) &  Institucional_Count==$panelcompelto & VariacionIncreasePrice30_==0, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
	coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS2) drop(0.TiempoPOS2) ci ///
	 rename(1.TiempoPOS2="-9" 2.TiempoPOS2 ="-8" 3.TiempoPOS2= "-7" 4.TiempoPOS2= "-6" 5.TiempoPOS2= "-5" 6.TiempoPOS2= "-4" 7.TiempoPOS2 ="-3" 8.TiempoPOS2= "-2" ///
	9.TiempoPOS2= "-1" 10.TiempoPOS2= "0" 11.TiempoPOS2= "1" 12.TiempoPOS2 ="2" 13.TiempoPOS2 ="3" 14.TiempoPOS2 ="4" 15.TiempoPOS2 ="5" 16.TiempoPOS2 ="6" 17.TiempoPOS2 ="7" 18.TiempoPOS2 ="8" 19.TiempoPOS2 ="9")  ///
	 vertical yla(, ang(h) nogrid labsize(large)) xla(,labsize(large))   yline(0) xtitle("Time to POS") ytitle("Coefficient on Quantity") /*title("Evolution of a drug's quantity sold before and after" "it is included in the benefit plan") */
	 graph export "$graphs/EventStudyPOS_quantity_completo_trend_`ano'_entry_ratio30_lagcontrols.pdf", replace
	 
	 
 }
 
