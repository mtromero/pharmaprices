*All3 2Or3 1orMore
foreach archivo in  2Or3 {
	use "$base_out/ICD_POS_`archivo'.dta", clear
	rename Periodo ano
	replace icd=regexr(icd,"\.","") 
	merge 1:m icd ano using "$base_out/Defunciones_Fast.dta"
	drop if ano<2007 | ano>2014
	drop if _merge==1
	drop  _merge

	gsort  icd +ano 
	bys icd: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
	bys icd: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)
	gsort  icd -ano 
	bys icd: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
	bys icd: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)




	merge m:1  ano using "$base_out/poblacion_Fast.dta"
	drop if _merge==2
	drop if _merge==1
	drop _merge
	sort icd ano
	bys ano: egen def_totales=sum(defuncion_basica)

	foreach tipo in basica directa pato basica_ricoedu0 basica_ricoedu1 basica_rico0 basica_rico1{
	*bys icd: gen defuncion_`tipo'_acc=sum(defuncion_`tipo')
	gen tasadef_`tipo'=1000*(defuncion_`tipo'/poblacion)
	gen propdef_`tipo'=1000*(defuncion_`tipo'/def_totales)
	}

	drop if poblacion==0 | mi(poblacion)
	tsset icd_2 ano, yearly

	gen anospostPOS=ano-primeravezPOS
	replace anospostPOS=-5 if anospostPOS<-5 & !missing(anospostPOS)
	replace anospostPOS=3 if anospostPOS>=3 & !missing(anospostPOS)
	replace anospostPOS=-6 if anospostPOS==. & !missing(POSDummy)
	replace anospostPOS=anospostPOS+6
	fvset base 5 anospostPOS
	char anospostPOS[omit] 0

	gen anospostPOS2=anospostPOS
	replace anospostPOS2=0 if anospostPOS2==.
	fvset base 5 anospostPOS2
	char anospostPOS2[omit] 0

	label var POSDummy "POS"
	gen POSDummy2=POSDummy
	replace POSDummy2=0 if POSDummy2==. 
	label var POSDummy2 "POS"
	gen numpos2=numpos
	replace numpos2=0 if numpos2==.


	/*
	reghdfe tasadef_basica POSDummy, ab(icd_2   ano) vce(cluster  icd_2) 
	reghdfe tasadef_basica POSDummy2,  ab(icd_2  ano ) vce(cluster  icd_2) 
	reghdfe tasadef_basica POSDummy , ab(icd_2  icd_2#c.ano ano) vce(cluster  icd_2)
	reghdfe tasadef_basica POSDummy2 , ab(icd_2  icd_2#c.ano ano ) vce(cluster  icd_2) 
	*/
	/*
	reghdfe tasadef_basica  , ab(icd_2  icd_2#c.ano ano ) vce(cluster  icd_2) residuals(resid)
	*reghdfe tasadef_basica  , ab(icd_2  ano ) vce(cluster  icd_2) residuals(resid)
	collapse (mean) resid, by(POSDummy icd)
	drop if POSDummy==.
	reshape wide resid, i(icd) j(POSDummy)
	gen Diff=resid1-resid0
	drop if Diff==.
	sort Diff
	*/
	reghdfe propdef_basica POSDummy2, ab(icd_2  icd_2#c.ano ano) vce(cluster  icd_2)
	*basica_ricoedu0 basica_ricoedu1
	*basica_rico0 basica_rico1
	foreach estadistico in tasadef_  propdef_{
		foreach tipo in basica  {
			eststo clear
			qui:eststo: reghdfe `estadistico'`tipo' POSDummy2, ab(icd_2  ano ) vce(cluster  icd_2)
			qui:estadd ysumm
			qui:eststo: reghdfe `estadistico'`tipo' POSDummy2, ab(icd_2  icd_2#c.ano) vce(cluster  icd_2)
			qui:estadd ysumm

			estout using "$latexcodes/RegMortalityFast_`estadistico'`tipo'_`archivo'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
			replace keep(POSDummy2) ///
			stats(ymean N N_clust1 N_clust2 , fmt(a2 a2 a2 a2 ) labels("Mean mortality" "Obs." "Municipalities" "Diseases"))
		}
	}

	/*
	foreach tipo in basica directa pato basica_ricoedu0 basica_ricoedu1 basica_rico0 basica_rico1{

	eststo clear
	/*
	*/
	qui: eststo: reghdfe tasadef_`tipo' POSDummy, ab(icd_2  ano ) vce(cluster  icd_2)
	qui:estadd ysumm
	qui:eststo: reghdfe tasadef_`tipo' POSDummy2, ab(icd_2  ano ) vce(cluster  icd_2)
	qui:estadd ysumm

	qui:eststo: reghdfe tasadef_`tipo' POSDummy, ab(icd_2  icd_2#c.ano ano) vce(cluster  icd_2)
	qui:estadd ysumm
	qui:eststo: reghdfe tasadef_`tipo' POSDummy2, ab(icd_2  icd_2#c.ano ano) vce(cluster  icd_2)
	qui:estadd ysumm
	/*
	qui:eststo: reghdfe defuncion_`tipo' POSDummy, ab(icd_2  ano ) vce(cluster  icd_2)
	qui:estadd ysumm
	qui:eststo: reghdfe defuncion_`tipo' POSDummy2, ab(icd_2  ano ) vce(cluster  icd_2)
	qui:estadd ysumm

	qui:eststo: reghdfe tasadef_`tipo' numpos, ab(icd_2  icd_2#c.ano ano) vce(cluster  icd_2)
	qui:estadd ysumm
	qui:eststo: reghdfe tasadef_`tipo' numpos2, ab(icd_2  icd_2#c.ano ano) vce(cluster  icd_2)
	qui:estadd ysumm
	*/
	estout, style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
	replace keep(POSDummy POSDummy2) ///
	stats(ymean N N_clust1 N_clust2 , fmt(a2 a2 a2 a2 ) labels("Mean mortality" "Obs." "Municipalities" "Diseases"))

	}
	*/

	/*
	foreach tipo in basica {
	eststo clear
	qui:eststo: reghdfe defuncion_`tipo'_acc POSDummy, ab(icd_2  ano ) vce(cluster  icd_2)
	qui:estadd ysumm
	qui:eststo: reghdfe defuncion_`tipo'_acc  POSDummy2, ab(icd_2  ano ) vce(cluster  icd_2)
	qui:estadd ysumm
	qui:eststo: reghdfe defuncion_`tipo'_acc  POSDummy2, ab(icd_2  icd_2#c.ano) vce(cluster  icd_2)
	qui:estadd ysumm
	estout, style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
	replace keep(POSDummy POSDummy2) ///
	stats(ymean N N_clust1 N_clust2 , fmt(a2 a2 a2 a2 ) labels("Mean mortality" "Obs." "Municipalities" "Diseases"))

	}
	*/

	/*
	twoway (scatter defuncion_basica ano if icd=="N18")
	twoway (scatter defuncion_basica_acc ano if icd=="N18")

	reghdfe tasadef_basica POSDummy , ab(icd_2 icd_2#c.ano  ano ) vce(cluster  icd_2)
	reghdfe tasadef_basica POSDummy2 , ab(icd_2 icd_2#c.ano  ano ) vce(cluster  icd_2)
	reghdfe tasadef_basica i.anospostPOS , ab(icd_2 icd_2#c.ano  ano ) vce(cluster  icd_2)
	reghdfe tasadef_basica i.anospostPOS2 , ab(icd_2 icd_2#c.ano  ano ) vce(cluster  icd_2)
	*/
	*basica_rico0 basica_rico1
	

	foreach estadistico in tasadef_  propdef_{
		foreach tipo in basica  {
			foreach cual in "2"{
				reghdfe `estadistico'`tipo' i.anospostPOS`cual' , ab(icd_2  ano ) vce(cluster  icd_2)
				coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS`cual') ci rename(1.anospostPOS`cual'="<-5" 2.anospostPOS`cual' ="-4" 3.anospostPOS`cual'= "-3" 4.anospostPOS`cual'= "-2" 5.anospostPOS`cual'= "-1" 6.anospostPOS`cual'= "0" 7.anospostPOS`cual' ="1" 8.anospostPOS`cual'= "=2" 9.anospostPOS`cual'= ">=3") ///
				vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 
				graph export "$graphs/EventStudyMortalityFast_`estadistico'`tipo'_`cual'_`archivo'.pdf", replace 
			}
		}
	}
	
	foreach estadistico in tasadef_  propdef_{
		foreach tipo in basica  {
			foreach cual in "2"{
				reghdfe `estadistico'`tipo' i.anospostPOS`cual' , ab(icd_2 icd_2#c.ano  ) vce(cluster  icd_2)
				coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS`cual') ci rename(1.anospostPOS`cual'="<-5" 2.anospostPOS`cual' ="-4" 3.anospostPOS`cual'= "-3" 4.anospostPOS`cual'= "-2" 5.anospostPOS`cual'= "-1" 6.anospostPOS`cual'= "0" 7.anospostPOS`cual' ="1" 8.anospostPOS`cual'= "=2" 9.anospostPOS`cual'= ">=3") ///
				vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 
				graph export "$graphs/EventStudyMortalityFast_Trend_`estadistico'`tipo'_`cual'_`archivo'.pdf", replace 
			}
		}
	}

}
