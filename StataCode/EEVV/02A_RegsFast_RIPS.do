*All3 2Or3  2Or3
foreach archivo in All3 2Or3 1orMore {
	use "$base_out/ICD_POS_`archivo'.dta", clear
	rename Periodo ano
	replace icd=regexr(icd,"\.","") 
	merge 1:m icd ano using "$base_out/RIPS.dta"
	drop if ano<2007 | ano>2016
	drop if _merge==1
	drop  _merge

	gsort  icd +ano 
	bys icd: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
	bys icd: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)
	gsort  icd -ano 
	bys icd: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
	bys icd: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)


	drop if icd==""

	merge m:1  ano using "$base_out/poblacion_Fast.dta"
	drop if _merge==2
	drop if _merge==1
	drop _merge
	sort icd ano
	bys ano: egen enfermos_totales=sum(enfermos)

	gen propenfermos=100*(enfermos/enfermos_totales)
	gen tasaenfermos=1000*(enfermos/poblacion)



	drop if poblacion==0 | mi(poblacion)
	tsset icd_2 ano, yearly

	gen anospostPOS=ano-primeravezPOS
	replace anospostPOS=-5 if anospostPOS<-5 & !missing(anospostPOS)
	replace anospostPOS=3 if anospostPOS>=3 & !missing(anospostPOS)
	replace anospostPOS=-6 if anospostPOS==. & !missing(POSDummy)
	replace anospostPOS=anospostPOS+6
	fvset base 5 anospostPOS
	char anospostPOS[omit] 0

	gen anospostPOS2=anospostPOS
	replace anospostPOS2=0 if anospostPOS2==.
	fvset base 5 anospostPOS2
	char anospostPOS2[omit] 0

	label var POSDummy "POS"
	gen POSDummy2=POSDummy
	label var POSDummy2 "POS"
	replace POSDummy2=0 if POSDummy2==. 
	gen numpos2=numpos
	replace numpos2=0 if numpos2==.


	/*
	reghdfe tasadef_basica POSDummy, ab(icd_2   ano) vce(cluster  icd_2) 
	reghdfe tasadef_basica POSDummy2,  ab(icd_2  ano ) vce(cluster  icd_2) 
	reghdfe tasadef_basica POSDummy , ab(icd_2  icd_2#c.ano ano) vce(cluster  icd_2)
	reghdfe tasadef_basica POSDummy2 , ab(icd_2  icd_2#c.ano ano ) vce(cluster  icd_2) 
	*/
	/*
	reghdfe tasaenfermos  , ab(icd_2  icd_2#c.ano ano ) vce(cluster  icd_2) residuals(resid)
	*reghdfe tasaenfermos  , ab(icd_2  ano ) vce(cluster  icd_2) residuals(resid)
	*reghdfe propenfermos  , ab(icd_2 icd_2#c.ano  ano ) vce(cluster  icd_2) residuals(resid)
	*reghdfe propenfermos  , ab(icd_2  ano ) vce(cluster  icd_2) residuals(resid)

	collapse (mean) resid, by(POSDummy icd)
	drop if POSDummy==.
	reshape wide resid, i(icd) j(POSDummy)
	gen Diff=resid1-resid0
	drop if Diff==.
	sort Diff
	*/


	foreach tipo in propenfermos tasaenfermos {
		eststo clear
		qui:eststo: reghdfe `tipo' POSDummy2, ab(icd_2  ano ) vce(cluster  icd_2)
		qui:estadd ysumm
		qui:eststo: reghdfe `tipo' POSDummy2, ab(icd_2  icd_2#c.ano) vce(cluster  icd_2)
		qui:estadd ysumm
		estout using "$latexcodes/RegRIPSFast`tipo'_`archivo'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
		replace keep(POSDummy2) ///
	stats(ymean N N_clust1 , fmt(a2 a2 a2 a2 ) labels("Mean" "Obs." "Diseases"))
	}


	foreach tipo in propenfermos tasaenfermos {
		foreach cual in "2"{
			reghdfe `tipo' i.anospostPOS`cual' , ab(icd_2 ano ) vce(cluster  icd_2)
			coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS`cual') ci rename(1.anospostPOS`cual'="<-5" 2.anospostPOS`cual' ="-4" 3.anospostPOS`cual'= "-3" 4.anospostPOS`cual'= "-2" 5.anospostPOS`cual'= "-1" 6.anospostPOS`cual'= "0" 7.anospostPOS`cual' ="1" 8.anospostPOS`cual'= "=2" 9.anospostPOS`cual'= ">=3") ///
			vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of morbidity rate") title("Evolution of morbidity rate before and after" "a drug for the disease is included in the benefit plan") 
			graph export "$graphs/EventStudyRIPSFast_`tipo'_`cual'_`archivo'.pdf", replace  
		}
	}
	
	foreach tipo in propenfermos tasaenfermos {
		foreach cual in "2"{
			reghdfe `tipo' i.anospostPOS`cual' , ab(icd_2 icd_2#c.ano ) vce(cluster  icd_2)
			coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS`cual') ci rename(1.anospostPOS`cual'="<-5" 2.anospostPOS`cual' ="-4" 3.anospostPOS`cual'= "-3" 4.anospostPOS`cual'= "-2" 5.anospostPOS`cual'= "-1" 6.anospostPOS`cual'= "0" 7.anospostPOS`cual' ="1" 8.anospostPOS`cual'= "=2" 9.anospostPOS`cual'= ">=3") ///
			vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of morbidity rate") title("Evolution of morbidity rate before and after" "a drug for the disease is included in the benefit plan") 
			graph export "$graphs/EventStudyRIPSFast_Trend_`tipo'_`cual'_`archivo'.pdf", replace  
		}
	}

}
