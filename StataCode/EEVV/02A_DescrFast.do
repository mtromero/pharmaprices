use "$base_out/ICD_POS_2Or3.dta", clear
rename Periodo ano
replace icd=regexr(icd,"\.","")
merge 1:m icd ano using "$base_out/RIPS.dta"
drop  _merge
drop icd_2
merge 1:m icd ano using "$base_out/Defunciones_Fast.dta"
*drop if _merge==1
drop  _merge
drop if icd==""
drop icd_2

encode icd, gen(icd_2)

drop if ano<2009 | ano>2016
keep icd ano numpos POSDummy algunavezpos algunaveznopos primeravezPOS ultimaveznoPOS enfermos atenciones icd_2 defuncion_basica


merge m:1  ano using "$base_out/poblacion_Fast.dta"
drop if _merge==2
drop if _merge==1
drop _merge
sort icd ano
*recode enfermos defuncion_basica (.=0)
gen icd1=substr(icd,1,2)

foreach tipo in defuncion_basica   enfermos{
*by ano: egen denom=total(`tipo')
*gen tasa`tipo'=1000*(`tipo'/denom)
gen tasa`tipo'=1000*(`tipo'/poblacion)
capture drop denom
bys ano icd1: egen denom=total(`tipo')
gen prop`tipo'=100*(`tipo'/denom)
}



/*
*Herpes and Herpes Zoter - valaciclovir 
B00
B02
 - J05AB11
*Leukemia- 	imatinib 
C92-L01XE01
 - J05AB11
C16- cancer estomago
E78 - Trastornosdel Metabolismo De Las Lipoproteinas Y Otras Lipidemias
M05 - artristis reumatoide
T86 - rechazo organo
*/



 foreach code in B00 B02 C92 C16 E78 M05 T86  {
sum primeravezPOS if icd=="`code'"
twoway  (connected enfermos ano if icd=="`code'" )  (connected denom ano if icd=="`code'", yaxis(2)), legend(order(1 "Morbidity `code'" 2 "Morbidity group") ) ytitle("Morbidity")  xline(`r(min)')
graph export "$graphs/RawBrute`code'.pdf", replace  
}

foreach code in  B00 B02 C92 C16 E78 M05 T86 {
sum primeravezPOS if icd=="`code'"
twoway  (connected tasaenfermos ano if icd=="`code'" )  , legend(order(1 "Morbidity") ) ytitle("Morbidity rate")   xline(`r(min)')
graph export "$graphs/RawData`code'.pdf", replace  
}

foreach code in  B00 B02 C92 C16 E78 M05 T86  {
sum primeravezPOS if icd=="`code'"
twoway  (connected propenfermos ano if icd=="`code'" ) , legend(order(1 "Morbidity") ) ytitle("Morbidity rate")   xline(`r(min)')
graph export "$graphs/RawPropData`code'.pdf", replace  
}


keep if icd=="B00" | icd=="B02" | icd=="C92" 
collapse (mean) enfermos defuncion_basica tasadefuncion_basica propdefuncion_basica tasaenfermos propenfermos, by( POSDummy icd)

graph bar (mean) enfermos, over(POSDummy) over(icd) ytitle(Morbidity (Raw)) title(Six cases)
graph export "$graphs/enfermos.pdf", replace  

graph bar (mean) defuncion_basica if icd=="C92" | icd=="C55", over(POSDummy) over(icd) ytitle(Mortality (Raw)) title(Six cases)
graph export "$graphs/defuncion_basica_C92.pdf", replace  

graph bar (mean) defuncion_basica if icd!="C92" & icd!="C55", over(POSDummy) over(icd) ytitle(Mortality (Raw)) title(Six cases)
graph export "$graphs/defuncion_basica.pdf", replace  

graph bar (mean) tasaenfermos, over(POSDummy) over(icd) ytitle(Morbidity (Per 1000)) title(Six cases)
graph export "$graphs/tasaenfermos.pdf", replace  

graph bar (mean) tasadefuncion_basica if icd!="C92" & icd!="C55", over(POSDummy) over(icd) ytitle(Mortality (Per 1000)) title(Six cases)
graph export "$graphs/tasadefuncion_basica.pdf", replace  

graph bar (mean) tasadefuncion_basica if icd=="C92" | icd=="C55", over(POSDummy) over(icd) ytitle(Mortality (Per 1000)) title(Six cases)
graph export "$graphs/tasadefuncion_basica_C92.pdf", replace 

graph bar (mean) propenfermos, over(POSDummy) over(icd) ytitle(Morbidity (Proportion)) title(Six cases)
graph export "$graphs/propenfermos.pdf", replace  

graph bar (mean) propdefuncion_basica if icd!="C92" & icd!="C55", over(POSDummy) over(icd) ytitle(Mortality (Proportion)) title(Six cases)
graph export "$graphs/propdefuncion_basica.pdf", replace  

graph bar (mean) propdefuncion_basica if icd=="C92" | icd=="C55", over(POSDummy) over(icd) ytitle(Mortality (Proportion)) title(Six cases)
graph export "$graphs/propdefuncion_basica_C92.pdf", replace  


/*
reghdfe tasaenfermos  , ab(icd_2  icd_2#c.ano ano ) vce(cluster  icd_2) residuals(resid_tasaenfermos)
reghdfe tasadefuncion_basica  , ab(icd_2  icd_2#c.ano ano ) vce(cluster  icd_2) residuals(resid_tasadefuncion_basica)

foreach code in D46 G35 K70 {
sum primeravezPOS if icd=="`code'"
twoway  (line resid_tasaenfermos ano if icd=="`code'" )  (line resid_tasadefuncion_basica ano if icd=="`code'", yaxis(2)), legend(order(1 "Morbidity" 2 "Mortality") ) ytitle("Morbidity rate") ytitle("Mortality rate", axis(2))  xline(`r(min)')
graph export "$graphs/ResidData`code'.pdf", replace  
}
*/
