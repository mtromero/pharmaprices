use "$base_out/ICD_POS.dta", clear
rename icd c_dir1
rename Periodo ano
replace c_dir1=regexr(c_dir1,"\.","") 
merge 1:m c_dir1 ano using "$base_out/DefuncionesCausaDirecta.dta"
drop if ano<2007 | ano>2014
drop if _merge==1
drop  _merge
gsort  c_dir1 +ano 
bys c_dir1: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
bys c_dir1: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)
gsort  c_dir1 -ano 
bys c_dir1: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
bys c_dir1: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)



destring coddane, replace
merge m:1 coddane ano using "$base_out/poblacion.dta"
drop if _merge==2
/*
preserve 
collapse (mean) poblacion (sum)  defuncion, by(ano coddane)
gen mort=1000*defuncion/poblacion
sum mort if ano==2011
sum mort [aw=poblacion]  if ano==2011
collapse (sum) poblacion  defuncion, by(ano)
di 1000*defuncion/poblacion
. di mort
restore
*/

drop if _merge==1
drop _merge
gen tasadef=1000*(defuncion/poblacion)
drop if poblacion==0 | mi(poblacion)


gen anospostPOS=ano-primeravezPOS
replace anospostPOS=-5 if anospostPOS<-5 & !missing(anospostPOS)
replace anospostPOS=3 if anospostPOS>=3 & !missing(anospostPOS)
replace anospostPOS=-6 if anospostPOS==. & !missing(POSDummy)
replace anospostPOS=anospostPOS+6
fvset base 5 anospostPOS
char anospostPOS[omit] 0

gen anospostPOS2=anospostPOS
replace anospostPOS2=0 if anospostPOS2==.
fvset base 5 anospostPOS2
char anospostPOS2[omit] 0

gen tasadef_intia=tasadef if ano==2007
gsort  c_dir1 coddane  +ano 
bys c_dir1 coddane: replace tasadef_intia=tasadef_intia[_n-1] if mi(tasadef_intia)

encode c_dir1, gen(c_dir1_2)
label var POSDummy "POS"

*I need to do something about the missing POSDummy! Mental note

/*
preserve 
collapse (mean) anospostPOS2 anospostPOS primeravezPOS POSDummy (sum) poblacion  defuncion, by(ano c_dir1_2)
xtset c_dir1_2 ano, yearly
decode c_dir1_2  , generate(c_dir1) 
gen tasadef= 1000*defuncion/poblacion
/*
preserve 
collapse (mean) poblacion (sum)  defuncion, by(ano)
gen mort=1000*defuncion/poblacion
sum mort if ano==2011
restore
*/
bys c_dir1_2: egen missing= count(POSDummy) 
replace  missing=1 if missing>0
replace missing=1-missing
gen POSDummy2=POSDummy
replace POSDummy2=0 if POSDummy2==.  

sum tasadef if ano==2011,d
di r(sum)
reghdfe tasadef POSDummy , ab(c_dir1_2  ano) vce(cluster c_dir1_2)
fvset base 5 anospostPOS
char anospostPOS[omit] 0
reghdfe tasadef i.anospostPOS , ab(c_dir1_2  ano) vce(cluster c_dir1_2)
coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS) ci rename(1.anospostPOS="<-5" 2.anospostPOS ="-4" 3.anospostPOS= "-3" 4.anospostPOS= "-2" 5.anospostPOS= "-1" 6.anospostPOS= "0" 7.anospostPOS ="1" 8.anospostPOS= "=2" 9.anospostPOS= ">=3") ///
vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 

fvset base 5 anospostPOS2
char anospostPOS2[omit] 0
reghdfe tasadef i.anospostPOS2 , ab(c_dir1_2  ano) vce(cluster c_dir1_2)
coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS2) ci rename(1.anospostPOS2="<-5" 2.anospostPOS2 ="-4" 3.anospostPOS2= "-3" 4.anospostPOS2= "-2" 5.anospostPOS2= "-1" 6.anospostPOS2= "0" 7.anospostPOS2 ="1" 8.anospostPOS2= "=2" 9.anospostPOS2= ">=3") ///
vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 


reghdfe tasadef POSDummy2 , ab(c_dir1_2  ano) vce(cluster c_dir1_2)
estadd ysumm
reghdfe tasadef POSDummy , ab(ano c.ano#c_dir1_2 c_dir1_2 ) vce(cluster c_dir1_2)
estadd ysumm
restore
*/



eststo clear
eststo: reghdfe tasadef POSDummy, ab(c_dir1_2  ano coddane) vce(cluster coddane c_dir1_2)
estadd ysumm

eststo: reghdfe tasadef POSDummy, ab(c_dir1_2#coddane  ano#coddane) vce(cluster coddane c_dir1_2)
estadd ysumm

estout using "$latexcodes/RegMortality_Direct.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
replace keep(POSDummy) ///
stats(ymean N N_clust1 N_clust2 , fmt(a2 a2 a2 a2 ) labels("Mean mortality" "Obs." "Municipalities" "Diseases"))



reghdfe tasadef i.anospostPOS, ab(c_dir1_2  ano coddane) vce(cluster coddane c_dir1_2)
coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS) ci rename(1.anospostPOS="<-3" 2.anospostPOS ="-3" 3.anospostPOS= "-2" 4.anospostPOS= "-1" 5.anospostPOS= "0" 6.anospostPOS= "1" 7.anospostPOS ="2" 8.anospostPOS= ">=3") ///
vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 
graph export "$graphs/EventStudyMoratility_1.pdf", replace  
  
reghdfe tasadef i.anospostPOS, ab(c_dir1_2#coddane  ano#coddane) vce(cluster coddane c_dir1_2)
coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS) ci rename(1.anospostPOS="<-3" 2.anospostPOS ="-3" 3.anospostPOS= "-2" 4.anospostPOS= "-1" 5.anospostPOS= "0" 6.anospostPOS= "1" 7.anospostPOS ="2" 8.anospostPOS= ">=3") ///
vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 
graph export "$graphs/EventStudyMoratility_2.pdf", replace  
    
  
