use "$base_out/ICD_POS.dta", clear
rename icd c_bas1
rename Periodo ano
replace c_bas1=regexr(c_bas1,"\.","") 
merge 1:m c_bas1 ano using "$base_out/DefuncionesCausaBasica.dta"
drop if ano<2007 | ano>2014
drop if _merge==1
drop  _merge

gsort  c_bas1 +ano 
bys c_bas1: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
bys c_bas1: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)
gsort  c_bas1 -ano 
bys c_bas1: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
bys c_bas1: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)


gen coddane=string(codptore,"%02.0f")+string(codmunre,"%03.0f")
destring coddane, replace
merge m:1 coddane ano using "$base_out/poblacion.dta"
drop if _merge==2
drop if _merge==1
drop _merge
gen tasadef=10000*(defuncion/poblacion)

gen anospostPOS=ano-primeravezPOS
replace anospostPOS=-4 if anospostPOS<-3
replace anospostPOS=3 if anospostPOS>=4
replace anospostPOS=-5 if anospostPOS==.
replace anospostPOS=anospostPOS+5
fvset base 4 anospostPOS
char anospostPOS[omit] 0

encode c_bas1, gen(c_bas1_2)
label var POSDummy "POS"

eststo clear
eststo: reghdfe tasadef POSDummy, ab(c_bas1_2  ano coddane) vce(cluster coddane c_bas1_2)
estadd ysumm

eststo: reghdfe tasadef POSDummy, ab(c_bas1_2#coddane  ano#coddane) vce(cluster coddane c_bas1_2)
estadd ysumm

estout using "$latexcodes/RegMortality_sufi.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
replace keep(POSDummy) ///
stats(ymean N N_clust1 N_clust2 , fmt(a2 a2 a2 a2 ) labels("Mean mortality" "Obs." "Municipalities" "Diseases"))


 

reghdfe tasadef i.anospostPOS, ab(c_bas1_2  ano coddane) vce(cluster coddane c_bas1_2)
coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS) ci rename(1.anospostPOS="<-3" 2.anospostPOS ="-3" 3.anospostPOS= "-2" 4.anospostPOS= "-1" 5.anospostPOS= "0" 6.anospostPOS= "1" 7.anospostPOS ="2" 8.anospostPOS= ">=3") ///
vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 
graph export "$graphs/EventStudyMoratility_1_sufi.pdf", replace  
  
reghdfe tasadef i.anospostPOS, ab(c_bas1_2#coddane  ano#coddane) vce(cluster coddane c_bas1_2)
coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS) ci rename(1.anospostPOS="<-3" 2.anospostPOS ="-3" 3.anospostPOS= "-2" 4.anospostPOS= "-1" 5.anospostPOS= "0" 6.anospostPOS= "1" 7.anospostPOS ="2" 8.anospostPOS= ">=3") ///
vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 
graph export "$graphs/EventStudyMoratility_2_sufi.pdf", replace  
    
  
