use "$base_out/ICD_POS.dta", clear
rename icd c_bas1
rename Periodo ano
replace c_bas1=regexr(c_bas1,"\.","") 
merge 1:m c_bas1 ano using "$base_out/DefuncionesCausaBasica_todos.dta"
drop if ano<2007 | ano>2014
drop if _merge==1
drop  _merge

gsort  c_bas1 +ano 
bys c_bas1: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
bys c_bas1: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)
gsort  c_bas1 -ano 
bys c_bas1: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
bys c_bas1: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)



destring coddane, replace
merge m:1 coddane ano using "$base_out/poblacion.dta"
drop if _merge==2
/*
preserve 
collapse (mean) poblacion (sum)  defuncion, by(ano coddane)
gen mort=1000*defuncion/poblacion
sum mort if ano==2011
sum mort [aw=poblacion]  if ano==2011
collapse (sum) poblacion  defuncion, by(ano)
di 1000*defuncion/poblacion
. di mort
restore
*/

drop if _merge==1
drop _merge
foreach tipo in "" "_0_" "_1_" "_edu0_" "_edu1_"{
	capture drop def_totales
	bys ano coddane: egen def_totales=sum(defuncion`tipo')
	gen tasadef`tipo'=1000*(defuncion`tipo'/def_totales)
}
drop if poblacion==0 | mi(poblacion)


gen anospostPOS=ano-primeravezPOS
replace anospostPOS=-5 if anospostPOS<-5 & !missing(anospostPOS)
replace anospostPOS=3 if anospostPOS>=3 & !missing(anospostPOS)
replace anospostPOS=-6 if anospostPOS==. & !missing(POSDummy)
replace anospostPOS=anospostPOS+6
fvset base 5 anospostPOS
char anospostPOS[omit] 0

gen anospostPOS2=anospostPOS
replace anospostPOS2=0 if anospostPOS2==.
fvset base 5 anospostPOS2
char anospostPOS2[omit] 0

/*
gen tasadef_intia=tasadef if ano==2007
gsort  c_bas1 coddane  +ano 
bys c_bas1 coddane: replace tasadef_intia=tasadef_intia[_n-1] if mi(tasadef_intia)
*/

encode c_bas1, gen(c_bas1_2)
label var POSDummy "POS"
gen POSDummy2=POSDummy
replace POSDummy2=0 if POSDummy2==. 
label var POSDummy2 "POS"
gen numpos2=numpos
replace numpos2=0 if numpos2==.

label var numpos "No. drugs in POS"
label var numpos2 "No. drugs in POS"

/*
preserve 
collapse (mean) anospostPOS2 anospostPOS primeravezPOS POSDummy (sum) poblacion  defuncion, by(ano c_bas1_2)
xtset c_bas1_2 ano, yearly
decode c_bas1_2  , generate(c_bas1) 
gen tasadef= 1000*defuncion/poblacion
/*
preserve 
collapse (mean) poblacion (sum)  defuncion, by(ano)
gen mort=1000*defuncion/poblacion
sum mort if ano==2011
restore
*/
bys c_bas1_2: egen missing= count(POSDummy) 
replace  missing=1 if missing>0
replace missing=1-missing
gen POSDummy2=POSDummy
replace POSDummy2=0 if POSDummy2==.  

sum tasadef if ano==2011,d
di r(sum)
reghdfe tasadef POSDummy , ab(c_bas1_2  ano) vce(cluster c_bas1_2)
fvset base 5 anospostPOS
char anospostPOS[omit] 0
reghdfe tasadef i.anospostPOS , ab(c_bas1_2  ano) vce(cluster c_bas1_2)
coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS) ci rename(1.anospostPOS="<-5" 2.anospostPOS ="-4" 3.anospostPOS= "-3" 4.anospostPOS= "-2" 5.anospostPOS= "-1" 6.anospostPOS= "0" 7.anospostPOS ="1" 8.anospostPOS= "=2" 9.anospostPOS= ">=3") ///
vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 

fvset base 5 anospostPOS2
char anospostPOS2[omit] 0
reghdfe tasadef i.anospostPOS2 , ab(c_bas1_2  ano) vce(cluster c_bas1_2)
coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS2) ci rename(1.anospostPOS2="<-5" 2.anospostPOS2 ="-4" 3.anospostPOS2= "-3" 4.anospostPOS2= "-2" 5.anospostPOS2= "-1" 6.anospostPOS2= "0" 7.anospostPOS2 ="1" 8.anospostPOS2= "=2" 9.anospostPOS2= ">=3") ///
vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 


reghdfe tasadef POSDummy2 , ab(c_bas1_2  ano) vce(cluster c_bas1_2)
estadd ysumm
reghdfe tasadef POSDummy , ab(ano c.ano#c_bas1_2 c_bas1_2 ) vce(cluster c_bas1_2)
estadd ysumm
restore
*/

foreach cual in "" "2"{
	foreach tipo in "" "_0_" "_1_" "_edu0_" "_edu1_"{
		eststo clear
		eststo: reghdfe tasadef`tipo' POSDummy`cual' [aw=poblacion], ab(c_bas1_2  ano coddane) vce(cluster coddane c_bas1_2)
		estadd ysumm

		eststo: reghdfe tasadef`tipo' numpos`cual' [aw=poblacion], ab(c_bas1_2  ano coddane) vce(cluster coddane c_bas1_2)
		estadd ysumm

		eststo: reghdfe tasadef`tipo' POSDummy`cual' [aw=poblacion], ab(c_bas1_2##coddane  ano##coddane) vce(cluster coddane c_bas1_2)
		estadd ysumm

		eststo: reghdfe tasadef`tipo' numpos`cual' [aw=poblacion], ab(c_bas1_2##coddane  ano##coddane) vce(cluster coddane c_bas1_2)
		estadd ysumm

		eststo: reghdfe tasadef`tipo' POSDummy`cual' [aw=poblacion], ab(c_bas1_2#coddane c_bas1_2#c.ano  ano#coddane) vce(cluster coddane c_bas1_2)
		estadd ysumm

		eststo: reghdfe tasadef`tipo' numpos`cual' [aw=poblacion], ab(c_bas1_2##coddane c_bas1_2#c.ano  ano##coddane) vce(cluster coddane c_bas1_2)
		estadd ysumm

		estout using "$latexcodes/RegMortality`tipo'_`cual'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
		replace keep(POSDummy`cual' numpos`cual') ///
		stats(ymean N N_clust1 N_clust2 , fmt(a2 a2 a2 a2 ) labels("Mean mortality" "Obs." "Municipalities" "Diseases"))


		reghdfe tasadef`tipo' i.anospostPOS`cual' [aw=poblacion], ab(c_bas1_2  ano coddane) vce(cluster coddane c_bas1_2)
		coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS`cual') ci rename(1.anospostPOS`cual'="<-5" 2.anospostPOS`cual' ="-4" 3.anospostPOS`cual'= "-3" 4.anospostPOS`cual'= "-2" 5.anospostPOS`cual'= "-1" 6.anospostPOS`cual'= "0" 7.anospostPOS`cual' ="1" 8.anospostPOS`cual'= "=2" 9.anospostPOS`cual'= ">=3") ///
		vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 
		graph export "$graphs/EventStudyMoratility1_`tipo'_`cual'.pdf", replace  
		  
		reghdfe tasadef`tipo' i.anospostPOS`cual' [aw=poblacion], ab(c_bas1_2#coddane  ano#coddane) vce(cluster coddane c_bas1_2)
		coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS`cual') ci rename(1.anospostPOS`cual'="<-5" 2.anospostPOS`cual' ="-4" 3.anospostPOS`cual'= "-3" 4.anospostPOS`cual'= "-2" 5.anospostPOS`cual'= "-1" 6.anospostPOS`cual'= "0" 7.anospostPOS`cual' ="1" 8.anospostPOS`cual'= "=2" 9.anospostPOS`cual'= ">=3") ///
		vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 
		graph export "$graphs/EventStudyMoratility2_`tipo'_`cual'.pdf", replace  
		    
		 reghdfe tasadef`tipo' i.anospostPOS`cual' [aw=poblacion], ab(c_bas1_2##coddane c_bas1_2#c.ano  ano##coddane) vce(cluster coddane c_bas1_2)
		coefplot, graphregion(color(white)) baselevels keep(*.anospostPOS`cual') ci rename(1.anospostPOS`cual'="<-5" 2.anospostPOS`cual' ="-4" 3.anospostPOS`cual'= "-3" 4.anospostPOS`cual'= "-2" 5.anospostPOS`cual'= "-1" 6.anospostPOS`cual'= "0" 7.anospostPOS`cual' ="1" 8.anospostPOS`cual'= "=2" 9.anospostPOS`cual'= ">=3") ///
		vertical yline(0) xtitle("Time to POS") ytitle("Coefficient of mortality rate") title("Evolution of mortality rate before and after" "a drug for the disease is included in the benefit plan") 
		graph export "$graphs/EventStudyMoratility3_`tipo'_`cual'.pdf", replace  	    	  
	}
}
