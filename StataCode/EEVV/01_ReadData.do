global nivel 3
import delimited "$basein/RIPS/RIPS2009.csv", clear
rename diagnósticos icd
rename númeropersonasatendidas enfermos
rename númerodeatenciones atenciones
drop costoprocedimiento valorconsulta
drop if icd=="Grand Total"
replace icd="" if icd=="1 - NO DEFINIDO"
replace icd="" if icd=="0 - NO REPORTADO"
replace icd=substr(icd,1,$nivel)
collapse (sum) enfermos atenciones, by(icd)
gen ano=2009
compress
save "$base_out/RIPS.dta", replace

forvalues i=2010/2016{
import delimited "$basein/RIPS/RIPS`i'.csv", clear
rename diagnósticos icd
rename númeropersonasatendidas enfermos
rename númerodeatenciones atenciones
drop costoprocedimiento valorconsulta
drop if icd=="Grand Total"
replace icd="" if icd=="1 - NO DEFINIDO"
replace icd="" if icd=="0 - NO REPORTADO"
replace icd=substr(icd,1,$nivel)
collapse (sum) enfermos atenciones, by(icd)
gen ano=`i'
compress
append using "$base_out/RIPS.dta"
save "$base_out/RIPS.dta", replace
}
encode icd, gen(icd_2)
tsset icd_2 ano, yearly
tsfill, full
xfill icd , i(icd_2) 
replace enfermos=0 if enfermos==.
replace atenciones=0 if atenciones==.
*replace consulta=0 if consulta==.
tsset icd_2 ano, yearly
compress
save "$base_out/RIPS.dta", replace


import delimited "$basein/EEVV/Defun_2000.txt", clear
keep codptore codmunre sit_defun ano sexo est_civil nivel_edu seg_social pman_muer cons_exp c_muerte asis_med c_dir1 c_ant1 c_ant2 c_ant3 c_pat1 c_bas1 c_mcm1 causa_666 cau_homol
compress
save "$base_out/Defunciones.dta", replace

forvalues i=2000/2014{
import delimited "$basein/EEVV/Defun_`i'.txt", clear

keep codptore codmunre sit_defun ano sexo est_civil nivel_edu seg_social pman_muer cons_exp c_muerte asis_med c_dir1 c_ant1 c_ant2 c_ant3 c_pat1 c_bas1 c_mcm1 causa_666 cau_homol
compress
append using "$base_out/Defunciones.dta"
save "$base_out/Defunciones.dta", replace

}


use  "$base_out/Defunciones.dta", clear
drop if ano<2007
drop if ano==.
gen defuncion=1
replace c_dir1=substr(c_dir1,1,$nivel)
collapse (sum)  defuncion, by(codptore codmunre c_dir1 ano)
gen coddane=string(codptore,"%02.0f")+string(codmunre,"%03.0f")
drop codptore codmunre
reshape wide defuncion, i(c_dir1 ano) j(coddane) string
recode defuncion* (.=0)
reshape long defuncion@, i(c_dir1 ano) j(coddane) string
gen cod=coddane+"-"+c_dir1
egen cod2 = group(cod)
tsset cod2 ano, yearly
tsfill, full
xfill coddane c_dir1 , i(cod2) 
replace defuncion=0 if defuncion==.
drop cod cod2
save  "$base_out/DefuncionesCausaDirecta.dta", replace

use  "$base_out/Defunciones.dta", clear
drop if ano<2007
drop if ano==.
gen defuncion=1
replace c_bas1=substr(c_bas1,1,$nivel)
collapse (sum)  defuncion, by(codptore codmunre c_bas1 ano)
gen coddane=string(codptore,"%02.0f")+string(codmunre,"%03.0f")
drop codptore codmunre
reshape wide defuncion, i(c_bas1 ano) j(coddane) string
recode defuncion* (.=0)
reshape long defuncion@, i(c_bas1 ano) j(coddane) string
gen cod=coddane+"-"+c_bas1
egen cod2 = group(cod)
tsset cod2 ano, yearly
tsfill, full
xfill coddane c_bas1 , i(cod2) 
replace defuncion=0 if defuncion==.
drop cod cod2
save  "$base_out/DefuncionesCausaBasica.dta", replace


use  "$base_out/Defunciones.dta", clear
drop if ano<2007
drop if ano==.
gen rico=.
replace rico=1 if seg_social==1
replace rico=0 if seg_social==2
replace rico=1 if (seg_social>=3 & seg_social<=4) & ano==2007
replace rico=0 if (seg_social==5 | seg_social==6 | seg_social==9) & ano==2007
replace rico=1 if (seg_social>=3 & seg_social<=4) & ano>=2008 & ano<=2014
replace rico=0 if (seg_social==5 | seg_social==9) & ano>=2008 & ano<=2014
gen defuncion=1
replace c_bas1=substr(c_bas1,1,$nivel)
collapse (sum)  defuncion, by(codptore rico codmunre c_bas1 ano)
gen coddane=string(codptore,"%02.0f")+string(codmunre,"%03.0f")
drop codptore codmunre
rename defuncion defuncion_
reshape wide defuncion, i(c_bas1 ano coddane) j(rico) 
rename defuncion_0 defuncion_0_
rename defuncion_1 defuncion_1_
reshape wide defuncion_0_ defuncion_1_, i(c_bas1 ano) j(coddane) string
recode defuncion* (.=0)
reshape long defuncion_0_@ defuncion_1_@, i(c_bas1 ano) j(coddane) string
gen cod=coddane+"-"+c_bas1
egen cod2 = group(cod)
tsset cod2 ano, yearly
tsfill, full
xfill coddane c_bas1 , i(cod2) 
recode defuncion* (.=0)
drop cod cod2
save  "$base_out/DefuncionesCausaBasica_Rico.dta", replace



use  "$base_out/Defunciones.dta", clear
drop if ano<2007
drop if ano==.
gen rico_edu=.
replace rico_edu=1 if (nivel_edu==6 | nivel_edu==7) & ano==2007
replace rico_edu=0 if (nivel_edu==8 | nivel_edu<=5 | nivel_edu==9) & ano==2007
replace rico_edu=1 if (nivel_edu>=7 & nivel_edu<=12) & ano>=2008 & ano<=2014
replace rico_edu=0 if (nivel_edu==13 | nivel_edu<=6 | nivel_edu==99) & ano>=2008 & ano<=2014
gen defuncion=1
replace c_bas1=substr(c_bas1,1,$nivel)
collapse (sum)  defuncion, by(codptore rico codmunre c_bas1 ano)
gen coddane=string(codptore,"%02.0f")+string(codmunre,"%03.0f")
drop codptore codmunre
rename defuncion defuncion_
drop if rico_edu==.
reshape wide defuncion, i(c_bas1 ano coddane) j(rico_edu) 
rename defuncion_0 defuncion_edu0_
rename defuncion_1 defuncion_edu1_
reshape wide defuncion_edu0_ defuncion_edu1_, i(c_bas1 ano) j(coddane) string
recode defuncion* (.=0)
reshape long defuncion_edu0_@ defuncion_edu1_@, i(c_bas1 ano) j(coddane) string
gen cod=coddane+"-"+c_bas1
egen cod2 = group(cod)
tsset cod2 ano, yearly
tsfill, full
xfill coddane c_bas1 , i(cod2) 
recode defuncion* (.=0)
drop cod cod2
save  "$base_out/DefuncionesCausaBasica_RicoEdu.dta", replace

use "$base_out/DefuncionesCausaBasica.dta"
merge 1:1 c_bas1 coddane ano using "$base_out/DefuncionesCausaBasica_Rico.dta"
drop _merge
merge 1:1 c_bas1 coddane ano using "$base_out/DefuncionesCausaBasica_RicoEdu.dta"
drop _merge
save "$base_out/DefuncionesCausaBasica_todos.dta", replace
****************************************
****************************************
use  "$base_out/Defunciones.dta", clear
drop if ano==.
gen defuncion_basica=1
replace c_bas1=substr(c_bas1,1,$nivel)
collapse (sum)  defuncion, by(c_bas1 ano)
encode c_bas1,gen(c_bas1_2)
tsset c_bas1_2 ano, yearly
tsfill, full
xfill c_bas1 , i(c_bas1_2) 
replace defuncion=0 if defuncion==.
rename c_bas1 icd
save  "$base_out/DefuncionesCausaBasica_Fast.dta", replace


use  "$base_out/Defunciones.dta", clear
drop if ano==.
gen defuncion_directa=1
replace c_dir1=substr(c_dir1,1,$nivel)
collapse (sum)  defuncion, by(c_dir1 ano)
encode c_dir1,gen(c_dir1_2)
tsset c_dir1_2 ano, yearly
tsfill, full
xfill c_dir1 , i(c_dir1_2) 
replace defuncion=0 if defuncion==.
rename c_dir1 icd
save  "$base_out/DefuncionesCausaDirecta_Fast.dta", replace

use  "$base_out/Defunciones.dta", clear
drop if ano==.
gen defuncion_pato=1
replace c_ant1=substr(c_ant1,1,$nivel)
collapse (sum)  defuncion, by(c_ant1 ano)
encode c_ant1,gen(c_ant1_2)
tsset c_ant1_2 ano, yearly
tsfill, full
xfill c_ant1 , i(c_ant1_2) 
replace defuncion=0 if defuncion==.
rename c_ant1 icd
save  "$base_out/DefuncionesCausaPatologia_Fast.dta", replace



****************************************
****************************************

use  "$base_out/Defunciones.dta", clear
drop if ano<2007
gen rico=.
replace rico=1 if seg_social==1
replace rico=0 if seg_social==2
replace rico=1 if (seg_social>=3 & seg_social<=4) & ano==2007
replace rico=0 if (seg_social==5 | seg_social==6 | seg_social==9) & ano==2007
replace rico=1 if (seg_social>=3 & seg_social<=4) & ano>=2008 & ano<=2014
replace rico=0 if (seg_social==5 | seg_social==9) & ano>=2008 & ano<=2014


gen rico_edu=.
replace rico_edu=1 if (nivel_edu==6 | nivel_edu==7) & ano==2007
replace rico_edu=0 if (nivel_edu==8 | nivel_edu<=5 | nivel_edu==9) & ano==2007
replace rico_edu=1 if (nivel_edu>=7 & nivel_edu<=12) & ano>=2008 & ano<=2014
replace rico_edu=0 if (nivel_edu==13 | nivel_edu<=6 | nivel_edu==99) & ano>=2008 & ano<=2014
drop if ano==.
gen defuncion_basica=1
replace c_bas1=substr(c_bas1,1,$nivel)

collapse (sum)  defuncion_basica, by(rico  c_bas1 ano)
reshape wide defuncion_basica, i(c_bas1 ano) j(rico)
recode defuncion_basica* (.=0)
reshape long defuncion_basica@, i(c_bas1 ano) j(rico)
gen cod=string(rico)+"-"+c_bas1
egen cod2 = group(cod)
tsset cod2 ano, yearly
tsfill, full
xfill rico c_bas1 , i(cod2) 
replace defuncion=0 if defuncion==.
drop cod cod2
reshape wide defuncion_basica, i(c_bas1 ano) j(rico)
rename defuncion_basica0 defuncion_basica_rico0
rename defuncion_basica1 defuncion_basica_rico1
rename c_bas1 icd
save  "$base_out/DefuncionRico_Fast.dta", replace


use  "$base_out/Defunciones.dta", clear
drop if ano<2007
gen rico=.
replace rico=1 if seg_social==1
replace rico=0 if seg_social==2
replace rico=1 if (seg_social>=3 & seg_social<=4) & ano==2007
replace rico=0 if (seg_social==5 | seg_social==6 | seg_social==9) & ano==2007
replace rico=1 if (seg_social>=3 & seg_social<=4) & ano>=2008 & ano<=2014
replace rico=0 if (seg_social==5 | seg_social==9) & ano>=2008 & ano<=2014


gen rico_edu=.
replace rico_edu=1 if (nivel_edu==6 | nivel_edu==7) & ano==2007
replace rico_edu=0 if (nivel_edu==8 | nivel_edu<=5 | nivel_edu==9) & ano==2007
replace rico_edu=1 if (nivel_edu>=7 & nivel_edu<=12) & ano>=2008 & ano<=2014
replace rico_edu=0 if (nivel_edu==13 | nivel_edu<=6 | nivel_edu==99) & ano>=2008 & ano<=2014
drop if ano==.
gen defuncion_basica=1
replace c_bas1=substr(c_bas1,1,$nivel)
drop if rico_edu==.
collapse (sum)  defuncion_basica, by(rico_edu  c_bas1 ano)
reshape wide defuncion_basica, i(c_bas1 ano) j(rico_edu)
recode defuncion_basica* (.=0)
reshape long defuncion_basica@, i(c_bas1 ano) j(rico_edu)
gen cod=string(rico_edu)+"-"+c_bas1
egen cod2 = group(cod)
tsset cod2 ano, yearly
tsfill, full
xfill rico c_bas1 , i(cod2) 
replace defuncion=0 if defuncion==.
drop cod cod2
reshape wide defuncion_basica, i(c_bas1 ano) j(rico_edu)
rename defuncion_basica0 defuncion_basica_ricoedu0
rename defuncion_basica1 defuncion_basica_ricoedu1
rename c_bas1 icd
save  "$base_out/DefuncionRicoEdu_Fast.dta", replace



****************************************
****************************************


use "$base_out/DefuncionesCausaBasica_Fast.dta", clear
merge 1:1 icd ano using "$base_out/DefuncionesCausaDirecta_Fast.dta"
drop _merge
merge 1:1 icd ano using "$base_out/DefuncionesCausaPatologia_Fast.dta"
drop _merge
merge 1:1 icd ano using "$base_out/DefuncionRicoEdu_Fast.dta"
drop _merge
merge 1:1 icd ano using "$base_out/DefuncionRico_Fast.dta"
drop _merge

drop *_2
encode icd, gen(icd_2)
save  "$base_out/Defunciones_Fast.dta", replace


****************************************
****************************************
/*
import delimited "$basein/ICD/atc_drugs.tsv", clear varnames(1)
rename drug_id drugbankid
save "$base_out/TDD.dta", replace

import delimited "$basein/ICD/drug links.csv", clear varnames(1)
keep drugbankid ttdid
merge 1:m drugbankid using "$base_out/TDD.dta"
drop if _merge!=3
drop _merge
drop if ttdid==""
drop drugbankid
save "$base_out/TDD.dta", replace
*/

import delimited "$basein/ICD/TTD_crossmatching.txt", clear delimiter(tab)
rename v1 ttdid
rename v4 atc
keep if v3=="SuperDrug ATC"
drop v3 v2
split atc, generate(atc_) parse(";")
drop atc
reshape long atc_@, i(ttdid) j(obs)
drop if atc_==""
rename atc_ atc_code
drop obs
save "$base_out/TDD.dta", replace

import delimited "$basein/ICD/drug-disease_TTD2016.txt", clear varnames(1) delimiter(tab)
keep ttddrugid icd10
rename ttddrugid ttdid
drop if icd10=="."
drop if icd10==""
split icd10, generate(D_) parse(",")
bys ttdid: gen id2=_n
reshape long D_@, i(ttdid id2) j(obs)
drop  obs icd10 id2
drop if D_==""
mmerge ttdid using "$base_out/TDD.dta"
drop if _merge!=3
drop _merge
drop ttdid
bys D_ atc_code: gen N=_n
drop if N>1
drop N 
gen range=regexm(D_,"-")
split D_, generate(icd_ ) parse("-")
gen same_letter=substr(icd_1,1,1)==substr(icd_2,1,1)
replace icd_1=substr(icd_1,1,$nivel)
replace icd_2=substr(icd_2,1,$nivel)
replace icd_2="" if icd_1==icd_2

forvalues i=3/200{
	gen icd_`i'=""
}
local N = _N
forvalues i = 1/`N' {
	if same_letter[`i']==1{
		local letter=substr(icd_1[`i'],1,1)
		local prim=substr(icd_1[`i'],2,.)
		local fin=substr(icd_2[`i'],2,.)	
		local Diff=`fin'-`prim'+1		
		display `i'
		forvalues k=3/`Diff'{
			replace icd_`k'="`letter'"+string(`prim'+`k'-2, "%02.0f") in `i'
		}
	}
}

local N = _N
forvalues i = 1/`N' {
	if substr(icd_1[`i'],1,1)=="A" & substr(icd_2[`i'],1,1)=="B"{
		forvalues k=3/101{
			replace icd_`k'="A"+string(`k'-2, "%02.0f") in `i'
		}
		forvalues k=102/200{
			replace icd_`k'="B"+string(`k'-102, "%02.0f") in `i'
		}
	}
}

local N = _N
forvalues i = 1/`N' {
	if substr(icd_1[`i'],1,1)=="S" & substr(icd_2[`i'],1,1)=="T"{
		forvalues k=3/101{
			replace icd_`k'="S"+string(`k'-2, "%02.0f") in `i'
		}
		forvalues k=102/199{
			replace icd_`k'="T"+string(`k'-102, "%02.0f") in `i'
		}
	}
}
keep atc_code icd_*
bys atc_code: gen id2=_n
reshape long icd_@, i(atc_code id2) j(D_)
drop id2
drop D_
drop if icd_==""
rename icd_ icd
rename atc_code atc
bys atc icd: gen n=_n
drop if n>1
drop n
save "$base_out/TDD.dta", replace

	
 
import delimited "$basein/ICD/MEDI_11242015.csv", clear varnames(1)
keep atc code
drop if atc=="" | code==""
drop if code=="00-99.99"
*split code, generate(D_) parse("-")
drop if regexm(code,"-")==1
drop if regexm(code,"\|")==1
replace code=regexr(code,"\.","") 
rename code icd9cm
bys atc icd9cm:  gen dup = cond(_N==1,0,_n)
drop if dup>1
drop dup
save "$base_out/MEDI_11242015.dta", replace

*use "$basein/ICD/icd9toicd10pcsgem.dta", clear /*esta no es */
*use "$basein/ICD/icd10pcstoicd9gem.dta", clear /*esta no es */
*use "$basein/ICD/icd10cmtoicd9gem.dta", clear
*use "$basein/ICD/icd10toicd9gem.dta", clear 
use "$basein/ICD/icd9toicd10cmgem.dta", clear 
keep icd10cm icd9cm
replace icd10cm=substr(icd10cm,1,$nivel)
bys icd10cm icd9cm:  gen dup = cond(_N==1,0,_n)
drop if dup>1
drop dup
bys icd9cm: gen N=_N
drop if N>1
drop N 
merge 1:m icd9cm using "$base_out/MEDI_11242015.dta"
drop if _merge!=3
drop if icd10cm=="Q15"
drop _merge
rename icd10cm icd
keep icd atc
bys atc icd: gen n=_n
drop if n>1
drop n
save "$base_out/CrossOver_vanderbilt.dta", replace

import delimited "$basein/ICD/MIA.csv", clear varnames(1)
drop if icd==""
replace icd=substr(icd,1,$nivel)
bys atc icd: gen n=_n
drop if n>1
drop n
save "$base_out/MIA.dta", replace


use "$base_out/MIA.dta", replace
append using "$base_out/CrossOver_vanderbilt.dta"
append using "$base_out/TDD.dta"
bys icd atc: gen n=_n
bys icd atc: gen N=_N
drop if n>1
drop n
preserve
keep if N==3
drop N
save "$base_out/CrossOver_All3.dta", replace
restore
preserve
keep if N>=2
drop N
save "$base_out/CrossOver_2Or3.dta", replace
restore

preserve
keep if N>=1
drop N
save "$base_out/CrossOver_1orMore.dta", replace
restore

/*
import delimited "$basein/ICD/CrossoverACTICD2.csv", clear varnames(1)
rename diag icd
drop if icd==""
save "$base_out/CrossoverACTICD.dta", replace
*/
*
foreach archivo in  All3 2Or3 1orMore{
	use "$base_out/MedicamentosRegsLong.dta", clear
	bys ATC0: egen minY=min(YearExpedicion)
	drop if minY>2007
	drop if Canal2==2
	keep POS2 Periodo CUM1 ATC0 ATC1 ATC2 ATC3
	reshape wide POS2, i(CUM1 ATC0 ATC1 ATC2 ATC3) j(Periodo)
	collapse (sum) POS2*, by(ATC0)
	rename ATC0 atc
	merge 1:m atc using "$base_out/CrossOver_`archivo'.dta"
	drop if _merge!=3
	drop _merge
	replace icd=substr(icd,1,$nivel)
	export delim "$base_out/ATC_ICD_POS_`archivo'.csv", replace
	collapse (sum) POS2*, by(icd)
	reshape long POS2, i(icd) j(Periodo)
	rename POS2 numpos 
	gen POSDummy=(numpos>0) if !mi(numpos)
	egen algunavezpos=max( POSDummy), by( icd)
	egen algunaveznopos=min( POSDummy), by( icd)
	sort  icd Periodo  
	bro if algunavezpos!=algunaveznopos
	egen primeravezPOS=min( Periodo) if POSDummy==1 & algunavezpos!=algunaveznopos, by( icd)
	egen ultimaveznoPOS=min( Periodo) if POSDummy==0 & algunavezpos!=algunaveznopos, by( icd)
	bys icd: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
	bys icd: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)
	gsort  icd -Periodo 
	bys icd: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
	bys icd: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)
	sort  icd Periodo 
	bro if primeravezPOS<ultimaveznoPOS
	save "$base_out/ICD_POS_`archivo'.dta", replace
	collapse (mean) primeravezPOS if primeravezPOS>ultimaveznoPOS, by(icd)
	replace icd=regexr(icd,"\.","") 
	save  "$base_out/AdicionesPOS_`archivo'.dta", replace


	import delim "$basein/ICD/tabla-cie-10.csv", clear varnames(1) 
	rename icd3 icd
	drop if icd==""
	keep icd icd3_descr sexo edad0 edad1
	collapse (firstnm) icd3_descr sexo edad0 edad1, by(icd)

	merge 1:1 icd using "$base_out/AdicionesPOS_`archivo'.dta"
	drop if _merge==1
	drop _merge
	save  "$base_out/AdicionesPOS_`archivo'.dta", replace
	export delim "$base_out/AdicionesPOS_`archivo'.csv", replace

	use "$base_out/Defunciones.dta", clear
	replace c_bas1=substr(c_bas1,1,$nivel)
	rename c_bas1 icd
	gen defuncion=1
	keep defuncion ano icd
	collapse (sum) defuncion, by(icd ano)
	merge 1:1 icd ano using "$base_out/RIPS.dta", keepus(enfermos atenciones)
	collapse (sum) defuncion enfermos atenciones, by(icd)
	merge 1:1 icd using "$base_out/AdicionesPOS_`archivo'.dta"
	drop if _merge==1
	drop _merge
	save  "$base_out/AdicionesPOS_`archivo'.dta", replace
	export delim "$base_out/AdicionesPOS_`archivo'.csv", replace
}

/*
use "$base_out/MedicamentosRegsLong.dta", clear
drop if YearExpedicion<=2007
drop if Periodo>2014
drop if Canal2==2
keep POS2 Periodo CUM1 ATC0 ATC1 ATC2 ATC3
reshape wide POS2, i(CUM1 ATC0 ATC1 ATC2 ATC3) j(Periodo)
collapse (sum) POS2*, by(ATC0)
rename ATC0 atc
merge 1:m atc using "$base_out/CrossoverACTICD.dta"
drop if _merge!=3
drop _merge
collapse (sum) POS2*, by(icd)
reshape long POS2, i(icd) j(Periodo)
rename POS2 numpos 
gen POSDummy=(numpos>0)
egen algunavezpos=max( POSDummy), by( icd)
egen algunaveznopos=min( POSDummy), by( icd)
sort  icd Periodo  
bro if algunavezpos!=algunaveznopos
egen primeravezPOS=min( Periodo) if POSDummy==1 & algunavezpos!=algunaveznopos, by( icd)
egen ultimaveznoPOS=min( Periodo) if POSDummy==0 & algunavezpos!=algunaveznopos, by( icd)
bys icd: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
bys icd: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)
gsort  icd -Periodo 
bys icd: replace primeravezPOS=primeravezPOS[_n-1] if mi(primeravezPOS)
bys icd: replace ultimaveznoPOS=ultimaveznoPOS[_n-1] if mi(ultimaveznoPOS)
sort  icd Periodo 
bro if primeravezPOS<ultimaveznoPOS
save "$base_out/ICD_POS_Sufi.dta", replace
*/


import delimited "$basein/EEVV/poblacion.csv", clear
reshape long v@, i(dpmp) j(ano)
replace ano=ano+1983
rename dpmp coddane
rename v poblacion
save "$base_out/poblacion.dta", replace

collapse (sum) poblacion, by(ano)
save "$base_out/poblacion_Fast.dta", replace



use "$base_out/ICD_POS_Sufi.dta", clear
keep if algunavezpos!=algunaveznopos
tab icd

use "$base_out/ICD_POS.dta", clear
replace icd=regexr(icd,"\.","") 
keep if algunavezpos!=algunaveznopos
tab icd
