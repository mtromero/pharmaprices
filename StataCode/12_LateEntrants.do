set more off
clear all
set maxvar 10000
set matsize 10000
use "$base_out/MedicamentosRegsLong.dta",clear
label var CompetidoresGrupo "Nº pharm. in therapeutical group"
label var CompetidoresATC "Nº pharm. in ATC group"
drop if Canal2==2
tsset CUM1 Periodo

encode ATC_Grupo0 , gen (ATC_Grupo)
encode ATC0 , gen (ATC)

label var NumPOSATC "Nº pharm. in ATC group in POS"
label var NumPOSGrupo "Nº pharm. in therapeutical in POS"
label var NumVMRATC "Nº pharm. in ATC group with RP"
label var NumVMRGrupo "Nº pharm. in therapeutical with RP"

compress
*xtset CUM1 Periodo, yearly



foreach var of varlist  Institucional_Prom_STD   Institucional_Unidades_STD {
	gen double Lg`var'=log(`var')
}

label var LgInstitucional_Prom_STD "Log average price for EPS and IPS"  
label var LgInstitucional_Unidades_STD "Log units for EPS and IPS"


label var POS "POS"
label var VMR "RP"
label var POS2 "POS"

label var VMR2 "RP"
label var VMR3 "RP"

*why did i had this here... no idea, but it makes no sense
*replace POSChange=1 if PrimerANOPOS==2007
replace PrimerANOPOS=. if POSChange==0
replace UltimoANOPOS=. if POSChange==0
gen TiempoPOS=Periodo-PrimerANOPOS
replace TiempoPOS=UltimoANOPOS-Periodo if UltimoANOPOS!=$ultimoano


gen TiempoVMR=Periodo-PrimerANOVMR
tabulate TiempoPOS, gen(TiempoPOSI)
tabulate TiempoVMR, gen(TiempoVMRI)


foreach var of varlist TiempoPOSI* TiempoVMRI*{
replace `var'=0 if `var'==.
}

sum TiempoPOS
gen TiempoPOS2=TiempoPOS-(r(min)-1)
replace TiempoPOS2=0 if TiempoPOS2==.
sum TiempoVMR
gen TiempoVMR2=TiempoVMR-(r(min)-1)
replace TiempoVMR2=0 if TiempoVMR2==.

gen TiempoPOS3=.
replace TiempoPOS3=1 if TiempoPOS<=-4 & !missing(TiempoPOS)
replace TiempoPOS3=2 if TiempoPOS==-3 & !missing(TiempoPOS)
replace TiempoPOS3=3 if TiempoPOS==-2 & !missing(TiempoPOS)
replace TiempoPOS3=4 if TiempoPOS==-1 & !missing(TiempoPOS)
replace TiempoPOS3=5 if TiempoPOS==0 & !missing(TiempoPOS)
replace TiempoPOS3=6 if TiempoPOS==1 & !missing(TiempoPOS)
replace TiempoPOS3=7 if TiempoPOS==2 & !missing(TiempoPOS)
replace TiempoPOS3=8 if TiempoPOS==3 & !missing(TiempoPOS)
replace TiempoPOS3=9 if TiempoPOS>=4 & !missing(TiempoPOS)
replace TiempoPOS3=0 if TiempoPOS3==.

sum TiempoPOS
scalar base1=-r(min)
fvset base `=scalar(base1)' TiempoPOS2
char TiempoPOS2[omit] 0
fvset base 4 TiempoPOS3
char TiempoPOS3[omit] 0
sum TiempoVMR
scalar base2=-r(min)
fvset base `=scalar(base2)' TiempoVMR2
char TiempoVMR2[omit] 0
 gen LgAnosExpedicion=log(AnosExpedicion)
by CUM1: gen CambioNumPOSATC=(D.NumPOSATC>0)
by CUM1: gen CambioNumPOSATC2=(D.NumPOSATC<0)
gen LgNumPOSATC=log(NumPOSATC)
gen LgNumPOSGrupo=log(NumPOSGrupo)
 gen DGenericosATC=(GenericosATC>0) if !missing(GenericosATC)
gen LgCompetidoresATC=log(CompetidoresATC)
gen LgCompetidoresATCPOS=NumPOSATC
replace LgCompetidoresATCPOS=NumPOSATC-1 if POS==1
gen LgCompetidoresATCNOPOS=(CompetidoresATC-1)-NumPOSATC if POS==0
replace LgCompetidoresATCNOPOS=(CompetidoresATC-1)-NumPOSATC if POS==1

gen a=NumPOSATC+LgCompetidoresATCNOPOS

gen missing_info=missing(Institucional_Prom_STD)
label var missing_info "Missing price/quantity information"
drop if POS2==.

global control_basico VMR2 DGenericosATC HHATCMax
global control_dinamico VMR2 L.VMR2  L.L.VMR2  DGenericosATC L.DGenericosATC  L.L.DGenericosATC  HHATCMax L.HHATCMax L.L.HHATCMax

/*
reghdfe HHATCMax POS2  if  (LateEntrants==1 | POSChange==0) & Institucional_Count==$panelcompelto , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
reghdfe DGenericosATC POS2  if  (LateEntrants==1 | POSChange==0) & Institucional_Count==$panelcompelto , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
reghdfe LgCompetidoresATC POS2  if  (LateEntrants==1 | POSChange==0)  & Institucional_Count==$panelcompelto, absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
reghdfe VMR2 POS2  if  (LateEntrants==1 | POSChange==0) & Institucional_Count==$panelcompelto , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
*/

eststo clear
foreach channel in   2{
	foreach tipo in   DGenericosATC LgCompetidoresATC VMR2 {
		foreach var in  Institucional {

			if "`tipo'"=="DGenericosATC" local name "Generic"
			if "`tipo'"=="LgCompetidoresATC" local name "Competitors"
			if "`tipo'"=="HHATCMax" local name "HH-Index"
			if "`tipo'"=="VMR2" local name "Price Regulations"
			

			if "`tipo'"=="DGenericosATC" local name2 "generic subsitutes"
			if "`tipo'"=="LgCompetidoresATC" local name2 "the number of competitors"
			if "`tipo'"=="HHATCMax" local name2 "HH-Index"
			if "`tipo'"=="VMR2" local name2 "price regulations"
			
			eststo `tipo'_normal: reghdfe `tipo' POS2, absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd scalar NumMolecules=e(K1)
			
			preserve
			replace TiempoPOS3=0 if LateEntrants==0
			replace POS2=0 if LateEntrants==0
			reghdfe `tipo' i.TiempoPOS3    , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on `name'") title("Evolution of `name2' before and after" "a drug is included in the benefit plan") 
			graph export "$graphs/LateEntrants_`tipo'_`channel'_longrun.pdf", replace 
			
			reghdfe `tipo' i.TiempoPOS3   if Institucional_Count==$panelcompelto, absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on `name'") title("Evolution of `name2' before and after" "a drug is included in the benefit plan") 
			graph export "$graphs/LateEntrants_`tipo'_`channel'_completo_longrun.pdf", replace 
			eststo `tipo'_late: reghdfe `tipo' POS2, absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd scalar NumMolecules=e(K1)
			restore
			
			preserve
			replace TiempoPOS3=0 if LateEntrants==1
			replace POS2=0 if LateEntrants==1
			reghdfe `tipo' i.TiempoPOS3   , absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on `name'") title("Evolution of `name2' before and after" "a drug is included in the benefit plan") 
			graph export "$graphs/EarlyEntrants_`tipo'_`channel'_longrun.pdf", replace 
			
			reghdfe `tipo' i.TiempoPOS3    if  Institucional_Count==$panelcompelto, absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
			9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on `name'") title("Evolution of `name2' before and after" "a drug is included in the benefit plan") 
			graph export "$graphs/EarlyEntrants_`tipo'_`channel'_completo_longrun.pdf", replace 
			eststo `tipo'_early: reghdfe `tipo' POS2, absorb(CUM1 Periodo) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd scalar NumMolecules=e(K1)
			restore
		}
	}
}

estout DGenericosATC_normal LgCompetidoresATC_normal VMR2_normal using "$latexcodes/Timing_Completo.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
replace keep(POS2) ///
stats(N N_clust NumMolecules, fmt(a2 a2 a2 ) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs"))

estout DGenericosATC_early LgCompetidoresATC_early VMR2_early using "$latexcodes/Timing_Early.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
replace keep(POS2) ///
stats(N N_clust NumMolecules, fmt(a2 a2 a2 ) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs"))

estout DGenericosATC_late LgCompetidoresATC_late VMR2_late using "$latexcodes/Timing_Late.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
replace keep(POS2) ///
stats(N N_clust NumMolecules, fmt(a2 a2 a2 ) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs"))


preserve
replace POS2=0 if LateEntrants==1
foreach channel in   2{
	foreach tipo in  Prom_STD Unidades_STD {
		foreach var in  Institucional{
			eststo clear
			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico  , absorb(Periodo CUM1) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "No"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "Yes"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "No"
			estadd scalar NumMolecules=e(N_clust)

			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico    if  `var'_Count==$panelcompelto , absorb(Periodo CUM1) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "Yes"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "No"
			estadd scalar NumMolecules=e(N_clust)

			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico   if `var'_Count==$panelcompelto , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(N_clust)


			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico   if `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(N_clust)
			
			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico   if `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , absorb(CUM1 Periodo#ATC)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "Yes"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(N_clust)


			eststo: reghdfe Lg`var'_`tipo' POS2 $control_dinamico if  `var'_Count==$panelcompelto & VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(N_clust)
			


			 estout using "$latexcodes/EarlyEntrants_`tipo'_`var'_`channel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
			 /*rename(D.POS2 POS2)*/ ///
			 replace keep(POS2) ///
			 stats(N N_clust NumMolecules Molecule ATC Year Controls LagControls  BalancedPanel MoleculeTrends Outliers, fmt(a2 a2 a2 %~#s %~#s %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "ATC x Year F.E." "Year F.E." "Controls" "Lagged Controls" "Balanced panel?" "Molecule trends?" "Outliers excluded?"))


		}
	}
}
restore

preserve
replace POS2=0 if LateEntrants==0
foreach channel in   2{
	foreach tipo in  Prom_STD Unidades_STD {
		foreach var in  Institucional{
			eststo clear
			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico  , absorb(Periodo CUM1) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "No"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "Yes"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "No"
			estadd scalar NumMolecules=e(N_clust)

			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico    if  `var'_Count==$panelcompelto , absorb(Periodo CUM1) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "Yes"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "No"
			estadd scalar NumMolecules=e(N_clust)

			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico   if `var'_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "No"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(N_clust)


			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico   if `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(N_clust)
			
			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico   if `var'_Count==$panelcompelto &  VariacionIncreasePrice30_==0 , absorb(CUM1 Periodo#ATC)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "Yes"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "No"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(N_clust)


			eststo: reghdfe Lg`var'_`tipo' POS2 $control_dinamico if  `var'_Count==$panelcompelto & VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local BalancedPanel "Yes"
			estadd local Outliers "Yes"
			estadd local Molecule "Yes"
			estadd local ATC "No"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local LagControls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(N_clust)
			



			 estout using "$latexcodes/LateEntrants_`tipo'_`var'_`channel'.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
			 /*rename(D.POS2 POS2)*/ ///
			 replace keep(POS2) ///
			 stats(N N_clust NumMolecules Molecule ATC Year Controls LagControls  BalancedPanel MoleculeTrends Outliers, fmt(a2 a2 a2 %~#s %~#s %~#s %~#s %~#s %~#s) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drug F.E." "ATC x Year F.E." "Year F.E." "Controls" "Lagged Controls" "Balanced panel?" "Molecule trends?" "Outliers excluded?"))


		}
	}
}
restore
