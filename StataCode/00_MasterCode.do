clear all
set seed 12345
*My two pc's
if "`c(username)'" == "Mauricio"  {
	global mipath "C:/Users/Mauricio/Dropbox/Research/Health/Medicamentos/Programa"
	global dir_do     "C:/Users/Mauricio/Documents/git/pharmaprices/StataCode"
}

if "`c(username)'" == "mauri"  {
	global mipath "C:/Users/mauri/Dropbox/Research/Health/Medicamentos/Programa"
	global dir_do     "C:/Users/mauri/Documents/git/pharmaprices/StataCode"
}
if "`c(username)'" == "mauricio"  {
	global mipath "/media/mauricio/TeraHDD2/Dropbox/Research/Health/Medicamentos/Programa"
	global dir_do     "/media/mauricio/TeraHDD2/git/pharmaprices/StataCode"
}


  global ultimoano=2016
  global panelcompelto=10
* Path Tree
  global basein 	"$mipath/RawData"
  global base_out   "$mipath/CreatedData"
  global results    "$mipath/Results"
  global exceltables  "$mipath/Results/ExcelTables"
  global graphs     "$mipath/Results/figures"
  global latexcodes     "$mipath/Results/latex"
	

*This is somewhat tricky... you should rund 01_Invima first and then run the two R codes: SISMED AND VMR, then come back and do the rest of the master
*do "$dir_do/01_ReadInvima"
*do "$dir_do/02_ReadPOSDATA"	
do "$dir_do/03_ReadDataMeds"	
do "$dir_do/04_DescriptiveStats"
do "$dir_do/05_MainRegs"
do "$dir_do/06_EventStudies"
do "$dir_do/07_Robustness_Hetero"
do "$dir_do/11_ATCLevel"
do "$dir_do/12_LateEntrants"

/*
do "$dir_do/EEVV/01_ReadData"
do "$dir_do/EEVV/02A_RegsFast"	
do "$dir_do/EEVV/02A_RegsFast_RIPS"

/*
do "$dir_do/EEVV/02_FirstRegs"	

