set more off
clear all
set maxvar 10000
set matsize 10000
use "$base_out/MedicamentosRegsLong.dta",clear
drop if Canal2==2

label var CompetidoresGrupo "N pharm. in therapeutical group"
label var CompetidoresATC "N pharm. in ATC group"



encode ATC_Grupo0 , gen (ATC_Grupo)
label var NumPOSATC "No. drugs in ATC group in POS"
label var NumPOSGrupo "No. drugs in therapeutical in POS"
label var NumVMRATC "No. drugs in ATC group with RP"
label var NumVMRGrupo "No. drugs in therapeutical with RP"

compress
xtset CUM1 Periodo, yearly



foreach var of varlist  Institucional_Prom_STD   Institucional_Unidades_STD {
	gen double Lg`var'=log(`var')
}

label var LgInstitucional_Prom_STD "Log(P)"  
label var LgInstitucional_Unidades_STD "Log(Q)"


label var POS "POS"
label var VMR "RP"
label var POS2 "POS"

label var VMR2 "RP"
label var VMR3 "RP"

gen AtLeastOnePOSATC=(NumPOSATC>=1) & !missing(NumPOSATC)
gen AtLeastOnePOSGrupo=(NumPOSGrupo >=1) & !missing(NumPOSGrupo )
gen MoreThanOnePOSATC=(NumPOSATC>1) & !missing(NumPOSATC)
gen MoreThanOnePOSGrupo=(NumPOSGrupo>1) & !missing(NumPOSGrupo )
gen OnePOSATC=(NumPOSATC==1) & !missing(NumPOSATC)
gen OnePOSGrupo=(NumPOSGrupo==1) & !missing(NumPOSGrupo )
gen LgNumPOSATC=log(NumPOSATC)
gen LgNumPOSGrupo=log(NumPOSGrupo)
gen DGenericosATC=(GenericosATC>0) if !missing(GenericosATC)
gen LgCompetidoresATC=log(CompetidoresATC)
gen Monopoly=(CompetidoresATC==1) & !missing(CompetidoresATC)
gen Duopoly=(CompetidoresATC==2) & !missing(CompetidoresATC)
gen Threepoly=(CompetidoresATC==3) & !missing(CompetidoresATC)
gen Morepoly=(CompetidoresATC>=4) & !missing(CompetidoresATC)
label var Threepoly "Tripoly"
label var Morepoly "4 or more competitors"

sort CUM1 Periodo
gen DGenericos_2007= DGenericosATC if Periodo==2007
by CUM1: replace DGenericos_2007=DGenericos_2007[_n-1] if DGenericos_2007==.
gen CompetidoresATC_2007= CompetidoresATC if Periodo==2007
by CUM1: replace CompetidoresATC_2007=CompetidoresATC_2007[_n-1] if CompetidoresATC_2007==.

gen CompetidoresATC_2007_recode=CompetidoresATC_2007
recode CompetidoresATC_2007_recode (10/1000=10)


bys CUM1: egen DGenericos_Ever=max(DGenericosATC) 



bys CUM1: egen minCompetidoresATC=min(CompetidoresATC)
bys CUM1: egen maxCompetidoresATC=max(CompetidoresATC) 

global control_basico VMR2 DGenericosATC HHATCMax
global control_basico_c c.VMR2 c.DGenericosATC c.HHATCMax
global control_basico2 VMR2  HHATCMax
global control_dinamico VMR2 L.VMR2  L.L.VMR2  DGenericosATC L.DGenericosATC  L.L.DGenericosATC  HHATCMax L.HHATCMax L.L.HHATCMax

*reghdfe LgInstitucional_Prom_STD OTC POS2 c.POS2#c.OTC c.(${control_basico})#c.OTC  ${control_basico}  if Institucional_Count==$panelcompelto  & VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo#OTC)  vce(cluster ATC_Grupo)
		
*& CUM1!=229857	
*Unidades_STD
/*
reghdfe LgInstitucional_Prom_STD POS2 $control_basico   if ImportarModalidad==0 & Institucional_Count==$panelcompelto  &  VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo) resid(resid)
 collapse resid PrimerANOPOS PrimerANOVMR maxCompetidoresATC  minCompetidoresATC (firstnm) ATC0 PRINCIPIO_ACTIVO0 DESCRIPCION_ATC0 , by(POS2 CUM1)
 drop if POS2==.
 reshape wide resid , i(CUM1) j(POS2)
 gen diff_precio=resid1-resid0
drop if diff_precio==.
*/


preserve
reghdfe LgInstitucional_Prom_STD c.POS2#i.CompetidoresATC_2007_recode c.(${control_basico})##(c.CompetidoresATC_2007_recode)   if Institucional_Count==$panelcompelto   , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.POS2) ci rename(1.CompetidoresATC_2007_recode#c.POS2="1" 2.CompetidoresATC_2007_recode#c.POS2="2" ///
3.CompetidoresATC_2007_recode#c.POS2="3" 4.CompetidoresATC_2007_recode#c.POS2="4" 5.CompetidoresATC_2007_recode#c.POS2="5" 6.CompetidoresATC_2007_recode#c.POS2="6" ///
7.CompetidoresATC_2007_recode#c.POS2="7" 8.CompetidoresATC_2007_recode#c.POS2="8" 9.CompetidoresATC_2007_recode#c.POS2="9" 10.CompetidoresATC_2007_recode#c.POS2=">=10" )   vertical yline(0) xtitle("Number of competitors") ytitle("Price effect") title("Heterogeneity in the price effect") 
graph export "$graphs/HeteroPrice_ByCompetencia2007_Completo.pdf", replace 

reghdfe LgInstitucional_Prom_STD c.POS2#i.CompetidoresATC_2007_recode c.(${control_basico})##(c.CompetidoresATC_2007_recode)   , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.POS2) ci rename(1.CompetidoresATC_2007_recode#c.POS2="1" 2.CompetidoresATC_2007_recode#c.POS2="2" ///
3.CompetidoresATC_2007_recode#c.POS2="3" 4.CompetidoresATC_2007_recode#c.POS2="4" 5.CompetidoresATC_2007_recode#c.POS2="5" 6.CompetidoresATC_2007_recode#c.POS2="6" ///
7.CompetidoresATC_2007_recode#c.POS2="7" 8.CompetidoresATC_2007_recode#c.POS2="8" 9.CompetidoresATC_2007_recode#c.POS2="9" 10.CompetidoresATC_2007_recode#c.POS2=">=10" )   vertical yline(0) xtitle("Number of competitors") ytitle("Price effect") title("Heterogeneity in the price effect") 
graph export "$graphs/HeteroPrice_ByCompetencia2007.pdf", replace 


reghdfe LgInstitucional_Unidades_STD c.POS2#i.CompetidoresATC_2007_recode c.(${control_basico})##(c.CompetidoresATC_2007_recode)   if Institucional_Count==$panelcompelto   , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.POS2) ci rename(1.CompetidoresATC_2007_recode#c.POS2="1" 2.CompetidoresATC_2007_recode#c.POS2="2" ///
3.CompetidoresATC_2007_recode#c.POS2="3" 4.CompetidoresATC_2007_recode#c.POS2="4" 5.CompetidoresATC_2007_recode#c.POS2="5" 6.CompetidoresATC_2007_recode#c.POS2="6" ///
7.CompetidoresATC_2007_recode#c.POS2="7" 8.CompetidoresATC_2007_recode#c.POS2="8" 9.CompetidoresATC_2007_recode#c.POS2="9" 10.CompetidoresATC_2007_recode#c.POS2=">=10" )   vertical yline(0) xtitle("Number of competitors") ytitle("Price effect") title("Heterogeneity in the price effect") 
graph export "$graphs/HeteroQuant_ByCompetencia2007_Completo.pdf", replace 

reghdfe LgInstitucional_Unidades_STD c.POS2#i.CompetidoresATC_2007_recode c.(${control_basico})##(c.CompetidoresATC_2007_recode)   , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.POS2) ci rename(1.CompetidoresATC_2007_recode#c.POS2="1" 2.CompetidoresATC_2007_recode#c.POS2="2" ///
3.CompetidoresATC_2007_recode#c.POS2="3" 4.CompetidoresATC_2007_recode#c.POS2="4" 5.CompetidoresATC_2007_recode#c.POS2="5" 6.CompetidoresATC_2007_recode#c.POS2="6" ///
7.CompetidoresATC_2007_recode#c.POS2="7" 8.CompetidoresATC_2007_recode#c.POS2="8" 9.CompetidoresATC_2007_recode#c.POS2="9" 10.CompetidoresATC_2007_recode#c.POS2=">=10" )   vertical yline(0) xtitle("Number of competitors") ytitle("Price effect") title("Heterogeneity in the price effect") 
graph export "$graphs/HeteroQuant_ByCompetencia2007.pdf", replace 
restore


preserve
recode CompetidoresATC (10/1000=10)
reghdfe LgInstitucional_Prom_STD c.POS2#i.CompetidoresATC c.(${control_basico})##(c.CompetidoresATC)  if Institucional_Count==$panelcompelto   , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.POS2) ci rename(1.CompetidoresATC#c.POS2="1" 2.CompetidoresATC#c.POS2="2" ///
3.CompetidoresATC#c.POS2="3" 4.CompetidoresATC#c.POS2="4" 5.CompetidoresATC#c.POS2="5" 6.CompetidoresATC#c.POS2="6" ///
7.CompetidoresATC#c.POS2="7" 8.CompetidoresATC#c.POS2="8" 9.CompetidoresATC#c.POS2="9" 10.CompetidoresATC#c.POS2=">=10" )   vertical yline(0) xtitle("Number of competitors") ///
ytitle("Price effect") yla(, ang(h) nogrid)  /*title("Heterogeneity in the price effect") */
graph export "$graphs/HeteroPrice_ByCompetencia_Completo.pdf", replace 

reghdfe LgInstitucional_Prom_STD c.POS2#i.CompetidoresATC c.(${control_basico})##(c.CompetidoresATC)   , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.POS2) ci rename(1.CompetidoresATC#c.POS2="1" 2.CompetidoresATC#c.POS2="2" ///
3.CompetidoresATC#c.POS2="3" 4.CompetidoresATC#c.POS2="4" 5.CompetidoresATC#c.POS2="5" 6.CompetidoresATC#c.POS2="6" ///
7.CompetidoresATC#c.POS2="7" 8.CompetidoresATC#c.POS2="8" 9.CompetidoresATC#c.POS2="9" 10.CompetidoresATC#c.POS2=">=10" )   vertical yline(0) xtitle("Number of competitors") ytitle("Price effect") title("Heterogeneity in the price effect") 
graph export "$graphs/HeteroPrice_ByCompetencia.pdf", replace 


reghdfe LgInstitucional_Unidades_STD c.POS2#i.CompetidoresATC c.(${control_basico})##(c.CompetidoresATC)   if Institucional_Count==$panelcompelto , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.POS2) ci rename(1.CompetidoresATC#c.POS2="1" 2.CompetidoresATC#c.POS2="2" ///
3.CompetidoresATC#c.POS2="3" 4.CompetidoresATC#c.POS2="4" 5.CompetidoresATC#c.POS2="5" 6.CompetidoresATC#c.POS2="6" ///
7.CompetidoresATC#c.POS2="7" 8.CompetidoresATC#c.POS2="8" 9.CompetidoresATC#c.POS2="9" 10.CompetidoresATC#c.POS2=">=10" )   vertical yline(0) xtitle("Number of competitors") ytitle("Price effect") title("Heterogeneity in the price effect") 
graph export "$graphs/HeteroQuant_ByCompetencia_Completo.pdf", replace 

reghdfe LgInstitucional_Unidades_STD c.POS2#i.CompetidoresATC c.(${control_basico})##(c.CompetidoresATC)  , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
coefplot, graphregion(color(white)) baselevels keep(*.POS2) ci rename(1.CompetidoresATC#c.POS2="1" 2.CompetidoresATC#c.POS2="2" ///
3.CompetidoresATC#c.POS2="3" 4.CompetidoresATC#c.POS2="4" 5.CompetidoresATC#c.POS2="5" 6.CompetidoresATC#c.POS2="6" ///
7.CompetidoresATC#c.POS2="7" 8.CompetidoresATC#c.POS2="8" 9.CompetidoresATC#c.POS2="9" 10.CompetidoresATC#c.POS2=">=10" )   vertical yline(0) xtitle("Number of competitors") ytitle("Price effect") title("Heterogeneity in the price effect") 
graph export "$graphs/HeteroQuant_ByCompetencia.pdf", replace 
restore






foreach channel in   2{
	foreach tipo in  Prom_STD Unidades_STD{
		foreach var in  Institucional{
			eststo clear

			eststo clear
			eststo: reghdfe Lg`var'_`tipo' c.POS2 $control_basico  if `var'_Count==$panelcompelto  &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local Products "All"
			estadd local BalancePanel "Yes"
			estadd local Molecule "Yes"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)

			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico   if   OTC==0 & `var'_Count==$panelcompelto  &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local Products "rX"
			estadd local BalancePanel "Yes"
			estadd local Molecule "Yes"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)

			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico   if    OTC==1 & `var'_Count==$panelcompelto  &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo) 
			estadd ysumm
			estadd local Products "OTC"
			estadd local BalancePanel "Yes"
			estadd local Molecule "Yes"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)

			eststo: reghdfe Lg`var'_`tipo' OTC POS2 c.POS2#c.OTC c.(${control_basico})#c.OTC  ${control_basico}  if `var'_Count==$panelcompelto  & VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo#OTC)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local Products "All"
			estadd local BalancePanel "Yes"
			estadd local Molecule "Yes"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)

			 estout using "$latexcodes/RegRobustVariosOTC`tipo'_`var'_`channel'_trends.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
			 replace keep(POS2 c.POS2*) ///
			 stats(N N_clust NumMolecules Products, fmt(a2 a2 a2  %~#s ) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drugs"))

		}
	}
}




foreach channel in   2{
	foreach tipo in  Prom_STD Unidades_STD{
		foreach var in  Institucional{
			eststo clear

			eststo clear
			eststo: reghdfe Lg`var'_`tipo' c.POS2 $control_basico  if `var'_Count==$panelcompelto  & VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local Products "All"
			estadd local BalancePanel "Yes"
			estadd local Molecule "Yes"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)

			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico  if `var'_Count==$panelcompelto  &  ImportarModalidad==0 &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local Products "Locally produced"
			estadd local BalancePanel "Yes"
			estadd local Molecule "Yes"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)

			eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico  if `var'_Count==$panelcompelto  &   ImportarModalidad==1 &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local Products "Imported"
			estadd local BalancePanel "Yes"
			estadd local Molecule "Yes"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)

			label var ImportarModalidad Imported
			eststo: reghdfe Lg`var'_`tipo' POS2 ImportarModalidad c.POS2#c.ImportarModalidad c.(${control_basico})#c.ImportarModalidad ${control_basico} if `var'_Count==$panelcompelto  &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo#ImportarModalidad)  vce(cluster ATC_Grupo)
			estadd ysumm
			estadd local Products "All"
			estadd local BalancePanel "Yes"
			estadd local Molecule "Yes"
			estadd local Year "No"
			estadd local Controls "Yes"
			estadd local MoleculeTrends "Yes"
			estadd scalar NumMolecules=e(K1)


			 estout using "$latexcodes/RegRobustVariosImported`tipo'_`var'_`channel'_trends.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
			 replace keep(POS2 c.POS2*) ///
			 stats(N N_clust NumMolecules Products, fmt(a2 a2 a2 %~#s ) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drugs"))
		}
	}
}


by CUM1: egen AnosExpedicion_2007=min(AnosExpedicion) 
gen AnosExpedicionMas7_2007=(AnosExpedicion_2007>=7) if !missing(AnosExpedicion_2007)
gen AnosExpedicionMas5_2007=(AnosExpedicion_2007>=5) if !missing(AnosExpedicion_2007)
gen AnosExpedicionMas10_2007=(AnosExpedicion_2007>=10) if !missing(AnosExpedicion_2007)


foreach edad in   5 7 10{
	foreach channel in   2{
		foreach tipo in  Prom_STD Unidades_STD{
			foreach var in  Institucional{
				eststo clear

				eststo clear
				eststo: reghdfe Lg`var'_`tipo' c.POS2 $control_basico  if  `var'_Count==$panelcompelto  &  VariacionIncreasePrice30_==0  , absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
				estadd ysumm
				estadd local Products "All"
				estadd local BalancePanel "Yes"
				estadd local Molecule "Yes"
				estadd local Year "No"
				estadd local Controls "Yes"
				estadd local MoleculeTrends "Yes"
				estadd scalar NumMolecules=e(K1)

				eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico  if `var'_Count==$panelcompelto  & AnosExpedicionMas`edad'_2007==0 &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				estadd ysumm
				estadd local Products "\$<`edad'\$ years in 2007"
				estadd local BalancePanel "Yes"
				estadd local Molecule "Yes"
				estadd local Year "No"
				estadd local Controls "Yes"
				estadd local MoleculeTrends "Yes"
				estadd scalar NumMolecules=e(K1)

				eststo: reghdfe Lg`var'_`tipo' POS2 $control_basico  if `var'_Count==$panelcompelto  & AnosExpedicionMas`edad'_2007==1 &  VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)
				estadd ysumm
				estadd local Products "\$\geq `edad'\$ years in 2007"
				estadd local BalancePanel "Yes"
				estadd local Molecule "Yes"
				estadd local Year "No"
				estadd local Controls "Yes"
				estadd local MoleculeTrends "Yes"
				estadd scalar NumMolecules=e(K1)

				label var AnosExpedicionMas`edad'_2007 "\$\geq `edad'\$ years in 2007"

				eststo: reghdfe Lg`var'_`tipo' AnosExpedicionMas`edad'_2007 POS2 c.POS2#c.AnosExpedicionMas`edad'_2007 c.(${control_basico})#c.AnosExpedicionMas`edad'_2007 ${control_basico}  if `var'_Count==$panelcompelto  & VariacionIncreasePrice30_==0 , absorb(CUM1 c.Periodo#ATC_Grupo#AnosExpedicionMas`edad'_2007)  vce(cluster ATC_Grupo)
				estadd ysumm
				estadd local Products "All"
				estadd local BalancePanel "Yes"
				estadd local Molecule "Yes"
				estadd local Year "No"
				estadd local Controls "Yes"
				estadd local MoleculeTrends "Yes"
				estadd scalar NumMolecules=e(K1)

				 estout using "$latexcodes/RegRobustVariosAge`edad'_`tipo'_`var'_`channel'_trends.tex", style(tex) starl(* 0.10 ** 0.05 *** 0.01) label cells(b(star fmt(a2)) se(par fmt(a2))) mlabels(none) collabels(none) ///
				 replace keep(POS2 c.POS2*) ///
				 stats(N N_clust NumMolecules  Products, fmt(a2 a2 a2 %~#s ) labels ("N. of obs." "N. of clusters (pharmacological subgroups)" "N. of drugs" "Drugs"))


			}
		}
	}
}



