set more off
clear all
set matsize 800
use "$base_out/MedicamentosRegsLong.dta",clear
bys Periodo: egen ValorTotalAno=sum(Institucional_Valor)
gsort +Periodo -Institucional_Valor
bys Periodo: gen Frac_ValorTotalAno=sum(Institucional_Valor)/ValorTotalAno
gen Keep=(Frac_ValorTotalAno<0.95) if !missing(Frac_ValorTotalAno)


label var Generico "Generic"
label var FranjaVerde "Essential Medicine"
label var FranjaVioleta "Controlled Substance"
label var Inserto "Insert"
label var OTC "OTC"
label var Presentations "No. of presentations"

label var CompetidoresGrupo "No. pharm. in therapeutical group"
label var CompetidoresATC "No. pharm. in ATC group"

drop if Canal2==2

encode ATC_Grupo0 , gen (ATC_Grupo)


label var NumPOSATC "No. drugs in ATC group in POS"
label var NumPOSGrupo "No. drugs in therapeutical in POS"
label var NumVMRATC "No. drugs in ATC group with RP"
label var NumVMRGrupo "No. drugs in pharmacological subgroup with RP"
gen DGenericosATC=(GenericosATC>0) if !missing(GenericosATC)
gen DGenericosGrupo=(GenericosGrupo>0) if !missing(GenericosGrupo)
gen LgCompetidoresATC=log(CompetidoresATC)
gen LgCompetidoresGrupo=log(CompetidoresGrupo)
gen LgNumPOSATC=log(NumPOSATC)
gen LgNumPOSGrupo=log(NumPOSGrupo)
replace LgNumPOSATC=0 if LgNumPOSATC==.
replace LgNumPOSGrupo=0 if LgNumPOSGrupo==.
label var LgNumPOSATC "log(No. drugs in ATC group in POS)"
label var LgNumPOSGrupo "log(No. drugs in pharmacological subgroup in POS)"

gen PropCompetidoresPOSATC=NumPOSATC/CompetidoresATC 
gen PropCompetidoresPOSGrupo=NumPOSGrupo/CompetidoresGrupo 
gen OnePOSATC=(NumPOSATC==1) & !missing(NumPOSATC)
gen OnePOSGrupo=(NumPOSGrupo==1) & !missing(NumPOSGrupo )


compress
xtset CUM1 Periodo, yearly
gen WeightShare2007=Institucional_Valor/ValorTotalAno if Periodo==2007
replace WeightShare2007=L.WeightShare2007 if WeightShare2007==.

gen missing_info=missing(Institucional_Prom_STD)
label var missing_info "Missing price/quantity information"



foreach var of varlist  Institucional_Prom_STD   Institucional_Unidades_STD {
	gen double Lg`var'=log(`var')
}

label var LgInstitucional_Prom_STD "Log average price for EPS and IPS"  
label var LgInstitucional_Unidades_STD "Log units for EPS and IPS"


label var POS "POS"
label var VMR "RP"
label var POS2 "POS"

label var VMR2 "RP"
label var VMR3 "RP"

replace POSChange=1 if PrimerANOPOS==2007
replace PrimerANOPOS=. if POSChange==0
replace UltimoANOPOS=. if POSChange==0
gen TiempoPOS=Periodo-PrimerANOPOS
replace TiempoPOS=UltimoANOPOS-Periodo if UltimoANOPOS!=$ultimoano


gen TiempoVMR=Periodo-PrimerANOVMR
tabulate TiempoPOS, gen(TiempoPOSI)
tabulate TiempoVMR, gen(TiempoVMRI)


foreach var of varlist TiempoPOSI* TiempoVMRI*{
replace `var'=0 if `var'==.
}

sum TiempoPOS
gen TiempoPOS2=TiempoPOS-(r(min)-1)
replace TiempoPOS2=0 if TiempoPOS2==.
sum TiempoVMR
gen TiempoVMR2=TiempoVMR-(r(min)-1)
replace TiempoVMR2=0 if TiempoVMR2==.

gen TiempoPOS3=.
replace TiempoPOS3=1 if TiempoPOS<=-4 & !missing(TiempoPOS)
replace TiempoPOS3=2 if TiempoPOS==-3 & !missing(TiempoPOS)
replace TiempoPOS3=3 if TiempoPOS==-2 & !missing(TiempoPOS)
replace TiempoPOS3=4 if TiempoPOS==-1 & !missing(TiempoPOS)
replace TiempoPOS3=5 if TiempoPOS==0 & !missing(TiempoPOS)
replace TiempoPOS3=6 if TiempoPOS==1 & !missing(TiempoPOS)
replace TiempoPOS3=7 if TiempoPOS==2 & !missing(TiempoPOS)
replace TiempoPOS3=8 if TiempoPOS==3 & !missing(TiempoPOS)
replace TiempoPOS3=9 if TiempoPOS>=4 & !missing(TiempoPOS)
replace TiempoPOS3=0 if TiempoPOS3==.

sum TiempoPOS
scalar base1=-r(min)
fvset base `=scalar(base1)' TiempoPOS2
char TiempoPOS2[omit] 0
fvset base 4 TiempoPOS3
char TiempoPOS3[omit] 0
sum TiempoVMR
scalar base2=-r(min)
fvset base `=scalar(base2)' TiempoVMR2
char TiempoVMR2[omit] 0
 
drop if POS2==.

global control_basico VMR2 DGenericosATC HHATCMax
global control_dinamico VMR2 L.VMR2  L.L.VMR2  DGenericosATC L.DGenericosATC  L.L.DGenericosATC  HHATCMax L.HHATCMax L.L.HHATCMax



keep if VariacionIncreasePrice30_==0 & Institucional_Count==$panelcompelto
bys CUM1: egen MeanPrice=mean(Institucional_Prom_STD) 
replace Institucional_Prom_STD=Institucional_Prom_STD-MeanPrice
gen Price2007=Institucional_Prom_STD if Periodo==2007
bys CUM1: replace Price2007=Price2007[_n-1] if Price2007==.
gen RelativePrice=Institucional_Prom_STD/Price2007

drop if UltimoANOPOS<PrimerANOPOS & !missing(PrimerANOPOS) & !missing(UltimoANOPOS)
reghdfe RelativePrice i.TiempoPOS3 $control_basico if VariacionIncreasePrice30_==0 & Institucional_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) resid(resid_price)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on `name'") title("Evolution of a drug's `name2' before and after" "it is included in the benefit plan") 

collapse (mean) RelativePrice, by(PrimerANOPOS Periodo)


twoway  (tsline RelativePrice if PrimerANOPOS==2007) ///
  (tsline RelativePrice if PrimerANOPOS==2012) (tsline RelativePrice if PrimerANOPOS==2014) ///
 (tsline RelativePrice if PrimerANOPOS==.), ///
legend(order(1 "Always" 2 "2012"  3 "2014" 4 "Never"))

twoway  (tsline RelativePrice if PrimerANOPOS==2012) (tsline RelativePrice if PrimerANOPOS==2014) ///
 (tsline RelativePrice if PrimerANOPOS==.), ///
legend(order(1 "2012"  2 "2014" 3 "Never"))

twoway  (tsline RelativePrice if PrimerANOPOS==2007) (tsline RelativePrice if PrimerANOPOS==2008) (tsline RelativePrice if PrimerANOPOS==2010) ///
(tsline RelativePrice if PrimerANOPOS==2011)  (tsline RelativePrice if PrimerANOPOS==2012) (tsline RelativePrice if PrimerANOPOS==2014) ///
(tsline RelativePrice if PrimerANOPOS==2015)  (tsline RelativePrice if PrimerANOPOS==2016)  (tsline RelativePrice if PrimerANOPOS==.), ///
legend(order(1 "Always" 2 "2008" 3 "2010" 4 "2011" 5 "2012"  6 "2014" 7 "2015" 8 "2016" 9 "Never"))


*reghdfe DGenericosATC POS2 if VariacionIncreasePrice30_==0, absorb(Periodo CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) 
*reghdfe DGenericosATC POS2 if Institucional_Count==$panelcompelto , absorb(Periodo CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) 
*reghdfe DGenericosATC POS2 if VariacionIncreasePrice30_==0 & Institucional_Count==$panelcompelto , absorb(Periodo CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) 

*reghdfe LgInstitucional_Prom_STD POS2 $control_basico if VariacionIncreasePrice30_==0 & Institucional_Count==$panelcompelto, absorb(Periodo CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
*reghdfe LgInstitucional_Prom_STD POS2 $control_basico if VariacionIncreasePrice30_==0 & Institucional_Count>=8, absorb(Periodo CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo)
*reghdfe LgInstitucional_Prom_STD c.POS2#CompetidoresATC_2007_recode  if Institucional_Count==$panelcompelto  &   VariacionIncreasePrice30_==0 , maxiter(100000) absorb(CUM1 c.Periodo#ATC_Grupo)  vce(cluster ATC_Grupo)

*reghdfe DGenericosATC i.TiempoPOS3 if VariacionIncreasePrice30_==0 & Institucional_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) resid(resid_generico)
reghdfe LgInstitucional_Prom_STD i.TiempoPOS3 $control_basico if VariacionIncreasePrice30_==0 & Institucional_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) 
*resid(resid_price)
coefplot, graphregion(color(white)) baselevels keep(*.TiempoPOS3) drop(0.TiempoPOS3) ci rename(1.TiempoPOS3="<-3" 2.TiempoPOS3 ="-3" 3.TiempoPOS3= "-2" 4.TiempoPOS3= "-1" 5.TiempoPOS3= "0" 6.TiempoPOS3= "1" 7.TiempoPOS3 ="2" 8.TiempoPOS3= "3" ///
9.TiempoPOS3= ">3")   vertical yline(0) xtitle("Time to POS") ytitle("Coefficient on `name'") title("Evolution of a drug's `name2' before and after" "it is included in the benefit plan") 

 
bys CUM1: egen minCompetidoresATC=min(CompetidoresATC)
bys CUM1: egen maxCompetidoresATC=max(CompetidoresATC)

collapse  resid_price PrimerANOPOS PrimerANOVMR maxCompetidoresATC  minCompetidoresATC (firstnm) ATC0 PRINCIPIO_ACTIVO0 DESCRIPCION_ATC0 , by(TiempoPOS3 CUM1)
drop if TiempoPOS3==0
*resid_generico
reshape wide  resid_price, i(CUM1) j(TiempoPOS3)
foreach j in 1 2 3 5 6 7 8 9{
replace  resid_price`j'=resid_price`j'-resid_price4
*replace resid_generico`j'=resid_generico`j'- resid_generico4
}
drop if resid_price4==. /*& resid_generico4==.*/


preserve
reghdfe DGenericosATC POS2 if VariacionIncreasePrice30_==0 & Institucional_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) resid(resid_generico)
reghdfe LgInstitucional_Prom_STD POS2 $control_basico if VariacionIncreasePrice30_==0 & Institucional_Count==$panelcompelto, absorb(CUM1 c.Periodo#ATC_Grupo) vce(cluster ATC_Grupo) resid(resid_price)
 
bys CUM1: egen minCompetidoresATC=min(CompetidoresATC)
bys CUM1: egen maxCompetidoresATC=max(CompetidoresATC)

 collapse resid_generico resid_price PrimerANOPOS PrimerANOVMR maxCompetidoresATC  minCompetidoresATC (firstnm) ATC0 PRINCIPIO_ACTIVO0 DESCRIPCION_ATC0 , by(POS2 CUM1)
 drop if POS2==.
 reshape wide resid_generico resid_price, i(CUM1) j(POS2)
 gen diff_precio=resid_price1-resid_price0
 gen diff_generico=resid_generico1- resid_generico0
drop if diff_precio==. & diff_generico==.
restore






