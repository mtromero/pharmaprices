import delimited "$base_out/ATC_Multi.csv", clear varnames(1)
drop v1
drop if atc==""
drop if drug=="combinations"
drop if drug=="various"
drop if drug=="various combinations"
egen id=group(drug)
bys id: gen N=_N
sort N id
rename atc ATC
gen atc=ATC
rename id id_combinado
drop drug N
compress
save "$base_out/ATC_Multi.dta", replace


import delim "$basein/POS/Hasta2007.csv", clear
keep principioactivo
egen principioactivo2=sieve(principioactivo), keep(alphabetic space)
replace principioactivo2=stritrim(principioactivo2)
replace principioactivo2=strtrim(principioactivo2)
replace principioactivo2=lower(principioactivo2)
gen id_2007=_n
save "$base_out/POS/Hasta2007.dta", replace


import delim "$basein/POS/AC08_2009_Bare.csv", clear varnames(1) bindquote(strict)
keep principioactivo
egen principioactivo2=sieve(principioactivo), keep(alphabetic space)
replace principioactivo2=stritrim(principioactivo2)
replace principioactivo2=strtrim(principioactivo2)
replace principioactivo2=lower(principioactivo2)
gen id_2010=_n
save "$base_out/POS/AC08_2009_Bare.dta", replace


import delim "$basein/POS/AC29_2011_Bare.csv", clear varnames(1)
keep principioactivo atc concentración formafarmacéutica
replace atc=stritrim(atc)
replace atc=strtrim(atc)
replace atc=ustrltrim(atc)
replace atc=regexr(atc," ","")
replace atc=regexr(atc," ","")
replace atc="S01HA03" if atc=="S01HA30"
replace atc="J07CA01" if atc=="J07CA99"
replace atc="P01BF01" if atc=="P01BE52"
replace atc="J05AR10" if atc=="J05AE11"
replace principioactivo=ustrto(ustrnormalize(principioactivo,"nfd"),"ascii",2)
egen principioactivo2=sieve(principioactivo), keep(alphabetic space)
replace principioactivo2=stritrim(principioactivo2)
replace principioactivo2=strtrim(principioactivo2)
replace principioactivo2=lower(principioactivo2)
replace principioactivo2=ustrto(ustrnormalize(principioactivo2, "nfd"), "ascii", 2)

gen id_2012=_n
save "$base_out/POS/AC29_2011_Bare.dta", replace
gen nchar= strlen(atc)

import delim "$basein/POS/Res5521_2013_Bare.csv", clear varnames(1) bindquote(strict)
keep principioactivo atc concentración formafarmacéutica
replace atc=stritrim(atc)
replace atc=strtrim(atc)
replace atc=ustrltrim(atc)
replace atc=regexr(atc," ","")
replace atc=regexr(atc," ","")
replace atc=substr(atc,1,7)
gen id_2014=_n
replace atc="S01HA03" if atc=="S01HA30"
replace atc="D06BB04" if atc=="D10AX95"
replace atc="L01XC07" if atc=="L01XY07"
replace atc="L03AX13" if atc=="V08AA05"
replace atc="J07CA01" if atc=="J07CA99"
replace atc="P01BF01" if atc=="P01BE52"
replace atc="J05AR10" if atc=="J05AE11"
save "$base_out/POS/Res5521_2013_Bare.dta", replace
gen nchar= strlen(atc) 

import delim "$basein/POS/Res5926_2014_Bare.csv", clear varnames(1) bindquote(strict)
keep principioactivo atc concentración formafarmacéutica
replace atc=stritrim(atc)
replace atc=strtrim(atc)
replace atc=ustrltrim(atc)
replace atc=regexr(atc," ","")
replace atc=regexr(atc," ","")
replace atc=substr(atc,1,7)
gen id_2015=_n
replace atc="S01HA03" if atc=="S01HA30"
replace atc="J07CA01" if atc=="J07CA99"
replace atc="P01BF01" if atc=="P01BE52"
replace atc="J05AR10" if atc=="J05AE11"
replace atc="D06BB04" if atc=="D10AX95"
replace atc="L01XC07" if atc=="L01XY07"
replace atc="L03AX13" if atc=="V08AA05"
save "$base_out/POS/Res5926_2014_Bare.dta", replace
gen nchar= strlen(atc) 

import delim "$basein/POS/Res5592_2015_Bare.csv", clear varnames(1) bindquote(strict)
keep principioactivo atc concentración formafarmacéutica
replace atc=stritrim(atc)
replace atc=strtrim(atc)
replace atc=ustrltrim(atc)
replace atc=regexr(atc," ","")
replace atc=regexr(atc," ","")
replace atc=substr(atc,1,7)
gen id_2015=_n
replace atc="S01HA03" if atc=="S01HA30"
replace atc="J07CA01" if atc=="J07CA99"
replace atc="P01BF01" if atc=="P01BE52"
replace atc="J05AR10" if atc=="J05AE11"
replace atc="D06BB04" if atc=="D10AX95"
replace atc="L01XC07" if atc=="L01XY07"
replace atc="L03AX13" if atc=="V08AA05"
save "$base_out/POS/Res5592_2015_Bare.dta", replace
gen nchar= strlen(atc) 
preserve
keep if nchar==5
rename atc atc5
save "$base_out/POS/Res5592_2015_Bare_5.dta", replace
restore
preserve
keep if nchar==4
rename atc atc4
save "$base_out/POS/Res5592_2015_Bare_4.dta", replace
restore

*Acuerdos de 2007 368 y 380
clear
set obs 4
gen atc="" 
replace atc="J05AR10" in 1
replace atc="G03FA01" in 2
replace atc="G03AC03" in 3
replace atc="G03AA08" in 4
drop if atc=="J05AR10"
save "$base_out/POS/InAfter2008.dta", replace



use "$base_out/POS/Hasta2007.dta", clear
reclink principioactivo2 using "$base_out/POS/AC08_2009_Bare.dta", idmaster(id_2007) idusing(id_2010) gen(score) minscore(0.9) 
keep if _merge==1
keep principioactivo2
gen atc=""
replace atc="R07AB01" if principioactivo2=="doxapramo clorhidrato"
replace atc="N06AF04" if principioactivo2=="tranilcipromina"
keep atc
save "$base_out/POS/OutIn2010.dta", replace




/*useless since dont know atc codes*/
/*
use "$base_out/POS/AC08_2009_Bare.dta", clear
reclink principioactivo2 using "$base_out/POS/AC29_2011_Bare.dta", idmaster(id_2010) idusing(id_2012) gen(score) minscore(0.96)
keep if _merge==1
keep principioactivo2
gen atc=""
keep atc
save "$base_out/POS/OutAfter2012.dta", replace
*/


use "$base_out/POS/AC08_2009_Bare.dta", clear
reclink principioactivo2 using "$base_out/POS/Hasta2007.dta", idmaster(id_2010) idusing( id_2007) gen(score) minscore(0.9) 
keep if _merge==1
keep principioactivo2
save "$base_out/POS/InAfter2010.dta", replace




use "$base_out/POS/AC29_2011_Bare.dta", clear
mmerge principioactivo2 using "$base_out/POS/InAfter2010.dta"
keep if _merge==3
bys atc: gen n=_n
drop if n>1
keep atc
merge 1:1 atc using "$base_out/POS/InAfter2008.dta"
drop if _merge==3
drop if _merge==2
drop _merge
drop if atc=="M05BA08"
save "$base_out/POS/InAfter2010.dta", replace




use "$base_out/POS/AC29_2011_Bare.dta", clear
reclink principioactivo2 using "$base_out/POS/AC08_2009_Bare.dta", idmaster(id_2012) idusing(id_2010) gen(score) minscore(0.9) 
drop if _merge==3
keep atc principioactivo concentración formafarmacéutica id_2012
drop if principioactivo=="SARAMPION, RUBEOLA, PAPERAS (SRP)"
bys atc: gen n=_n
drop if n>1
keep atc
drop if atc=="H02AB07"
drop if atc=="J01CR02"
drop if atc=="N03AB02"
drop if atc=="A02BC05"
save "$base_out/POS/InAfter2012.dta", replace

*ac 34 de 2012- misoprosol*
clear
set obs 1
gen atc="" 
replace atc="G02AD06" in 1
save "$base_out/POS/InAfter2013.dta", replace


use "$base_out/POS/AC29_2011_Bare.dta", clear
mmerge atc using "$base_out/POS/Res5521_2013_Bare.dta"
drop if _merge!=1
keep if atc=="G02AB03" | atc=="L03AB01"
keep atc
save "$base_out/POS/OutAfter2014.dta", replace


use "$base_out/POS/AC29_2011_Bare.dta", clear
mmerge atc using "$base_out/POS/Res5521_2013_Bare.dta"
keep if _merge==2
bys atc: gen n=_n
drop if n>1
keep atc
merge 1:1 atc using "$base_out/POS/InAfter2013.dta"
drop if _merge==3
drop if _merge==2
drop _merge
drop if atc =="B05BB02"
save "$base_out/POS/InAfter2014.dta", replace



use "$base_out/POS/Res5521_2013_Bare.dta", clear
keep if concentración=="Incluye todas las concentraciones" & 	formafarmacéutica=="INCLUYE TODAS LAS FORMAS FARMACÉUTICAS"
bys atc: gen n=_n
drop if n>1
keep atc
save "$base_out/POS/InAfter2014_AllATC.dta", replace




*Between 2014 and 2015 no new pharmas, no drops, just a change in how many forms are covered

/*
use "$base_out/POS/Res5521_2013_Bare.dta", clear
mmerge atc using "$base_out/POS/Res5926_2014_Bare.dta"
drop if _merge!=1
drop if atc=="S01CA02"
drop if atc=="S01FA56"
save "$base_out/POS/OutAfter2015.dta", replace

use "$base_out/POS/Res5521_2013_Bare.dta", clear
mmerge atc using "$base_out/POS/Res5926_2014_Bare.dta"
drop if _merge!=2
*/
use "$base_out/POS/Res5926_2014_Bare.dta", clear
keep if (concentración=="Incluye todas las concentraciones" | concentración=="Incluye todas las") & 	(formafarmacéutica=="INCLUYE TODAS LAS FORMAS FARMACÉUTICAS" | formafarmacéutica=="INCLUYE TODAS")
bys atc: gen n=_n
drop if n>1
keep atc
save "$base_out/POS/InAfter2015_AllATC.dta", replace





*just changes in names
/*
use "$base_out/POS/Res5926_2014_Bare.dta", clear
mmerge atc using "$base_out/POS/Res5592_2015_Bare.dta"
drop if _merge!=1
gen atc5=substr(atc,1,5)
mmerge atc5 using "$base_out/POS/Res5592_2015_Bare_5.dta"
drop if _merge!=1
gen atc4=substr(atc,1,4)
mmerge atc4 using "$base_out/POS/Res5592_2015_Bare_4.dta"
drop if _merge!=1
save "$base_out/POS/OutAfter2016.dta", replace

principioactivo
GELATINA ABSORBIBLE ESTÉRIL
SODIO CLORURO
VENDAJE CON GELATINA DE ZINC.
CLINDAMICINA (FOSFATO)
LOPINAVIR + RITONAVIR
LOPINAVIR + RITONAVIR
FENTANILO CITRATO

VACUNA CONTRA LA DIFTERIA, TÉTANO Y TOS FERINA (D.P.T)
ARTEMETER + LUMENFANTRINE

*/



use "$base_out/POS/Res5926_2014_Bare.dta", clear
mmerge atc using "$base_out/POS/Res5592_2015_Bare.dta"
drop if _merge!=2
gen nchar=strlen(atc)
drop if nchar<7
keep atc
mmerge atc using "$base_out/POS/Res5592_2015_Bare.dta"
keep if _merge==3
bys atc: gen n=_n
drop if n>1
keep atc
save "$base_out/POS/InAfter2016.dta", replace

use "$base_out/POS/Res5592_2015_Bare.dta", clear
keep if (concentración=="Incluye todas las concentraciones" | concentración=="Incluye todas las concentraciones" | concentración=="Incluye todos los volúmenes o todas las concentraciones") ///
& 	(formafarmacéutica=="INCLUYE TODAS LAS FORMAS FARMACÉUTICAS" )
bys atc: gen n=_n
drop if n>1
keep atc
gen nchar=strlen(atc)
drop if nchar<7
save "$base_out/POS/InAfter2016_AllATC.dta", replace

use "$base_out/POS/Res5592_2015_Bare_5.dta", clear
keep if (concentración=="Incluye todas las concentraciones" | concentración=="Incluye todas las concentraciones" | concentración=="Incluye todos los volúmenes o todas las concentraciones") ///
& 	(formafarmacéutica=="INCLUYE TODAS LAS FORMAS FARMACÉUTICAS" )
bys atc: gen n=_n
drop if n>1
keep atc
save "$base_out/POS/InAfter2016_AllATC_5.dta", replace

use "$base_out/POS/Res5592_2015_Bare_4.dta", clear
keep if (concentración=="Incluye todas las concentraciones" | concentración=="Incluye todas las concentraciones" | concentración=="Incluye todos los volúmenes o todas las concentraciones") ///
& 	(formafarmacéutica=="INCLUYE TODAS LAS FORMAS FARMACÉUTICAS")
bys atc: gen n=_n
drop if n>1
keep atc
save "$base_out/POS/InAfter2016_AllATC_4.dta", replace



