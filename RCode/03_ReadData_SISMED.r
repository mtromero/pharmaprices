rm(list=ls())
library(foreign)
library(plyr)
#setwd("/media/mauricio/TeraHDD2/Dropbox/Research/Health/Medicamentos/Programa")
setwd("C:/Users/Mauricio/Dropbox/Research/Health/Medicamentos/Programa")
load("CreatedData/VMR_TODOS.RData")
#Remember
#VMR_2011_1020=VMR_2011_1697
#VMR_2010_5229=VMR_2011_005

CodigosINVIMA=read.dta("CreatedData/CodigosINVIMA.dta")

Medicamentos=NULL
IPC_ANUAL=read.csv(paste("RawData/IPC_ANUAL.csv",sep=""))
for(Periodo in  2006:2016){

MedicamentosPeriodo=read.csv(paste("RawData/",Periodo,".csv",sep=""), encoding="latin1")
MedicamentosPeriodo$Medicamento=as.character(MedicamentosPeriodo$Medicamento)
MedicamentosPeriodo$Presentacion=as.character(MedicamentosPeriodo$Presentacion)
MedicamentosPeriodo$FabricacionNacional=as.character(MedicamentosPeriodo$FabricacionNacional)
MedicamentosPeriodo$CodigoATC=as.character(MedicamentosPeriodo$CodigoATC)
MedicamentosPeriodo$ATC=as.character(MedicamentosPeriodo$ATC)
MedicamentosPeriodo$POS=as.character(MedicamentosPeriodo$POS)
MedicamentosPeriodo$POS=iconv(MedicamentosPeriodo$POS, to='ASCII//TRANSLIT')
MedicamentosPeriodo$POS[which(MedicamentosPeriodo$POS=="NO")]="0"
MedicamentosPeriodo$POS[which(MedicamentosPeriodo$POS=="SI")]="1"
MedicamentosPeriodo$POS=as.numeric(MedicamentosPeriodo$POS)
MedicamentosPeriodo$Comercial_Lab_Prom=as.numeric(MedicamentosPeriodo$Comercial_Lab_Prom)
MedicamentosPeriodo[,c("Comercial_Lab_Prom","Comercial_May_Prom","Institucional_Lab_Prom","Institucional_May_Prom","Compra_Prom")]=MedicamentosPeriodo[,c("Comercial_Lab_Prom","Comercial_May_Prom","Institucional_Lab_Prom","Institucional_May_Prom","Compra_Prom")]*100/IPC_ANUAL[which(IPC_ANUAL$ANO==Periodo),2]
MedicamentosPeriodo[,c("Comercial_Lab_Min","Comercial_May_Min","Institucional_Lab_Min","Institucional_May_Min","Compra_Min")]=MedicamentosPeriodo[,c("Comercial_Lab_Min","Comercial_May_Min","Institucional_Lab_Min","Institucional_May_Min","Compra_Min")]*100/IPC_ANUAL[which(IPC_ANUAL$ANO==Periodo),2]
MedicamentosPeriodo[,c("Comercial_Lab_Max","Comercial_May_Max","Institucional_Lab_Max","Institucional_May_Max","Compra_Max")]=MedicamentosPeriodo[,c("Comercial_Lab_Max","Comercial_May_Max","Institucional_Lab_Max","Institucional_May_Max","Compra_Max")]*100/IPC_ANUAL[which(IPC_ANUAL$ANO==Periodo),2]



MedicamentosPeriodo$MediID=paste(MedicamentosPeriodo$CUM1,MedicamentosPeriodo$CUM2)
MedicamentosPeriodo$PrincipioActivo=as.character(MedicamentosPeriodo$PrincipioActivo)
MedicamentosPeriodo$ViaAdministracion=as.character(MedicamentosPeriodo$ViaAdministracion)
MedicamentosPeriodo$Comercial_Lab_Min=as.numeric(as.character(MedicamentosPeriodo$Comercial_Lab_Min))
MedicamentosPeriodo$Comercial_Lab_Max=as.numeric(as.character(MedicamentosPeriodo$Comercial_Lab_Max))
MedicamentosPeriodo$Comercial_May_Min=as.numeric(as.character(MedicamentosPeriodo$Comercial_May_Min))
MedicamentosPeriodo$Comercial_May_Max=as.numeric(as.character(MedicamentosPeriodo$Comercial_May_Max))
MedicamentosPeriodo$Institucional_Lab_Min=as.numeric(as.character(MedicamentosPeriodo$Institucional_Lab_Min))
MedicamentosPeriodo$Institucional_Lab_Max=as.numeric(as.character(MedicamentosPeriodo$Institucional_Lab_Max))
MedicamentosPeriodo$Institucional_May_Min=as.numeric(as.character(MedicamentosPeriodo$Institucional_May_Min))
MedicamentosPeriodo$Institucional_May_Max=as.numeric(as.character(MedicamentosPeriodo$Institucional_May_Max))
MedicamentosPeriodo$Compra_Min=as.numeric(as.character(MedicamentosPeriodo$Compra_Min))
MedicamentosPeriodo$Compra_Max=as.numeric(as.character(MedicamentosPeriodo$Compra_Max))

MedicamentosPeriodo=aggregate(MedicamentosPeriodo[,c(8,12:31)],by=list(MedicamentosPeriodo$MediID),FUN=mean,na.rm=T) 
names(MedicamentosPeriodo)[1]="MediID"

#Quite esto por que me quitaba informacion del POS... prefiero si acaso borrarlo despues
CodigosAno=CodigosINVIMA[(Periodo>=pmax(as.numeric(format(as.Date(CodigosINVIMA$FECHA_EXPEDICION),"%Y")),as.numeric(format(as.Date(CodigosINVIMA$FECHA_ACTIVO),"%Y")),na.rm=T)) & (Periodo<=pmin(as.numeric(format(as.Date(CodigosINVIMA$FECHA_VENCIMIENTO),"%Y")),as.numeric(format(as.Date(CodigosINVIMA$FECHA_INACTIVO),"%Y")),na.rm=T)),]
MedicamentosPeriodo=merge(MedicamentosPeriodo,CodigosINVIMA,by="MediID",all.y=F,all.x=T)
MedicamentosPeriodo=rbind.fill(MedicamentosPeriodo,CodigosAno[which(!(CodigosAno$MediID %in% MedicamentosPeriodo$MediID)),])

#Note to self: you should really comment your code more...it took me ages to figure whats going on above. You are adding the pharmaceuticals that, according to the INVIMA, where in the market that year. You previously had all.x=F. I think this is a mistake, since if the pharma is sold, then its in the market implictly. I changed it.Now we first add the invima info, and then add the pharmaceuticals that are "in" that year but we dont have in the data yet

#MedicamentosPeriodo=merge(MedicamentosPeriodo,CodigosINVIMA,by="MediID",all.y=F,all.x=T)
MedicamentosPeriodo$Periodo=Periodo
#MedicamentosPeriodo[,"UNIDAD_MEDIDA"]=as.character(MedicamentosPeriodo[,"UNIDAD_MEDIDA"])
MedicamentosPeriodo[,"ATC0"]=as.character(MedicamentosPeriodo[,"ATC0"])
MedicamentosPeriodo[,"ATC1"]=as.character(MedicamentosPeriodo[,"ATC1"])
MedicamentosPeriodo[,"ATC2"]=as.character(MedicamentosPeriodo[,"ATC2"])
MedicamentosPeriodo[,"ATC3"]=as.character(MedicamentosPeriodo[,"ATC3"])
MedicamentosPeriodo$DESCRIPCION_ATC0=as.character(MedicamentosPeriodo$DESCRIPCION_ATC0)
MedicamentosPeriodo$DESCRIPCION_ATC1=as.character(MedicamentosPeriodo$DESCRIPCION_ATC1)
MedicamentosPeriodo$DESCRIPCION_ATC2=as.character(MedicamentosPeriodo$DESCRIPCION_ATC2)
MedicamentosPeriodo$DESCRIPCION_ATC3=as.character(MedicamentosPeriodo$DESCRIPCION_ATC3)
MedicamentosPeriodo$PRINCIPIO_ACTIVO0=as.character(MedicamentosPeriodo$PRINCIPIO_ACTIVO0)
MedicamentosPeriodo$PRINCIPIO_ACTIVO1=as.character(MedicamentosPeriodo$PRINCIPIO_ACTIVO1)
MedicamentosPeriodo$PRINCIPIO_ACTIVO2=as.character(MedicamentosPeriodo$PRINCIPIO_ACTIVO2)
MedicamentosPeriodo$PRINCIPIO_ACTIVO3=as.character(MedicamentosPeriodo$PRINCIPIO_ACTIVO3)


MedicamentosPeriodo=subset(MedicamentosPeriodo,subset=(!is.na(MedicamentosPeriodo$ATC0)))
#MedicamentosPeriodo$VIA_ADMINISTRACION=as.character(MedicamentosPeriodo$VIA_ADMINISTRACION) 

MedicamentosPeriodo[which(MedicamentosPeriodo$Comercial_Lab_Unidades==0),c("Comercial_Lab_Min","Comercial_Lab_Max","Comercial_Lab_Unidades" ,"Comercial_Lab_Prom")]=NA
MedicamentosPeriodo[which(MedicamentosPeriodo$Comercial_May_Unidades==0),c("Comercial_May_Min","Comercial_May_Max","Comercial_May_Unidades" ,"Comercial_May_Prom")]=NA
MedicamentosPeriodo[which(MedicamentosPeriodo$Institucional_Lab_Unidades==0),c("Institucional_Lab_Min","Institucional_Lab_Max","Institucional_Lab_Unidades" ,"Institucional_Lab_Prom")]=NA
MedicamentosPeriodo[which(MedicamentosPeriodo$Institucional_May_Unidades==0),c("Institucional_May_Min","Institucional_May_Max","Institucional_May_Unidades" ,"Institucional_May_Prom")]=NA
MedicamentosPeriodo[which(MedicamentosPeriodo$Compra_Unidades==0),c("Compra_Min","Compra_Max","Compra_Unidades" ,"Compra_Prom")]=NA

MedicamentosPeriodo[which(MedicamentosPeriodo$Comercial_Lab_Prom==0),c("Comercial_Lab_Min","Comercial_Lab_Max","Comercial_Lab_Unidades" ,"Comercial_Lab_Prom")]=NA
MedicamentosPeriodo[which(MedicamentosPeriodo$Comercial_May_Prom==0),c("Comercial_May_Min","Comercial_May_Max","Comercial_May_Unidades" ,"Comercial_May_Prom")]=NA
MedicamentosPeriodo[which(MedicamentosPeriodo$Institucional_Lab_Prom==0),c("Institucional_Lab_Min","Institucional_Lab_Max","Institucional_Lab_Unidades" ,"Institucional_Lab_Prom")]=NA
MedicamentosPeriodo[which(MedicamentosPeriodo$Institucional_May_Prom==0),c("Institucional_May_Min","Institucional_May_Max","Institucional_May_Unidades" ,"Institucional_May_Prom")]=NA
MedicamentosPeriodo[which(MedicamentosPeriodo$Compra_Prom==0),c("Compra_Min","Compra_Max","Compra_Unidades" ,"Compra_Prom")]=NA
   
NombresInst=c("Institucional_Lab_Prom","Institucional_Lab_Unidades","Institucional_May_Prom","Institucional_May_Unidades")
#MedicamentosPeriodo$Inst_Total_Prom=apply(MedicamentosPeriodo[,NombresInst],1,function(x) weighted.mean(x[c(1,3)],x[c(2,4)],na.rm=T))
#MedicamentosPeriodo$Inst_Total_Unidades=rowSums(MedicamentosPeriodo[,c("Institucional_Lab_Unidades","Institucional_May_Unidades")],na.rm=T)


MedicamentosPeriodo$Comercial_Lab_Prom_STD=MedicamentosPeriodo$Comercial_Lab_Prom/(MedicamentosPeriodo$CANTIDAD)
MedicamentosPeriodo$Comercial_May_Prom_STD=MedicamentosPeriodo$Comercial_May_Prom/(MedicamentosPeriodo$CANTIDAD)
MedicamentosPeriodo$Institucional_Lab_Prom_STD=MedicamentosPeriodo$Institucional_Lab_Prom/(MedicamentosPeriodo$CANTIDAD)
MedicamentosPeriodo$Institucional_May_Prom_STD=MedicamentosPeriodo$Institucional_May_Prom/(MedicamentosPeriodo$CANTIDAD)
MedicamentosPeriodo$Compra_Prom_STD=MedicamentosPeriodo$Compra_Prom/(MedicamentosPeriodo$CANTIDAD)
       
MedicamentosPeriodo$Comercial_Lab_Min_STD=MedicamentosPeriodo$Comercial_Lab_Min/(MedicamentosPeriodo$CANTIDAD)
MedicamentosPeriodo$Comercial_May_Min_STD=MedicamentosPeriodo$Comercial_May_Min/(MedicamentosPeriodo$CANTIDAD)
MedicamentosPeriodo$Institucional_Lab_Min_STD=MedicamentosPeriodo$Institucional_Lab_Min/(MedicamentosPeriodo$CANTIDAD)
MedicamentosPeriodo$Institucional_May_Min_STD=MedicamentosPeriodo$Institucional_May_Min/(MedicamentosPeriodo$CANTIDAD)
MedicamentosPeriodo$Compra_Min_STD=MedicamentosPeriodo$Compra_Min/(MedicamentosPeriodo$CANTIDAD)

MedicamentosPeriodo$Comercial_Lab_Max_STD=MedicamentosPeriodo$Comercial_Lab_Max/(MedicamentosPeriodo$CANTIDAD)
MedicamentosPeriodo$Comercial_May_Max_STD=MedicamentosPeriodo$Comercial_May_Max/(MedicamentosPeriodo$CANTIDAD)
MedicamentosPeriodo$Institucional_Lab_Max_STD=MedicamentosPeriodo$Institucional_Lab_Max/(MedicamentosPeriodo$CANTIDAD)
MedicamentosPeriodo$Institucional_May_Max_STD=MedicamentosPeriodo$Institucional_May_Max/(MedicamentosPeriodo$CANTIDAD)
MedicamentosPeriodo$Compra_Max_STD=MedicamentosPeriodo$Compra_Max/(MedicamentosPeriodo$CANTIDAD)

#MedicamentosPeriodo$Inst_Total_Prom_STD=MedicamentosPeriodo$Inst_Total_Prom/(MedicamentosPeriodo$CANTIDAD)
#
MedicamentosPeriodo$Comercial_Lab_Unidades_STD=MedicamentosPeriodo$Comercial_Lab_Unidades*MedicamentosPeriodo$CANTIDAD
MedicamentosPeriodo$Comercial_May_Unidades_STD=MedicamentosPeriodo$Comercial_May_Unidades*MedicamentosPeriodo$CANTIDAD
MedicamentosPeriodo$Institucional_Lab_Unidades_STD=MedicamentosPeriodo$Institucional_Lab_Unidades*MedicamentosPeriodo$CANTIDAD
MedicamentosPeriodo$Institucional_May_Unidades_STD=MedicamentosPeriodo$Institucional_May_Unidades*MedicamentosPeriodo$CANTIDAD
MedicamentosPeriodo$Compra_Unidades_STD=MedicamentosPeriodo$Compra_Unidades*MedicamentosPeriodo$CANTIDAD



VariablesPrecios=c("Comercial_Lab_Prom_STD","Comercial_May_Prom_STD","Institucional_Lab_Prom_STD","Institucional_May_Prom_STD","Compra_Prom_STD")
for(vari in VariablesPrecios){
  if(length(which(MedicamentosPeriodo[,vari]==Inf))>0){
    print("paso")
    MedicamentosPeriodo[which(MedicamentosPeriodo[,vari]==Inf),vari]=NA
  }
}


MedicamentosPeriodo$VMR=0
MedicamentosPeriodo$PMV=0
   
if( Periodo==2010) MedicamentosPeriodo$VMR[which(MedicamentosPeriodo$MediID %in% VMR_2011_005$MediID & MedicamentosPeriodo$MediID %in% VMR_CNPM_2010_C004$MediID)] =213/365 #it started in Dec 2014 of 2010 at first 2011 is the same as 2010
if( Periodo==2010) MedicamentosPeriodo$VMR[which(MedicamentosPeriodo$MediID %in% VMR_2011_005$MediID & !(MedicamentosPeriodo$MediID %in% VMR_CNPM_2010_C004$MediID))] =15/365 #it started in Dec 2014 of 2010 at first 2011 is the same as 2010
if( Periodo==2010) MedicamentosPeriodo$VMR[which(!(MedicamentosPeriodo$MediID %in% VMR_2011_005$MediID) & MedicamentosPeriodo$MediID %in% VMR_CNPM_2010_C004$MediID)] =196/365 #it started in Dec 2014 of 2010 at first 2011 is the same as 2010

if( Periodo==2011) MedicamentosPeriodo$VMR[which(MedicamentosPeriodo$MediID %in% VMR_2011_005$MediID & 
MedicamentosPeriodo$MediID %in% VMR_2011_1697$MediID & MedicamentosPeriodo$MediID %in% VMR_2011_3026$MediID &
 MedicamentosPeriodo$MediID %in% VMR_2011_3470$MediID &  MedicamentosPeriodo$MediID %in% VMR_2011_4316$MediID)]=1 #the ones that where on the whole year 2011
 

if( Periodo==2011) MedicamentosPeriodo$VMR[which(!(MedicamentosPeriodo$MediID %in% VMR_2011_005$MediID) & 
MedicamentosPeriodo$MediID %in% VMR_2011_1697$MediID & MedicamentosPeriodo$MediID %in% VMR_2011_3026$MediID &
MedicamentosPeriodo$MediID %in% VMR_2011_3470$MediID &  MedicamentosPeriodo$MediID %in% VMR_2011_4316$MediID)]=(365-90)/365 #the ones that started in march 31 of 2011. Added to VMR_2011_1020/VMR_2011_1697. Some were added in march, none removed!


if( Periodo==2011) MedicamentosPeriodo$VMR[which(!(MedicamentosPeriodo$MediID %in% VMR_2011_005$MediID) & 
!(MedicamentosPeriodo$MediID %in% VMR_2011_1697$MediID) & MedicamentosPeriodo$MediID %in% VMR_2011_3026$MediID &
MedicamentosPeriodo$MediID %in% VMR_2011_3470$MediID &  MedicamentosPeriodo$MediID %in% VMR_2011_4316$MediID)]=(365-202)/365 #the ones that started in July 22 of 2011.  Added to VMR_2011_3026. Some were added in march, none removed!
 
  #Here  it gets tricky. first the ones added on each round that were not there before 
if( Periodo==2011) MedicamentosPeriodo$VMR[which(!(MedicamentosPeriodo$MediID %in% VMR_2011_005$MediID) & 
!(MedicamentosPeriodo$MediID %in% VMR_2011_1697$MediID) & !(MedicamentosPeriodo$MediID %in% VMR_2011_3026$MediID) &
MedicamentosPeriodo$MediID %in% VMR_2011_3470$MediID &  MedicamentosPeriodo$MediID %in% VMR_2011_4316$MediID)]=(365-229)/365 #the ones that started in August 18 of 2011. Some were added to VMR_2011_3470
 
#Now the ones that are removed!
if( Periodo==2011) MedicamentosPeriodo$VMR[which((MedicamentosPeriodo$MediID %in% VMR_2011_005$MediID) & 
(MedicamentosPeriodo$MediID %in% VMR_2011_1697$MediID) & (MedicamentosPeriodo$MediID %in% VMR_2011_3026$MediID) &
!(MedicamentosPeriodo$MediID %in% VMR_2011_3470$MediID) &  !(MedicamentosPeriodo$MediID %in% VMR_2011_4316$MediID))]=(229)/365 #the ones that were removed in August 18 of 2011. deleted from     VMR_2011_3470

if( Periodo==2012) MedicamentosPeriodo$VMR[which(MedicamentosPeriodo$MediID %in% VMR_2011_4316$MediID & !(MedicamentosPeriodo$MediID %in% VMR_2012_2569$MediID))]=124/365
if( Periodo==2012) MedicamentosPeriodo$VMR[which(MedicamentosPeriodo$MediID %in% VMR_2011_4316$MediID & MedicamentosPeriodo$MediID %in% VMR_2012_2569$MediID)]=1
if( Periodo==2012) MedicamentosPeriodo$VMR[which(!(MedicamentosPeriodo$MediID %in% VMR_2011_4316$MediID) & MedicamentosPeriodo$MediID %in% VMR_2012_2569$MediID)]=1-124/365

if( Periodo>=2013) MedicamentosPeriodo$VMR[which(MedicamentosPeriodo$MediID %in% VMR_2012_2569$MediID)]=1

if(Periodo>=2012) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% CNPM_2011_C001$MediID)]=1 

if(Periodo==2012) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% CNPM_2012_C001$MediID)]=118/365 
if(Periodo==2012) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% setdiff(CNPM_2012_C003$MediID,CNPM_2012_C001$MediID))]=53/365
if(Periodo==2012) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% setdiff(CNPM_2012_C001$MediID,CNPM_2012_C003$MediID))]=54/365

if(Periodo>=2013) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% CNPM_2012_C003$MediID)]=1

if(Periodo==2013) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% CNPM_2013_C004_POS$MediID)]=1-133/365
if(Periodo>=2014) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% CNPM_2013_C004_POS$MediID)]=1
if(Periodo==2013) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% CNPM_2013_C005_POS$MediID)]=106/365
if(Periodo>=2014) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% CNPM_2013_C005_POS$MediID)]=1

if(Periodo==2013) MedicamentosPeriodo$VMR[which(MedicamentosPeriodo$MediID %in% setdiff(CNPM_2013_C005_NOPOS$MediID,VMR_2012_2569$MediID))]=106/365
if(Periodo==2013) MedicamentosPeriodo$VMR[which(MedicamentosPeriodo$MediID %in% setdiff(CNPM_2013_C004_NOPOS$MediID,VMR_2012_2569$MediID))]=1-133/365
if(Periodo>=2014) MedicamentosPeriodo$VMR[which(MedicamentosPeriodo$MediID %in% union(CNPM_2013_C005_NOPOS$MediID,VMR_2012_2569$MediID))]=1

if(Periodo==2013) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% CNPM_2013_C006_NOPOS$MediID)]=84/365
if(Periodo>=2014) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% CNPM_2013_C006_NOPOS$MediID)]=1

if(Periodo>=2014) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% CNPM_2013_C007_PVM$MediID)]=1
if(Periodo>=2014) MedicamentosPeriodo$VMR[which(MedicamentosPeriodo$MediID %in% CNPM_2013_C007_VMR$MediID)]=1


if(Periodo==2014) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% CNPM_2014_C001$MediID)]=1-92/365
if(Periodo>=2015) MedicamentosPeriodo$PMV[which(MedicamentosPeriodo$MediID %in% CNPM_2014_C001$MediID)]=1


Medicamentos=rbind(Medicamentos,MedicamentosPeriodo)
}


Medicamentos$MediID2=as.numeric(gsub(" ","",Medicamentos$MediID))

#
Conteo1=aggregate(Medicamentos$POS,by=list(Medicamentos$MediID2,Medicamentos$Periodo),FUN=mean,na.rm=T)
Conteo2=aggregate(Medicamentos$POS,by=list(Medicamentos$CUM1,Medicamentos$Periodo),FUN=mean,na.rm=T)
Conteo3=aggregate(Medicamentos$POS,by=list(Medicamentos$ATC0,Medicamentos$Periodo),FUN=mean,na.rm=T)
#

Medicamentos[which(Medicamentos[,"ATC0"]==""),"ATC0"]="NA"
Medicamentos[which(Medicamentos[,"ATC1"]==""),"ATC1"]="NA"
Medicamentos[which(Medicamentos[,"ATC2"]==""),"ATC2"]="NA"
Medicamentos[which(Medicamentos[,"ATC3"]==""),"ATC3"]="NA"

Medicamentos[which(Medicamentos[,"DESCRIPCION_ATC0"]==""),"DESCRIPCION_ATC0"]="NA"
Medicamentos[which(Medicamentos[,"DESCRIPCION_ATC1"]==""),"DESCRIPCION_ATC1"]="NA"
Medicamentos[which(Medicamentos[,"DESCRIPCION_ATC2"]==""),"DESCRIPCION_ATC2"]="NA"
Medicamentos[which(Medicamentos[,"DESCRIPCION_ATC3"]==""),"DESCRIPCION_ATC3"]="NA"

Medicamentos[which(Medicamentos[,"PRINCIPIO_ACTIVO0"]==""),"PRINCIPIO_ACTIVO0"]="NA"
Medicamentos[which(Medicamentos[,"PRINCIPIO_ACTIVO1"]==""),"PRINCIPIO_ACTIVO1"]="NA"
Medicamentos[which(Medicamentos[,"PRINCIPIO_ACTIVO2"]==""),"PRINCIPIO_ACTIVO2"]="NA"
Medicamentos[which(Medicamentos[,"PRINCIPIO_ACTIVO3"]==""),"PRINCIPIO_ACTIVO3"]="NA"

Medicamentos[which(Medicamentos[,"FECHA_ACTIVO"]==""),"FECHA_ACTIVO"]="NA"
Medicamentos[which(Medicamentos[,"FECHA_INACTIVO"]==""),"FECHA_INACTIVO"]="NA"

Medicamentos[which(Medicamentos[,"FECHA_EXPEDICION"]==""),"FECHA_EXPEDICION"]="NA"
Medicamentos[which(Medicamentos[,"FECHA_VENCIMIENTO"]==""),"FECHA_VENCIMIENTO"]="NA"


write.dta(Medicamentos,file="CreatedData/Medicamentos_Total.dta",version=12)
save(Medicamentos,file="CreatedData/Medicamentos_Total.RData")

                                                                                                                                                                                                  