rm(list=ls())
#setwd("/media/mauricio/TeraHDD2/Dropbox/Research/Health/Medicamentos/Programa")
setwd("C:/Users/Mauricio/Dropbox/Research/Health/Medicamentos/Programa")
load("CreatedData/CodigosINVIMA.RData")

CodigosINVIMA$FECHA_EXPEDICION=as.Date(as.character(CodigosINVIMA$FECHA_EXPEDICION),format="%Y-%m-%d")
CodigosINVIMA$FECHA_VENCIMIENTO=as.Date(as.character(CodigosINVIMA$FECHA_VENCIMIENTO),format="%Y-%m-%d")

CodigosINVIMAUnique=CodigosINVIMA[!duplicated(CodigosINVIMA$EXPEDIENTE),]

MediPeriodo=NULL
CumPeriodo=NULL

MediSIMSED=NULL
CumSIMSED=NULL
for(Periodo in  2006:2016){

MedicamentosPeriodo=read.csv(paste("RawData/",Periodo,".csv",sep=""))
MediSIMSED[Periodo-2005]=dim(MedicamentosPeriodo)[1]
MediPeriodo[Periodo-2005]=length(which(as.numeric(format(CodigosINVIMA$FECHA_EXPEDICION,"%Y"))<=Periodo & as.numeric(format(CodigosINVIMA$FECHA_VENCIMIENTO,"%Y"))>=Periodo))

CumSIMSED[Periodo-2005]=length(unique(MedicamentosPeriodo$CUM1))
CumPeriodo[Periodo-2005]=length(which(as.numeric(format(CodigosINVIMAUnique$FECHA_EXPEDICION,"%Y"))<=Periodo & as.numeric(format(CodigosINVIMAUnique$FECHA_VENCIMIENTO,"%Y"))>=Periodo))
}

xtable(rbind(2006:2014,round((CumSIMSED/CumPeriodo),2)))

