

setwd("E:/Copy/Research/Health/Medicamentos")

listaReportes=c("201201a201209","201201a201212","201301a201303","201304a201306","201307a201309","201310a201312","201301a201312")
listaReportes=c("201201a201212","201301a201312")

Medicamentos=NULL
for(Periodo in  listaReportes){
MedicamentosPeriodo=read.csv(paste("data/sismed/Modi/Publicacion_PreciosReportados_",Periodo,".csv",sep=""))
MedicamentosPeriodo$Fecha=Periodo
Medicamentos=rbind(Medicamentos,MedicamentosPeriodo)
}



CodigosATC=unique(as.character(Medicamentos$Codigo.ATC))
 CodigosATC=strsplit(CodigosATC, "//")
  CodigosATC=unlist(CodigosATC)
 CodigosATC=gsub(" ", "", CodigosATC)
 CodigosATC=unique(CodigosATC)

 CodVig=read.csv(paste("data/sismed/Modi/Listado Codigo Unico Vigentes -Mzo162014.csv",sep=""))
CodigosATCVigentes=unique(as.character(CodVig$ATC))

 CodVig=read.csv(paste("data/sismed/Modi/Listado Codigo Unico Renovacion_ mzo162014.csv",sep=""))
CodigosATCRenov=unique(as.character(CodVig$ATC))

 CodVig=read.csv(paste("data/sismed/Modi/Listado Codigo Unico Otros Estados_mzo162014.csv",sep=""))
CodigosATCOtros=unique(as.character(CodVig$ATC))

 CodVig=read.csv(paste("data/sismed/Modi/Listado Codigo Unico Vencido_mzo162014.csv",sep=""))
CodigosATCVencs=unique(as.character(CodVig$ATC))


 ATC5521=read.csv(paste("data/ATC5521.csv",sep=""))
 ATC5521=as.character(ATC5521$COD_ATC)
 ATC5521=substr(ATC5521, 1, 7)
 
 
 
 write.csv(ATC5521,file="data/sismed/Modi/ATC5521.csv")
 write.csv(union(ATC5521,CodigosATC),file="data/sismed/Modi/ATC_SISMED.csv")
 write.csv(union(union(union(ATC5521,CodigosATC),union(CodigosATCVigentes,CodigosATCRenov)),union(CodigosATCOtros,CodigosATCVencs)),file="data/sismed/Modi/ATC_INVIMA.csv")

 

 