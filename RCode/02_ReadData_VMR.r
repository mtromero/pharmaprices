
rm(list=ls())
setwd("/media/mauricio/TeraHDD2/Dropbox/Research/Health/Medicamentos/Programa")
#setwd("C:/Users/Mauricio/Dropbox/Research/Health/Medicamentos/Programa")

VMR_CNPM_2010_C004=NULL
VMR_CNPM_2010_C004$MediID[1]="19956000 1"
VMR_CNPM_2010_C004$MediID[2]="204751 2"
VMR_CNPM_2010_C004$MediID[3]="216049 2"
VMR_CNPM_2010_C004$MediID[4]="58875 1"
VMR_CNPM_2010_C004$MediID[5]="226777 1"
VMR_CNPM_2010_C004$MediID[6]="226777 2"
VMR_CNPM_2010_C004$MediID[7]="19903070 1"
VMR_CNPM_2010_C004$MediID[8]="46041 1"
VMR_CNPM_2010_C004$MediID[9]="19932792 1"
VMR_CNPM_2010_C004$MediID[10]="19932793 1"
VMR_CNPM_2010_C004$MediID[11]="200666 1"
VMR_CNPM_2010_C004$MediID[12]="19927730 1"
VMR_CNPM_2010_C004$MediID[13]="19939766 1"
VMR_CNPM_2010_C004$MediID[14]="19905280 1"
VMR_CNPM_2010_C004$MediID[15]="19901547 1"
VMR_CNPM_2010_C004$MediID[16]="19978839 5"
VMR_CNPM_2010_C004$MediID[17]="19978841 2"
VMR_CNPM_2010_C004$MediID[18]="19968208 2"
VMR_CNPM_2010_C004$MediID[19]="225056 1"
VMR_CNPM_2010_C004$MediID[20]="230435 1"
VMR_CNPM_2010_C004$MediID[21]="19909460 1"
VMR_CNPM_2010_C004$MediID[22]="19988005 1"
VMR_CNPM_2010_C004$MediID[23]="19951125 1"
VMR_CNPM_2010_C004$MediID[24]="19951126 1"
VMR_CNPM_2010_C004$MediID[25]="19979757 1"
VMR_CNPM_2010_C004$MediID[26]="19961931 1"


load("CreatedData/CodigosINVIMA.RData")

VMR_2010_5229=read.csv("RawData/VMR/2010_5229.csv", encoding="latin1")
VMR_2011_005=read.csv("RawData/VMR/2011_005.csv", encoding="latin1")
VMR_2011_1020=read.csv("RawData/VMR/2011_1020.csv", encoding="latin1")
VMR_2011_1697=read.csv("RawData/VMR/2011_1697.csv", encoding="latin1")
VMR_2011_3026=read.csv("RawData/VMR/2011_3026.csv", encoding="latin1")
VMR_2011_3470=read.csv("RawData/VMR/2011_3470.csv", encoding="latin1")
VMR_2011_4316=read.csv("RawData/VMR/2011_4316.csv", encoding="latin1")
VMR_2012_2569=read.csv("RawData/VMR/2012_2569.csv", encoding="latin1")
CNPM_2011_C001=read.csv("RawData/VMR/CNPM_2011_C001.csv", encoding="latin1")
CNPM_2012_C001=read.csv("RawData/VMR/CNPM_2012_C001.csv", encoding="latin1")
#CNPM_2012_C001_NOPOS=read.csv("RawData/VMR/CNPM_2012_C001_NOPOS.csv")
#CNPM_2012_C001_POS=read.csv("RawData/VMR/CNPM_2012_C001_POS.csv")
CNPM_2012_C003=read.csv("RawData/VMR/CNPM_2012_C003.csv", encoding="latin1")
CNPM_2012_C004_POS=read.csv("RawData/VMR/CNPM_2012_C004_POS.csv", encoding="latin1")
CNPM_2013_C004_NOPOS=read.csv("RawData/VMR/CNPM_2013_C004_NOPOS.csv", encoding="latin1")
CNPM_2013_C004_POS=read.csv("RawData/VMR/CNPM_2013_C004_POS.csv", encoding="latin1")
CNPM_2013_C005_NOPOS=read.csv("RawData/VMR/CNPM_2013_C005_NOPOS.csv", encoding="latin1")
CNPM_2013_C005_POS=read.csv("RawData/VMR/CNPM_2013_C005_POS.csv", encoding="latin1")
CNPM_2013_C006_NOPOS=read.csv("RawData/VMR/CNPM_2013_C006_NOPOS.csv", encoding="latin1")
CNPM_2013_C007_VMR=read.csv("RawData/VMR/CNPM_2013_C007_VMR.csv", encoding="latin1")
CNPM_2013_C007_PVM=read.csv("RawData/VMR/CNPM_2013_C007_PVM.csv", encoding="latin1")
CNPM_2014_C001=read.csv("RawData/VMR/CNPM_2014_C001.csv", encoding="latin1")
CNPM_2016_C001=read.csv("RawData/VMR/CNPM_2016_C001.csv", encoding="latin1")







NOMBRES2=setdiff(VMR_2011_1020[,1], VMR_2011_005[,1])
NOMBRES1=setdiff(VMR_2011_3026[,1], VMR_2011_1697[,1])



Anexo25_2012=read.csv("RawData/VMR/Anexo25_2012.csv")
Anexo25_2012$MediID=as.character(paste(as.character(Anexo25_2012$Expediente),as.character(Anexo25_2012$Consecutivo)))
colnames(Anexo25_2012)=toupper(colnames(Anexo25_2012))
Anexo25_2012=subset(Anexo25_2012,select=-c(EXPEDIENTE_CONSECUTIVO,CUM,ATC2))

Anexo25_2012$PRINCIPIOACTIVO=iconv(Anexo25_2012$PRINCIPIOACTIVO, to='ASCII//TRANSLIT')
Anexo25_2012$DESCRIP_ATC1=iconv(Anexo25_2012$DESCRIP_ATC1, to='ASCII//TRANSLIT')
Anexo25_2012$VIAADMINISTRACION=iconv(Anexo25_2012$VIAADMINISTRACION, to='ASCII//TRANSLIT') 
Anexo25_2012$FORMAFARMACEUTICA=iconv(Anexo25_2012$FORMAFARMACEUTICA, to='ASCII//TRANSLIT') 
Anexo25_2012$PRESENTACIONCOMERCIAL=iconv(Anexo25_2012$PRESENTACIONCOMERCIAL, to='ASCII//TRANSLIT') 
Anexo25_2012$NOMBREPRODUCTO=iconv(Anexo25_2012$NOMBREPRODUCTO, to='ASCII//TRANSLIT') 
ColumnasInvima=c("EXPEDIENTE","CONSECUTIVO","PRODUCTO","PRESENTACION_COMERCIAL","FORMA_FARMACEUTICA","VIA_ADMINISTRACION","ATC","DESCRIPCION_ATC","PRINCIPIO_ACTIVO","CANTIDAD_DOSIS","UNIDAD_MEDIDA","MediID")
colnames(Anexo25_2012)=ColumnasInvima


#que need to modify it a little bit, but not much
VMR_2011_3026=Anexo25_2012
#remove
VMR_2011_3026=VMR_2011_3026[-grepl("CLOPIDOGREL",VMR_2011_3026$DESCRIPCION_ATC),]
#VMR_2011_3026=VMR_2011_3026[-grepl("DOXORUBICINA",VMR_2011_3026$DESCRIP_ATC1),] #aparece tambien
#VMR_2011_3026=VMR_2011_3026[-grepl("M05BA02",VMR_2011_3026$ATC1),] #no hay ninguni acido CLODRONICO
#VMR_2011_3026=VMR_2011_3026[-grepl("ALANTAMINA",VMR_2011_3026$DESCRIP_ATC1),] #creo que es lo mismo que galamantina
#add
VMR_2011_3026=rbind(VMR_2011_3026,CodigosINVIMA[which(grepl("INSULINA \\(HUMANA\\)",CodigosINVIMA$DESCRIPCION_ATC)  & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])

#Lets start 1697 with 3026
VMR_2011_1697=VMR_2011_3026
#we only need to remove stuff
NOMBRES1=NOMBRES1[-c(1:2,7,10,62)]
NOMBRES1[which(NOMBRES1=="TENECTEPLASE")]="TENECTEPLASA"
NOMBRES1[which(NOMBRES1=="IVABRADINA")]="TRIMETAZIDINA"
NOMBRES1[which(NOMBRES1=="DORIPENEM")]="IMIPENEM E INHIBIDOR DE ENZIMA"
NOMBRES1[which(NOMBRES1=="ENFUVIRTIDA")]="IOXITALAMICO ACIDO"
NOMBRES1[which(NOMBRES1=="IDARUBICINA")]="IDARRUBICINA"
NOMBRES1[which(NOMBRES1=="LEFLUNOMIDA")]="LEFLUNOMIDE"
NOMBRES1[which(NOMBRES1=="ACIDO IBANDRONICO ")]="IBANDRONICO ACIDO"
NOMBRES1[which(NOMBRES1=="ACIDO RISEDRONICO")]="RISEDRONICO ?ACIDO"
NOMBRES1[which(NOMBRES1=="ACIDO ZOLEDRONICO ")]="ZOLEDRONICO ACIDO"
NOMBRES1[which(NOMBRES1=="DONEPECILO CLORHIDRATO")]="DONEPEZIL"
NOMBRES1[which(NOMBRES1=="BROMURO DE TIOTROPIO")]="TIOTROPIO BROMURO"
NOMBRES1[which(NOMBRES1=="OXIDO NITRICO")]="NITRICO OXIDO"

for(nombre in NOMBRES1[1:13]){
index=which(grepl(nombre,VMR_2011_1697$DESCRIPCION_ATC))
print(length(index))
if(length(index)>0) VMR_2011_1697=VMR_2011_1697[-index,]
}
#there was no change
VMR_2011_1020=VMR_2011_1697

#need to remove some
VMR_2011_005=VMR_2011_1020
NOMBRES2[which(NOMBRES2=="TEMOZOLAMIDA")]="TEMOZOLOMIDA"
NOMBRES2[which(NOMBRES2=="RANIBIZUMAB")]="IOXITALAMICO ACIDO"
NOMBRES2[which(NOMBRES2=="ACETATO DE LEUPROLIDE")]="LEUPRORELINA ACETATO"
NOMBRES2[which(NOMBRES2=="ATORVASTATINA")]="ATORVASTATIN"
NOMBRES2[which(NOMBRES2=="ATORVASTATINA EN COMBINACI?N")]="ATORVASTATIN"
NOMBRES2[which(NOMBRES2=="DORNASA")]="DORNASE ALFA"
NOMBRES2[which(NOMBRES2=="LANREOTIDE")]="LANREOTIDO"
NOMBRES2[which(NOMBRES2=="GOSERELINA")]="GOSRELIN"

for(nombre in NOMBRES2){
index=which(grepl(nombre,VMR_2011_005$DESCRIPCION_ATC))
if(length(index)>0) VMR_2011_005=VMR_2011_005[-index,]
}

#they are the same
VMR_2010_5229=VMR_2011_005

#3470 has the exact same ones as  4316, which is the same as the appendix ( Anexo25_2012)
VMR_2011_3470=Anexo25_2012
#aT THE END OF 2011 THIS IS WHATS RELEVANT AS THE INFO IS FROM FEB 2012, WHICH APPLIES TO 2011
VMR_2011_4316=Anexo25_2012

#que need to modify it a little bit, but not much
VMR_2012_2569=Anexo25_2012
#WE NEED TO ADD SOME PHARMACEUTICALS and remove
VMR_2012_2569=VMR_2012_2569[-grepl("BOSENTAN",VMR_2012_2569$DESCRIPCION_ATC),]
VMR_2012_2569=VMR_2012_2569[-grepl("PEGFILGRASTIM",VMR_2012_2569$DESCRIPCION_ATC),]
VMR_2012_2569=VMR_2012_2569[-grepl("ESOMEPRAZOL",VMR_2012_2569$DESCRIPCION_ATC),]
VMR_2012_2569=VMR_2012_2569[-grepl("INSULINA LISPRO",VMR_2012_2569$DESCRIPCION_ATC),]
VMR_2012_2569=VMR_2012_2569[-grepl("NOREPINEFRINA",VMR_2012_2569$DESCRIPCION_ATC),]
VMR_2012_2569=VMR_2012_2569[-grepl("MILRINONA",VMR_2012_2569$DESCRIPCION_ATC),]
VMR_2012_2569=VMR_2012_2569[-grepl("CEFEPIMA",VMR_2012_2569$DESCRIPCION_ATC),]
VMR_2012_2569=VMR_2012_2569[-grepl("CASPOFUNGIN",VMR_2012_2569$DESCRIPCION_ATC),]
VMR_2012_2569=VMR_2012_2569[-grepl("ATAZANAVIR",VMR_2012_2569$DESCRIPCION_ATC),]
VMR_2012_2569=VMR_2012_2569[-grepl("SERTRALINA",VMR_2012_2569$DESCRIPCION_ATC),]
# entra

VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("OMEPRAZOL",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("DABIGATRAN ETEXILATO",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(( grepl("B02BD03",CodigosINVIMA$ATC)) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA) )),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("ERITROPOYETINA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("AMBRISENTAN",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("METOPROLOL",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("CARVEDILOL",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("IRBESARTAN",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima] )
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("TOLTERODINA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima]  )
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("SOMATOSTATINA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima] )
#Son nuevos y poco uisados... mejor buscar por principio activo
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which((grepl("TERIPARATIDE",CodigosINVIMA$PRINCIPIO_ACTIVO) | grepl("H05AA02",CodigosINVIMA$ATC)) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima] )
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which((grepl("ERTAPENEM",CodigosINVIMA$PRINCIPIO_ACTIVO) | grepl("J01DH03",CodigosINVIMA$ATC)) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
#Esta en todas las formas
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("ITRACONAZOL",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("ITRACONAZOL",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("SOLUCION ORAL",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("ITRACONAZOL",CodigosINVIMA$DESCRIPCION_ATC)  & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
#back to normal
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("RALTEGRAVIR",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("PEMETREXED",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima] )
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("CLOFARABINA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima]   )
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("DECITABINA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima]   )
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("NILOTINIB",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima] )
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("VORINOSTAT",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima] )
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("TRIPTORELIN",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("FLUVESTRANT",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima]  )
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("ANASTROZOL",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("FILGRASTIM",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("ANTITIMOCITO IMMUNOGLOBULINA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("NATALIZUMAB",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("CERTOLIZUMAB PEGOL",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("USTEKINUMAB",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("FENTANYL",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("PARCHE",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which((grepl("NARATRIPTAN",CodigosINVIMA$DESCRIPCION_ATC) | grepl("N02CC02",CodigosINVIMA$ATC)) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which((grepl("CARBAMAZEPINA",CodigosINVIMA$DESCRIPCION_ATC)) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SUSPENSION ORAL",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which((grepl("VALPROICO ACIDO",CodigosINVIMA$DESCRIPCION_ATC) | grepl("N03AG01",CodigosINVIMA$ATC)) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("GRANULOS",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("VIGABATRINA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("CLOBAZAM",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("VENLAFAXINA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("DESVENLAFAXINA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("ATOMOXETINA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("CAFEINA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("LIOFILIZADO",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("SOLUCION INYECTABLE",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("RILUZOL",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("CAPSULA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA) | grepl("TABLETA",CodigosINVIMA$FORMA_FARMACEUTICA))),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("MOMETASONA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("INHALACION",CodigosINVIMA$FORMA_FARMACEUTICA) )),ColumnasInvima])
VMR_2012_2569=rbind(VMR_2012_2569,CodigosINVIMA[which(grepl("DORZOLAMIDA",CodigosINVIMA$DESCRIPCION_ATC) & (grepl("GOTAS",CodigosINVIMA$FORMA_FARMACEUTICA) )),ColumnasInvima])

#Esta es la  CNPM_2012_C004
VMR_2012_CNPM_004=VMR_2012_2569
VMR_2012_CNPM_004=VMR_2012_CNPM_004[-grepl("TENECTEPLASA",VMR_2012_CNPM_004$DESCRIPCION_ATC),]
VMR_2012_CNPM_004=VMR_2012_CNPM_004[-grepl("CLOBAZAM",VMR_2012_CNPM_004$DESCRIPCION_ATC),]


CNPM_2011_C001$MediID=gsub("-"," ",as.character(CNPM_2011_C001$MediID))
CNPM_2012_C001$MediID=gsub("-"," ",as.character(CNPM_2012_C001$MediID))
CNPM_2012_C003$MediID=gsub("-"," ",as.character(CNPM_2012_C003$MediID))

CNPM_2013_C004_NOPOS$MediID=gsub("-"," ",as.character(CNPM_2013_C004_NOPOS$MediID))
CNPM_2013_C004_POS$MediID=gsub("-"," ",as.character(CNPM_2013_C004_POS$MediID))
CNPM_2013_C005_POS$MediID=gsub("-"," ",as.character(CNPM_2013_C005_POS$MediID))
CNPM_2013_C005_NOPOS$MediID=gsub("-"," ",as.character(CNPM_2013_C005_NOPOS$MediID))
CNPM_2013_C006_NOPOS$MediID=gsub("-"," ",as.character(CNPM_2013_C006_NOPOS$MediID))
CNPM_2013_C007_PVM$MediID=gsub("-"," ",as.character(CNPM_2013_C007_PVM$MediID))
CNPM_2013_C007_VMR$MediID=gsub("-"," ",as.character(CNPM_2013_C007_VMR$MediID))
CNPM_2014_C001$MediID=gsub("-"," ",as.character(CNPM_2014_C001$MediID))
CNPM_2016_C001$MediID=gsub("-"," ",as.character(CNPM_2016_C001$MediID))

save(VMR_CNPM_2010_C004,VMR_2010_5229,VMR_2011_005,VMR_2011_1020,
     VMR_2011_1697,VMR_2011_3026,VMR_2011_3470,VMR_2011_4316,
     CNPM_2011_C001,VMR_2012_2569,
     CNPM_2012_C001,
     CNPM_2012_C003,
     CNPM_2012_C004_POS,
     CNPM_2013_C004_NOPOS,CNPM_2013_C004_POS,
     CNPM_2013_C005_POS,CNPM_2013_C005_NOPOS,
     CNPM_2013_C006_NOPOS,
     CNPM_2013_C007_PVM,CNPM_2013_C007_VMR,
     CNPM_2014_C001,CNPM_2016_C001,
     file="CreatedData/VMR_TODOS.RData")



# VMR_CNPM_2010_C004: 1 Junio 2010 VMR
# VMR_2010_5229: 14 Dic 2010 VMR
# VMR_2011_005: 11 Enero 2011 VMR
# VMR_2011_1020: 31 Marzo 2011 VMR
# VMR_2011_1697: 18 Mayo 2011 VMR
# VMR_2011_3026: 30 Junio 2011 VMR
# VMR_2011_3470: 18 Agosto 2011 VMR
# VMR_2011_4316: 27 Sept 2011 VMR
# CNPM_2011_C001: 30 Dic 2011 (PMV: Solo Venta Publico y dentro del POS)
# VMR_2012_2569: 30 Agost 2012 VMR
# CNPM_2012_C001: 4 Sept 2012 #Solo anade... no se quita nada   (PMV: Solo Venta Publico y dentro del POS)
# CNPM_2012_C003: 8 Nov 2012  #modifica un poquito que esta en q PVM
# CNPM_2012_C004: 8 Nov 2012   #MNODIFICA PVM
# CNPM_2013_C004: 13 Mayo 2013 #both PVM and VMR
# CNPM_2013_C005:   16 de Sep de 2013 #modifica la 004  #both PVM and VMR
# CNPM_2013_C006: 8 Oct 2013    #mete un solo medicamento
# CNPM_2013_C007: 20 Dic 2013 tanto PMV como VMR
# CNPM_2014_C001: 2 de april de 2014, mas PMV
# CNPM_2014_C002: Borrador no mas
# CNPM_2015_C003:  Borrador no mas
# RES_2015_0718:  11 marzo de 2015 solo cambia el PRECIO usando IPC no cambia medicamentos
# CNPM_2016_C001: 1 junio de 2016 ajuste de ipc otra vez
# CNPM_2016_C004: 22 dic de 2016 cambio de imatinab de novartis i.e. glivec

#Para el CNPM_2012_C001

#A=read.table("clipboard",sep="\t",quote="")
#VectResult=NULL
#for(i in 1:dim(A)[1]){
#White=as.numeric(gregexpr(" ", as.character(A[i,]))[[1]])
#Guion=as.numeric(regexpr("-", as.character(A[i,]))[[1]])
#
#VectResult=c(VectResult, substr(as.character(A[i,]),White[max(which(White<Guion[1]))]+1,White[min(which(White>Guion[1]))]-1 ))
#}
#VectResult=gsub("S", "5", VectResult)
#VectResult=gsub("O", "0", VectResult)
#VectResult=VectResult[!is.na(VectResult)]
#write.csv(VectResult,"file1.csv") 
