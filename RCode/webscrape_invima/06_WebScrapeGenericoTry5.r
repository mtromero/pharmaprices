﻿rm(list=ls())
setwd("E:/Copy/Research/Health/Medicamentos/Programa")
setwd("C:/Users/Mauricio/Copy/Research/Health/Medicamentos/Programa")
setwd("H:/Copy/Research/Health/Medicamentos/Programa")
fun_tabla=function(x) return("Rol" %in% names(try(readHTMLTable(appData, which = x,header=T),silent=T)))
require(RSelenium)
RSelenium::startServer()
RSelenium::checkForServer()
#pJS <- phantom()
#Sys.sleep(5) # give the binary a moment

#load(file="CreatedData/CodigosINVIMA.RData")
#ListCUM1=unique(CodigosINVIMA$EXPEDIENTE)
#Data=data.frame(ListCUM1,as.numeric(rep(NA,length(ListCUM1))),rep("",length(ListCUM1)),as.numeric(rep(NA,length(ListCUM1))),rep("",length(ListCUM1)),rep("",length(ListCUM1)),as.numeric(rep(NA,length(ListCUM1))),rep("",length(ListCUM1)),stringsAsFactors =F)
#colnames(Data)=c("CUM1","Generico","Franja","Inserto","CondicionVenta","TipoIDRegistro","IDRegistro","NombreRegistro") 
#rm(CodigosINVIMA)

load("CreatedData/GenericoData2.RData")
ListCUM1=Data$CUM1[is.na(Data$Generico)]
appURL <- "http://farmacovigilancia.invima.gov.co:8082/Consultas/consultas/consreg_encabcum.jsp"
#eCap <- list(phantomjs.page.settings.userAgent 
#         = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20120101 Firefox/29.0")
#, extraCapabilities = eCap
remDr <- remoteDriver(browserName = "chrome")
remDr$open()
  remDr$setImplicitWaitTimeout(100*10000)
  remDr$setTimeout(type = "page load", milliseconds = 100*10000)
  remDr$setAsyncScriptTimeout(milliseconds = 100*10000)
remDr$navigate(appURL)


for(cum in rev(ListCUM1)){
    if(is.na(cum)==F){
    Sys.sleep(2) # give the binary a moment
  paso=0
    print(cum)
    remDr$refresh()
    Sys.sleep(1) # give the binary a moment
    # Get the third list item of the select box (MEDICAMENTOS)
    if(length(getXMLErrors(remDr$getPageSource()[[1]]))==0){
      paso="a"
        if(length(htmlTreeParse(remDr$getPageSource()[[1]])[[1]][[2]])>1){
        paso=1
          NombreAtributo=htmlTreeParse(remDr$getPageSource()[[1]])[[1]][[2]][[5]][[3]][[1]][[1]][[1]][[1]][[1]][[1]][[1]][[1]][[1]]$value
          while(identical(NombreAtributo,"Grupo:Â  Error: No se pueden consultar grupos") | length(getXMLErrors(remDr$getPageSource()[[1]]))>0){
             paso="1a"
             remDr$refresh()
             Sys.sleep(1) # give the binary a moment
             NombreAtributo=htmlTreeParse(remDr$getPageSource()[[1]])[[1]][[2]][[5]][[3]][[1]][[1]][[1]][[1]][[1]][[1]][[1]][[1]][[1]]$value
             Sys.sleep(1) # give the binary a moment
             paso="1b"
          }
          paso=2
        #    #for to refresh page until it works!
        #    while(inherits(A, "try-error")==T){
        #    remDr$refresh()
        #    A=try(remDr$findElement("css", "select[name='grupo'] option:nth-child(3)")$clickElement(),silent=T)
        #    }#close while to regresh until page works!
          
          remDr$findElement("css", "select[name='grupo'] option:nth-child(3)")$clickElement()
          paso=3
          
          # Send text to input value="" name="expediente
          remDr$findElement("css", "input[name='expediente']")$sendKeysToElement(list(as.character(cum)))
          paso=4
          
          # Click the Buscar button
          remDr$findElement("id", "INPUT2")$clickElement()
          paso=5
          Sys.sleep(1) # give the binary a moment
          remDr$switchToFrame(remDr$findElement("css", "iframe[name='datos']"))
          paso=6
          
          #Whats the number of items found. it must be 1
          Sys.sleep(2) # give the binary a moment
          if(length(htmlTreeParse(remDr$getPageSource()[[1]])[[1]][[2]][[1]][[2]])>0){ #tiene que haber algo que buscar!
          paso=7
            NumRegistros=as.numeric(htmlTreeParse(remDr$getPageSource()[[1]])[[1]][[2]][[1]][[2]][[1]]$value)
            print(paste( "Este CUM",cum,"tiene",NumRegistros,"registros"))
            if(identical(NumRegistros,2)){
             Data[which(Data$CUM1==cum),2]=2
            }
            if(identical(NumRegistros,1)){
            paso=8
                Sys.sleep(1) # give the binary a moment
                A=try(remDr$findElement("css", "a")$clickElement(),silent=T) # click the link given in the iframe
                Sys.sleep(2) # give the binary a moment
                #Only continue if we found the link
                if(inherits(A, "try-error")!=T){
                paso=9  
                  # get the resulting data
                  appData <- remDr$getPageSource()[[1]]
                  urlParse=htmlTreeParse(appData)
                  root=xmlRoot(urlParse)
                  child = xmlChildren(root)
                  resultados=child[[2]]
                  resultados2=resultados[[1]]
                  paso=10
                  if(identical(resultados2[[4]]$value,"Error: Se ha generado un error en consulta")==F){
                    if((length(resultados2[[4]][[1]]$value)>0 & identical(resultados2[[4]][[1]]$value,"Medida Cautelar"))){
                    resultados3=try(resultados2[[9]],silent=T)
                    }
                    else if(is.null(resultados2[[4]][[1]]$value)){
                    resultados3=try(resultados2[[7]],silent=T)
                    }
                    else{next}
                    paso=11
                    if(inherits(resultados3, "try-error")!=T){
                    paso=12
            
                        resultados4=resultados3[[1]]
                        if(length(resultados4)>1){
                        paso=13
                        
                          if(sum(names(xmlAttrs(resultados4[[4]][[6]][[2]]))=="checked")==1)  Data[which(Data$CUM1==cum),2]=1
                          else if(sum(names(xmlAttrs(resultados4[[4]][[6]][[2]]))=="checked")==0)  Data[which(Data$CUM1==cum),2]=0
                          else if(sum(names(xmlAttrs(resultados4[[4]][[6]][[2]]))=="checked")>1)  Data[which(Data$CUM1==cum),2]=2
                          
                          
                          Franja=try(resultados4[[1]][[4]][[1]]$value,silent=T)
                          if(inherits(Franja, "try-error")==T){
                            Franja=""
                          }
                          Data[which(Data$CUM1==cum),3]=Franja
                          
                          
                          
                          if(sum(names(xmlAttrs(resultados4[[4]][[1]][[2]]))=="checked")==1)  Data[which(Data$CUM1==cum),4]=1
                          else if(sum(names(xmlAttrs(resultados4[[4]][[1]][[2]]))=="checked")==0)  Data[which(Data$CUM1==cum),4]=0
                          else if(sum(names(xmlAttrs(resultados4[[4]][[1]][[2]]))=="checked")>1)  Data[which(Data$CUM1==cum),4]=2
                          
                          Res_Venta= try(resultados4[[4]][[5]][[1]]$value,silent=T)
                            if(inherits(Res_Venta, "try-error")==T){
                          Res_Venta=""
                          }
                          Data[which(Data$CUM1==cum),5]=Res_Venta
                          
                          #Read the apporpiate tabla de roles 
                          CualEsLaTabla=which(sapply(1:15,fun_tabla)==T)
                          if(length(CualEsLaTabla==1)){
                            tablaROLES=readHTMLTable(appData, which =CualEsLaTabla)
                            INDEX_TABLA=which(tablaROLES$V1=="TITULAR REGISTRO SANITARIO")
                            if(length(INDEX_TABLA)==1) {#stop("This is weird-check it out")
                            Data[which(Data$CUM1==cum),6]=as.character(tablaROLES$V2[INDEX_TABLA])
                            Data[which(Data$CUM1==cum),7]=as.numeric(as.character(tablaROLES$V3[INDEX_TABLA]))
                            Data[which(Data$CUM1==cum),8]=as.character(tablaROLES$V4[INDEX_TABLA])
                            }
                            if(length(INDEX_TABLA)!=1) {#stop("This is weird-check it out")
                            next
                            }
                          }
                          
                          #remDr$findElement("id", "INPUT3")$clickElement()
                          print(100*which(Data$CUM1==cum)/length(Data$CUM1))
                          #remDr$switchToFrame(remDr$findElement("id", "TABLE1"))
                          #  remDr$findElement("id", "INPUT3")$clickElement()
                          # print(cum)
                          
                          save(Data,file="CreatedData/GenericoData2.RData")
                      }#Close que haya info
                  }#Close appropiate number
                }#$Close if of finding the link
              }#se ha generado un error de consulta
            }#Close if he found only 1 item that matches
          } #cerra que haya encontrado algo
        }#quie no sea error 
      }#que no sea 404
      }
}#Close the for

# remDr$close()
# pJS$stop()
#

