rm(list=ls())
setwd("E:/Copy/Research/Health/Medicamentos/Programa")
setwd("C:/Users/Mauricio/Copy/Research/Health/Medicamentos/Programa")
setwd("H:/Copy/Research/Health/Medicamentos/Programa")

require(RSelenium)
RSelenium::checkForServer()
#pJS <- phantom()
#Sys.sleep(5) # give the binary a moment

#load(file="CreatedData/CodigosINVIMA.RData")
#ListCUM1=unique(CodigosINVIMA$EXPEDIENTE)
#Data=data.frame(ListCUM1,as.numeric(rep(NA,length(ListCUM1))),rep("",length(ListCUM1)),as.numeric(rep(NA,length(ListCUM1))),rep("",length(ListCUM1)),stringsAsFactors =F)
#colnames(Data)=c("CUM1","Generico","Franja","Inserto","CondicionVenta")
#
#rm(CodigosINVIMA)

load("CreatedData/GenericoData.RData")
ListCUM1=Data$CUM1[is.na(Data$Generico)]
appURL <- "http://web.sivicos.gov.co:8080/consultas/consultas/consreg_encabcum.jsp"
  remDr <- remoteDriver(browserName = "chrome")
  remDr$open()
        remDr$setImplicitWaitTimeout(15*10000)
        remDr$setTimeout(type = "page load", milliseconds = 15*10000)
    remDr$navigate(appURL)
#cum=2202    
 for(cum in ListCUM1){
 print(cum)
   remDr$refresh()
  # Get the third list item of the select box (MEDICAMENTOS)
  A=try(webElem <- remDr$findElement("css", "select[name='grupo'] option:nth-child(3)")$clickElement(),silent=T)
  while(inherits(A, "try-error")==T){
  remDr$refresh()
  A=try(webElem <- remDr$findElement("css", "select[name='grupo'] option:nth-child(3)")$clickElement(),silent=T)
  }
  
  
  # Send text to input value="" name="expediente
  remDr$findElement("css", "input[name='expediente']")$sendKeysToElement(list(as.character(cum)))

  # Click the Buscar button
  remDr$findElement("id", "INPUT2")$clickElement()
Sys.sleep(1) # give the binary a moment
  remDr$switchToFrame(remDr$findElement("css", "iframe[name='datos']"))
  A=try(remDr$findElement("css", "a")$clickElement(),silent=T) # click the link given in the iframe
  if(inherits(A, "try-error")==T){
next
}
 Sys.sleep(1) # give the binary a moment
  # get the resulting data
  appData <- remDr$getPageSource()[[1]]
urlParse=htmlTreeParse(appData)
root=xmlRoot(urlParse)
child = xmlChildren(root)
resultados=child[[2]]
resultados2=resultados[[1]]
resultados3=try(resultados2[[7]],silent=T)
if(inherits(resultados3, "try-error")==T){
next
}
if(length(resultados[[1]][[4]][[1]]$value)>0){if(resultados[[1]][[4]][[1]]$value=="Medida Cautelar") next}
resultados4=resultados3[[1]]

if(sum(names(xmlAttrs(resultados4[[4]][[6]][[2]]))=="checked")==1)  Data[which(Data$CUM1==cum),2]=1
else if(sum(names(xmlAttrs(resultados4[[4]][[6]][[2]]))=="checked")==0)  Data[which(Data$CUM1==cum),2]=0
else if(sum(names(xmlAttrs(resultados4[[4]][[6]][[2]]))=="checked")>1)  Data[which(Data$CUM1==cum),2]=2


Data[which(Data$CUM1==cum),3]= resultados4[[1]][[4]][[1]]$value


if(sum(names(xmlAttrs(resultados4[[4]][[1]][[2]]))=="checked")==1)  Data[which(Data$CUM1==cum),4]=1
else if(sum(names(xmlAttrs(resultados4[[4]][[1]][[2]]))=="checked")==0)  Data[which(Data$CUM1==cum),4]=0
else if(sum(names(xmlAttrs(resultados4[[4]][[1]][[2]]))=="checked")>1)  Data[which(Data$CUM1==cum),4]=2

Data[which(Data$CUM1==cum),5]= resultados4[[4]][[5]][[1]]$value

#remDr$findElement("id", "INPUT3")$clickElement()
print(100*which(Data$CUM1==cum)/length(Data$CUM1))
#remDr$switchToFrame(remDr$findElement("id", "TABLE1"))
#  remDr$findElement("id", "INPUT3")$clickElement()
# print(cum)
save(Data,file="CreatedData/GenericoData.RData")
}

 remDr$close()
# pJS$stop()
#

