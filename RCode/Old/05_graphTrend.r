
rm(list=ls())
setwd("E:/Copy/Research/Health/Medicamentos/Programa")
setwd("C:/Copy/Research/Health/Medicamentos/Programa")
library(foreign)
library(zoo)
library(xtable)
library(stargazer)
library(dummies)
load("CreatedData/Medicamentos_Wide_Long.RData")
MedicamentosL2=MedicamentosL
#MedicamentosL2=MedicamentosL[which(!is.na(MedicamentosL$Institucional_Lab_Prom_STD)),]
MedicamentosL2$Institucional_Lab_Prom_STD=log(MedicamentosL2$Institucional_Lab_Prom_STD)
Medias=aggregate(MedicamentosL2$Institucional_Lab_Prom_STD[complete.cases(MedicamentosL2[,c("Institucional_Lab_Prom_STD")])],by=list( MedicamentosL2$CUM1[complete.cases(MedicamentosL2[,c("Institucional_Lab_Prom_STD")])]),FUN=mean,na.rm=T)
MedicamentosL2$Institucional_Lab_Prom_STD_demean=MedicamentosL2$Institucional_Lab_Prom_STD- Medias[match(MedicamentosL2$CUM1,Medias[,1]),2]
MedicamentosL2=cbind(MedicamentosL2,dummy(MedicamentosL2$Periodo))

NombresPeriodo=which(grepl("MedicamentosL", names(MedicamentosL2)))
Medias=aggregate(MedicamentosL2[complete.cases(MedicamentosL2[,c("Institucional_Lab_Prom_STD")]),NombresPeriodo],by=list( MedicamentosL2$CUM1[complete.cases(MedicamentosL2[,c("Institucional_Lab_Prom_STD")])]),FUN=mean,na.rm=T)
MedicamentosL2[,NombresPeriodo]=MedicamentosL2[,NombresPeriodo]- Medias[match(MedicamentosL2$CUM1,Medias[,1]),-1]

Reg1=lm(Institucional_Lab_Prom_STD_demean~-1+MedicamentosL22008+MedicamentosL22009+MedicamentosL22010+MedicamentosL22011+
MedicamentosL22012+MedicamentosL22013,data=MedicamentosL2)




MedicamentosL2$Error[complete.cases(MedicamentosL2[,c("Institucional_Lab_Prom_STD_demean",names(MedicamentosL2)[NombresPeriodo])])]=Reg1$residuals

StatusQ=quantile(MedicamentosL2$Error[which(MedicamentosL2$Grupo!=0)],probs=c(0.025,0.05,0.95,0.975),na.rm=T)
StatusA=quantile(MedicamentosL2$Error[which(MedicamentosL2$Grupo==1)],probs=c(0.025,0.05,0.95,0.975),na.rm=T)
StatusN=quantile(MedicamentosL2$Error[which(MedicamentosL2$Grupo==2)],probs=c(0.025,0.05,0.95,0.975),na.rm=T)


ERORES_LAG1=NULL
ERORES_LAG2=NULL
ERORES_LAG3=NULL
ERORES_LAG4=NULL
ERORES_FWD1=NULL
ERORES_FWD2=NULL
ERORES_FWD3=NULL
ERORES_FWD4=NULL
for(cum in unique(MedicamentosL2$CUM1)){
MediCum=MedicamentosL2[which(MedicamentosL2$CUM1==cum),]
SeriePOS=MediCum$POS
if(mean(SeriePOS,na.rm=T)!=0 & mean(SeriePOS,na.rm=T)!=1){
  for(i in 2:length(SeriePOS)){
  if(isTRUE(all.equal(SeriePOS[i],1)) & isTRUE(all.equal(SeriePOS[i-1],0))) ERORES_LAG1=c(ERORES_LAG1,MediCum$Error[i])
  }
  for(i in 3:length(SeriePOS)){
  if(isTRUE(all.equal(SeriePOS[i],1)) & isTRUE(all.equal(SeriePOS[i-1],0)) & isTRUE(all.equal(SeriePOS[i-2],0))) ERORES_LAG2=c(ERORES_LAG2,MediCum$Error[i])
  }
  for(i in 4:length(SeriePOS)){
  if(isTRUE(all.equal(SeriePOS[i],1)) & isTRUE(all.equal(SeriePOS[i-1],0)) & isTRUE(all.equal(SeriePOS[i-2],0)) & isTRUE(all.equal(SeriePOS[i-3],0))) ERORES_LAG3=c(ERORES_LAG3,MediCum$Error[i])
  }
  for(i in 5:length(SeriePOS)){
  if(isTRUE(all.equal(SeriePOS[i],1)) & isTRUE(all.equal(SeriePOS[i-1],0)) & isTRUE(all.equal(SeriePOS[i-2],0)) & isTRUE(all.equal(SeriePOS[i-3],0)) & isTRUE(all.equal(SeriePOS[i-4],0))) ERORES_LAG4=c(ERORES_LAG4,MediCum$Error[i])
  }
  for(i in 1:(length(SeriePOS)-1)){
  if(isTRUE(all.equal(SeriePOS[i],1)) & isTRUE(all.equal(SeriePOS[i+1],0))) ERORES_FWD1=c(ERORES_LAG1,MediCum$Error[i+1])
  }
  for(i in 1:(length(SeriePOS)-2)){
  if(isTRUE(all.equal(SeriePOS[i],1)) & isTRUE(all.equal(SeriePOS[i+1],0)) & isTRUE(all.equal(SeriePOS[i+2],0))) ERORES_FWD2=c(ERORES_LAG2,MediCum$Error[i+1])
  }
  for(i in 1:(length(SeriePOS)-3)){
  if(isTRUE(all.equal(SeriePOS[i],1)) & isTRUE(all.equal(SeriePOS[i+1],0)) & isTRUE(all.equal(SeriePOS[i+2],0)) & isTRUE(all.equal(SeriePOS[i+3],0))) ERORES_FWD3=c(ERORES_LAG3,MediCum$Error[i+1])
  }
  for(i in 1:(length(SeriePOS)-4)){
  if(isTRUE(all.equal(SeriePOS[i],1)) & isTRUE(all.equal(SeriePOS[i+1],0)) & isTRUE(all.equal(SeriePOS[i+2],0)) & isTRUE(all.equal(SeriePOS[i+3],0)) & isTRUE(all.equal(SeriePOS[i+4],0))) ERORES_FWD4=c(ERORES_LAG4,MediCum$Error[i+1])
  }
}
}

#boxplot(list(ERORES_LAG4,ERORES_LAG3,ERORES_LAG2,ERORES_LAG1,ERORES_FWD1,ERORES_FWD2,ERORES_FWD3,ERORES_FWD4),names=c("t-4","t-3","t-2","t-1","t+1","t+2","t+3","t+4"))
boxplot(list(ERORES_LAG3,ERORES_LAG2,ERORES_LAG1,ERORES_FWD1,ERORES_FWD2,ERORES_FWD3),names=c("t-3","t-2","t-1","t+1","t+2","t+3"),notch=T)
#abline(h=StatusQ[2:3],col=2)
#abline(h=StatusA[2:3],col=3)
abline(h=StatusN[2:3],col=2)

MediCum[which(diff(MediCum$POS)==1),"Error"]

MediCum[which(diff(MediCum$POS)==-1),"Error"]



MediCum[which(diff(MediCum$POS)==1,lag=2),"Error"]
MediCum[which(diff(MediCum$POS)==-1,lag=2),"Error"]
MediCum[which(diff(MediCum$POS)==1,lag=2),"Error"]
MediCum[which(diff(MediCum$POS)==-1,lag=2),"Error"]

}
}
# MedicamentosW2=reshape(MedicamentosL2,direction="wide",timevar="Periodo",idvar="CUM1")
#Nombresprecio=which(grepl("Institucional_Lab_Prom_STD", names(MedicamentosW2)))
#NombresRelevantes=names(MedicamentosW2)[which(substr(names(MedicamentosW2),1,3)=="POS")]
#POS_RELEVANTES=MedicamentosW2[which(MedicamentosW2$POS.2007==0 &  MedicamentosW2$POS.2008==1 |
#MedicamentosW2$POS.2008==0 &  MedicamentosW2$POS.2009==1 |
#MedicamentosW2$POS.2009==0 &  MedicamentosW2$POS.2010==1 |
#MedicamentosW2$POS.2010==0 &  MedicamentosW2$POS.2011==1 |
#MedicamentosW2$POS.2011==0 &  MedicamentosW2$POS.2012==1 |
#MedicamentosW2$POS.2012==0 &  MedicamentosW2$POS.2013==1 ),NombresRelevantes]
#
#apply(
#

   ####Grafica de MITCH
MedicamentosW2=MedicamentosW[which(apply(MedicamentosW[,Nombresprecio],1,function(x) sum(is.na(x)))!=7),]
MedicamentosW2$Grupo =1
MedicamentosW2$Grupo[rowMeans(MedicamentosW2[,NombresRelevantes],na.rm=T)==1] =0
MedicamentosW2$Grupo[rowMeans(MedicamentosW2[,NombresRelevantes],na.rm=T)==0] =0
   MedicamentosL2=reshape(MedicamentosW2)
   MedicamentosL2$Grupo2=MedicamentosL2$Grupo*MedicamentosL2$POS
  aggregate(MedicamentosW2[,Nombresprecio],by=list(MedicamentosW2$Grupo),FUN=median,na.rm=T)