rm(list=ls())
#setwd("E:/Copy/Research/Health/Medicamentos/Programa")
#setwd("C:/Copy/Research/Health/Medicamentos/Programa")
setwd("/media/mauricio/TeraHDD2/Dropbox/Research/Health/Medicamentos/Programa")

library(RCurl)
library(XML)
library(rvest)

load(file="CreatedData/CodigosINVIMA.RData")

ListATCCodes=unique(as.character(CodigosINVIMA$ATC))

MatrixResultados=NULL

for(code in ListATCCodes){
  print(code)
  Sys.sleep(sample(seq(1, 3, by=0.001), 1))
  urlkeep = try(read_html(paste('http://www.whocc.no/atc_ddd_index/?code=',code,sep="")))
  if(inherits(urlkeep, "try-error")) Sys.sleep(sample(seq(1, 3, by=0.001), 1)); urlkeep = try(read_html(paste('http://www.whocc.no/atc_ddd_index/?code=',code,sep="")))
  if(inherits(urlkeep, "try-error")) next
  tabla=html_table(urlkeep)
  if(length(tabla)>0){
    medicine=tabla[[1]][2,2]
    MatrixResultados=rbind(MatrixResultados,c(code,medicine))
  }
}
MatrixResultados=data.frame(MatrixResultados,stringsAsFactors =F)
colnames(MatrixResultados)=c("ATC","Drug")
save(MatrixResultados,file="CreatedData/ATC_Multi.RData")
write.csv(MatrixResultados,file="CreatedData/ATC_Multi.csv")


ListATCCodes[which(!ListATCCodes %in% MatrixResultados$ATC)]
