rm(list=ls())
setwd("E:/Copy/Research/Health/Medicamentos/Programa")
setwd("C:/Copy/Research/Health/Medicamentos/Programa")
setwd("/media/mauricio/TeraHDD2/Dropbox/Research/Health/Medicamentos/Programa")

library(RHTMLForms)
library(RCurl)
library(XML)
library(rvest)

load(file="CreatedData/CodigosINVIMA.RData")

ListATCCodes=unique(as.character(CodigosINVIMA$ATC))

MatrixResultados=NULL

for(code in ListATCCodes){
urlkeep = html(paste('http://www.whocc.no/atc_ddd_index/?code=',code,sep=""))
urlParse=htmlTreeParse(urlkeep)
root=xmlRoot(urlParse)
child = xmlChildren(root)
resultados=child[[2]]
resultados2=resultados[[1]]
resultados3=resultados2[[3]]
resultados4=resultados3[[2]]
resultados5=resultados4[[1]]
if(length(resultados5$children)>15){
resultados6=resultados5[[15]]
resultados7=resultados6[[1]]

for(i in 2:length(resultados7$children)){
resultados8=resultados7[[i]]
MatrixResultados=rbind(MatrixResultados,c(code,gsub("?", "", resultados8[[3]][[1]]$value),gsub("?", "", resultados8[[4]][[1]]$value),gsub("?", "", resultados8[[5]][[1]]$value)))
if(code==ListATCCodes[1] & i==2){
MatrixResultados=data.frame(MatrixResultados,stringsAsFactors =F)
colnames(MatrixResultados)=c("ATC","Dose","Units","Admn")
} #Just to make sure if is close and data.frame is created correctly
} #close the for for different DDD for the same drug


}#only go forward if there is info

}

WHODDD=MatrixResultados
save(WHODDD,file="CreatedData/WHODDD.RData")

ListATCCodes[which(!(ListATCCodes %in% WHODDD$ATC ))]
