
rm(list=ls())
setwd("H:/Copy/Research/Health/Medicamentos/Programa")
setwd("C:/Users/Mauricio/Copy/Research/Health/Medicamentos/Programa")
library(foreign)
library(zoo)
library(xtable)
library(stargazer)
library(data.table)
library(ggplot2)
Medicamentos=read.dta("CreatedData/MedicamentosRegs.dta")
Medicamentos=data.table(Medicamentos)
#Medicamentos=Medicamentos[which(Medicamentos$VariacionIncrease_Lab==0),]
#Medicamentos=Medicamentos[which(Medicamentos$CompletoInstitucional_Lab==1),]
Medicamentos=Medicamentos[which(Medicamentos$Institucional_LabCount==8),]

#Medicamentos$PrecioVariacion=Medicamentos$Institucional_Lab_Prom_STD/Medicamentos$Institucional_LabFirst
#Medicamentos$PrecioVariacionNormal=as.numeric(!(Medicamentos$PrecioVariacion>1000 |  Medicamentos$PrecioVariacion<10))
#Medicamentos[,DVariacionNormal := max(PrecioVariacionNormal), by = list(CUM1)]
#Medicamentos=Medicamentos[which(Medicamentos$DVariacionNormal==0),]

ChangeATC=Medicamentos[,list(AlgoChagnge=mean(POSChange, na.rm = TRUE)),by = "ATC0"]
ATCCambio=ChangeATC$ATC0[which(ChangeATC$AlgoChagnge>0)]
Medicamentos$GrupoCambio[Medicamentos$NeverPOS==1]="Never in POS"
Medicamentos$GrupoCambio[Medicamentos$AlwaysPOS==1]="Always in POS"
Medicamentos$GrupoCambio[Medicamentos$POSChange==1]="Change POS status"
Medicamentos$GrupoCambio=as.factor(Medicamentos$GrupoCambio)

for(ATCEspecifico in ATCCambio){
#ATCEspecifico=ATCCambio[1]
MEDIUso=Medicamentos[which(Medicamentos$ATC0==ATCEspecifico),]

Plot=ggplot(MEDIUso, aes(Periodo, 100*Institucional_Lab_Prom_STD/Institucional_LabFirst,colour=as.character(CUM1),shape=GrupoCambio)) +
    geom_line() +
    geom_point(size=3)+
    labs(title = MEDIUso$DESCRIPCION_ATC0[1],x="Year",y="Price(t)/Price(2007)")+
    geom_vline(xintercept = setdiff(as.numeric(na.omit(unique(MEDIUso$PrimerANOPOS))),2007))+
    scale_shape(name="POS Status")+
    scale_colour_hue(name="Pharmaceutical code")
    
ggsave(Plot, file=paste("Results/figures/casestudies/",MEDIUso$DESCRIPCION_ATC0[1],".pdf",sep=""))
}



